#include <amdis/AMDiS.hpp>
#include <amdis/DOFVector.hpp>
#include <amdis/ProblemStat.hpp>
#include <amdis/functions/GlobalBasis.hpp>

#include <dune/grid/yaspgrid.hh>

int test()
{
  using namespace AMDiS;
  using namespace Dune::Functions::BasisFactory;

  // construct a grid
  Dune::YaspGrid<2> grid({1.0, 1.0}, {8,8});
  auto gridView = grid.leafGridView();

  // construct a basis
  GlobalBasis basis1{gridView, power<2>(lagrange<1>(), blockedInterleaved())};
  GlobalBasis basis2{gridView, power<2>(lagrange<1>(), blockedLexicographic())};
  GlobalBasis basis3{gridView, power<2>(lagrange<1>(), flatInterleaved())};
  GlobalBasis basis4{gridView, power<2>(lagrange<1>(), flatLexicographic())};
  GlobalBasis basis5{gridView, power<2>(lagrange<1>())};
  GlobalBasis basis6{gridView, composite(lagrange<1>())};

  // construct a ProblemStat
  ProblemStat prob1{"prob1", grid, power<2>(lagrange<1>(), blockedInterleaved())};
  ProblemStat prob2{"prob2", grid, power<2>(lagrange<1>(), blockedLexicographic())};
  ProblemStat prob3{"prob3", grid, power<2>(lagrange<1>(), flatInterleaved())};
  ProblemStat prob4{"prob4", grid, power<2>(lagrange<1>(), flatLexicographic())};
  ProblemStat prob5{"prob5", grid, power<2>(lagrange<1>())};
  ProblemStat prob6{"prob6", grid, composite(lagrange<1>())};

  // construct a DOFVector
  DOFVector vec1{gridView, power<2>(lagrange<1>(), blockedInterleaved())};
  DOFVector vec2{gridView, power<2>(lagrange<1>(), blockedLexicographic())};
  DOFVector vec3{gridView, power<2>(lagrange<1>(), flatInterleaved())};
  DOFVector vec4{gridView, power<2>(lagrange<1>(), flatLexicographic())};
  DOFVector vec5{gridView, power<2>(lagrange<1>())};
  DOFVector vec6{gridView, composite(lagrange<1>())};

  return 0;
}

int main(int argc, char** argv)
{
  AMDiS::Environment env(argc, argv);

  return test();
}
