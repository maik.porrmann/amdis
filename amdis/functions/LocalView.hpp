#pragma once

#include <tuple>
#include <optional>
#include <vector>

#include <dune/common/concept.hh>
#include <dune/functions/functionspacebases/concepts.hh>

#include <amdis/common/OptionalNoCopy.hpp>
#include <amdis/common/TypeTraits.hpp>
#include <amdis/functions/NodeCache.hpp>

// NOTE: This is a variant of dune-functions DefaultLocalView

namespace AMDiS
{
  /// \brief The restriction of a finite element basis to a single element
  template <class GB>
  class LocalView
  {
    using PrefixPath = Dune::TypeTree::HybridTreePath<>;

  public:
    /// The global FE basis that this is a view on
    using GlobalBasis = GB;

    /// The grid view the global FE basis lives on
    using GridView = typename GlobalBasis::GridView;

    /// Type of the grid element we are bound to
    using Element = typename GridView::template Codim<0>::Entity;

    /// The type used for sizes
    using size_type = std::size_t;

    /// Tree of local finite elements / local shape function sets
    using Tree = typename GlobalBasis::PreBasis::Node;

    /// Cached basis-tree
    using TreeCache = NodeCache_t<Tree>;

    /// Type used for global numbering of the basis vectors
    using MultiIndex = typename GlobalBasis::MultiIndex;

  public:
    /// \brief Construct local view for a given global finite element basis
    LocalView (GlobalBasis const& globalBasis)
      : globalBasis_(&globalBasis)
      , tree_(globalBasis_->preBasis().makeNode())
    {
      static_assert(Dune::models<Dune::Functions::Concept::BasisTree<GridView>, Tree>());
      initializeTree(tree_);
    }

    /// \brief Bind the view to a grid element
    /**
     * Having to bind the view to an element before being able to actually access any of
     * its data members offers to centralize some expensive setup code in the `bind`
     * method, which can save a lot of run-time.
     */
    void bind (Element const& element)
    {
      element_ = element;
      bindTree(tree_, element_);
      indices_.resize(tree_.size());
      globalBasis_->preBasis().indices(tree_, indices_.begin());
      bound_ = true;
    }

    // do not bind an rvalue-reference
    void bind (Element&& element)
    {
      static_assert(not std::is_rvalue_reference_v<Element&&>);
    }

    /// \brief Return if the view is bound to a grid element
    bool bound () const
    {
      return bound_;
    }

    /// \brief Return the grid element that the view is bound to
    Element const& element () const
    {
      assert(bound());
      return element_;
    }

    /// \brief Unbind from the current element
    /**
     * Calling this method should only be a hint that the view can be unbound.
     */
    void unbind ()
    {
      bound_ = false;
    }

    /// \brief Return the local ansatz tree associated to the bound entity
    Tree const& tree () const
    {
      return tree_;
    }

    /// \brief Cached version of the local ansatz tree
    TreeCache const& treeCache () const
    {
      if (!treeCache_)
        treeCache_.emplace(makeNodeCache(tree_));

      return *treeCache_;
    }

    /// \brief Total number of degrees of freedom on this element
    size_type size () const
    {
      assert(bound());
      return tree_.size();
    }

    /// \brief Maximum local size for any element on the GridView
    /**
     * This is the maximal size needed for local matrices and local vectors.
     */
    size_type maxSize () const
    {
      return globalBasis_->preBasis().maxNodeSize();
    }

    /// \brief Maps from subtree index set [0..size-1] to a globally unique multi index
    /// in global basis
    MultiIndex index (size_type i) const
    {
      assert(bound());
      return indices_[i];
    }

    /// \brief Return the global basis that we are a view on
    GlobalBasis const& globalBasis () const
    {
      return *globalBasis_;
    }

    /// \brief Return this local-view
    LocalView const& rootLocalView () const
    {
      return *this;
    }

  protected:
    GlobalBasis const* globalBasis_;
    Element element_;
    Tree tree_;

    mutable OptionalNoCopy<TreeCache> treeCache_ = std::nullopt;
    std::vector<MultiIndex> indices_ = {};
    bool bound_ = false;
  };

} // end namespace AMDiS
