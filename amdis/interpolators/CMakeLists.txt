install(FILES
    AverageInterpolator.hpp
    LocalAverageInterpolator.hpp
    SimpleInterpolator.hpp
DESTINATION ${CMAKE_INSTALL_INCLUDEDIR}/amdis/interpolators)
