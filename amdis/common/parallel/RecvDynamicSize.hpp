#pragma once

#if HAVE_MPI
#include <functional>
#include <optional>
#include <utility>

#include <amdis/common/Concepts.hpp>

namespace AMDiS { namespace Mpi
{

  /// \brief Test-functor for dynamic size data to be used in \ref Request class.
  /**
   * Implements the iprob - irecv - test workflow to receive  data of unknown size.
   * At first, when the header of the data is arrived, the size is fetched from the
   * MPI_Status given by MPI_Iprob. This can be done in a user functor `recv_` that
   * receives an MPI_Status and returns an MPI_Receive handler. Typically, the recv_
   * functor increases the size of an internal receive-buffer and calls MPI_Irecv.
   * When an MPI_Test on the MPI_Receive handler results in a completed communication,
   * a finalize user functor `finish_` is called to make some postprocessing on the
   * received data. Afterwards, the MPI_Status of the MPI_Test command is returned.
   **/
  class RecvDynamicSize
  {
    enum Progress {
      STARTED,
      INITIALIZED,
      RECEIVING,
      FINISHED
    };

  public:

    /// Constructor with user receive-functor `R`.
    template <class R,
      REQUIRES( Concepts::Callable<R, MPI_Status> )>
    RecvDynamicSize(int from, int tag, MPI_Comm comm, R&& r)
      : from_(from)
      , tag_(tag)
      , comm_(comm)
      , recv_(std::forward<R>(r))
      , finish_([](MPI_Status){})
    {}

    /// Constructor with user receive-functor `R` and user finilize-functor `F`.
    template <class R, class F,
      REQUIRES( Concepts::Callable<R, MPI_Status> && Concepts::Callable<F, MPI_Status> )>
    RecvDynamicSize(int from, int tag, MPI_Comm comm, R&& r, F&& f)
      : from_(from)
      , tag_(tag)
      , comm_(comm)
      , recv_(std::forward<R>(r))
      , finish_(std::forward<F>(f))
    {}

    /// Operator called as test function in the \ref Request class.
    std::optional<MPI_Status> operator()()
    {
      if (progress_ == STARTED) {
        int flag = 0;
        // Wait for a message from rank from_ with tag tag_
        MPI_Iprobe(from_, tag_, comm_, &flag, &status_);

        if (flag != 0)
          progress_ = INITIALIZED;
      }

      if (progress_ == INITIALIZED) {
        req_ = recv_(status_);

        progress_ = RECEIVING;
      }

      if (progress_ == RECEIVING) {
        int flag = 0;
        MPI_Test(&req_, &flag, &status_);

        if (flag != 0)
          progress_ = FINISHED;
      }

      if (progress_ == FINISHED) {
        finish_(status_);

        return status_;
      } else
        return {};
    }


  private:

    int from_ = 0;  //< source rank
    int tag_ = 0;   //< communication tag
    MPI_Comm comm_; //< communicator

    std::function<MPI_Request(MPI_Status)> recv_; //< user receive-functor
    std::function<void(MPI_Status)> finish_;      //< user finalize-functor

    Progress progress_ = STARTED;         //< internal progress flag
    MPI_Status status_ = MPI_Status{};    //< the status information, filled by MPI_Iprob and MPI_Test
    MPI_Request req_ = MPI_REQUEST_NULL;  //< the request handler, filled by the user receive-functor \ref recv_
  };

}} // end namespace AMDiS::Mpi

#endif // HAVE_MPI
