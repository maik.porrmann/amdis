#pragma once

#include <dune/common/fmatrix.hh>
#include <dune/common/fvector.hh>
#include <dune/istl/bcrsmatrix.hh>
#include <dune/istl/bvector.hh>
#include <dune/istl/operators.hh>

#include <amdis/ProblemStatTraits.hpp>
#include <amdis/linearalgebra/istl/Traits.hpp>

namespace Dune
{
  // some default types used in explicit template instantiation
  namespace Precompiled
  {
    using Matrix
      = Dune::BCRSMatrix<Dune::FieldMatrix<double,1,1>>;

    using Vector
      = Dune::BlockVector<Dune::FieldVector<double,1>>;

    using LinOp
      = Dune::MatrixAdapter<Matrix, Vector, Vector>;
  }
}

namespace AMDiS
{
  namespace Precompiled
  {
    using Basis = typename AMDiS::YaspGridBasis<2,2>::GlobalBasis;

    using SolverTraits = AMDiS::SolverTraits<
      typename BackendTraits::template Matrix<Basis,Basis>::template Impl<double>,
      typename BackendTraits::template Vector<Basis>::template Impl<double>>;
  }
}
