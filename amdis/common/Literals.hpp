#pragma once

#include <cassert>
#include <type_traits>

namespace AMDiS
{
  // inspired by Boost.hana
  // see also: http://blog.mattbierner.com/stupid-template-tricks-stdintegral_constant-user-defined-literal/

  namespace Impl
  {
    constexpr unsigned char2digit(const char c)
    {
      assert(c >= '0' && c <= '9' && "Unknown digit in integral constant");
      return unsigned(c) - unsigned('0');
    }

    template <char... digits>
    constexpr std::size_t string2num()
    {
      const char arr[] = {digits...};
      assert(arr[0] != '-' && "Negative integral constant");

      std::size_t result = 0;
      std::size_t power  = 1;

      const int N = sizeof...(digits);
      for (int i = 0; i < N; ++i) {
          char c = arr[N - 1 - i];
          result+= char2digit(c) * power;
          power *= 10u;
      }

      return result;
    }

  } // end namespace Impl

  /// Literal to create integer compile-time constant, e.g. 0_c -> index_<0>
  template <char... digits>
  constexpr auto operator"" _c()
  {
    return std::integral_constant<std::size_t,Impl::string2num<digits...>()>{};
  }

} // end namespace AMDiS
