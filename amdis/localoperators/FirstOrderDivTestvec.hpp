#pragma once

#include <type_traits>

#include <amdis/GridFunctionOperator.hpp>
#include <amdis/common/StaticSize.hpp>

namespace AMDiS
{
  /**
   * \addtogroup operators
   * @{
   **/

  namespace tag
  {
    struct divtestvec {};
  }


  /// first-order operator \f$ \langle\nabla\cdot\Psi, c\rangle \f$
  class FirstOrderDivTestVec
  {
  public:
    FirstOrderDivTestVec(tag::divtestvec) {}

    template <class CG, class Node, class Quad, class LF, class Vec>
    void assemble(CG const& context, Node const& node, Quad const& quad,
                  LF const& localFct, Vec& elementVector) const
    {
      static_assert(Node::isPower, "Node must be a Power-Node.");
      static_assert(static_size_v<typename LF::Range> == 1,
        "Expression must be of scalar type.");
      assert(node.degree() == CG::dow);

      std::size_t feSize = node.child(0).size();

      using RangeFieldType = typename Node::ChildType::LocalBasis::Traits::RangeFieldType;
      using WorldVector = FieldVector<RangeFieldType,CG::dow>;
      std::vector<WorldVector> gradients;

      for (auto const& qp : quad) {
        // Position of the current quadrature point in the reference element
        auto&& local = context.local(qp.position());

        // The transposed inverse Jacobian of the map from the reference element to the element
        const auto jacobian = context.geometry().jacobianInverseTransposed(local);

        // The multiplicative factor in the integral transformation formula
        const auto factor = localFct(local) * context.integrationElement(qp.position()) * qp.weight();

        // The gradients of the shape functions on the reference element
        auto const& shapeGradients = node.child(0).localBasisJacobiansAt(local);

        // Compute the shape function gradients on the real element
        gradients.resize(shapeGradients.size());

        for (std::size_t i = 0; i < gradients.size(); ++i)
          jacobian.mv(shapeGradients[i][0], gradients[i]);

        for (std::size_t j = 0; j < feSize; ++j) {
          for (std::size_t k = 0; k < CG::dow; ++k) {
            const auto local_kj = node.child(k).localIndex(j);
            elementVector[local_kj] += factor * gradients[j][k];
          }
        }
      }

    }
  };

  template <class LC>
  struct GridFunctionOperatorRegistry<tag::divtestvec, LC>
  {
    static constexpr int degree = 1;

    using type = FirstOrderDivTestVec;
  };

  /** @} **/

} // end namespace AMDiS
