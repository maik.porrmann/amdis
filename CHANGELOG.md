## Master (will become release v2.9)

### Added

- Added new implementations of function interpolation: `SimplexInterpolation`, `AverageInterpolation`,
  and `LocalAverageInterpolation`. To decide how to interpolate, an `InterpolatorFactory` is
  provided that accepts the interpolation tag like `tag::assign` or `tag::average`.
- Add a free function `interpolate<tag>(basis,coeff,f)` that calls the interpolators based on the
  given `tag`. The default tag is `tag::assign`.
- A new adaptive container and GridFunction is added: `ElementVector` and
  `ElementGridFunction` that represent element-wise constant values.
- Add helper class `OptionalNoCopy` that represents a `std::optional` that does not
  copy the stored object on copy/move operations.
- Add `gradientOf`, `divergenceOf` and `partialDerivativeOf` as friend function to
  `DOFVector`.
- Provide a function `entitySet(basis)` and `basis.entitySet()` that defines the
  set of entities the basis can be bound to. This set is typically related to the
  linear-algebra backend traits' `PartitionSet`.
- Make the function `ProblemInstat::oldSolutionVector()` also available with mutable access.
- A data transfer strategy based on a simple caching and interpolation is added, called
  `SimpleDataTransfer`. It can be selected with the datatranser tag `tag::simple_datatransfer`.
  Note, this datatransfer is restricted to grids with a single GeometryType.
- Add scalar grid operations `tanh(value)` and `signum(value)`

### Fixed

- Corrected a bug that emerged in the copy of `LocalView`, by removing the self-defined
  copy and move operations and instead relying on the rule-of-zero.
- Fix issue #14 with a wrong dimension used in the `checkInside` function in `DataTransfer`.
- Fix implementation of local operator `SecondOrderDivTestvecDivTrialvec`.
- Use `comm()` from `GridView` for domain decomposition, instead of a global communicator.

### Changed

- Mark `makeTreePath` and `treePathIndex` as `constexpr`.
- Print timings in `ProblemStat` only for log-level >= 2
- The quadrature order of local-functions in the `GridFunctionOperator` is now
  determined by taking into account the user-specified polynomial order first
  and second the derived polynomial of the expression. This allows to manually
  increase the quadrature order of an operator term.
- Remove a specialization of `Underlying_t` and only remove top-level`const`
  qualifiers instead of all `const` qualifiers.
- Change the constructor of `DiscreteFunction` to allow references and smart pointers
  to be passed for the coefficient vector and global basis.
- Make `valueOf` a friend function of `DOFVector`.
- Traversal in interpolators and assemblers is replaced by the new introduced
  `entitySet()` function.
- Make the `DataTransfer` a customization point by providing a class `DataTransferFactory`
  that can be specialized with a `<tag>` and must implement a `create(basis,coefficients)`
  static function.
- Replace `Communication` by `IndexDistribution` in the backends and incorporate the `DOFMapping`
  directly into PETSc's index distribution. The `GlobalBasis` provides the method `indexDistribution()`
  instead of `comm()`.
- Specialize the `IndexDistribution` over the `GridView::CollectiveCommunication` to decide whether to
  use sequential or parallel index distributions.
- Pass the `IndexDistribution` directly to the `MatrixBAckend` and `VectorBackend`.

### Removed

- Specialization of `GlobalIdSet` for `Dune::Functions::TaylorHoodPreBasis` is removed since this
  pre-basis is just for illustration.
- The helper class `MacroGridFactory` is removed, since a proper specialization for the
  `StructuredGridFactory<AlbertaGrid>` is now available in dune-grid.
- Utility `functionFromCallable` is removed. It was used for backward compatibility with older
  interpolate functions. dune-localfunctions now supports callables.
- Backward-compatibility utilities in `Nodes.hpp` are removed now.
- Remove `order()` function from `Power` operation.
- Constructor of `DiscreteFunction` that takes a `DOFVector` is removed.

## Release 2.8

- Restructuring of local operators. Introduce (global) `Operator` class and use type-erasure
  for the storage of `LocalOperator`s in the operator list. In the `GridFunctionLocalOperator`
  the local-function is passed to the `assemble()` method directly.
- Move recursive algorithms to subdirectoy `amdis/algorithm/`
- Rename `Recursive::Apply` into `Recursive::Map`

## Release v0.3

See release notes on https://gitlab.com/amdis/amdis/-/releases/v0.3
