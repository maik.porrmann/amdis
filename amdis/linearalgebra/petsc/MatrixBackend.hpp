#pragma once

#include <algorithm>
#include <memory>
#include <vector>

#include <petscmat.h>

#include <dune/common/timer.hh>

#include <amdis/Output.hpp>
#include <amdis/linearalgebra/petsc/MatrixNnzStructure.hpp>
#include <amdis/linearalgebra/SymmetryStructure.hpp>

namespace AMDiS
{
  template <class> class Constraints;

  /// \brief The basic container that stores a base matrix
  template <class DofMap>
  class PetscMatrix
  {
    template <class> friend class Constraints;

  public:
    /// The matrix type of the underlying base matrix
    using BaseMatrix = ::Mat;

    /// The type of the elements of the DOFMatrix
    using value_type = PetscScalar;

    /// The index/size - type
    using size_type = PetscInt;

  public:
    /// Constructor. Constructs new BaseMatrix.
    PetscMatrix(DofMap const& rowDofMap, DofMap const& colDofMap)
      : rowDofMap_(rowDofMap)
      , colDofMap_(colDofMap)
    {}

    // disable copy and move operations
    PetscMatrix(PetscMatrix const&) = delete;
    PetscMatrix(PetscMatrix&&) = delete;
    PetscMatrix& operator=(PetscMatrix const&) = delete;
    PetscMatrix& operator=(PetscMatrix&&) = delete;

    ~PetscMatrix()
    {
      destroy();
    }

    /// Return a reference to the data-matrix \ref matrix
    BaseMatrix& matrix()
    {
      return matrix_;
    }

    /// Return a reference to the data-matrix \ref matrix
    BaseMatrix const& matrix() const
    {
      return matrix_;
    }

    /// Insert a single value into the matrix
    template <class RowIndex, class ColIndex>
    void insert(RowIndex const& r, ColIndex const& c, PetscScalar value)
    {
      PetscInt row = rowDofMap_.global(r);
      PetscInt col = colDofMap_.global(c);
      MatSetValue(matrix_, row, col, value, ADD_VALUES);
    }

    /// Insert an element-matrix with row-indices == col-indices
    template <class LocalInd, class LocalMatrix>
    void scatter(LocalInd const& localInd, LocalMatrix const& localMat)
    {
      if (&rowDofMap_ == &colDofMap_) {
        thread_local std::vector<PetscInt> idx;
        idx.resize(localInd.size());

        // create a vector of global indices from the local indices using the local-to-global map
        std::transform(localInd.begin(), localInd.end(), idx.begin(),
          [this](auto const& mi) { return rowDofMap_.global(mi); });

        MatSetValues(matrix_, idx.size(), idx.data(), idx.size(), idx.data(), localMat.data(), ADD_VALUES);
      } else {
        scatter(localInd, localInd, localMat);
      }
    }

    /// Insert an element-matrix
    template <class RowLocalInd, class ColLocalInd, class LocalMatrix>
    void scatter(RowLocalInd const& rowLocalInd, ColLocalInd const& colLocalInd, LocalMatrix const& localMat)
    {
      thread_local std::vector<PetscInt> ri;
      thread_local std::vector<PetscInt> ci;
      ri.resize(rowLocalInd.size());
      ci.resize(colLocalInd.size());

      // create vectors of global indices from the local indices using the local-to-global map
      std::transform(rowLocalInd.begin(), rowLocalInd.end(), ri.begin(),
        [this](auto const& mi) { return rowDofMap_.global(mi); });
      std::transform(colLocalInd.begin(), colLocalInd.end(), ci.begin(),
        [this](auto const& mi) { return colDofMap_.global(mi); });

      MatSetValues(matrix_, ri.size(), ri.data(), ci.size(), ci.data(), localMat.data(), ADD_VALUES);
    }

    /// Create and initialize the matrix
    void init(MatrixNnzStructure const& pattern)
    {
      Dune::Timer t;

      // destroy an old matrix if created before
      destroy();
      info(3, "  destroy old matrix needed {} seconds", t.elapsed());
      t.reset();

      // create a MATAIJ or MATSEQAIJ matrix with provided sparsity pattern
      MatCreateAIJ(comm(),
        rowDofMap_.localSize(), colDofMap_.localSize(),
        rowDofMap_.globalSize(), colDofMap_.globalSize(),
        0, pattern.d_nnz().data(), 0, pattern.o_nnz().data(), &matrix_);

      // keep sparsity pattern even if we delete a row / column with e.g. MatZeroRows
      MatSetOption(matrix_, MAT_KEEP_NONZERO_PATTERN, PETSC_TRUE);

      // set symmetry properties of the matrix
      switch (pattern.symmetry()) {
        case SymmetryStructure::spd:
          MatSetOption(matrix_, MAT_SPD, PETSC_TRUE);
          break;
        case SymmetryStructure::symmetric:
          MatSetOption(matrix_, MAT_SYMMETRIC, PETSC_TRUE);
          break;
        case SymmetryStructure::hermitian:
          MatSetOption(matrix_, MAT_HERMITIAN, PETSC_TRUE);
          break;
        case SymmetryStructure::structurally_symmetric:
          MatSetOption(matrix_, MAT_STRUCTURALLY_SYMMETRIC, PETSC_TRUE);
          break;
        default:
          /* do nothing */
          break;
      }

      info(3, "  create new matrix needed {} seconds", t.elapsed());
      t.reset();

      initialized_ = true;
    }

    /// Reuse the matrix pattern and set all entries to zero
    void init()
    {
      MatZeroEntries(matrix_);
      initialized_ = true;
    }

    /// Finish assembly. Must be called before matrix can be used in a KSP
    void finish()
    {
      Dune::Timer t;
      MatAssemblyBegin(matrix_, MAT_FINAL_ASSEMBLY);
      MatAssemblyEnd(matrix_, MAT_FINAL_ASSEMBLY);
      info(3, "  finish matrix assembling needed {} seconds", t.elapsed());
    }

    /// Return the local number of nonzeros in the matrix
    std::size_t nnz() const
    {
      MatInfo info;
      MatGetInfo(matrix_, MAT_LOCAL, &info);
      return std::size_t(info.nz_used);
    }

    // An MPI Communicator or PETSC_COMM_SELF
    MPI_Comm comm() const
    {
      return rowDofMap_.comm();
    }

  private:
    // Destroy the matrix if initialized before.
    void destroy()
    {
      if (initialized_)
        MatDestroy(&matrix_);
    }

  private:
    // The local-to-global maps
    DofMap const& rowDofMap_;
    DofMap const& colDofMap_;

    /// The data-matrix
    Mat matrix_;

    bool initialized_ = false;
  };

} // end namespace AMDiS
