#pragma once

#include <memory>
#include <vector>

#include <dune/common/hybridutilities.hh>
#include <dune/common/std/type_traits.hh>

#include <amdis/Boundary.hpp>
#include <amdis/common/Concepts.hpp>

namespace AMDiS
{
  class BoundaryManagerBase
  {
  public:
    BoundaryManagerBase(std::size_t numBoundarySegments)
      : boundaryIds_(numBoundarySegments, BoundaryType{1})
    {}

    /// Return the stored boundary id for the given intersection
    template <class Intersection>
    BoundaryType boundaryId(Intersection const& intersection) const
    {
      return boundaryIds_[intersection.boundarySegmentIndex()];
    }

    std::vector<BoundaryType> const& boundaryIds() const
    {
      return boundaryIds_;
    }

  protected:
    std::vector<BoundaryType> boundaryIds_; // maps a boundarySegmentIndex to an ID
  };


  /// Manage boundary ids of boundary segments in a grid
  /**
   * Manager for boundary IDs, that can be initialized from different sources
   * - cube domains can be assigned box boundaries, by specifying left-right,
   *   front-back, and bottom-top ids
   * - An indicator from global coordinates that returns an id. The indicator function
   *   is evaluated in the center points of intersections.
   * - A predicate that return true/false for a boundary part and sets the given id
   * - Read ids from the grid. Those may be initialized by some grid readers. Note:
   *   not all grids support `intersection.boundaryId()`, but e.g. AlbertaGrid and
   *   ALUGrid do support this. Can be read by DGF parser and AlbertaGrid constructor
   *   from macroFileName.
   **/
  template <class G>
  class BoundaryManager
      : public BoundaryManagerBase
  {
    using Super = BoundaryManagerBase;

  public:
    using Grid = G;
    enum { dim = Grid::dimension };
    enum { dow = Grid::dimensionworld };

    using Segment = typename Grid::LevelGridView::Intersection;
    using Domain = typename Grid::template Codim<0>::Geometry::GlobalCoordinate;

  public:
    /// Constructor. Stores a shared pointer to the grid and initializes all
    /// boundary IDs to the value stored in the grid
    BoundaryManager(std::shared_ptr<G> const& grid)
      : Super(grid->numBoundarySegments())
      , grid_(grid)
    {
      setBoundaryId();
    }

    BoundaryManager(G& grid)
      : BoundaryManager{Dune::stackobject_to_shared_ptr(grid)}
    {}


    /// Set boundary ids [left,right, front,back, bottom,top] for cube domains
    void setBoxBoundary(std::array<BoundaryType, 2*dow> const& ids)
    {
      auto gv = grid_->leafGridView();
      for (auto const& e : elements(gv))
      {
        for (auto const& segment : intersections(gv,e)) {
          if (!segment.boundary())
            continue;

          auto n = segment.centerUnitOuterNormal();
          auto index = segment.boundarySegmentIndex();

          for (int i = 0; i < dow; ++i) {
            if (n[i] < -0.5)
              boundaryIds_[index] = ids[2*i];
            else if (n[i] > 0.5)
              boundaryIds_[index] = ids[2*i+1];
          }
        }
      }
    }


    /// Set indicator(center) for all boundary intersections
    template <class Indicator,
      REQUIRES(Concepts::Functor<Indicator, int(Domain)>) >
    void setIndicator(Indicator const& indicator)
    {
      auto gv = grid_->leafGridView();
      for (auto const& e : elements(gv))
      {
        for (auto const& segment : intersections(gv,e)) {
          if (!segment.boundary())
            continue;

          auto index = segment.boundarySegmentIndex();
          boundaryIds_[index] = indicator(segment.geometry().center());
        }
      }
    }


    /// Set id for all boundary intersections with pred(center) == true
    template <class Predicate,
      REQUIRES(Concepts::Functor<Predicate, bool(Domain)>) >
    void setPredicate(Predicate const& pred, BoundaryType id)
    {
      auto gv = grid_->leafGridView();
      for (auto const& e : elements(gv))
      {
        for (auto const& segment : intersections(gv,e)) {
          if (!segment.boundary())
            continue;

          auto index = segment.boundarySegmentIndex();
          if (pred(segment.geometry().center()))
            boundaryIds_[index] = id;
        }
      }
    }


    template <class I>
    using HasBoundaryId = decltype(std::declval<I>().boundaryId());

    /// Set boundary ids as stored in the grid, e.g. read by grid reader
    void setBoundaryId()
    {
      if (!Dune::Std::is_detected<HasBoundaryId, Segment>::value)
        return;

      auto gv = grid_->leafGridView();
      for (auto const& e : elements(gv))
      {
        for (auto const& segment : intersections(gv,e)) {
          if (!segment.boundary())
            continue;

          if constexpr (Dune::Std::is_detected<HasBoundaryId, Segment>::value) {
            auto index = segment.boundarySegmentIndex();
            boundaryIds_[index] = segment.boundaryId();
          }
        }
      }
    }

    template <class I>
    void setBoundaryIds(std::vector<I> const& ids)
    {
      test_exit(ids.size() == boundaryIds_.size(), "Number of boundary IDs does not match!");
      std::copy(ids.begin(), ids.end(), boundaryIds_.begin());
    }

  private:
    std::shared_ptr<Grid> grid_;
    using Super::boundaryIds_;
  };

} // end namespace AMDiS
