#include <config.h>

#include <iostream>

#if HAVE_DUNE_SPGRID
#include <dune/grid/spgrid.hh>
#endif

#include <amdis/AMDiS.hpp>
#include <amdis/Integrate.hpp>
#include <amdis/ProblemStat.hpp>
#include <amdis/LocalOperators.hpp>

using namespace AMDiS;


template <class Grid>
void print(Grid const& grid)
{
  auto const& indexSet = grid.leafIndexSet();
  std::cout << "vertices:\n";
  for (auto const& v : vertices(grid.leafGridView())) {
    auto coord = v.geometry().corner(0);
    std::cout << indexSet.index(v) << ": [" << coord[0] << ", " << coord[1] << "]\n";
  }
  std::cout << "\n";

  std::cout << "elements:\n";
  for (auto const& e : elements(grid.leafGridView())) {
    std::cout << indexSet.index(e) << ": {" << indexSet.subIndex(e,0,2);
    for (unsigned int i = 1; i < e.subEntities(2); ++i) {
      std::cout << ", " << indexSet.subIndex(e,i,2);
     }
     std::cout << "}\n";
  }
  std::cout << "\n";

  std::cout << "boundary-intersections:\n";
  for (auto const& e : elements(grid.leafGridView())) {
    for (auto const& inter : intersections(grid.leafGridView(), e)) {
      if (!inter.boundary())
        continue;

      std::cout << inter.boundarySegmentIndex() << ": [" << inter.geometry().center()
                << "] {" << indexSet.index(inter.inside());
      if (inter.neighbor())
        std::cout << ", " << indexSet.index(inter.outside());
      std::cout << "}\n";
    }
  }
}

template <class Grid>
void run(Grid& grid)
{
  using Traits = LagrangeBasis<Grid, 1>;

  ProblemStat<Traits> prob("ellipt", grid);
  prob.initialize(INIT_ALL);
  prob.boundaryManager()->setBoxBoundary({-1,-1,1,1});

  print(grid);

  auto periodicBC = makePeriodicBC(*prob.globalBasis(),
                                  {*prob.boundaryManager(), -1},
                                  {{{1.0,0.0}, {0.0,1.0}}, {1.0, 0.0}});
  periodicBC.init();
  std::cout << "periodicNodes:\n";
  for (std::size_t i = 0; i < periodicBC.periodicNodes().size(); ++i)
    std::cout << i << ": " << periodicBC.periodicNodes()[i] << "\n";
  std::cout << "\n";

  std::cout << "associations:\n";
  for (auto const& a : periodicBC.associations())
    std::cout << a.first << " -> " << a.second << "\n";
  std::cout << "\n";
}

int main(int argc, char** argv)
{
  Environment env(argc, argv);

#if HAVE_DUNE_SPGRID
  Dune::SPCube<double,2> cube({0.0,0.0},{1.0,1.0});
  Dune::SPDomain<double,2> domain({cube}, Dune::SPTopology<2>(0b01));
  Dune::SPGrid<double,2> grid1(domain, Dune::SPMultiIndex<2>({2,2}));
  run(grid1);
#endif

  // NOTE: 'periodic' YaspGrid not supported yet, but a simple YaspGrid can be made periodic
  Dune::YaspGrid<2> grid2({1.0,1.0}, {2,2});
  run(grid2);

  return 0;
}
