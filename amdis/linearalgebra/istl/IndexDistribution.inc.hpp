#pragma once

#include <amdis/Initfile.hpp>
#include <amdis/Output.hpp>

namespace AMDiS {

#if HAVE_MPI

template <class G, class L>
  template <class Basis>
ISTLIndexDistribution<G,L>::ISTLIndexDistribution(Basis const& basis)
{
  using SolverType = Dune::SolverCategory::Category;
  std::string category = "default";
  Parameters::get("solver category", category);

  auto const& gv = basis.gridView();
  int mpiSize = gv.comm().size();

  cat_ = SolverType::sequential;
  if (category == "default")
  {
    if (mpiSize == 1)
    {
      cat_ = SolverType::sequential;
    }
    else
    {
      // Use overlapping solver if grid has overlap
      if (gv.overlapSize(0) > 0)
        cat_ = SolverType::overlapping;
      else
      {
        // TODO(FM): Remove once nonoverlapping solvers are supported
        warning("Nonoverlapping solvers are currently not supported.");
        cat_ = SolverType::nonoverlapping;
      }
    }
  }
  else if (category != "sequential" && mpiSize == 1)
  {
    warning("Only one process detected. Solver category set to sequential\n");
    cat_ = SolverType::sequential;
  }
  else if (category == "sequential")
  {
    test_exit(mpiSize == 1, "Solver category sequential is not supported in parallel\n");
    cat_ = SolverType::sequential;
  }
  else if (category == "overlapping")
  {
    if (gv.overlapSize(0) == 0)
      warning("Overlapping solver category chosen for grid with no overlap\n");
    cat_ = SolverType::overlapping;
  }
  else if (category == "nonoverlapping")
  {
    // TODO(FM): Remove once nonoverlapping solvers are supported
    warning("Nonoverlapping solvers are currently not supported.");
    cat_ = SolverType::nonoverlapping;
  }
  else
  {
    error_exit("Unknown solver category\n");
  }

  update(basis);
}

#endif // HAVE_MPI

} // end namespace AMDiS
