#pragma once

#include <type_traits>

#include <amdis/GridFunctionOperator.hpp>
#include <amdis/common/StaticSize.hpp>
#include <amdis/utility/LocalToGlobalAdapter.hpp>

namespace AMDiS
{
  /**
   * \addtogroup operators
   * @{
   **/

  namespace tag
  {
    struct test_partialtrial
    {
      std::size_t comp;
    };

    struct partial_trial
    {
      std::size_t comp;
    };
  }


  /// first-order operator \f$ \langle\psi, c\,\partial_i\phi\rangle \f$
  class FirstOrderTestPartialTrial
  {
    int comp_;

  public:
    FirstOrderTestPartialTrial(tag::test_partialtrial tag)
      : comp_(tag.comp)
    {}

    template <class CG, class RN, class CN, class Quad, class LocalFct, class Mat>
    void assemble(CG const& contextGeo, RN const& rowNode, CN const& colNode,
                  Quad const& quad, LocalFct const& localFct, Mat& elementMatrix) const
    {
      static_assert(static_size_v<typename LocalFct::Range> == 1,
        "Expression must be of scalar type.");
      static_assert(RN::isLeaf && CN::isLeaf,
        "Operator can be applied to Leaf-Nodes only.");

      std::size_t rowSize = rowNode.size();
      std::size_t colSize = colNode.size();

      LocalToGlobalBasisAdapter colLocalBasis(colNode, contextGeo.geometry());
      for (auto const& qp : quad) {
        // Position of the current quadrature point in the reference element
        decltype(auto) local = contextGeo.local(qp.position());

        // The multiplicative factor in the integral transformation formula
        auto factor = contextGeo.integrationElement(qp.position()) * qp.weight();
        factor *= localFct(local);

        // the values of the shape functions on the reference element at the quadrature point
        auto const& shapeValues = rowNode.localBasisValuesAt(local);

        // Compute the shape function gradients on the real element
        auto const& colPartial = colLocalBasis.partialsAt(local, comp_);

        for (std::size_t j = 0; j < colSize; ++j) {
          const auto local_j = colNode.localIndex(j);
          const auto value = factor * colPartial[j];
          for (std::size_t i = 0; i < rowSize; ++i) {
            const auto local_i = rowNode.localIndex(i);
            elementMatrix[local_i][local_j] += value * shapeValues[i];
          }
        }
      }
    }
  };

  template <class LC>
  struct GridFunctionOperatorRegistry<tag::test_partialtrial, LC>
  {
    static constexpr int degree = 1;
    using type = FirstOrderTestPartialTrial;
  };


  /// Create a first-order term with derivative on trial-function
  template <class Expr>
  auto fot(Expr&& expr, tag::partial_trial t, int quadOrder = -1)
  {
    return makeOperator(tag::test_partialtrial{t.comp}, FWD(expr), quadOrder);
  }

  /** @} **/

} // end namespace AMDiS
