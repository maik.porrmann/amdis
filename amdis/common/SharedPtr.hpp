#pragma once

#include <memory>
#include <type_traits>
#include <utility>

#include <dune/common/shared_ptr.hh>

#include <amdis/common/TypeTraits.hpp>

namespace AMDiS
{
  /**
   *  Helper function converting the argument into a shared_ptr. The following cases are handled:
   *  - objects passed as rvalue references are used to construct a new shared_ptr
   *  - objects passed as references are used to construct a shared_ptr with a null deleter that
   *    does nothing when the pointer is destroyed, using the stored base pointer in case of a
   *    unique_ptr argument
   *  - shared_ptr are simply forwarded
   */
  template <class T>
  std::shared_ptr<T> wrap_or_share(T& t)
  {
    return Dune::stackobject_to_shared_ptr(t);
  }

  template<class T>
  std::shared_ptr<T> wrap_or_share(T&& t)
  {
    return std::make_shared<T>(FWD(t));
  }

  template <class T>
  std::shared_ptr<T> wrap_or_share(T* t)
  {
    static_assert(not std::is_pointer_v<T*>,
      "Raw pointers must be wrapped into smart pointers or references to clarify ownership");
  }

  template <class T>
  std::shared_ptr<T> wrap_or_share(std::shared_ptr<T> t)
  {
    return t;
  }

  template <class T>
  std::shared_ptr<T> wrap_or_share(std::unique_ptr<T> t)
  {
    return std::shared_ptr<T>(std::move(t));
  }

} // end namespace AMDiS
