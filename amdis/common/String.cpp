#include <config.h>

#include "String.hpp"

namespace AMDiS
{
  /// convert all characters in a string to upper case
  std::string to_upper(std::string input)
  {
    for (auto& c : input)
      c = static_cast<char>(std::toupper(static_cast<unsigned char>(c)));
    return input;
  }

  /// convert all characters in a string to upper case
  std::string to_lower(std::string input)
  {
    for (auto& c : input)
      c = static_cast<char>(std::tolower(static_cast<unsigned char>(c)));
    return input;
  }

  /// trim a string from the left
  std::string& ltrim(std::string& str)
  {
    auto it =  std::find_if(str.begin(), str.end(), [](char ch)
    {
      return !std::isspace<char>(ch, std::locale::classic());
    });
    str.erase(str.begin() , it);
    return str;
  }

  /// trim a string from the right
  std::string& rtrim(std::string& str)
  {
    auto it =  std::find_if(str.rbegin(), str.rend(), [](char ch)
    {
      return !std::isspace<char>(ch, std::locale::classic());
    });
    str.erase(it.base(), str.end());
    return str;
  }

  /// trim a string from both sides
  std::string& trim(std::string& str)
  {
    return ltrim(rtrim(str));
  }

  /// trim a (copy of the) string from both sides
  std::string trim_copy(std::string const& str)
  {
    auto s = str;
    return trim(s);
  }

  /// Replace all occurences of substring `from` with `to` in source `str`.
  void replaceAll(std::string& str, std::string const& from, std::string const& to)
  {
    if (from.empty())
      return;

    typename std::string::size_type start_pos = 0;
    while ((start_pos = str.find(from, start_pos)) != std::string::npos)
    {
      str.replace(start_pos, from.length(), to);
      start_pos += to.length();
    }
  }

} // end namspace AMDiS
