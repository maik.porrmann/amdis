#pragma once

namespace AMDiS
{
  /// \brief Dummy implementation for sequential index "distribution"
  class DefaultIndexDistribution
  {
  public:
    using Impl = DefaultIndexDistribution;

    template <class T>
    DefaultIndexDistribution(T&&) {}

    template <class Basis>
    void update(Basis const&) { /* do nothing */ }
  };

} // end namespace AMDiS
