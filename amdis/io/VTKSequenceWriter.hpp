#pragma once

#include <vector>
#include <iostream>
#include <sstream>
#include <fstream>
#include <iomanip>
#include <memory>

#include <dune/grid/io/file/vtk/common.hh>
#include <dune/common/path.hh>

#include <dune/grid/io/file/vtk/vtkwriter.hh>

#include <amdis/Environment.hpp>

namespace AMDiS
{
  /// \brief class to write pvd-files which contains a list of all collected vtk-files
  /**
   * Write pvd-file suitable for easy visualization with
   * <a href="http://www.vtk.org/">The Visualization Toolkit (VTK)</a>.
   *
   * \tparam GridView Grid view of the grid we are writing
   */
  template <class GridView>
  class VTKSequenceWriter
  {
    using VTKWriter = Dune::VTKWriter<GridView>;

  public:
    /// \brief Set up the VTKSequenceWriter class
    /**
     * \param vtkWriter Writer object used to write the individual time step data files
     */
    VTKSequenceWriter(std::shared_ptr<VTKWriter> vtkWriter)
      : vtkWriter_(std::move(vtkWriter))
    {}

    /// \brief Set up the VTKSequenceWriter class by creating a timestep writer of type \ref VTKWriter
    /**
     * \param gridView  GridView object passed to the constructor of the VTKWriter
     * \param args...   Additional arguments forwarded to the VTKWriter constructor.
     */
    template <class... Args>
    VTKSequenceWriter(GridView const& gridView, Args&&... args)
      : VTKSequenceWriter<GridView>(std::make_shared<VTKWriter>(gridView, FWD(args)...))
    {}

    virtual ~VTKSequenceWriter() = default;

    /// accessor for the underlying VTKWriter instance
    std::shared_ptr<VTKWriter> const& vtkWriter() const
    {
      return vtkWriter_;
    }

    /// \brief Adds a field of cell data to the VTK file
    template <class... Args>
    void addCellData(Args&&... args)
    {
      vtkWriter_->addCellData(FWD(args)...);
    }

    /// \brief Adds a field of vertex data to the VTK file
    template <class... Args>
    void addVertexData (Args&&... args)
    {
      vtkWriter_->addVertexData(FWD(args)...);
    }

    /// \brief Writes VTK data for the given time,
    /**
     * \param time The time(step) for the data to be written.
     * \param name The basename of the .pvd file (without file extension)
     * \param type VTK output type.
     */
    virtual void write(double time, std::string const& name, Dune::VTK::OutputType type = Dune::VTK::ascii)
    {
      pwrite(time, name, ".", "", type);
    }

    /// \brief Writes VTK data for the given time,
    /**
     * \param time The time(step) for the data to be written.
     * \param name The basename of the .pvd file (without file extension)
     * \param path The directory where to put the .pvd file
     * \param extendpath The (relative) subdirectory to path, where to put the timestep files
     * \param type VTK output type.
     */
    virtual void pwrite(double time, std::string const& name, std::string const& path, std::string const& extendpath,
                        Dune::VTK::OutputType type = Dune::VTK::ascii)
    {
      // remember current time step
      unsigned int count = timesteps_.size();
      timesteps_.push_back(time);

      // write VTK file
      vtkWriter_->pwrite(seqName(name, count), Dune::concatPaths(path, extendpath), "", type);

      // write pvd file ... only on rank 0
      if (Environment::mpiRank() == 0) {
        std::ofstream pvdFile;
        pvdFile.exceptions(std::ios_base::badbit | std::ios_base::failbit |
                           std::ios_base::eofbit);
        std::string pvdname = path + "/" + name + ".pvd";
        pvdFile.open(pvdname.c_str());
        pvdFile << "<?xml version=\"1.0\"?>\n"
                << "<VTKFile type=\"Collection\" version=\"0.1\" byte_order=\"" << Dune::VTK::getEndiannessString() << "\">\n"
                << "<Collection>\n";
        for (unsigned int i = 0; i <= count; ++i) {
          std::string piecepath = extendpath;
          std::string fullname = getParallelHeaderName(seqName(name,i), piecepath, Environment::mpiSize());
          pvdFile << "<DataSet timestep=\"" << timesteps_[i]
                  << "\" part=\"0\" file=\""
                  << fullname << "\"/>\n";
        }
        pvdFile << "</Collection>\n"
                << "</VTKFile>\n" << std::flush;
        pvdFile.close();
      }
    }

  protected:
    // create sequence name
    static std::string seqName(std::string const& name, unsigned int count)
    {
      std::stringstream n;
      n.fill('0');
      n << name << "-" << std::setw(5) << count;
      return n.str();
    }

    static std::string getParallelHeaderName(std::string const& name, std::string const& path, int commSize)
    {
      std::ostringstream s;
      if (path.size() > 0) {
        s << path;
        if (path[path.size()-1] != '/')
          s << '/';
      }
      s << 's' << std::setw(4) << std::setfill('0') << commSize << '-';
      s << name;
      if (GridView::dimension > 1)
        s << ".pvtu";
      else
        s << ".pvtp";
      return s.str();
    }

  private:
    std::shared_ptr<VTKWriter> vtkWriter_;
    std::vector<double> timesteps_;
  };

} // end namespace AMDiS

