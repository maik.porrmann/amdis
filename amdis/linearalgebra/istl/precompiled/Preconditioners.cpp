#include <config.h>

#include "Preconditioners.hpp"

namespace Dune
{
  template class SeqJac<Precompiled::Matrix, Precompiled::Vector, Precompiled::Vector>;
  template class SeqSOR<Precompiled::Matrix, Precompiled::Vector, Precompiled::Vector>;
  template class SeqSSOR<Precompiled::Matrix, Precompiled::Vector, Precompiled::Vector>;
  template class Richardson<Precompiled::Vector, Precompiled::Vector>;
  template class SeqILU<Precompiled::Matrix, Precompiled::Vector, Precompiled::Vector>;
  template class SeqILDL<Precompiled::Matrix, Precompiled::Vector, Precompiled::Vector>;

} // end namespace Dune
