#pragma once

#if AMDIS_HAS_HYPRE && HAVE_MPI

#include <HYPRE.h>

#include <amdis/Initfile.hpp>
#include <amdis/linearalgebra/mtl/PreconditionerInterface.hpp>
#include <amdis/linearalgebra/mtl/itl/hypre.hpp>

namespace AMDiS
{
  /// \brief Interface to the HYPRE BoomerAMG preconditioner [...]
  /**
   * Parameters provided by AMDiS:
   *
   * [PRECON]->cycle mode:
   * 	1...V-cycle
   *	2...W-cycle
   *
   * [PRECON]->interpolation type:
   *  0...classical modified interpolation
   *	1...LS interpolation (for use with GSMG)
   * 	2...classical modified interpolation for hyperbolic PDEs
   * 	3...direct interpolation (with separation of weights)
   * 	4...multipass interpolation
   * 	5...multipass interpolation (with separation of weights)
   * 	6...extended+i interpolation
   * 	7...extended+i (if no common C neighbor) interpolation
   * 	8...standard interpolation
   * 	9...standard interpolation (with separation of weights)
   * 	10..classical block interpolation (for use with nodal systems version only)
   * 	11..classical block interpolation (for use with nodal systems version only)
   * 		with diagonalized diagonal blocks
   * 	12..FF interpolation
   * 	13..FF1 interpolation
   * 	14..extended interpolation
   *
   *  [PRECON]->info:
   * 	0...no printout (default)
   * 	1...print setup information
   * 	2...print solve information
   * 	3...print both setup and solve information
   *
   *  [PRECON]->relax type:
   * 	0...Jacobi
   * 	1...Gauss-Seidel, sequential (very slow!)
   * 	2...Gauss-Seidel, interior points in parallel, boundary sequential (slow!)
   * 	3...hybrid Gauss-Seidel or SOR, forward solve
   * 	4...hybrid Gauss-Seidel or SOR, backward solve
   * 	5...hybrid chaotic Gauss-Seidel (works only with OpenMP)
   * 	6...hybrid symmetric Gauss-Seidel or SSOR
   * 	9...Gaussian elimination (only on coarsest level)
   **/
  template <class M, class X, class Y>
  class HyprePrecon
      : public PreconditionerInterface<M,X,Y>
  {
    using Self = HyprePrecon;
    using Super = PreconditionerInterface<M,X,Y>;

  public:
    /// A creator to be used instead of the constructor.
    struct Creator : CreatorInterfaceName<Super>
    {
      std::unique_ptr<Super> createWithString(std::string prefix) override
      {
        return std::make_unique<Self>(prefix);
      }
    };

  public:
    HyprePrecon(std::string const& prefix)
    {
      // read HYPRE parameters
      int maxIter = 1, cycleMode = -1, interpolation = -1, relaxation = -1, info = 1;
      Parameters::get(prefix + "->max iteration", maxIter);
      Parameters::get(prefix + "->cycle mode", cycleMode);
      Parameters::get(prefix + "->interpolation type", interpolation);
      Parameters::get(prefix + "->relax type", relaxation);
      Parameters::get(prefix + "->info", info);

      config_.maxIter(maxIter);
      config_.interpolationType(interpolation);
      config_.relaxType(relaxation);
      config_.cycle(cycleMode);
      config_.printLevel(info);
    }

    /// Implementation of \ref PreconditionerBase::init()
    void init(M const& mat) override
    {
      hypreMatrix_.init(mat);
      HYPRE_IJMatrixGetObject(hypreMatrix_, (void**)(&matrix_));
      HYPRE_BoomerAMGCreate(&solver_);

      // apply solver parameters
      config_(solver_);

      mtl::dense_vector<double> swap(1, 0.0);
      mtl::HypreParVector x(swap);
      HYPRE_BoomerAMGSetup(solver_, matrix_, x, x);

      solverCreated_ = true;
    }

    /// Implementation of \ref PreconditionerInterface::finish()
    void finish() override
    {
      if (solverCreated_)
	      HYPRE_BoomerAMGDestroy(solver_);
      solverCreated_ = false;
    }

    /// Implementation of \ref PreconditionerInterface::solve()
    void solve(X const& in, Y& out) const override
    {
      assert(solverCreated_);
      mtl::HypreParVector x(in); // use b as initial guess
      mtl::HypreParVector b(in);

      [[maybe_unused]] int error = HYPRE_BoomerAMGSolve(solver_, matrix_, b, x);
      assert(error != 0);

      // write output back to MTL vector
      mtl::convert(x.getHypreVector(), out);
    }

    /// Implementation of \ref PreconditionerInterface::adjoint_solve()
    void adjoint_solve(X const& in, Y& out) const override
    {
      assert(solverCreated_);
      mtl::HypreParVector x(in); // use b as initial guess
      mtl::HypreParVector b(in);

      [[maybe_unused]] int error = HYPRE_BoomerAMGSolveT(solver_, matrix_, b, x);
      assert(error != 0);

      // write output back to MTL vector
      mtl::convert(x.getHypreVector(), out);
    }

  private:
    mtl::HypreMatrix hypreMatrix_;
    HYPRE_ParCSRMatrix matrix_;
    HYPRE_Solver solver_;

    mtl::AMGConfigurator config_;
    bool solverCreated_ = false;
  };

} // end namespace AMDiS

#endif // AMDIS_HAS_HYPRE && HAVE_MPI
