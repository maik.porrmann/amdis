#pragma once

#include <string>

namespace AMDiS
{
  enum class SymmetryStructure
  {
    unknown,
    spd,                    //< symmetric positive definite
    symmetric,              //< symmetric in terms of both structure and value
    hermitian,              //< transpose is the complex conjugation
    structurally_symmetric  //< symmetric nonzero structure
  };

  inline SymmetryStructure symmetryStructure(std::string str)
  {
    if (str == "spd")
      return SymmetryStructure::spd;
    else if (str == "symmetric")
      return SymmetryStructure::symmetric;
    else if (str == "hermitian")
      return SymmetryStructure::hermitian;
    else if (str == "structurally_symmetric")
      return SymmetryStructure::structurally_symmetric;
    else
      return SymmetryStructure::unknown;
  }

  inline std::string to_string(SymmetryStructure symmetry)
  {
    switch (symmetry) {
      case SymmetryStructure::spd:
        return "spd";
      case SymmetryStructure::symmetric:
        return "symmetric";
      case SymmetryStructure::hermitian:
        return "hermitian";
      case SymmetryStructure::structurally_symmetric:
        return "structurally_symmetric";
      default:
        return "unknown";
    }
  }

} // end namespace AMDiS
