#pragma once

#include <dune/functions/functionspacebases/basistags.hh>
#include <dune/functions/functionspacebases/compositebasis.hh>
#include <dune/functions/functionspacebases/lagrangebasis.hh>
#include <dune/functions/functionspacebases/powerbasis.hh>
#include <dune/grid/yaspgrid.hh>

#include <amdis/AdaptiveGrid.hpp>
#include <amdis/common/Logical.hpp>
#include <amdis/common/TypeTraits.hpp>
#include <amdis/functions/GlobalBasis.hpp>

namespace AMDiS
{
  namespace Impl
  {
    template <bool same, int... degs>
    struct LagrangePreBasisCreatorImpl;

    // specialization for single node basis
    template <int deg>
    struct LagrangePreBasisCreatorImpl<true, deg>
    {
      static auto create()
      {
        using namespace Dune::Functions::BasisFactory;
        return lagrange<deg>();
      }
    };

    // specialization for composite basis
    template <int... degs>
    struct LagrangePreBasisCreatorImpl<false, degs...>
    {
      static auto create()
      {
        using namespace Dune::Functions::BasisFactory;
        return composite(lagrange<degs>()..., flatLexicographic());
      }
    };

    // specialization for power basis
    template <int deg, int... degs>
    struct LagrangePreBasisCreatorImpl<true, deg, degs...>
    {
      static auto create()
      {
        using namespace Dune::Functions::BasisFactory;
        return power<1+sizeof...(degs)>(lagrange<deg>(), flatLexicographic());
      }
    };

    // factory to construct a global basis of several lagrange bases, with flat indexing.
    template <int deg, int... degs>
    struct LagrangePreBasisCreator
        : public LagrangePreBasisCreatorImpl<((deg == degs) &&...), deg, degs...>
    {};


    template <int dow, int k = 1>
    struct TaylorHoodPreBasisCreator
    {
      static auto create()
      {
        using namespace Dune::Functions::BasisFactory;
        return composite(power<dow>(lagrange<k+1>(), flatInterleaved()), lagrange<k>(), flatLexicographic());
      }
    };

  } // end namespace Impl


  /// Wrapper around a global basis providing default traits
  template <class GB, class T = double, class Traits = BackendTraits>
  struct DefaultProblemTraits
  {
    using GlobalBasis = GB;
    using CoefficientType = T;
    using Backend = Traits;
  };

  /// Generator for a basis and default problem traits
  template <class HostGrid, class PreBasisCreator, class T = double, class Traits = BackendTraits>
  struct DefaultBasisCreator
  {
    using Grid = AdaptiveGrid_t<HostGrid>;
    using GridView = typename Grid::LeafGridView;

    static auto create(std::string const& name, GridView const& gridView)
    {
      return AMDiS::GlobalBasis{name, gridView, PreBasisCreator::create()};
    }

    static auto create(GridView const& gridView)
    {
      return AMDiS::GlobalBasis{gridView, PreBasisCreator::create()};
    }

    using GlobalBasis = decltype(create(std::declval<GridView>()));
    using CoefficientType = T;
    using Backend = Traits;
  };


  /// \brief ProblemStatTraits for a (composite) basis composed of
  /// lagrange bases of different degree.
  template <class Grid, int... degrees>
  struct LagrangeBasis
      : public DefaultBasisCreator<Grid, Impl::LagrangePreBasisCreator<degrees...>>
  {};

  /// \brief Specialization of \ref LagrangeBasis for Grid type
  /// \ref Dune::YaspGrid for a given dimension.
  template <int dim, int... degrees>
  struct YaspGridBasis
      : public LagrangeBasis<Dune::YaspGrid<dim>, degrees...>
  {};

  /// \brief ProblemStatTraits of Taylor-Hood basis of lagrange-type
  /// with pressure degree k
  template <class Grid, int k = 1>
  struct TaylorHoodBasis
      : public DefaultBasisCreator<Grid, Impl::TaylorHoodPreBasisCreator<Grid::dimensionworld,k>>
  {};

} // end namespace AMDiS
