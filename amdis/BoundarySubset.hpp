#pragma once

#include <functional>

#include <amdis/Boundary.hpp>
#include <amdis/BoundaryManager.hpp>
#include <amdis/common/Concepts.hpp>
#include <amdis/common/ConceptsBase.hpp>

namespace AMDiS
{
  /// \brief Class defining a subset of a domain boundary.
  /**
   * Stores a predicate identifying boundary segments. This may be given as
   *  - a boundary manager and a boundary ID, see \ref BoundaryManager,
   *  - a functor of the form bool(GlobalCoordinate)
   *  - no argument in which case the whole boundary is used.
   *
   *  \tparam IS  Type of the intersection of the elements with the boundary
   **/
  template <class IS>
  class BoundarySubset
  {
    using Domain = typename IS::GlobalCoordinate;

  public:
    using Intersection = IS;

    /// Default constructor. Uses a predicate that returns true on the complete boundary.
    BoundarySubset()
      : predicate_([](Intersection const& is) -> bool { return is.boundary(); })
    {}

    /// Use a boundary manager and id to determine a subset.
    BoundarySubset(BoundaryManagerBase& boundaryManager, BoundaryType id)
      : predicate_([&boundaryManager, id](Intersection const& is) -> bool {
                     return is.boundary() && boundaryManager.boundaryId(is) == id;
                     })
    {}

    /// Use a predicate of the form bool(GlobalCoordinate) to determine a subset.
    template <class Predicate,
      REQUIRES(Concepts::Functor<Predicate, bool(Domain)>)>
    BoundarySubset(Predicate&& predicate)
      : predicate_([predicate](Intersection const& is) -> bool {
                     return predicate(is.geometry().center());
                     })
    {}

    /// Return true if intersection is on boundary segment
    bool operator()(Intersection const& is) const
    {
      return predicate_(is);
    }

  protected:
    std::shared_ptr<BoundaryManagerBase const> boundaryManager_;
    BoundaryType id_{0};
    std::function<bool(Intersection const&)> predicate_;
  };

} // end namespace AMDiS
