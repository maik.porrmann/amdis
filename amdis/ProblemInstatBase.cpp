#include <config.h>

#include "ProblemInstatBase.hpp"

#include "AdaptInfo.hpp"
#include "AdaptStationary.hpp"
#include "ProblemStatBase.hpp"
#include "StandardProblemIteration.hpp"

namespace AMDiS {

void ProblemInstatBase::setTime(AdaptInfo& adaptInfo)
{
  time_ = adaptInfo.time();
  tau_ = adaptInfo.timestep();
  invTau_ = 1.0 / tau_;
}


void ProblemInstatBase::solveInitialProblem(AdaptInfo& adaptInfo)
{
  if (initialProblem_) {
    StandardProblemIteration iteration(*initialProblem_);
    AdaptStationary initialAdapt(name_ + "->initial->adapt", iteration, adaptInfo);
    initialAdapt.adapt();
  }
}

} // end namespace AMDiS
