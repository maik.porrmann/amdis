#pragma once

#include <dune/istl/bvector.hh>
#include <dune/functions/backends/istlvectorbackend.hh>

#include <amdis/Output.hpp>
#include <amdis/common/FakeContainer.hpp>
#include <amdis/linearalgebra/VectorBase.hpp>
#include <amdis/typetree/MultiIndex.hpp>

namespace AMDiS
{
  template <class T, class = void>
  struct BlockVectorType
  {
    using type = Dune::FieldVector<T,1>;
  };

  template <class T>
  struct BlockVectorType<T, typename T::field_type>
  {
    using type = T;
  };

	template <class T>
	class ISTLBlockVector
      : public VectorBase<ISTLBlockVector<T>>
	{
	public:
    /// The vector type of the underlying base vector
	  using BaseVector = Dune::BlockVector<typename BlockVectorType<T>::type>;

    /// The type of the elements of the DOFVector
	  using block_type = typename BaseVector::block_type;

    /// The type of the elements of the DOFVector
    using value_type = T;

    /// The underlying field type
    using field_type = typename block_type::field_type;

    /// The index/size - type
	  using size_type  = typename BaseVector::size_type;

  public:
    /// Constructor. Constructs new BaseVector.
    template <class Comm,
      Dune::disableCopyMove<ISTLBlockVector,Comm> = 0>
    explicit ISTLBlockVector(Comm const&) {}

    ISTLBlockVector() = default;

    /// Return the data-vector \ref vector
    BaseVector const& vector() const
    {
      return vector_;
    }

    /// Return the data-vector \ref vector
    BaseVector& vector()
    {
      return vector_;
    }

    /// Return the current size of the \ref vector_
    std::size_t size() const
    {
      return vector_.size();
    }

    /// Resize the \ref vector_ to the size given by the \p sizeProvider
    // NOTE: this resize works recursively on the hierarchy of the vector
    template <class SizeInfo>
    void init(SizeInfo const& sizeInfo, bool clear)
    {
      Dune::Functions::istlVectorBackend(vector_).resize(sizeInfo);
      if (clear)
        vector_ = 0;
    }

    /// Access the entry \p idx of the \ref vector_ with write-access.
    template <class MultiIndex>
    value_type& at(MultiIndex const& idx)
    {
      return Dune::Functions::istlVectorBackend(vector_)[idx];
    }

    /// Access the entry \p idx of the \ref vector_ with read-access.
    template <class MultiIndex>
    value_type const& at(MultiIndex const& idx) const
    {
      return Dune::Functions::istlVectorBackend(vector_)[idx];
    }

	private:
    BaseVector vector_;
	};


  namespace Recursive
  {
    template <class T>
    struct ForEach<ISTLBlockVector<T>> : ForEach<VectorBase<ISTLBlockVector<T>>> {};

    template <class T>
    struct Transform<ISTLBlockVector<T>> : Transform<VectorBase<ISTLBlockVector<T>>> {};

    template <class T>
    struct InnerProduct<ISTLBlockVector<T>> : InnerProduct<VectorBase<ISTLBlockVector<T>>> {};

  } // end namespace Recursive
} // end namespace AMDiS
