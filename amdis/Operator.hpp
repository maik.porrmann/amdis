#pragma once

#include <type_traits>

#include <dune/common/concept.hh>
#include <dune/functions/common/typeerasure.hh>

#include <amdis/LocalOperator.hpp>
#include <amdis/common/ConceptsBase.hpp>
#include <amdis/typetree/TreeContainerTrafo.hpp>

namespace AMDiS {
namespace Impl {

template <class GV, class LC, class C>
struct OperatorTraits
{
  using GridView = GV;
  using LocalContext = LC;
  using ElementContainer = C;
};

template <class Traits, class... Nodes>
struct OperatorDefinition
{
  using GridView = typename Traits::GridView;

  // Definition of the interface an Operator must fulfill
  struct Interface
  {
    virtual ~Interface() = default;
    virtual void update(GridView const&) = 0;
    virtual LocalOperator<Traits,Nodes...> asLocalOperator() const = 0;
  };

  // Templatized implementation of the interface
  template <class Impl>
  struct Model : public Impl
  {
    using Impl::Impl;
    void update(GridView const& gv) final { this->get().update(gv); }
    LocalOperator<Traits,Nodes...> asLocalOperator() const final {
      return localOperator(this->get()); }
  };

  // The Concept definition of an Operator
  struct Concept
  {
    using LOpConcept = typename LocalOperatorDefinition<Traits,Nodes...>::Concept;

    template <class Op>
    auto require(Op&& op) -> decltype
    (
      op.update(std::declval<GridView>()),
      Dune::Concept::requireConcept<LOpConcept>(localOperator(op))
    );
  };

  using Base = Dune::Functions::TypeErasureBase<Interface,Model>;
};

} // end namespace Impl


/**
 * \addtogroup operators
 * @{
 **/

/// \brief The base class for an operator to be used in an \ref Assembler.
/**
 * Type-erasure base class for grid-operators.
 *
 * \tparam Traits    Parameters for the Operator collected in a class.
 *                   See \ref Impl::OperatorTraits.
 * \tparam Nodes...  Basis-node (pair) the operator is associated to.
 **/
template <class Traits, class... Nodes>
class Operator
    : public Impl::OperatorDefinition<Traits,Nodes...>::Base
{
  using Definition = Impl::OperatorDefinition<Traits,Nodes...>;
  using Super = typename Definition::Base;

public:
  // The gridview the operator is associated to
  using GridView = typename Traits::GridView;

public:
  /// Constructor. Pass any type supporting the \ref OperatorInterface
  template <class Impl, Dune::disableCopyMove<Operator,Impl> = 0>
  Operator(Impl&& impl)
    : Super{FWD(impl)}
  {
    static_assert(Concepts::models<typename Definition::Concept(Impl)>,
      "Implementation does not model the Operator concept.");
  }

  /// Default Constructor.
  Operator() = default;

  /// Update the operator data on a \ref GridView
  void update(GridView const& gv)
  {
    this->asInterface().update(gv);
  }

  /// Transform an operator into a local-operator
  friend LocalOperator<Traits,Nodes...> localOperator(Operator const& op)
  {
    return op.asInterface().asLocalOperator();
  }
};

template <class LocalContext, class Op, class GV>
auto makeOperator(Op op, GV const& gridView)
{
  op.update(gridView);
  return op;
}

/// Transform an operator into a local operator and also containers of operators
/// into containers of local operators
template <class Container>
auto localOperators(Container const& c)
{
  return Recursive::map([](auto const& op) { return localOperator(op); }, c);
}


} // end namespace AMDiS
