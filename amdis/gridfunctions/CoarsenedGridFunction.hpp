#pragma once

#include <functional>
#include <type_traits>

#include <dune/common/referencehelper.hh>
#include <dune/functions/gridfunctions/gridviewentityset.hh>

#include <amdis/common/TypeTraits.hpp>
#include <amdis/Output.hpp>

// backport of dune/functions/gridfunctions/coarsenedgridfunction.hh

namespace AMDiS {

/// \brief A grid function defining data on a coarser entity level than it can be bound to.
/**
 * This wrapper function allows to bind the wrapped grid function to elements on a finer
 * grid than this grid function was defined on. It works by traversing from the refined element
 * through the hierarchy until a coarser element is found that is in the entity-set of the
 * bound grid function.
 *
 * \tparam GV  The GridView the wrapper is bound to
 * \tparam GF  A GridFunction that is bound to a coarser GridView
 */
template <class GV, class GF>
class CoarsenedGridFunction
{
public:
  /// \brief The set of entities this function can be evaluated on
  using EntitySet = Dune::Functions::GridViewEntitySet<GV,0>;

  /// \brief The global coordinates
  using Domain = typename EntitySet::GlobalCoordinate;

  /// \brief The result type of the function
  using Range = std::invoke_result_t<GF, Domain>;

  /// \brief The local coordinates
  using LocalDomain = typename EntitySet::LocalCoordinate;

  /// \brief Type of the grid element the LocalFunction can be bound to
  using Element = typename EntitySet::Element;
    
  using Grid = typename GV::Grid;
  using Geometry = typename Element::Geometry;
    
  template <class ES, class LF>
  class CoarsenedLocalFunction
  {
  public:
    using Domain = LocalDomain;
    using Range = CoarsenedGridFunction::Range;
    using Element = CoarsenedGridFunction::Element;

    using Mapping = std::function<Domain(Domain const&)>;
    struct ChildElement {
      typename Element::HierarchicIterator it;
      Mapping local;
    };

  public:
    /// \brief Constructor. Stores the `localFct` by value.
    CoarsenedLocalFunction(ES const& entitySet, LF const& localFct, int maxLevel)
      : entitySet_(entitySet)
      , localFct_(localFct)
      , maxLevel_(maxLevel)
    {}

    CoarsenedLocalFunction(ES const& entitySet, LF&& localFct, int maxLevel)
      : entitySet_(entitySet)
      , localFct_(std::move(localFct))
      , maxLevel_(maxLevel)
    {}

    /// \brief Bind the wrapped local-function to grid element.
    /**
     * The given `element` might not be the element the wrapped local-function can be
     * bound to. It has to be iterated in the hierarchy down (to finer elements) until the
     * entity-set of the local function contains the found father element.
     */
    void bind(Element const& element)
    {
      element_.emplace(element);

      childs_.clear();
      if (entitySet_.contains(*element_)) {
        localFct_.bind(*element_);
        return;
      }

      test_exit_dbg(!element_->isLeaf(), "Element is leaf. Cannot traverse its children.");
      for (auto it = element_->hbegin(maxLevel_); it != element_->hend(maxLevel_); ++it) {
        if (entitySet_.contains(*it))
          childs_.emplace_back(ChildElement{.it=it, .local=makeMapping(*element_,*it)});
      }

      test_exit_dbg(!childs_.empty(), "No child element in entitySet found!");
    }

    /// \brief Unbind the wrapped local-function.
    void unbind()
    {
      element_.reset();
    }

    /// \brief Evaluate LocalFunction at bound element.
    Range operator()(Domain const& x) const
    {
      if (childs_.empty())
        return localFct_(x);
        
        // calculate checkInsideTolerance
        typename Grid::ctype const checkInsideTolerance = std::sqrt(std::numeric_limits<typename Grid::ctype>::epsilon());
        for (auto const& child : childs_) {
          auto refElem = referenceElement(*child.it);
          auto local = child.local(x);
          auto refTypeId = refElem.type().id();
          bool isInside = Dune::Geo::Impl::checkInside(refTypeId, Geometry::mydimension, local, checkInsideTolerance);
          if (isInside) {
            localFct_.bind(*child.it);
            return localFct_(local);
        }
      }

      error_exit("No child element with x in child found!");
      return Range{};
    }

    /// \brief Return the element this LocalFunction is bound to.
    Element const& localContext() const
    {
      assert(!!element_);
      return *element_;
    }

    /// \brief Construct a derivative by wrapping the derivative of the wrapped local-function.
    template <class LF_ = LF>
    auto makeDerivative() const
      -> CoarsenedLocalFunction<ES,TYPEOF(derivative(std::declval<LF_ const&>()))>
    {
      return {entitySet_, derivative(localFct_)};
    }

  protected:
    // Construct the coordinate mapping
    Mapping makeMapping(Element const& coarse, Element fine) const
    {
      Mapping map = [](Domain const& x) { return x; };
      while (coarse != fine && fine.hasFather()) {
        map = [map, geo=fine.geometryInFather()](Domain const& x) {
          return map(geo.local(x));
        };
        fine = fine.father();
      }
      return map;
    }

  private:
    ES const& entitySet_;
    mutable LF localFct_;
    int maxLevel_;
    std::optional<Element> element_;

    std::vector<ChildElement> childs_;
  };


  /// \brief Constructor.
  /**
   * Stores the GridFunction by value. Use a reference wrapper if copying the GridFunction
   * is an expensive operation!
   **/
  CoarsenedGridFunction(GV const& gridView, GF const& gridFct)
    : gridView_{gridView}
    , entitySet_{gridView}
    , gridFct_{gridFct}
  {}

  CoarsenedGridFunction(GV const& gridView, GF&& gridFct)
    : gridView_{gridView}
    , entitySet_{gridView}
    , gridFct_{std::move(gridFct)}
  {}

  /// \brief Evaluate the wrapped grid-function.
  Range operator()(Domain const& x) const
  {
    return gridFct_(x);
  }

  /// \brief Construct a derivative by wrapping the derivative of the wrapped grid-function
  template <class GF_ = GF>
  auto makeDerivative() const
    -> CoarsenedGridFunction<GV,
        TYPEOF(derivative(Dune::resolveRef(std::declval<GF_ const&>())))>
  {
    return {gridView_, derivative(Dune::resolveRef(gridFct_))};
  }

  /// \brief Construct local function from a DiscreteGlobalBasisFunction
  auto makeLocalFunction() const
  {
    using LF = TYPEOF(localFunction(Dune::resolveRef(gridFct_)));
    using LocalFunction = CoarsenedLocalFunction<EntitySet,LF>;
    return LocalFunction{gridFct_.entitySet(),
                         localFunction(Dune::resolveRef(gridFct_)),
                         gridView_.grid().maxLevel()};
  }

  /// \brief Get associated EntitySet.
  EntitySet const& entitySet () const
  {
    return entitySet_;
  }

private:
  GV gridView_;
  EntitySet entitySet_;
  GF gridFct_;
};

} // end namespace AMDiS
