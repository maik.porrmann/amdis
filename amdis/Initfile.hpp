#pragma once

#include <optional>
#include <string>

#include <dune/common/parametertree.hh>


namespace AMDiS
{
  class Initfile
  {
  public:
    /// initialize singleton object and global parameters
    static void init(std::string const& in);

    /// \brief Get parameter-values from parameter-tree
    /**
     * Looks for the `key` in the parameter-tree and returns
     * the stored and parsed value if found and parsable.
     *
     * May throw an exception if the value can not be parsed into type T
     **/
    template <class T>
    static std::optional<T> get(std::string const& key)
    {
      if (pt().hasKey(key))
        return pt().get<T>(key);
      else
        return {};
    }

    /// \brief Get parameter-values from parameter-tree with default value
    /**
     *  initialized in init()-method.
     *  Cast the value to the desired type.
     *
     *  \param key: The tag to look for
     *  \param value: The default value and result.
     **/
    template <class T>
    static void get(std::string const& key, T& value)
    {
      value = pt().get(key, value);
    }

    template <class T>
    static void set(std::string const& key, T const& value)
    {
      pt()[key] = value;
    }

    /// Returns specified info level
    static int getMsgInfo()
    {
      return singlett().msgInfo_;
    }

    /// print all data in singleton to std::cout
    static void printParameters();

    static void clearData() {}

  protected:
    Initfile() = default;

    /// Return the singleton that contains the data
    static Initfile& singlett()
    {
      static Initfile initfile;
      return initfile;
    }

    /// Return the parameter-tree
    static Dune::ParameterTree& pt()
    {
      return singlett().pt_;
    }

    /// Fill an parametr-tree from a file with filename fn
    void read(std::string const& fn, bool force = false);
    void write(std::string const& fn);

    /// read standard values for output and information of parameter-values
    void getInternalParameters();

    int msgInfo_ = 0;
    int paramInfo_ = 1;
    bool breakOnMissingTag_ = false;

    /// ParameterTree to read/store parameter values
    Dune::ParameterTree pt_;
  };

  using Parameters = Initfile;

} // end namespace AMDiS
