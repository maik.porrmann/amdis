install(FILES
    Collective.hpp
    Communicator.hpp
    Communicator.inc.hpp
    MpiTraits.hpp
    RecvDynamicSize.hpp
    Request.hpp
    RequestChain.hpp
    RequestOperations.hpp
DESTINATION ${CMAKE_INSTALL_INCLUDEDIR}/amdis/common/parallel)
