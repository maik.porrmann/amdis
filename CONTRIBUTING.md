# GIT Workflow

Follow the git workflow:
- Create a new branch for all new features, following the naming convention
  `feature/XYZ`
- Correct Bugs in issue branches, following the naming convention `issue/XYZ`
- For features and issues *Merge Request* into the master branch should be created in GitLab
- Do not push into `master` directly
- Releases are created in a new branch `releases/VERSION` that is not deleted after merged into master.
  After the merge, a tag with name `vVERSION` should be created.

# Code Style-Guide

This style-guide is intended for developers writing code for AMDiS, is not complete
and probably not applied to all parts of the AMDiS code. Feel free to edit existing
source files in order to fulfill the styles.

Parts of this convention are taken from well established style guides, like the
*Google C++ Style Guide*.

In general, the code should follow the [C++ Core Guidelines](https://github.com/isocpp/CppCoreGuidelines).

## File naming conventions

Filenames should be mixed lower and upper case (CamelCase), starting with an uppercase letter.
They should not include underscores or dashed. Use an uppercase letter to indicate
a new subword. Sourcefiles should end in `.cpp` and header files should end in `.hpp`.
In case you move the code of a template class to a separate file, included textually
at the end of the corresponding header file, use the extensions `.inc.hpp`.

The name of a file should follow the name of the implemented class in this file.

Examples of valid filenames:
* `AdaptInstat.hpp`
* `AdaptInstat.cpp`
* `DOFVector.hpp`
* `DOFVector.cpp`
* `DOFVector.inc.hpp` (the implementation of the methods of the template class
  `DOFVector<T>`)

Do not use filenames that already exist in /usr/include or are standard C/C++ include
files, such as `math.h` (remember that windows files-systems are case insensitive and
thus, there is no difference between `math.h` and `Math.H`.)

## Generale file structure

### Names and Order of Includes

All of the project's header files should be listed as descendants of the project's
source directory. The includes should be grouped following the rules:
* [class header file] ... for source files that implement an interface specified
  in a header file
* C system files.
* C++ system files.
* Other external libraries' header files.
* Your project's header files.

For better readability a comment above each group can be added. Within each section
the includes should be ordered alphabetically. Project's header files should be
surrounded by `"`, while external header files should be surrounded by `<...>`.

For example, the includes in `io/VtkWriter.cpp` might look like this:

```c++
#include "io/VtkWriter.hpp"

// [open]mpi header
#include <mpi.h>

// std c++ headers
#include <cmath>
#include <fstream>
// ...

// boost headers
#include <boost/filesystem/convenience.hpp>
#include <boost/filesystem/operations.hpp>

// AMDiS headers
#include <amdis/AdaptInfo.hpp>
#include <amdis/DOFVector.hpp>
// ...
```

### Namespaces

All implementation should be put into the namespace `AMDiS`. When a namespace closes,
a corresponding comment should be added to the closing brackets:

``` c++
namespace AMDiS
{
// ...
} // end namespace AMDiS
```

Implementation details are put into a subnamespace `Impl`. A few more subnamespaces
of `AMDiS` are allowed, e.g., `Concepts`. If one of these subnamespaces need another
subsubnamespace for implementation details, it should be named `Impl_` or `Impl2`.

## Formatting

### Line length
Each line of text in your code should be at most 100 characters long.

**Exceptions:**
* An #include statement with a long path may exceed 100 columns.
* A raw-string literal may have content that exceeds 100 characters.
* ...

### Indentation
Use two spaces instead of tabs! Follow the formatting rules of the K&R style.

### Variable qualifiers
Use trailing `const`/`volatile` qualifiers for variables, e.g.

```c++
double const& d_ref = d;
```

### Functions
Try to put all parameters in the line of the function declaration until it exceeds
the maximum line length. Then list remaining arguments aligned with the first
function argument.  Follow the style of the brackets, like

```c++
return_type function_name(Arg1 arg1, LongArgumentType longArgumentName,
                          Arg3 argument3)
{
  // implementation of the function
}
```

### Classes
Put base-classes on a new line, do not indent labels, member variables are appended
with an underscore and follow the other formatting rules in the followign example:

```c++
template <class Type>
class SubClass
    : public BaseClass
{
public:
  SubClass(Arg1 arg1, Arg2 arg2)
    : member1_(arg1)
    , member2_(arg2)
  {}

private:
  Arg1 member1_;
  Arg2 member2_;
};
```

## Documentation

Use Doxygen-Style comments for the documentation of functions and classes, except
when the function name already indicates its meaning completely.
