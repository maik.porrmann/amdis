// -*- tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 2 -*-
// vi: set et ts=4 sw=2 sts=2:

#include <iostream>

#include <amdis/AMDiS.hpp>
#include <amdis/Operations.hpp>
#include "Tests.hpp"

using namespace AMDiS;

int main(int argc, char** argv)
{
  Environment env(argc, argv);

  constexpr Operation::StaticConstant<int,0> op0a;
  constexpr Operation::Zero op0b;
  AMDIS_TEST_EQ(order(op0a,4,2,6,3,4), 0);
  AMDIS_TEST_EQ(order(op0b,4,2,6,3,4), 0);

  constexpr int zero = op0a(1,2,3,4,5);
  AMDIS_TEST_EQ(zero, 0);

  constexpr Operation::StaticConstant<int,1> op1a;
  constexpr Operation::One op1b;
  AMDIS_TEST_EQ(order(op1a,4,2,6,3,4), 0);
  AMDIS_TEST_EQ(order(op1b,4,2,6,3,4), 0);

  constexpr int one = op1a(1,2,3,4,5);
  AMDIS_TEST_EQ(one, 1);

  constexpr auto op1a_0 = partial(op1a, index_t<0>{});
  AMDIS_TEST((std::is_same_v<decltype(op1a_0), decltype(op0a)>));

  constexpr Operation::Id op2;
  AMDIS_TEST_EQ(op2(7), 7);
  AMDIS_TEST_EQ(order(op2,7), 7);

  constexpr Operation::Constant<int> op3(42);
  AMDIS_TEST_EQ(op3(1,2,3,4,5), 42);
  AMDIS_TEST_EQ(order(op3,4,2,6,3,4), 0);

  constexpr Operation::Arg<0> op4a;
  constexpr Operation::Arg<1> op4b;
  AMDIS_TEST_EQ(op4a(1,2,3,4,5), 1);
  AMDIS_TEST_EQ(op4b(1,2,3,4,5), 2);
  AMDIS_TEST_EQ(order(op4a,4,2,6,3,4), 4);
  AMDIS_TEST_EQ(order(op4b,4,2,6,3,4), 2);

  constexpr Operation::Plus op5;
  constexpr auto erg5 = op5(7,8);
  constexpr int order5 = order(op5,2,3);
  AMDIS_TEST_EQ(erg5, 15);
  AMDIS_TEST_EQ(order5, 3);

  constexpr Operation::Minus op6;
  constexpr auto erg6 = op6(15,8);
  constexpr int order6 = order(op6,2,3);
  AMDIS_TEST_EQ(erg6, 7);
  AMDIS_TEST_EQ(order6, 3);

  constexpr Operation::Multiplies op7;
  constexpr auto erg7 = op7(2,4);
  constexpr int order7 = order(op7,2,3);
  AMDIS_TEST_EQ(erg7, 8);
  AMDIS_TEST_EQ(order7, 5);

  constexpr auto op7_0 = partial(op7, index_t<0>{});
  constexpr auto op7_1 = partial(op7, index_t<1>{});
  constexpr auto erg7_0 = op7_0(2,4);
  constexpr auto erg7_1 = op7_1(2,4);
  AMDIS_TEST_EQ(erg7_0, 4);
  AMDIS_TEST_EQ(erg7_1, 2);

  constexpr Operation::Divides op8;
  constexpr auto erg8 = op8(16,8);
  AMDIS_TEST_EQ(erg8, 2);
  // constexpr int order8 = order(op8,2,3); // no order() for divides

  return report_errors();
}
