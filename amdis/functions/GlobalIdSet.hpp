#pragma once

#include <tuple>
#include <utility>
#include <vector>

#include <dune/common/exceptions.hh>
#include <dune/common/hybridutilities.hh>
#include <dune/common/version.hh>
#include <dune/functions/functionspacebases/nodes.hh>
#include <dune/functions/functionspacebases/lagrangebasis.hh>
#include <dune/functions/functionspacebases/lagrangedgbasis.hh>
#include <dune/grid/common/gridenums.hh>
#include <dune/localfunctions/common/localkey.hh>

#include <amdis/Output.hpp>
#include <amdis/common/Apply.hpp>
#include <amdis/common/ConceptsBase.hpp>
#include <amdis/common/ForEach.hpp>
#include <amdis/common/TupleUtility.hpp>
#include <amdis/utility/Twist.hpp>


namespace AMDiS
{
  // forward declaration
  template <class PreBasis, class TP, class NodeTag = typename PreBasis::Node::NodeTag>
  class NodeIdSet;

  /// \brief Type of a global used used to represent DOFs uniquely
  /**
   * A global DOF id is a pair an id of the entity the DOF is related to and
   * the local number on that entity. See also LocalBasisKey.
   **/
  template <class EntityIdType>
  class GlobalIdType : public std::pair<EntityIdType, std::size_t>
  {
    using Super = std::pair<EntityIdType, std::size_t>;

  public:
    GlobalIdType(std::size_t i = 0)
      : Super(EntityIdType{}, i)
    {}

    using Super::Super;

    /// Increment the second index only
    GlobalIdType& operator++()
    {
      ++this->second;
      return *this;
    }

    // Add to the second index only
    GlobalIdType operator+(std::size_t shift) const
    {
      return GlobalIdType{this->first, this->second + shift};
    }

    friend std::ostream& operator<<(std::ostream& os, GlobalIdType const& id)
    {
      os << "(" << id.first << "," << id.second << ")";
      return os;
    }
  };

  template <class Basis>
  using GlobalIdType_t
    = GlobalIdType<typename Basis::GridView::Grid::GlobalIdSet::IdType>;


  /// \brief Provide global ids for all DOFs in a global basis
  /**
   * A GlobalBasisIdSet provides and interface to retrieve unique IDs
   * (over all partitions) for all DOFs in the basis. It is implemented
   * in terms of the \ref NodeIdSet for each basis node that locally
   * on each element calculates a global unique id.
   *
   * **Examples:**
   * ```
    GlobalBasisIdSet<Basis> idSet(basis);
    for (const auto& e : elements(basis.gridView())) {
      idSet.bind(e);
      for (std::size_t i = 0; i < idSet.size(); ++i) {
        auto id = idSet.id(i); // global unique ID of i'th local DOF
        auto pt = idSet.partitionType(i); // partition type of i'th local DOF
      }
      idSet.unbind();
    }
   * ```
   *
   **/
  template <class GB, class = std::void_t<> >
  class GlobalBasisIdSet
  {
  public:
    using GlobalBasis = GB;
    using Tree = typename GB::LocalView::Tree;
    using size_type = std::size_t;

    using GridView = typename GlobalBasis::GridView;
    using Grid = typename GridView::Grid;
    using Element = typename GridView::template Codim<0>::Entity;
    using EntityIdType = typename Grid::GlobalIdSet::IdType;
    using PartitionType = Dune::PartitionType;
    using IdType = GlobalIdType_t<GB>;

    using PreBasis = typename GlobalBasis::PreBasis;
    using TreePath = typename GlobalBasis::PrefixPath;
    using NodeIdSet = AMDiS::NodeIdSet<PreBasis, TreePath>;
    using Twist = AMDiS::Twist<typename Grid::GlobalIdSet, GridView::dimension>;

  public:
    GlobalBasisIdSet(GlobalBasis const& globalBasis)
      : tree_(globalBasis.localView().tree())
      , nodeIdSet_(globalBasis.gridView())
      , twist_(globalBasis.gridView().grid().globalIdSet())
    {
      initializeTree(tree_);
    }

    /// \brief Bind the IdSet to a grid element.
    /**
     * Binding the IdSet to an element collects the ids from all DOFs
     * on that element and stores it in a local vector. Thus, the global
     * DOF ids can be extracted using \ref id() afterwards cheaply.
     **/
    void bind(Element const& element)
    {
      bindTree(tree_, element);
      nodeIdSet_.bind(tree_);
      twist_.bind(element);
      data_.resize(size());
      nodeIdSet_.fillIn(twist_, data_.begin());
    }

    /// \brief unbind from the element
    void unbind()
    {
      nodeIdSet_.unbind();
    }

    /// \brief The number of DOFs on the current element in the basis tree
    size_type size() const
    {
      return tree_.size();
    }

    /// \brief Return the global unique ID of the `i`th DOF on the
    /// currently bound element in the basis tree.
    IdType id(size_type i) const
    {
      assert( i < data_.size() );
      return data_[i].first;
    }

    /// \brief Return the global unique ID of the entity associated to the
    /// `i`th DOF on the currently bound element in the basis tree.
    EntityIdType entityId(size_type i) const
    {
      return id(i).first;
    }

    /// \brief Return the partition type of the `i`th DOF on the
    /// currently bound element in the basis tree.
    PartitionType partitionType(size_type i) const
    {
      assert( i < data_.size() );
      return data_[i].second;
    }

  protected:
    Tree tree_;
    NodeIdSet nodeIdSet_;
    Twist twist_;
    using Data = std::pair<IdType, PartitionType>;
    std::vector<Data> data_;
  };


  // Specialization for SubspaceBasis
  template <class Basis>
  class GlobalBasisIdSet<Basis, std::void_t<typename Basis::RootBasis>>
      : public GlobalBasisIdSet<typename Basis::RootBasis>
  {
  public:
    GlobalBasisIdSet(Basis const& basis)
      : GlobalBasisIdSet<typename Basis::RootBasis>(basis.rootBasis())
    {}
  };


  template <class PB, class TP, class NodeTag>
  class NodeIdSet
  {
  public:
    using PreBasis = PB;
    using Node = typename PreBasis::Node;
    using GridView = typename PreBasis::GridView;
    using size_type = std::size_t;

    static_assert(Node::isLeaf, "Generic NodeIdSet implemented for LeafNodes only. Provide a specialization for your node!");

  private:
    static constexpr int dim = GridView::template Codim<0>::Entity::mydimension;

  public:
    NodeIdSet(GridView const& gridView)
      : gridView_(gridView)
      , sizes_{}
    {}

    /// \brief Bind the idset to a tree node
    void bind(const Node& node)
    {
      node_ = &node;
      size_ = node_->finiteElement().size();

      std::fill(std::begin(sizes_), std::end(sizes_), 0u);
      for (size_type i = 0; i < size_ ; ++i) {
        Dune::LocalKey const& localKey = node_->finiteElement().localCoefficients().localKey(i);
        sizes_[localKey.codim()]++;
      }
      auto refElem = Dune::referenceElement<double,GridView::dimension>(node_->element().type());
      for (size_type c = 0; c <= GridView::dimension ; ++c)
        sizes_[c] /= refElem.size(c);
    }

    /// \brief Unbind the idset
    void unbind()
    {
      node_ = nullptr;
    }

    /// \brief Number of DOFs in this node
    size_type size() const
    {
      return size_;
    }

    /// \brief Maps from subtree index set [0..size-1] to a globally unique id in global basis
    template <class Twist, class It>
    It fillIn(Twist const& twist, It it, size_type shift = 0) const
    {
      assert(node_ != nullptr);
      const auto& gridIdSet = gridView_.grid().globalIdSet();

      for (size_type i = 0; i < size_ ; ++i, ++it) {
        Dune::LocalKey localKey = node_->finiteElement().localCoefficients().localKey(i);
        unsigned int s = localKey.subEntity();
        unsigned int c = localKey.codim();
        it->first = {gridIdSet.subId(node_->element(), s, c), shift + twist.get(localKey,sizes_[c])};

        it->second = Dune::Hybrid::switchCases(std::make_index_sequence<dim+1>{}, c,
          [&](auto codim) { return node_->element().template subEntity<codim>(s).partitionType(); },
          [&]() {
            error_exit("Invalid codimension c = {}", c);
            return Dune::PartitionType{};
          });
      }

      return it;
    }

  protected:
    GridView gridView_;
    const Node* node_ = nullptr;
    size_type size_ = 0;
    std::array<unsigned int,GridView::dimension+1> sizes_;
  };


  // Specialization for PowerBasis
  template <class PreBasis, class TP>
  class NodeIdSet<PreBasis, TP, Dune::TypeTree::PowerNodeTag>
  {
  public:
    using Node = typename PreBasis::Node;
    using GridView = typename PreBasis::GridView;
    using size_type = std::size_t;

  protected:
    using SubPreBasis = typename PreBasis::SubPreBasis;
    using SubTreePath = decltype(Dune::TypeTree::push_back(std::declval<TP>(), std::size_t(0)));
    using SubNodeIdSet = NodeIdSet<SubPreBasis, SubTreePath>;

  public:
    NodeIdSet(GridView const& gridView)
      : subIds_(gridView)
    {}

    /// \brief Bind the idset to a tree node
    void bind(const Node& node)
    {
      node_ = &node;
      subIds_.bind(node.child(0));
    }

    /// \brief Unbind the idset
    void unbind()
    {
      node_ = nullptr;
      subIds_.unbind();
    }

    /// \brief Number of DOFs in this node
    size_type size() const
    {
      return node_->size();
    }

    /// \brief Maps from subtree index set [0..size-1] to a globally unique id in global basis
    template <class Twist, class It>
    It fillIn(Twist const& twist, It it, size_type shift = 0) const
    {
      assert(node_ != nullptr);
      for (std::size_t child = 0; child < node_->degree(); ++child)
      {
        size_type subTreeSize = subIds_.size();
        it = subIds_.fillIn(twist, it, shift);
        shift += subTreeSize;
      }
      return it;
    }

  protected:
    SubNodeIdSet subIds_;
    const Node* node_ = nullptr;
  };


  // Specialization for CompositePreBasis
  template <class PreBasis, class TP>
  class NodeIdSet<PreBasis, TP, Dune::TypeTree::CompositeNodeTag>
  {
  public:
    using Node = typename PreBasis::Node;
    using GridView = typename PreBasis::GridView;
    using size_type = std::size_t;

  protected:
    static const std::size_t children = Node::degree();
    using ChildIndices = std::make_index_sequence<children>;

    // The I'th SubPreBasis
    template <std::size_t I>
    using SubPreBasis = typename PreBasis::template SubPreBasis<I>;

    template <std::size_t I>
    using SubTreePath = decltype(Dune::TypeTree::push_back(std::declval<TP>(), Dune::index_constant<I>{}));

    template <std::size_t I>
    using SubNodeIdSet = NodeIdSet<SubPreBasis<I>, SubTreePath<I>>;

    // A tuple of NodeIdSets for the SubPreBases
    using IdsTuple = IndexMapTuple_t<std::make_index_sequence<children>, SubNodeIdSet>;

  public:
    NodeIdSet(GridView const& gridView)
      : idsTuple_(Ranges::applyIndices<children>([&](auto... i) {
          return std::make_tuple(SubNodeIdSet<VALUE(i)>(gridView)...);
          }))
    {}

    /// \brief Bind the idset to a tree node
    void bind(const Node& node)
    {
      node_ = &node;
      Ranges::forIndices<0,children>([&](auto i) {
        std::get<VALUE(i)>(idsTuple_).bind(node.child(i));
      });
    }

    /// \brief Unbind the idset
    void unbind()
    {
      node_ = nullptr;
      Ranges::forEach(idsTuple_, [](auto& ids) {
        ids.unbind();
      });
    }

    /// \brief Number of DOFs in this node
    size_type size() const
    {
      return node_->size();
    }

    /// \brief Maps from subtree index set [0..size-1] to a globally unique id in global basis
    template <class Twist, class It>
    It fillIn(Twist const& twist, It it, size_type shift = 0) const
    {
      assert(node_ != nullptr);
      Ranges::forEach(idsTuple_, [&](auto const& ids)
      {
        size_type subTreeSize = ids.size();
        it = ids.fillIn(twist, it, shift);
        shift += subTreeSize;
      });
      return it;
    }

  private:
    IdsTuple idsTuple_;
    const Node* node_ = nullptr;
  };


  template <class GV, int k, class TP>
  class NodeIdSet<Dune::Functions::LagrangeDGPreBasis<GV, k>, TP>
  {
  public:
    using PreBasis = Dune::Functions::LagrangeDGPreBasis<GV, k>;
    using Node = typename PreBasis::Node;
    using GridView = typename PreBasis::GridView;
    using size_type = std::size_t;

    NodeIdSet(GridView const& gridView)
      : gridView_(gridView)
    {}

    /// \brief Bind the idset to a tree node
    void bind(const Node& node)
    {
      node_ = &node;
      size_ = node_->finiteElement().size();
    }

    /// \brief Unbind the idset
    void unbind()
    {
      node_ = nullptr;
    }

    /// \brief Number of DOFs in this node
    size_type size() const
    {
      return size_;
    }

    /// \brief Maps from subtree index set [0..size-1] to a globally unique id in global basis
    template <class Twist, class It>
    It fillIn(Twist const& /*twist*/, It it, size_type shift = 0) const
    {
      assert(node_ != nullptr);
      const auto& gridIdSet = gridView_.grid().globalIdSet();
      auto elementId = gridIdSet.id(node_->element());

      for (size_type i = 0; i < size_; ++i, ++it)
      {
        it->first = {elementId, shift + i};
        it->second = node_->element().partitionType();
      }

      return it;
    }

  protected:
    GridView gridView_;
    const Node* node_ = nullptr;
    size_type size_ = 0;
  };


} // end namespace AMDiS
