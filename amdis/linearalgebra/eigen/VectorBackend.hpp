#pragma once

#include <Eigen/Dense>

#include <dune/common/ftraits.hh>

#include <amdis/Output.hpp>
#include <amdis/algorithm/ForEach.hpp>
#include <amdis/algorithm/Transform.hpp>
#include <amdis/common/FakeContainer.hpp>
#include <amdis/linearalgebra/VectorBase.hpp>
#include <amdis/typetree/MultiIndex.hpp>

namespace AMDiS
{
  class DefaultIndexDistribution;

  /// The basic container that stores a base vector and a corresponding basis
  template <class T>
  class EigenVector
      : public VectorBase<EigenVector<T>>
  {
  public:
    /// The type of the base vector
    using BaseVector = Eigen::Matrix<T, Eigen::Dynamic, 1>;

    /// The type of the elements of the DOFVector
    using value_type = typename BaseVector::Scalar;

    /// The index/size - type
    using size_type  = typename BaseVector::Index;

  private:
    /// The type of the elements of the DOFVector
    using block_type = value_type;

    /// The underlying field type
    using field_type = typename Dune::FieldTraits<value_type>::field_type;

  public:
    /// Constructor. Constructs new BaseVector.
    explicit EigenVector(DefaultIndexDistribution const&) {}

    /// Return the data-vector \ref vector_
    BaseVector const& vector() const
    {
      return vector_;
    }

    /// Return the data-vector \ref vector_
    BaseVector& vector()
    {
      return vector_;
    }

    /// Return the current size of the \ref vector_
    size_type size() const
    {
      return vector_.size();
    }


    /// Resize the \ref vector_ to the size \p s
    template <class SizeInfo>
    void init(SizeInfo const& size, bool clear)
    {
      vector_.resize(size_type(size));
      if (clear)
        vector_.setZero();
    }


    /// Access the entry \p i of the \ref vector with write-access.
    template <class MultiIndex>
    value_type& at(MultiIndex const& idx)
    {
      const size_type i = flatMultiIndex(idx);
      test_exit_dbg(i < size(), "Index {} out of range [0,{})", i, size());
      return vector_.coeffRef(i);
    }

    /// Access the entry \p i of the \ref vector with read-access.
    template <class MultiIndex>
    value_type const& at(MultiIndex const& idx) const
    {
      const size_type i = flatMultiIndex(idx);
      test_exit_dbg(i < size(), "Index {} out of range [0,{})", i, size());
      return vector_.coeff(i);
    }

  private:
    /// The data-vector (can hold a new BaseVector or a pointer to external data
    BaseVector vector_;
  };


  namespace Recursive
  {
    template <class T>
    struct ForEach<EigenVector<T>> : ForEach<VectorBase<EigenVector<T>>> {};

    template <class T>
    struct Transform<EigenVector<T>> : Transform<VectorBase<EigenVector<T>>> {};

    template <class T>
    struct InnerProduct<EigenVector<T>> : InnerProduct<VectorBase<EigenVector<T>>> {};

  } // end namespace Recursive
} // end namespace AMDiS
