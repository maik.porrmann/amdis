#pragma once

#include <dune/common/typetraits.hh>
#include <dune/common/exceptions.hh>

#include <dune/grid/common/capabilities.hh>
#include <dune/grid/common/gridview.hh>

namespace AMDiS
{
  /*
   * NOTE: this is a modification of dune/grid/common/defaultgridview.hh since we
   * want all implementation specific methods to be put into the grid class and not
   * into the entity class. See ibegin(), iend().
   */

  template <class GridImp>
  class DefaultLevelGridView;

  template <class GridImp>
  class DefaultLeafGridView;


  template <class GridImp>
  struct DefaultLevelGridViewTraits
  {
    using GridViewImp = DefaultLevelGridView<GridImp>;

    /// type of the grid
    using Grid = std::remove_const_t<GridImp>;

    /// type of the index set
    using IndexSet = typename Grid::Traits::LevelIndexSet;

    /// type of the intersection
    using Intersection = typename Grid::Traits::LevelIntersection;

    /// type of the intersection iterator
    using IntersectionIterator = typename Grid::Traits::LevelIntersectionIterator;

    /// type of the collective communication
    using CollectiveCommunication = typename Grid::Traits::CollectiveCommunication;

    template <int cd>
    struct Codim
    {
      using Entity = typename Grid::Traits::template Codim<cd>::Entity;
      using Geometry = typename Grid::template Codim<cd>::Geometry;
      using LocalGeometry = typename Grid::template Codim<cd>::LocalGeometry;

      /// Define types needed to iterate over entities of a given partition type
      template <Dune::PartitionIteratorType pit>
      struct Partition
      {
        /// iterator over a given codim and partition type
        using Iterator = typename Grid::template Codim<cd>::template Partition<pit>::LevelIterator;
      };

      using Iterator = typename Partition<Dune::All_Partition>::Iterator;
    };

    enum { conforming = Dune::Capabilities::isLevelwiseConforming<Grid>::v };
  };


  template <class GridImp>
  class DefaultLevelGridView
  {
  public:
    using Traits = DefaultLevelGridViewTraits<GridImp>;
    using Grid = typename Traits::Grid;
    using IndexSet = typename Traits::IndexSet;
    using Intersection = typename Traits::Intersection;
    using IntersectionIterator = typename Traits::IntersectionIterator;
    using CollectiveCommunication = typename Traits::CollectiveCommunication;

    template <int cd>
    using Codim = typename Traits::template Codim<cd>;

    enum { conforming = Traits::conforming };

  public:
    DefaultLevelGridView(Grid const& grid, int level)
      : grid_(&grid)
      , level_(level)
    {}

    /// Obtain a const reference to the underlying hierarchic grid
    Grid const& grid() const
    {
      assert(grid_);
      return *grid_;
    }

    /// Obtain the index set
    IndexSet const& indexSet() const
    {
      return grid().levelIndexSet(level_);
    }

    /// Obtain number of entities in a given codimension
    int size(int codim) const
    {
      return grid().size(level_, codim);
    }

    /// Obtain number of entities with a given geometry type
    int size(Dune::GeometryType const& type) const
    {
      return grid().size(level_, type);
    }

    /// Obtain begin iterator for this view
    template <int cd, Dune::PartitionIteratorType pit = Dune::All_Partition>
    typename Codim<cd>::template Partition<pit>::Iterator begin() const
    {
      return grid().template lbegin<cd, pit>(level_);
    }

    /// Obtain end iterator for this view
    template <int cd, Dune::PartitionIteratorType pit = Dune::All_Partition>
    typename Codim<cd>::template Partition<pit>::Iterator end() const
    {
      return grid().template lend<cd, pit>(level_);
    }

    /// Obtain begin intersection iterator with respect to this view
    IntersectionIterator ibegin(typename Codim<0>::Entity const& entity) const
    {
      return grid().ilevelbegin(entity);
    }

    /// Obtain end intersection iterator with respect to this view
    IntersectionIterator iend(typename Codim<0>::Entity const& entity) const
    {
      return grid().ilevelend(entity);
    }

    /// Obtain collective communication object
    CollectiveCommunication const& comm() const
    {
      return grid().comm();
    }

    /// Return size of the overlap region for a given codim on the grid view.
    int overlapSize(int codim) const
    {
      return grid().overlapSize(level_, codim);
    }

    /// Return size of the ghost region for a given codim on the grid view.
    int ghostSize(int codim) const
    {
      return grid().ghostSize(level_, codim);
    }

    /// Communicate data on this view
    template <class DataHandleImp, class DataType>
    void communicate(Dune::CommDataHandleIF<DataHandleImp, DataType>& data,
                     Dune::InterfaceType iftype,
                     Dune::CommunicationDirection dir) const
    {
      return grid().communicate(data, iftype, dir, level_);
    }

  private:
    Grid const* grid_;
    int level_;
  };


  template <class GridImp>
  struct DefaultLeafGridViewTraits
  {
    using GridViewImp = DefaultLeafGridView<GridImp>;

    /// type of the grid
    using Grid = std::remove_const_t<GridImp>;

    /// type of the index set
    using IndexSet = typename Grid::Traits::LeafIndexSet;

    /// type of the intersection
    using Intersection = typename Grid::Traits::LeafIntersection;

    /// type of the intersection iterator
    using IntersectionIterator = typename Grid::Traits::LeafIntersectionIterator;

    /// type of the collective communication
    using CollectiveCommunication = typename Grid::Traits::CollectiveCommunication;

    template <int cd>
    struct Codim
    {
      using Entity = typename Grid::Traits::template Codim<cd>::Entity;
      using Geometry = typename Grid::template Codim<cd>::Geometry;
      using LocalGeometry = typename Grid::template Codim<cd>::LocalGeometry;

      /// Define types needed to iterate over entities of a given partition type
      template <Dune::PartitionIteratorType pit>
      struct Partition
      {
        /// iterator over a given codim and partition type
        using Iterator = typename Grid::template Codim<cd>::template Partition<pit>::LeafIterator;
      };

      using Iterator = typename Partition<Dune::All_Partition>::Iterator;
    };

    enum { conforming = Dune::Capabilities::isLeafwiseConforming<Grid>::v };
  };


  template <class GridImp>
  class DefaultLeafGridView
  {
  public:
    using Traits = DefaultLeafGridViewTraits<GridImp>;
    using Grid = typename Traits::Grid;
    using IndexSet = typename Traits::IndexSet;
    using Intersection = typename Traits::Intersection;
    using IntersectionIterator = typename Traits::IntersectionIterator;
    using CollectiveCommunication = typename Traits::CollectiveCommunication;

    template <int cd>
    using Codim = typename Traits::template Codim<cd>;

    enum { conforming = Traits::conforming };

  public:
    DefaultLeafGridView(Grid const& grid)
      : grid_(&grid)
    {}

    /// obtain a const reference to the underlying hierarchic grid
    Grid const& grid() const
    {
      assert(grid_);
      return *grid_;
    }

    /// Obtain the index set
    IndexSet const& indexSet() const
    {
      return grid().leafIndexSet();
    }

    /// Obtain number of entities in a given codimension
    int size(int codim) const
    {
      return grid().size(codim);
    }

    /// Obtain number of entities with a given geometry type
    int size(Dune::GeometryType const& type) const
    {
      return grid().size(type);
    }


    /// Obtain begin iterator for this view
    template <int cd, Dune::PartitionIteratorType pit = Dune::All_Partition>
    typename Codim<cd>::template Partition<pit>::Iterator begin() const
    {
      return grid().template leafbegin<cd, pit>();
    }

    /// Obtain end iterator for this view
    template <int cd, Dune::PartitionIteratorType pit = Dune::All_Partition>
    typename Codim<cd>::template Partition<pit>::Iterator end() const
    {
      return grid().template leafend<cd, pit>();
    }

    /// Obtain begin intersection iterator with respect to this view
    IntersectionIterator ibegin(typename Codim<0>::Entity const& entity) const
    {
      return grid().ileafbegin(entity);
    }

    /// Obtain end intersection iterator with respect to this view
    IntersectionIterator iend(typename Codim<0>::Entity const& entity) const
    {
      return grid().ileafend(entity);
    }

    /// Obtain collective communication object
    CollectiveCommunication const& comm() const
    {
      return grid().comm();
    }

    /// Return size of the overlap region for a given codim on the grid view.
    int overlapSize(int codim) const
    {
      return grid().overlapSize(codim);
    }

    /// Return size of the ghost region for a given codim on the grid view.
    int ghostSize(int codim) const
    {
      return grid().ghostSize(codim);
    }

    /// Communicate data on this view
    template <class DataHandleImp, class DataType>
    void communicate(Dune::CommDataHandleIF<DataHandleImp, DataType>& data,
                     Dune::InterfaceType iftype,
                     Dune::CommunicationDirection dir) const
    {
      return grid().communicate(data, iftype, dir);
    }

  private:
    Grid const* grid_;
  };

} // end namespace AMDiS
