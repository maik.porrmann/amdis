#pragma once

#include <type_traits>

#include <amdis/localoperators/ZeroOrderTestTrialvec.hpp>

namespace AMDiS
{
  /**
   * \addtogroup operators
   * @{
   **/

  namespace tag
  {
    struct testvec_trial {};
  }


  /// zero-order operator \f$ \langle\Psi, \mathbf{b}\,\phi\rangle \f$
  template <class LC>
  struct GridFunctionOperatorRegistry<tag::testvec_trial, LC>
  {
    static constexpr int degree = 0;
    static tag::test_trialvec transposedTag(tag::testvec_trial) { return {}; }
    using type = ZeroOrderTestTrialvec;
  };

  /** @} **/

} // end namespace AMDiS
