#pragma once

#include <tuple>
#include <type_traits>
#include <utility>

#include <dune/common/hash.hh>

namespace AMDiS
{
  namespace Impl
  {
    // Recursive template code derived from Matthieu M.
    template <class Tuple, std::size_t I = std::tuple_size<Tuple>::value - 1>
    struct HashTupleImpl
    {
      static void apply(size_t& seed, Tuple const& tuple)
      {
        HashTupleImpl<Tuple, I-1>::apply(seed, tuple);
        Dune::hash_combine(seed, std::get<I>(tuple));
      }
    };

    template <class Tuple>
    struct HashTupleImpl<Tuple, 0>
    {
      static void apply(std::size_t& seed, Tuple const& tuple)
      {
        Dune::hash_combine(seed, std::get<0>(tuple));
      }
    };

  } // end namespace Impl

  template <class Tuple, template <class> class Map>
  struct MapTuple;

  template <class Tuple, template <class> class Map>
  using MapTuple_t = typename MapTuple<Tuple,Map>::type;

  template <class... T, template <class> class Map>
  struct MapTuple<std::tuple<T...>, Map>
  {
    using type = std::tuple<Map<T>...>;
  };

  template <class Indices, template <std::size_t> class Map>
  struct IndexMapTuple;

  template <class Indices, template <std::size_t> class Map>
  using IndexMapTuple_t = typename IndexMapTuple<Indices,Map>::type;

  template <std::size_t... I, template <std::size_t> class Map>
  struct IndexMapTuple<std::index_sequence<I...>, Map>
  {
    using type = std::tuple<Map<I>...>;
  };

} // end namespace AMDiS
