#pragma once

#include <memory>
#include <type_traits>
#include <utility>

#include <dune/common/hybridutilities.hh>
#include <dune/common/shared_ptr.hh>
#include <dune/common/std/type_traits.hh>
#include <dune/istl/preconditioner.hh>
#include <dune/istl/solvercategory.hh>

#include <amdis/common/TypeTraits.hpp>

namespace AMDiS
{
  /// Wrapper around dune-istl preconditioners, like Dune::BlockPreconditioner,
  /// or Dune::NonoverlappingBlockPreconditioner, to store a shared_ptr instead of a reference.
  template <class P, class S>
  class PreconWrapper
      : public Dune::Preconditioner<typename P::domain_type, typename P::range_type>
  {
    using Preconditioner = P;
    using Storage = S;

  public:
    using domain_type = typename P::domain_type;
    using range_type = typename P::range_type;

  private:
    template <class P_>
    using HasApplyFoward = decltype(std::declval<P_>().template apply<true>(std::declval<domain_type&>(), std::declval<range_type const&>()));

  public:
    template <class... Args>
    explicit PreconWrapper(Storage& storage, Args&&... args)
      : storage_(Dune::stackobject_to_shared_ptr(storage))
      , precon_(*storage_, FWD(args)...)
    {}

    template <class... Args>
    explicit PreconWrapper(std::shared_ptr<Storage> storage, Args&&... args)
      : storage_(std::move(storage))
      , precon_(*storage_, FWD(args)...)
    {}

    /// \brief Prepare the preconditioner.
    void pre(domain_type& x, range_type& b) override
    {
      precon_.pre(x, b);
    }

    /// \brief Apply one step of the preconditioner to the system A(v)=d.
    void apply(domain_type& v, range_type const& d) override
    {
      precon_.apply(v, d);
    }

    /// \brief Apply one step of the preconditioner in forward (or backward) direction
    template <bool forward>
    void apply(domain_type& v, range_type const& d)
    {
      if constexpr (Dune::Std::is_detected<HasApplyFoward, P>::value)
        precon_.template apply<forward>(v,d);
      else
        precon_.apply(v,d);
    }

    /// \brief Clean up.
    void post(domain_type& x) override
    {
      precon_.post(x);
    }

    /// Category of the preconditioner
    Dune::SolverCategory::Category category() const override
    {
      return precon_.category();
    }

  private:
    std::shared_ptr<Storage> storage_;
    Preconditioner precon_;
  };

} // end namespace AMDiS
