#pragma once

#if AMDIS_BACKEND == AMDIS_BACKEND_MTL

#include <amdis/linearalgebra/mtl/Constraints.hpp>
#include <amdis/linearalgebra/mtl/LinearSolver.hpp>
#include <amdis/linearalgebra/mtl/Preconditioners.hpp>
#include <amdis/linearalgebra/mtl/Solvers.hpp>
#include <amdis/linearalgebra/mtl/Traits.hpp>
#include <amdis/linearalgebra/mtl/MatrixBackend.hpp>
#include <amdis/linearalgebra/mtl/VectorBackend.hpp>
#include <amdis/linearalgebra/mtl/Operations.hpp>

#elif AMDIS_BACKEND == AMDIS_BACKEND_EIGEN

#include <amdis/linearalgebra/eigen/Constraints.hpp>
#include <amdis/linearalgebra/eigen/LinearSolver.hpp>
#include <amdis/linearalgebra/eigen/Solvers.hpp>
#include <amdis/linearalgebra/eigen/Traits.hpp>
#include <amdis/linearalgebra/eigen/MatrixBackend.hpp>
#include <amdis/linearalgebra/eigen/VectorBackend.hpp>
#include <amdis/linearalgebra/eigen/Operations.hpp>

#elif AMDIS_BACKEND == AMDIS_BACKEND_PETSC

#include <amdis/linearalgebra/petsc/Constraints.hpp>
#include <amdis/linearalgebra/petsc/LinearSolver.hpp>
#include <amdis/linearalgebra/petsc/Traits.hpp>
#include <amdis/linearalgebra/petsc/MatrixBackend.hpp>
#include <amdis/linearalgebra/petsc/VectorBackend.hpp>
#include <amdis/linearalgebra/petsc/Operations.hpp>

#elif AMDIS_BACKEND == AMDIS_BACKEND_ISTL

#include <amdis/linearalgebra/istl/Constraints.hpp>
#include <amdis/linearalgebra/istl/LinearSolver.hpp>
#include <amdis/linearalgebra/istl/Preconditioners.hpp>
#include <amdis/linearalgebra/istl/Solvers.hpp>
#include <amdis/linearalgebra/istl/Traits.hpp>
#include <amdis/linearalgebra/istl/MatrixBackend.hpp>
#include <amdis/linearalgebra/istl/VectorBackend.hpp>
#include <amdis/linearalgebra/istl/Operations.hpp>

#endif

#include <amdis/linearalgebra/MatrixFacade.hpp>
#include <amdis/linearalgebra/VectorFacade.hpp>
#include <amdis/linearalgebra/LinearSolverInterface.hpp>
