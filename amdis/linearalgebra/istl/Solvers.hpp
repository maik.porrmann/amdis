#pragma once

#include <memory>

#include <dune/common/classname.hh>
#include <dune/common/version.hh>

#include <dune/common/ftraits.hh>

#include <amdis/CreatorMap.hpp>
#include <amdis/Output.hpp>

#include <amdis/linearalgebra/istl/ISTLSolverCreator.hpp>
#include <amdis/linearalgebra/istl/precompiled/Solvers.hpp>

namespace AMDiS
{
  /// Adds default creators for linear solvers based on `Dune::BCRSMatrix`.
  /**
   * Adds creators for full-matrix aware solvers.
   * - *cg*: conjugate gradient method, \see Dune::CGSolver
   * - *pcg*: Generalized preconditioned conjugate gradient solver, \see Dune::GeneralizedPCGSolver
   * - *fcg*: Accelerated flexible conjugate gradient method (dune >= 2.7), \see Dune::RestartedFCGSolver
   * - *cfcg*: Complete flexible conjugate gradient method (dune >= 2.7), \see Dune::CompleteFCGSolver
   * - *bcgs*: stabilized bi-conjugate gradient method, \see Dune::BiCGSTABSolver
   * - *minres*: Minimal residul method, \see Dune::MINRESSolver
   * - *gmres*: Generalized minimal residual method, \see Dune::RestartedGMResSolver
   * - *fgmres*: Flexible Generalized Minimal Residual (FGMRes) method (dune >= 2.7), \see Dune::RestartedFlexibleGMResSolver
   * - *umfpack*: external UMFPACK solver, \see Dune::UMFPack
   * - *ldl*: external LDL solver, \see Dune::LDL
   * - *spqr*: external SQPR solver, \see Dune::SQPR
   * - *cholmod*: external Cholmod solver (dune >= 2.7), \see Dune::Cholmod
   * - *superlu*: external SuperLU solver, \see Dune::SuperLU
   **/
  template <class Traits>
  class DefaultCreators<tag::solver<Traits>>
  {
    using M = typename Traits::M;
    using X = typename Traits::X;
    using Y = typename Traits::Y;

    using FTraits = Dune::FieldTraits<typename M::field_type>;

    template <class S>
    using Creator = ISTLSolverCreator<S,Traits>;

    using Map = CreatorMap<tag::solver<Traits>>;

  public:
    static void init()
    {
      auto cg = new Creator<Dune::CGSolver<X>>;
      Map::addCreator("cg", cg);

      // Generalized preconditioned conjugate gradient solver.
      auto pcg = new Creator<Dune::GeneralizedPCGSolver<X>>;
      Map::addCreator("pcg", pcg);

      auto fcg = new Creator<Dune::RestartedFCGSolver<X>>;
      Map::addCreator("fcg", fcg);

      auto cfcg = new Creator<Dune::CompleteFCGSolver<X>>;
      Map::addCreator("cfcg", cfcg);

      auto bicgstab = new Creator<Dune::BiCGSTABSolver<X>>;
      Map::addCreator("bicgstab", bicgstab);
      Map::addCreator("bcgs", bicgstab);
      Map::addCreator("default", bicgstab);

      auto minres = new Creator<Dune::MINRESSolver<X>>;
      Map::addCreator("minres", minres);

      auto gmres = new Creator<Dune::RestartedGMResSolver<X,Y>>;
      Map::addCreator("gmres", gmres);

      auto fgmres = new Creator<Dune::RestartedFlexibleGMResSolver<X,Y>>;
      Map::addCreator("fgmres", fgmres);

      init_direct(std::is_same<typename FTraits::real_type, double>{});
    }

    static void init_direct(std::false_type)
    {
      warning("Direct solvers not created for the matrix with real_type = {}.",
        Dune::className<typename FTraits::real_type>());
    }

    static void init_direct(std::true_type)
    {
#if HAVE_SUITESPARSE_UMFPACK
      auto umfpack = new Creator<Dune::UMFPack<M>>;
      Map::addCreator("umfpack", umfpack);
#endif

#if HAVE_SUITESPARSE_LDL
      auto ldl = new Creator<Dune::LDL<M>>;
      Map::addCreator("ldl", ldl);
#endif

#if HAVE_SUITESPARSE_SPQR
      auto spqr = new Creator<Dune::SPQR<M>>;
      Map::addCreator("spqr", spqr);
#endif

#if HAVE_SUPERLU
      auto superlu = new Creator<Dune::SuperLU<M>>;
      Map::addCreator("superlu", superlu);
#endif

      // default direct solvers
#if HAVE_SUITESPARSE_UMFPACK
      Map::addCreator("direct", umfpack);
#elif HAVE_SUPERLU
      Map::addCreator("direct", superlu);
#endif
    }
  };


  // extern template declarations:
  extern template class DefaultCreators< tag::solver<Precompiled::SolverTraits> >;

} // end namespace AMDiS
