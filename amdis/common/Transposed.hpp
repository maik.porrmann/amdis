#pragma once

#include <cassert>
#include <type_traits>
#include <amdis/common/TypeTraits.hpp>
#include <dune/common/typeutilities.hh>

namespace AMDiS
{
  /// \brief The transposed view onto a matrix
  template <class Matrix>
  class TransposedMatrix
  {
    using RawMatrix = remove_cvref_t<Matrix>;

  public:
    using size_type = typename RawMatrix::size_type;
    using value_type = typename RawMatrix::value_type;

  private:
    struct ConstRowProxy
    {
      RawMatrix const* mat;
      size_type row;

      value_type const& operator[](size_type col) const
      {
        return (*mat)[col][row];
      }
    };

    struct MutableRowProxy
    {
      RawMatrix* mat;
      size_type row;

      value_type& operator[](size_type col)
      {
        return (*mat)[col][row];
      }
    };

  public:
    template <class M, Dune::disableCopyMove<TransposedMatrix,M> = 0>
    TransposedMatrix(M&& matrix)
      : matrix_(FWD(matrix))
    {}

    ConstRowProxy operator[](size_type row) const
    {
      return ConstRowProxy{&matrix_, row};
    }

    template <class M = Matrix,
      std::enable_if_t<not std::is_const_v<M>, int> = 0>
    MutableRowProxy operator[](size_type row)
    {
      return MutableRowProxy{&matrix_, row};
    }

    size_type N() const
    {
      return matrix_.M();
    }

    size_type M() const
    {
      return matrix_.N();
    }

    template <class Mat>
    TransposedMatrix& operator+=(Mat const& mat)
    {
      assert(mat.N() == N());
      assert(mat.M() == M());
      for (size_type i = 0; i < N(); ++i)
        for (size_type j = 0; j < M(); ++j)
          (*this)[i][j] += mat[i][j];

      return *this;
    }

  private:
    Matrix& matrix_;
  };

  template <class Matrix>
  auto transposed(Matrix&& matrix)
  {
    using M = std::remove_reference_t<Matrix>;
    return TransposedMatrix<M>{FWD(matrix)};
  }

} // end namespace AMDiS
