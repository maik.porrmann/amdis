
#include <amdis/AMDiS.hpp>
#include "DataTransferTest.hpp"

int main(int argc, char** argv)
{
  Environment env(argc, argv);

#if HAVE_DUNE_UGGRID
  using Grid = Dune::UGGrid<2>;
#else
  using Grid = Dune::YaspGrid<2>;
#endif

  using Domain = typename Dune::FieldVector<double, 2>;

  // polynomial of order 1
  auto p1 = [](const Domain& x) -> double { return {0.5 + 0.25*(x[0]+x[1])}; };

  // polynomial of order 2
  auto p2 = [](const Domain& x) -> double { return {0.5 + 0.25*(2*std::pow(x[0],2) + x[0]*x[1] - std::pow(x[1],2))}; };

  // polynomial of order 3
  auto p3 = [](const Domain& x) -> double { return {0.5 + 0.25*(2*std::pow(x[0],3) + x[0]*x[1] - std::pow(x[1],3))}; };

  // analytic function
  auto f = [](const Domain& x) -> double { return {0.5 + 0.25*(std::sin(2*pi*x[0]) + 0.25*std::cos(6*pi*x[1]))}; };

  AMDIS_TEST( (unchanged_test< Lagrange3<Grid> >({f})) );
  AMDIS_TEST( (coarsen_test< Lagrange3<Grid> >({f})) );
  AMDIS_TEST( (refine_test< Lagrange3<Grid> >({p3})) );

  AMDIS_TEST( (unchanged_test< TaylorHood<Grid> >({f,f,f})) );
  AMDIS_TEST( (coarsen_test< TaylorHood<Grid> >({f,f,f})) );
  AMDIS_TEST( (refine_test< TaylorHood<Grid> >({p2,p2,p1})) );

  return report_errors();
}