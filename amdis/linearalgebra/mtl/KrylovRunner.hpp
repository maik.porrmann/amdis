#pragma once

#include <string>

// MTL4 headers
#include <boost/numeric/itl/itl.hpp>
#include <boost/numeric/mtl/mtl.hpp>

// AMDiS headers
#include <amdis/Environment.hpp>
#include <amdis/linearalgebra/LinearSolverInterface.hpp>
#include <amdis/linearalgebra/mtl/Preconditioners.hpp>

namespace AMDiS
{
  /** \ingroup Solver
   *
   * \brief
   * Wrapper class for different MTL4 itl-solvers. These solvers
   * are parametrized by Matrix and Vector.
   **/
  template <class M, class X, class Y, class ITLSolver>
  class KrylovRunner
      : public LinearSolverInterface<M,X,Y>
  {
    using Self = KrylovRunner;

    using PreconBase = PreconditionerInterface<M,X,Y>;
    using FloatType = typename M::value_type;

  public:
    struct Creator final : CreatorInterfaceName<LinearSolverInterface<M,X,Y>>
    {
      std::unique_ptr<LinearSolverInterface<M,X,Y>>
      createWithString(std::string prefix) final
      {
        return std::make_unique<Self>(prefix);
      }
    };

  public:
    /// Constructor.
    explicit KrylovRunner(std::string const& prefix)
      : itlSolver_(prefix)
    {
      Parameters::get(prefix + "->absolute tolerance", aTol_);
      Parameters::get(prefix + "->relative tolerance", rTol_);
      Parameters::get(prefix + "->max iteration", maxIter_);
      Parameters::get(prefix + "->print cycle", printCycle_);

      createPrecon(prefix);
    }

    /// Implementation of \ref LinearSolverInterface::init()
    void init(M const& A) override
    {
      P_->init(A);
      A_ = &A;
    }

    /// Implementation of \ref LinearSolverInterface::finish()
    void finish() override
    {
      P_->finish();
    }

    /// Implementation of \ref LinearSolverInterface::apply()
    void apply(X& x, Y const& b, Dune::InverseOperatorResult& stat) override
    {
      Dune::Timer t;

      // print information about the solution process
      FloatType r0 = two_norm(b - (*A_)*x);
      itl::cyclic_iteration<FloatType> iter(r0, maxIter_, rTol_, aTol_, printCycle_);
      iter.set_quite(Environment::infoLevel() == 0);
      iter.suppress_resume(Environment::infoLevel() == 0);

      [[maybe_unused]] int error = itlSolver_(*A_, x, b, *P_, iter);

      stat.iterations = iter.iterations();
      stat.converged = iter.is_converged();
      stat.reduction = iter.relresid();
      stat.elapsed = t.elapsed();
    }

  protected:
    /// Create left/right preconditioners from parameters given in the init-file
    void createPrecon(std::string const& prefix)
    {
      // Creator for the left preconditioner
      std::string preconName = "default";
      Parameters::get(prefix + "->precon", preconName);

      auto creator
        = named(CreatorMap<PreconBase>::get(preconName, prefix + "->precon"));
      P_ = creator->createWithString(prefix + "->precon");
    }

  private:
    /// The itl solver object that performes the actual solve
    ITLSolver itlSolver_;

    /// The parameter prefix
    std::string prefix_;

    /// Pointer to the left/right preconditioner
    std::shared_ptr<PreconBase> P_;

    /// The linear operator
    M const* A_ = nullptr;

    /// Absolute solver tolerance ||b - A*x|| < aTol
    FloatType aTol_ = 0;

    /// Relative solver tolerance ||b - A*x||/||b - A*x0|| < rTol
    FloatType rTol_ = 1.e-6;

    /// The maximal number of iterations
    std::size_t maxIter_ = 1000;

    /// Print solver residuals every \ref printCycle 's iteration
    std::size_t printCycle_ = 100;
  };

} // end namespace AMDiS
