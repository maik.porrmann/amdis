#pragma once

#include <functional>
#include <memory>

#include <amdis/common/Index.hpp>
#include <amdis/linearalgebra/SymmetryStructure.hpp>

namespace AMDiS
{
  class MatrixSize
  {
  public:
    template <class RowBasis, class ColBasis>
    MatrixSize(RowBasis const& rowBasis, ColBasis const& colBasis,
               SymmetryStructure symmetry = SymmetryStructure::unknown)
      : rows_(rowBasis.dimension())
      , cols_(colBasis.dimension())
    {}

    /// Number of rows in the matrix
    std::size_t rows() const
    {
      return rows_;
    }

    /// Number of columns in the matrix
    std::size_t cols() const
    {
      return cols_;
    }

  private:
    std::size_t rows_;
    std::size_t cols_;
  };

} // end namespace AMDiS
