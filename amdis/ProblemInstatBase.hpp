#pragma once

#include <string>

#include "ProblemTimeInterface.hpp"

namespace AMDiS
{
  // forward declarations
  class AdaptInfo;
  class ProblemStatBase;

  /**
   * \ingroup Problem
   *
   * \brief
   * Base class for \ref ProblemInstat.
   */
  class ProblemInstatBase
      : public virtual ProblemTimeInterface
  {
  public:
    /// Constructor.
    ProblemInstatBase(std::string const& name)
      : ProblemTimeInterface()
      , name_(name)
    {}

    /// Constructor. Stores a pointer to the provided initialProblem.
    ProblemInstatBase(std::string const& name,
                      ProblemStatBase& initialProblem)
      : ProblemTimeInterface()
      , name_(name)
      , initialProblem_(&initialProblem)
    {}

    /// Destructor.
    ~ProblemInstatBase() override = default;

    /// Implementation of \ref ProblemTimeInterface::setTime().
    void setTime(AdaptInfo& adaptInfo) override;

    /// Implementation of \ref ProblemTimeInterface::solveInitialProblem().
    void solveInitialProblem(AdaptInfo& adaptInfo) override;

    /// Return the name of the instationary problem \ref name_
    virtual std::string const& name() const
    {
      return name_;
    }

    /// Return reference to current simulation time \ref time_ set in \ref setTime
    /// from `AdaptInfo::time()`.
    double const& time() const
    {
      return time_;
    }

    /// Return reference to current simulation timestep \ref tau_ set in \ref setTime
    /// from `AdaptInfo::timestep()`.
    double const& tau() const&
    {
      return tau_;
    }

    /// Return reference to current simulation 1.0/timestep \ref invTau_ set in
    /// \ref setTime from `1.0 / AdaptInfo::timestep()`.
    double const& invTau() const
    {
      return invTau_;
    }

  protected:
    /// Name of the instationary problem.
    std::string name_;

    /// An initialization problem solved in \ref solveInitialProblem(). non-owning pointer.
    ProblemStatBase* initialProblem_ = nullptr;

    /// The current time, set from adaptInfo.time()
    double time_ = 0.0;

    /// Timestep, set from adaptInfo.timestep()
    double tau_ = 1.0;

    /// 1 / timestep, calculated after timestep is set
    double invTau_ = 1.0;
  };

} // end namespace AMDiS
