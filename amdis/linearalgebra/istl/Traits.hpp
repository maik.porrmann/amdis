#pragma once

#include <dune/common/fmatrix.hh>
#include <dune/common/fvector.hh>
#include <dune/grid/common/partitionset.hh>
#include <dune/istl/operators.hh>
#include <dune/istl/preconditioner.hh>
#include <dune/istl/scalarproducts.hh>
#include <dune/istl/solver.hh>

#include <amdis/linearalgebra/SparsityPattern.hpp>
#include <amdis/linearalgebra/istl/IndexDistribution.hpp>
#include <amdis/linearalgebra/istl/Creators.hpp>
#include <amdis/linearalgebra/istl/MatrixBackend.hpp>
#include <amdis/linearalgebra/istl/VectorBackend.hpp>

namespace AMDiS
{
  /** Traits class for a linear solver for the system AX=B using an FE space described by a dune-functions Basis
   *  Contains typedefs specific to the ISTL backend.
   */
  struct ISTLTraits
  {
    template <class Basis>
    using IndexDist = ISTLIndexDistribution_t<Basis>;

    template <class RowBasis, class ColBasis>
    struct Matrix
    {
      using SparsityPattern = AMDiS::SparsityPattern;

      template <class Value>
      using Impl = ISTLBCRSMatrix<Value, IndexDist<RowBasis>>;
    };

    template <class>
    struct Vector
    {
      template <class Value>
      using Impl = ISTLBlockVector<Value>;
    };

    using PartitionSet = Dune::Partitions::All;
  };

  using BackendTraits = ISTLTraits;


  template <class Mat, class VecX, class VecY = VecX>
  struct SolverTraits
  {
    using M = typename Mat::BaseMatrix;
    using X = typename VecX::BaseVector;
    using Y = typename VecY::BaseVector;
    using Comm = typename Mat::Comm;

    using ScalProd        = Dune::ScalarProduct<X>;
    using LinOp           = Dune::AssembledLinearOperator<M, X, Y>;
    using Solver          = Dune::InverseOperator<X, Y>;
    using Prec            = Dune::Preconditioner<X, Y>;
    using ScalProdCreator = ISTLScalarProductCreator<X>;
    using ParPrecCreator  = ISTLParallelPreconditionerCreator<X, Y>;
    using LinOpCreator    = ISTLLinearOperatorCreator<M, X, Y>;
  };

  template <class Traits>
  struct SeqSolverTraits : Traits
  {
    using Comm = typename Traits::Comm::Sequential;
  };

} // end namespace AMDiS
