#include <set>

#include <amdis/AMDiS.hpp>
#include <amdis/ProblemStatTraits.hpp>
#include <amdis/functions/GlobalIdSet.hpp>

#include <dune/functions/functionspacebases/lagrangedgbasis.hh>

#include "Tests.hpp"

using namespace AMDiS;

template <class Basis>
void checkBasisIds(const Basis& basis)
{
  using IdSet = GlobalBasisIdSet<Basis>;
  using IdType = typename IdSet::IdType;

  std::set<IdType> cache{};

  IdSet idSet(basis);
  for (const auto& e : elements(basis.gridView()))
  {
    idSet.bind(e);

    for (std::size_t i = 0; i < idSet.size(); ++i)
    {
      auto id = idSet.id(i);
      cache.insert(id);

      [[maybe_unused]] auto pt = idSet.partitionType(i);
    }

    idSet.unbind();
  }

  AMDIS_TEST(cache.size() == basis.dimension());
}

template <class Grid>
void checkGrid(const Grid& grid)
{
  // create basis
  using namespace Dune::Functions::BasisFactory;
  auto basis1 = makeBasis(grid.leafGridView(),
    composite(
      power<2>(lagrange<2>()),
      lagrange<1>()
    ));

  auto basis2 = makeBasis(grid.leafGridView(),
    composite(
      power<2>(lagrangeDG<2>()),
      lagrangeDG<1>()
    ));

  checkBasisIds(basis1);
  checkBasisIds(basis2);
}

int main(int argc, char** argv)
{
  Environment env(argc, argv);

  // non-overlapping
  Dune::YaspGrid<2> grid1({1.0, 1.0}, {8,8}, 0, 0);
  Dune::YaspGrid<3> grid2({1.0, 1.0, 1.0}, {8,8,8}, 0, 0);

  // overlapping
  Dune::YaspGrid<2> grid3({1.0, 1.0}, {8,8}, 0, 1);
  Dune::YaspGrid<3> grid4({1.0, 1.0, 1.0}, {8,8,8}, 0, 1);

  checkGrid(grid1);
  checkGrid(grid2);
  checkGrid(grid3);
  checkGrid(grid4);

  return report_errors();
}
