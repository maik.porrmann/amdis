#include <amdis/AMDiS.hpp>
#include <amdis/Integrate.hpp>
#include <amdis/LocalOperators.hpp>
#include <amdis/ProblemStat.hpp>
#include <amdis/common/parallel/Communicator.hpp>
#include <amdis/common/parallel/RequestOperations.hpp>

#include <dune/grid/onedgrid.hh>
#include <dune/grid/yaspgrid.hh>
#if HAVE_DUNE_SPGRID
#include <dune/grid/spgrid.hh>
#endif
#if HAVE_DUNE_ALUGRID
#include <dune/alugrid/grid.hh>
#endif
#if HAVE_DUNE_UGGRID
#include <dune/grid/uggrid.hh>
#endif

#include <dune/functions/functionspacebases/lagrangedgbasis.hh>
#include <dune/functions/functionspacebases/powerbasis.hh>
#include <dune/functions/functionspacebases/compositebasis.hh>

#include "Tests.hpp"
using namespace AMDiS;

template <class Basis>
void test_basis(Basis const& basis, std::string gridName, std::string basisName)
{
  msg("test grid={}, basis={}...", gridName, basisName);

  Mpi::Communicator world(basis.gridView().comm());

  using Traits = BackendTraits;
  using IndexDist = typename Traits::IndexDist<Basis>;

  IndexDist dofMap{basis};

  using GlobalIndex = typename IndexDist::GlobalIndex;
  std::vector<std::vector<GlobalIndex>> data(world.size());

  data[world.rank()].reserve(dofMap.localSize());
  auto lv = basis.localView();
  for (auto const& e : elements(basis.gridView())) {
    lv.bind(e);
    for (std::size_t i = 0; i < lv.size(); ++i)
      data[world.rank()].push_back(dofMap.global(lv.index(i)));
  }

  Mpi::Tag tag{3912};
  if (world.rank() != 0)
    world.send(data[world.rank()], 0, tag);
  else {
    std::vector<Mpi::Request> recvRequests;
    for (int p = 1; p < world.size(); ++p)
      recvRequests.emplace_back( world.irecv(data[p], p, tag) );

    Mpi::wait_all(recvRequests.begin(), recvRequests.end());

    std::set<GlobalIndex> globalDofs;
    for (auto const& d : data)
      for (auto const& g : d)
        globalDofs.insert(g);

    // all global DOF indices are collected
    AMDIS_TEST( globalDofs.size() == dofMap.globalSize() );

    // global DOFs form a consecutive range
    AMDIS_TEST( *std::min_element(globalDofs.begin(), globalDofs.end()) == GlobalIndex(0u) );
    AMDIS_TEST( *std::max_element(globalDofs.begin(), globalDofs.end()) == GlobalIndex(dofMap.globalSize()-1) );
  }
}

std::string numCells(int dim)
{
  std::string result = "";
  for (int i = 0; i < dim; ++i)
    result += "8 ";
  return result;
}

template <class Grid>
void test_grid(std::string gridName)
{
  Parameters::set("mesh->num cells", numCells(Grid::dimension)); // 8 8 8
  Parameters::set("mesh->overlap", "0");

  auto grid = MeshCreator<Grid>("mesh").create();
  grid->loadBalance();

  auto gv = grid->leafGridView();
  auto const& indexSet = gv.indexSet();

  bool allSimplex = true;
  for (auto const& type : indexSet.types(0))
    allSimplex = allSimplex && type.isSimplex();

  // continuous basis
  Ranges::forIndices<1,4>([&](auto const k) {
    if (allSimplex || k < 3) {
      using namespace Dune::Functions::BasisFactory;
      auto basis = makeBasis(gv, lagrange<k>());
      test_basis(basis, gridName, "lagrange<" + std::to_string(k.value) + ">");
    }
  });

  // discontinuous basis
  Ranges::forIndices<1,5>([&](auto const k) {
    using namespace Dune::Functions::BasisFactory;
    auto basis = makeBasis(gv, lagrangeDG<k>());
    test_basis(basis, gridName, "lagrangeDG<" + std::to_string(k.value) + ">");
  });

  // Taylor-Hood basis
  {
    using namespace Dune::Functions::BasisFactory;
    auto basis = makeBasis(gv, composite(power<Grid::dimensionworld>(lagrange<2>(), flatInterleaved()), lagrange<1>(), flatLexicographic()));
    test_basis(basis, gridName, "TaylorHood");
  }
}


int main(int argc, char** argv)
{
  Environment env(argc, argv);

#if GRID_ID == 0
  test_grid<Dune::YaspGrid<2>>("YaspGrid<2>");
  test_grid<Dune::YaspGrid<3>>("YaspGrid<3>");
#elif GRID_ID == 1 && HAVE_DUNE_UGGRID
  Parameters::set("mesh->structured", "cube");
  test_grid<Dune::UGGrid<2>>("UGGrid<2,cube>");
  test_grid<Dune::UGGrid<3>>("UGGrid<3,cube>");
#elif GRID_ID == 2 && HAVE_DUNE_ALUGRID
  Parameters::set("mesh->structured", "cube");
  // test_grid<Dune::ALUGrid<2,2,Dune::cube,Dune::nonconforming>>("ALUGrid<2,cube,nonconforming>");
  // test_grid<Dune::ALUGrid<3,3,Dune::cube,Dune::nonconforming>>("ALUGrid<3,cube,nonconforming>");
  //test_grid<Dune::ALUGrid<2,3,Dune::cube,Dune::nonconforming>>();
#elif GRID_ID == 3 && HAVE_DUNE_SPGRID
  test_grid<Dune::SPGrid<double,2>>("SPGrid<2>");
  test_grid<Dune::SPGrid<double,3>>("SPGrid<3>");
#elif GRID_ID == 4 && HAVE_DUNE_UGGRID
  Parameters::set("mesh->structured", "simplex");
  test_grid<Dune::UGGrid<2>>("UGGrid<2,simplex>");
  test_grid<Dune::UGGrid<3>>("UGGrid<3,simplex>");
#elif GRID_ID == 5 && HAVE_DUNE_ALUGRID
  Parameters::set("mesh->structured", "simplex");
  // test_grid<Dune::ALUGrid<2,2,Dune::simplex,Dune::nonconforming>>("ALUGrid<2,simplex,nonconforming>");
  // test_grid<Dune::ALUGrid<3,3,Dune::simplex,Dune::nonconforming>>("ALUGrid<3,simplex,nonconforming>");
  //test_grid<Dune::ALUGrid<2,3,Dune::simplex,Dune::nonconforming>>();
#elif GRID_ID == 6 && HAVE_DUNE_ALUGRID
  Parameters::set("mesh->structured", "simplex");
  test_grid<Dune::ALUGrid<2,2,Dune::simplex,Dune::conforming>>("ALUGrid<2,simplex,conforming>");
  test_grid<Dune::ALUGrid<3,3,Dune::simplex,Dune::conforming>>("ALUGrid<2,simplex,conforming>");
  //test_grid<Dune::ALUGrid<2,3,Dune::simplex,Dune::conforming>>();
#endif

  return env.mpiRank() == 0 ? report_errors() : 0;
}
