#pragma once

#include <type_traits>

#include <dune/common/dynmatrix.hh>
#include <dune/common/dynvector.hh>

#include <amdis/ContextGeometry.hpp>

namespace AMDiS
{
  template <class LC, class C>
  struct DefaultAssemblerTraits
  {
    using LocalContext = LC;
    using ElementContainer = C;
  };

  /// Abstract base-class of a \ref Assembler
  template <class Traits, class... Nodes>
  class AssemblerInterface
  {
    using LocalContext = typename Traits::LocalContext;
    using ContextType = Impl::ContextTypes<LocalContext>;

  public:
    /// The codim=0 grid entity
    using Element = typename ContextType::Entity;
    /// The geometry of the \ref Element
    using Geometry = typename Element::Geometry;

    static constexpr int numNodes = sizeof...(Nodes);
    static_assert( numNodes == 1 || numNodes == 2,
      "VectorAssembler gets 1 Node, MatrixAssembler gets 2 Nodes!");

  public:
    /// Virtual destructor
    virtual ~AssemblerInterface() = default;

    /// Bind the local-assembler to the grid-element with its corresponding geometry
    virtual void bind(Element const& element, Geometry const& geometry) = 0;

    /// Unbind from the element
    virtual void unbind() = 0;

    /// Assemble an element matrix or element vector on the test- (and trial-) function node(s)
    virtual void assemble(typename Traits::LocalContext const& localContext,
                          Nodes const&... nodes,
                          typename Traits::ElementContainer& elementMatrixVector) = 0;
  };

} // end namespace AMDiS
