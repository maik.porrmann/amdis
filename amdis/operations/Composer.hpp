#pragma once

#include <tuple>
#include <type_traits>

#include <dune/common/std/apply.hh>

#include <amdis/common/Apply.hpp>
#include <amdis/common/Concepts.hpp>
#include <amdis/common/Logical.hpp>
#include <amdis/operations/Basic.hpp>

namespace AMDiS
{
  namespace Operation
  {
	    /// \brief Composition of Functors.
	    /**
	     * \ingroup operations
	     * Implement the composition `c:=f(g1,g2,...,gN), where `f` is an N-ary functor
	     * `gI` are functors of the same arity. The composition is defined as
	     * ```
	     * c(a1,a2,...,aM) := f(g1(a1,a2,..,aM), g2(a1,a2,...,aM),...,gN(a1,a2,...,aM))
	     * ```
	     *
	     * **Requirements:**
	     * - sizeof...(Gs) == N, with N = arity(F)
	     * - arity(Gs[0]) == arity(Gs[i]) for i = 1,...,N-1
	     **/
    template <class F, class... Gs>
    struct Composer
    {
      template <class F_, class... Gs_,
        REQUIRES( Concepts::Similar<Types<F,Gs...>, Types<F_,Gs_...>>) >
      constexpr Composer(F_&& f, Gs_&&... gs)
        : f_(FWD(f))
        , gs_(FWD(gs)...)
      {}

      template <class... Ts>
      constexpr auto operator()(Ts const&... args) const
      {
        auto eval = [&](auto const& g) { return g(args...); };
        return Ranges::apply([&,this](auto const&... gs) { return f_(eval(gs)...); }, gs_);
      }

      F f_;
      std::tuple<Gs...> gs_;
    };

#ifndef DOXYGEN
    template <class F, class... Gs>
    struct ComposerBuilder
    {
      using type = Composer<F, Gs...>;

      template <class F_, class... Gs_>
      static constexpr type build(F_&& f, Gs_&&... gs)
      {
        return type{FWD(f), FWD(gs)...};
      }
    };
#endif

    /// Generator function for \ref Composer
    template <class F, class... Gs>
    constexpr auto compose(F&& f, Gs&&... gs)
    {
      return ComposerBuilder<TYPEOF(f), TYPEOF(gs)...>::build(FWD(f), FWD(gs)...);
    }

    // Polynomial order or composed function combines the orders of the sub-functions
    template <class F, class... Gs, class... Int,
      REQUIRES(Concepts::HasFunctorOrder<F,sizeof...(Gs)>
               && (Concepts::HasFunctorOrder<Gs,sizeof...(Int)> &&...))>
    int order(Composer<F,Gs...> const& c, Int... degrees)
    {
      auto deg = [&](auto const& g) { return order(g, int(degrees)...); };
      return Ranges::apply([&](auto const&... gs) { return order(c.f_, deg(gs)...); }, c.gs_);
    }

    /// Partial derivative of composed function:
    /// Implements: // sum_i [ d_i(f)[g...] * d_j(g_i) ]
    template <int J, class F, class... Gs>
    auto partial(Composer<F,Gs...> const& c, index_t<J> _j);


#ifndef DOXYGEN
    // some specialization for the composer

    // id(g) => g
    template <class F>
    struct ComposerBuilder<Id, F>
    {
      using type = F;

      template <class F_>
      static constexpr F build(F_&& f)
      {
        return F{FWD(f)};
      }
    };
#endif

  } // end namespace Operation
} // end namespace AMDiS
