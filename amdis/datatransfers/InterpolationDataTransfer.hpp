#pragma once

#include <map>
#include <memory>

#include <amdis/DataTransfer.hpp>
#include <amdis/typetree/TreeContainer.hpp>

namespace AMDiS {

/**
 * \addtogroup Adaption
 * @{
 **/

namespace tag {

  struct interpolation_datatransfer
      : datatransfer
  {};

} // end namespace tag


// forward declaration
template <class Node, class Container, class Basis>
class NodeDataTransfer;


/** Data Transfer implementation for a single grid using interpolation
 *  Handles computations related to the geometric information of the grid and passes that to the
 *    underlying NodeDataTransfer classes
 */
template <class Basis, class Container>
class InterpolationDataTransfer
{
  using LocalView = typename Basis::LocalView;
  using Tree = typename LocalView::Tree;
  using GridView = typename Basis::GridView;
  using Grid = typename GridView::Grid;

  using Element = typename GridView::template Codim<0>::Entity;
  using Geometry = typename Element::Geometry;
  using LocalCoordinate = typename Geometry::LocalCoordinate;
  using IdType = typename Grid::LocalIdSet::IdType;

  template <class Node>
  using NodeElementData = typename NodeDataTransfer<Node, Container, Basis>::NodeElementData;
  using ElementData = TypeTree::TreeContainer<NodeElementData,Tree,true>;

public:
  InterpolationDataTransfer() = default;

  /** Saves data contained in coeff in the PersistentContainer
   *  To be called after grid.preAdapt() and before grid.adapt()
   */
  void preAdapt(Basis const& basis, Container const& coeff, bool mightCoarsen);

  /** Unpacks data from the PersistentContainer
   *  To be called after grid.adapt() and before grid.postAdapt()
   */
  // [[expects: basis is updated in gridView]]
  // [[expects: comm is updated in basis]]
  void adapt(Basis const& basis, Container& coeff);

  /** Performs cleanup
   *  To be called after grid.postAdapt()
   */
  void postAdapt(Container& coeff);

private:
  /// Container with data that persists during grid adaptation
  using PersistentContainer = std::map<IdType, ElementData>;
  PersistentContainer persistentContainer_{};

  /// Data transfer on a single basis node
  template <class Node>
  using NDT = NodeDataTransfer<Node, Container, Basis>;
  using NodeDataTransferContainer = TypeTree::TreeContainer<NDT,Tree,true>;
  NodeDataTransferContainer nodeDataTransfer_{};
};

template <>
struct DataTransferFactory<tag::interpolation_datatransfer>
{
  template <class Basis, class T, template <class> class Impl,
    REQUIRES(Concepts::GlobalBasis<Basis>)>
  static DataTransfer<Basis,VectorFacade<T,Impl>> create(Basis const&, VectorFacade<T,Impl> const&)
  {
    return InterpolationDataTransfer<Basis, VectorFacade<T,Impl>>{};
  }
};

/// @}

} // end namespace AMDiS

#include "InterpolationDataTransfer.inc.hpp"
