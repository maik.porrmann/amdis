#pragma once

#include <type_traits>

#include <amdis/GridFunctionOperator.hpp>
#include <amdis/common/StaticSize.hpp>

namespace AMDiS
{
  /**
   * \addtogroup operators
   * @{
   **/

  namespace tag
  {
    struct test {};
  }

  /// zero-order vector-operator \f$ (c\, \psi) \f$
  class ZeroOrderTest
  {
  public:
    ZeroOrderTest(tag::test) {}

    template <class CG, class Node, class Quad, class LocalFct, class Vec>
    void assemble(CG const& contextGeo, Node const& node, Quad const& quad,
                  LocalFct const& localFct, Vec& elementVector) const
    {
      static_assert(static_size_v<typename LocalFct::Range> == 1,
        "Expression must be of scalar type." );
      static_assert(Node::isLeaf,
        "Operator can be applied to Leaf-Nodes only");

      std::size_t size = node.size();

      for (auto const& qp : quad) {
        // Position of the current quadrature point in the reference element
        auto&& local = contextGeo.local(qp.position());

        // The multiplicative factor in the integral transformation formula
        auto factor = contextGeo.integrationElement(qp.position()) * qp.weight();
        factor *= localFct(local);

        auto const& shapeValues = node.localBasisValuesAt(local);
        for (std::size_t i = 0; i < size; ++i) {
          const auto local_i = node.localIndex(i);
          elementVector[local_i] += factor * shapeValues[i];
        }
      }
    }
  };

  template <class LC>
  struct GridFunctionOperatorRegistry<tag::test, LC>
  {
    static constexpr int degree = 0;
    using type = ZeroOrderTest;
  };

  /** @} **/

} // end namespace AMDiS
