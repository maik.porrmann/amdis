#include <dune/common/version.hh>
#include <dune/istl/preconditioners.hh>
#include <dune/istl/novlpschwarz.hh>
#include <dune/istl/schwarz.hh>

#include <amdis/linearalgebra/istl/precompiled/Common.hpp>

namespace Dune
{
  extern template class SeqJac<Precompiled::Matrix, Precompiled::Vector, Precompiled::Vector>;
  extern template class SeqSOR<Precompiled::Matrix, Precompiled::Vector, Precompiled::Vector>;
  extern template class SeqSSOR<Precompiled::Matrix, Precompiled::Vector, Precompiled::Vector>;
  extern template class Richardson<Precompiled::Vector, Precompiled::Vector>;
  extern template class SeqILU<Precompiled::Matrix, Precompiled::Vector, Precompiled::Vector>;
  extern template class SeqILDL<Precompiled::Matrix, Precompiled::Vector, Precompiled::Vector>;

} // end namespace Dune
