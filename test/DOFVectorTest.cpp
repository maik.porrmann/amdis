#include <config.h>

#include <memory>

#include <dune/common/filledarray.hh>
#include <dune/common/shared_ptr.hh>
#include <dune/grid/yaspgrid.hh>
#include <dune/functions/functionspacebases/compositebasis.hh>
#include <dune/functions/functionspacebases/powerbasis.hh>
#include <dune/functions/functionspacebases/lagrangebasis.hh>

#include <amdis/AdaptiveGrid.hpp>
#include <amdis/AMDiS.hpp>
#include <amdis/LinearAlgebra.hpp>
#include <amdis/functions/GlobalBasis.hpp>

#include "Tests.hpp"

using namespace AMDiS;
using namespace Dune::Functions::BasisFactory;

template <class B, class T>
void test_dofvector(DOFVector<B,T>& vec)
{
  using MultiIndex = typename B::MultiIndex;

  auto const& basis = vec.basis();

  vec.init(sizeInfo(basis), true);
  AMDIS_TEST_EQ(vec.localSize(), basis.size());

  vec.insert(MultiIndex{0}, T(1));
  vec.finish();

  AMDIS_TEST_EQ(vec.get(MultiIndex{0}), T(1));

  T min(10);
  T max(-10);
  Recursive::forEach(vec.coefficients(), [&min,&max](auto value) {
    min = std::min(min, value);
    max = std::max(max, value);
  });

  if (vec.localSize() > 1)
    AMDIS_TEST_EQ(min, T(0));
  AMDIS_TEST_EQ(max, T(1));
}

int main(int argc, char** argv)
{
  Environment env(argc, argv);

  // create grid
  using HostGrid = Dune::YaspGrid<2>;
  Dune::FieldVector<double, 2> L; L = 1.0;
  auto s = Dune::filledArray<2>(1);
  HostGrid hostGrid(L, s);
  AdaptiveGrid<HostGrid> grid(hostGrid);
  auto const& gridView = grid.leafGridView();

  // create basis
  auto preBasis = composite(power<2>(lagrange<2>(), flatInterleaved()),
                            lagrange<1>(),
                            flatLexicographic());
  GlobalBasis basis{gridView, preBasis};

  using Basis = decltype(basis);

  {
    DOFVector<Basis> vec1(basis);
    test_dofvector(vec1);

    DOFVector<Basis, double> vec2(basis);
    test_dofvector(vec2);

    auto vec3 = makeDOFVector(basis);
    test_dofvector(vec3);

    auto vec4 = makeDOFVector<double>(basis);
    test_dofvector(vec4);

    DOFVector vec5(basis);
    test_dofvector(vec5);
  }

  // test automatic updates
  {
    DOFVector<Basis> vec1(basis);

    // Conversion from Dune::Functions::DefaultGlobalBasis
    DOFVector vec2(gridView, preBasis);

    for (auto const& e : elements(gridView))
      grid.mark(1, e);

    grid.preAdapt();
    grid.adapt();
    grid.postAdapt();

    test_dofvector(vec1);
    test_dofvector(vec2);
  }

  report_errors();
}
