#include <config.h>

#include "ProblemInstat.hpp"

#include <dune/grid/yaspgrid.hh>

namespace AMDiS
{
  // explicit template instatiation
  template class ProblemInstat<LagrangeBasis<Dune::YaspGrid<2>,1>>;
  template class ProblemInstat<LagrangeBasis<Dune::YaspGrid<2>,1,1>>;

} // end namespace AMDiS
