// -*- tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 2 -*-
// vi: set et ts=4 sw=2 sts=2:

#include <iostream>

#include <dune/geometry/type.hh>

#include <amdis/AMDiS.hpp>
#include <amdis/Integrate.hpp>
#include <amdis/ProblemStat.hpp>

#include "Tests.hpp"

#include <dune/grid/yaspgrid.hh>

using namespace AMDiS;

using ElliptParam   = LagrangeBasis<Dune::YaspGrid<2>, 1>;
using ElliptProblem = ProblemStat<ElliptParam>;

int main(int argc, char** argv)
{
  Environment env(argc, argv);

  ElliptProblem prob("ellipt");
  prob.initialize(INIT_ALL);

  auto u = prob.solution();
  auto gv = u.basis().gridView();

  u << 1.0;
  double i1 = integrate(1.0, gv);
  double i2 = integrate(u, gv);
  double i3 = integrate([](auto const& x) { return 1.0; }, gv, 0);

  AMDIS_TEST_APPROX(i1, 1.0);
  AMDIS_TEST_APPROX(i2, 1.0);
  AMDIS_TEST_APPROX(i3, 1.0);

  return report_errors();
}
