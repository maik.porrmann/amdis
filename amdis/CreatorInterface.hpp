#pragma once

#include <memory>
#include <string>

#include <amdis/Output.hpp>

namespace AMDiS
{
  /** \ingroup Common
   * \brief
   * Interface for the implementation of the factory method pattern.
   * The creation of an object of a sub class of BaseClass is deligated
   * to a corresponding sub class of Creator<BaseClass>. So it is possible to
   * manage a CreatorMap, which can be extended at run-time. An example is
   * the LinearSolverInterfaceMap: If you write your own LinearSolverInterface sub class and a
   * corresponding Creator<LinearSolverInterface<T> >, you can add the creator together
   * with a key string to the LinearSolverInterfaceMap. Then you can create an LinearSolverInterface
   * depending of a key string read from the init file, which can also be
   * your own new solver.
   */
  template <class BaseClass>
  class CreatorInterface
  {
  public:
    virtual ~CreatorInterface() = default;

    /** \brief
     * Must be implemented by sub classes of CreatorInterface.
     * Creates a new instance of the sub class of BaseClass.
     */
    virtual std::unique_ptr<BaseClass> create() = 0;
  };

  /**
   * \ingroup Common
   *
   * \brief
   * Interface for creators with name.
   */
  template <class BaseClass>
  class CreatorInterfaceName
      : public CreatorInterface<BaseClass>
  {
  public:

    std::unique_ptr<BaseClass> create() final
    {
      error_exit("Should not be called. Call create(string) instead!");
      return {};
    };

    /** \brief
     * Must be implemented by sub classes of CreatorInterfaceName.
     * Creates a new instance of the sub class of BaseClass by passing a
     * string to the constructor.
     */
    virtual std::unique_ptr<BaseClass> createWithString(std::string) = 0;
  };


  /// cast a ptr of CreatorInterface to CreatorInterfaceName
  template <class BaseClass>
  inline CreatorInterfaceName<BaseClass>* named(CreatorInterface<BaseClass>* ptr)
  {
    auto result = dynamic_cast<CreatorInterfaceName<BaseClass>*>(ptr);
    test_exit_dbg(result, "Can not cast CreatorInterface to CreatorInterfaceName!");
    return result;
  }

} // end namespace AMDiS
