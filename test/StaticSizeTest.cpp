#include <amdis/AMDiS.hpp>
#include <amdis/common/StaticSize.hpp>

#include <dune/common/fvector.hh>
#include <dune/common/fmatrix.hh>
#include <dune/istl/multitypeblockvector.hh>
#include <dune/istl/multitypeblockmatrix.hh>

#if AMDIS_HAS_EIGEN
#include <Eigen/Dense>
#endif

#include "Tests.hpp"

using namespace AMDiS;

struct Null {};

int main(int argc, char** argv)
{
  Environment env(argc, argv);

  static_assert(static_size_v<double> == 1, "");
  static_assert(static_num_rows_v<double> == 1, "");
  static_assert(static_num_cols_v<double> == 1, "");
  static_assert(static_size_v<Null> == 0, "");
  static_assert(static_num_rows_v<Null> == 0, "");
  static_assert(static_num_cols_v<Null> == 0, "");

  using Vec1 = Dune::FieldVector<double,2>;
  using Vec2 = Dune::MultiTypeBlockVector<double,double>;
  using Vec3 = std::array<double,2>;
  using Vec4 = std::tuple<double,double>;
  using Vec5 = Dune::TupleVector<double,double>;

  static_assert(static_size_v<Vec1> == 2, "");
  static_assert(static_size_v<Vec2> == 2, "");
  static_assert(static_size_v<Vec3> == 2, "");
  static_assert(static_size_v<Vec4> == 2, "");
  static_assert(static_size_v<Vec5> == 2, "");

  using Mat1 = Dune::FieldMatrix<double,2,2>;
  using Mat2 = Dune::MultiTypeBlockMatrix<Vec2,Vec2>;

  static_assert(static_num_rows_v<Mat1> == 2, "");
  static_assert(static_num_cols_v<Mat1> == 2, "");
  static_assert(static_num_rows_v<Mat2> == 2, "");
  static_assert(static_num_cols_v<Mat2> == 2, "");

#if AMDIS_HAS_EIGEN
  using Vec6 = Eigen::Vector2d;
  using Vec7 = Eigen::Matrix<double,2,1>;
  using Vec8 = Eigen::Matrix<double,1,2>;
  static_assert(static_size_v<Vec6> == 2, "");
  static_assert(static_size_v<Vec7> == 2, "");
  static_assert(static_size_v<Vec8> == 2, "");

  using Mat3 = Eigen::Matrix2d;
  using Mat4 = Eigen::Matrix<double,2,2>;
  static_assert(static_num_rows_v<Mat3> == 2, "");
  static_assert(static_num_cols_v<Mat3> == 2, "");
  static_assert(static_num_rows_v<Mat4> == 2, "");
  static_assert(static_num_cols_v<Mat4> == 2, "");
#endif

  return 0;
}
