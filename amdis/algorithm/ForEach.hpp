#pragma once

#include <array>
#include <tuple>
#include <vector>
#include <type_traits>
#include <utility>

#include <dune/common/tuplevector.hh>
#include <amdis/common/ForEach.hpp>
#include <amdis/common/TypeTraits.hpp>

namespace AMDiS {
namespace Recursive {

template <class Vec>
struct ForEach;

/// \brief Recursive application of a functor `f` to a hierarchic container of containers.
/**
 * This utility function applies the given functor `f` to the "leaf" entries in
 * a hierarchic container. Therefore, the container is traversed recursively,
 * using specializations of the `ForEach<Container>::impl` class method.
 * If no specialization is provided, the function is applied to the whole container
 * or leaf entry, respectively.
 **/
template <class Container, class F>
void forEach (Container&& container, F&& f)
{
  ForEach<TYPEOF(container)>::impl(container,f);
}


// specializations for container types


template <class>
struct ForEach
{
private:
  // ranges with dynamic index access
  template <class Vec, class F,
    class = decltype(std::begin(std::declval<Vec>())),
    class = decltype(std::end(std::declval<Vec>()))>
  static void impl2 (Dune::PriorityTag<2>, Vec&& vec, F&& f)
  {
    for (auto&& v : vec)
      Recursive::forEach(v, f);
  }

  // ranges with static index access
  template <class Vec, class F,
    class = decltype(std::get<0>(std::declval<Vec>()))>
  static void impl2 (Dune::PriorityTag<1>, Vec&& vec, F&& f)
  {
    Ranges::forEach(vec, [&](auto&& v) {
      Recursive::forEach(v, f);
    });
  }

  // no range
  template <class Value, class F>
  static void impl2 (Dune::PriorityTag<0>, Value&& value, F&& f)
  {
    f(value);
  }

public:
  template <class Vec, class F>
  static void impl (Vec&& vec, F&& f)
  {
    impl2(Dune::PriorityTag<5>{}, vec, f);
  }
};

}} // end namespace AMDiS::Recursive
