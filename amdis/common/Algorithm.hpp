#pragma once

#include <algorithm>
#include <functional>
#include <iterator>
#include <tuple>
#include <type_traits>
#include <utility>

namespace AMDiS {

/// \brief Split a sequence `[first,last)` by the separators `sep` and pass the tokens as
/// begin-end iterator pair to the provided functor `f = void(InputIterator, InputIterator)`
template <class InputIter, class Tp, class BinaryFunc>
void split(InputIter first, InputIter last, Tp sep, BinaryFunc f)
{
  if (first == last)
    return;

  while (true) {
    InputIter found = std::find(first, last, sep);
    f(first, found);
    if (found == last)
      break;
    first = ++found;
  }
}

/// \brief Split a sequence `[first,last)` by any of the separators `[s_first, s_last)` and pass the tokens as
/// begin-end iterator pair to the provided functor `f = void(InputIterator, InputIterator)`
template <class InputIter, class SeparaterIter, class BinaryFunc>
void split(InputIter first, InputIter last, SeparaterIter s_first, SeparaterIter s_last, BinaryFunc f)
{
  if (first == last)
    return;

  while (true) {
    InputIter found = std::find_first_of(first, last, s_first, s_last);
    f(first, found);
    if (found == last)
      break;
    first = ++found;
  }
}

/// \brief Provide python-like indexing iterator that returns a pair of [i,element]
// see: http://reedbeta.com/blog/python-like-enumerate-in-cpp17
template <class Range,
  class Iter = decltype(std::begin(std::declval<Range>())),
  class      = decltype(std::end(std::declval<Range>()))>
constexpr auto enumerate(Range&& range)
{
  struct iterator
  {
    typename std::iterator_traits<Iter>::difference_type i;
    Iter iter;
    bool operator!=(iterator const& other) const { return iter != other.iter; }
    void operator++() { ++i; ++iter; }
    auto operator*() const { return std::tie(i, *iter); }
  };

  struct range_wrapper
  {
    Range range;
    auto begin() { return iterator{0, std::begin(range)}; }
    auto end() { return iterator{0, std::end(range)}; }
  };

  return range_wrapper{std::forward<Range>(range)};
}

} // end namepace AMDiS
