#pragma once

#if HAVE_QUADMATH
#include <functional>
#include <type_traits>

#include <dune/common/hash.hh>
#include <dune/common/quadmath.hh>

#include <amdis/common/DerivativeTraits.hpp>
#include <amdis/common/StaticSize.hpp>
#include <amdis/common/ValueCategory.hpp>
#include <amdis/gridfunctions/ConstantGridFunction.hpp>

namespace std
{
  template <class T>
  struct common_type<Dune::Float128, T>
  {
    using type = Dune::Float128;
  };

  template <class T>
  struct common_type<T, Dune::Float128>
  {
    using type = Dune::Float128;
  };

  template <>
  struct common_type<Dune::Float128, Dune::Float128>
  {
    using type = Dune::Float128;
  };

  template <>
  struct hash<Dune::Float128>
  {
    typedef Dune::Float128 argument_type;
    typedef std::size_t result_type;

    std::size_t operator()(const Dune::Float128& arg) const
    {
      hash<long double> hasher_ld;
      return hasher_ld((long double)(arg));
    }
  };

  template <>
  struct hash<const Dune::Float128>
      : public hash<Dune::Float128>
  {};

} // end namespace std


namespace Dune
{
  namespace Impl
  {
    // specialization for float arguments due to ambiguity
    template <class T,
      std::enable_if_t<not std::is_integral_v<T> && std::is_arithmetic_v<T>, int> = 0>
    inline Float128 pow(const Float128& x, const T& p)
    {
      return powq(float128_t(x), float128_t(p));
    }

  } // end namespace Impl
} // end namespace Dune


namespace AMDiS
{
  namespace Concepts
  {
    namespace Definition
    {
      template <>
      struct ConstantToGridFunction<Dune::Float128>
        : std::true_type {};

    } // end namespace Definition
  } // end namespace Concepts

  namespace Impl
  {
    template <>
    struct SizeImpl<Dune::Float128>
    {
      static constexpr auto eval(Dune::Float128)
        -> std::integral_constant<std::size_t, 1> { return {}; }
    };

    template <>
    struct NumRowsImpl<Dune::Float128>
    {
      static constexpr auto eval(Dune::Float128)
        -> std::integral_constant<std::size_t, 1> { return {}; }
    };

    template <>
    struct NumColsImpl<Dune::Float128>
    {
      static constexpr auto eval(Dune::Float128)
        -> std::integral_constant<std::size_t, 1> { return {}; }
    };

  } // end namespace Impl

  template <class K, int N>
  struct DerivativeTraits<Dune::Float128(Dune::FieldVector<K,N>), tag::gradient>
  {
    using Range = Dune::FieldVector<Dune::Float128,N>;
  };

  template <>
  struct ValueCategory<Dune::Float128>
  {
    using type = tag::scalar;
  };

} // end namespace AMDiS

#endif // HAVE_QUADMATH
