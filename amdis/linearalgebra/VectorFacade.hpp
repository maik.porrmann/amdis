#pragma once

#include <string>
#include <type_traits>
#include <utility>

#include <dune/common/classname.hh>
#include <dune/common/deprecated.hh>
#include <dune/common/hybridutilities.hh>
#include <dune/common/rangeutilities.hh>
#include <dune/common/shared_ptr.hh>
#include <dune/common/std/type_traits.hh>
#include <dune/functions/functionspacebases/concepts.hh>
#include <dune/functions/functionspacebases/sizeinfo.hh>

#include <amdis/Output.hpp>
#include <amdis/algorithm/ForEach.hpp>
#include <amdis/algorithm/InnerProduct.hpp>
#include <amdis/algorithm/Transform.hpp>
#include <amdis/common/Concepts.hpp>
#include <amdis/common/ConceptsBase.hpp>
#include <amdis/common/FakeContainer.hpp>
#include <amdis/common/TypeTraits.hpp>
#include <amdis/functions/NodeIndices.hpp>
#include <amdis/operations/Assigner.hpp>
#include <amdis/typetree/MultiIndex.hpp>

namespace AMDiS
{
  /// \brief The basic container that stores a base vector and a corresponding basis
  /**
   * A vector storing all the assembled Operators indexed with DOF indices. The
   * vector data is associated to a global basis.
   *
   * \tparam T            The coefficient type of the vector
   * \tparam VectorImpl   A linear-algebra backend for the vector storage
   **/
  template <class T, template <class> class VectorImpl>
  class VectorFacade
  {
    using Self = VectorFacade;
    using Impl = VectorImpl<T>;

  private:
    // States the vector can be in. Is changed on various insertion or gathering methods.
    enum class VectorState
    {
      unknown = 0,
      synchronized = 1,
      insert_values = 2,
      add_values = 3
    };

    template <class A>
    using VectorStateOf_t = std::conditional_t<std::is_same_v<A,Assigner::plus_assign>,
      std::integral_constant<VectorState, VectorState::add_values>,
      std::integral_constant<VectorState, VectorState::insert_values>>;

  public:
    using size_type = typename Impl::size_type;
    using value_type = typename Impl::value_type;

    /// Constructor. Forwards the basis to the implementation class and
    /// constructs a (type-erased) size-info object.
    template <class GlobalBasis,
      class = std::void_t<decltype(std::declval<GlobalBasis>().indexDistribution())> >
    VectorFacade(GlobalBasis const& basis)
      : impl_(basis.indexDistribution())
    {
      resizeZero(sizeInfo(basis));
    }

    /// Return the underlying linear algebra backend
    Impl const& impl() const { return impl_; }
    Impl&       impl()       { return impl_; }


    template <class V>
    using HasLocalSize = decltype(std::declval<V>().localSize());

    template <class V>
    using HasGlobalSize = decltype(std::declval<V>().globalSize());

    /// Return the number of entries in the local part of the vector
    std::size_t localSize() const
    {
      if constexpr (Dune::Std::is_detected<HasLocalSize,Impl>::value)
        return impl_.localSize();
      else
        return impl_.size();
    }

    /// Return the number of entries in the global vector
    std::size_t globalSize() const
    {
      if constexpr (Dune::Std::is_detected<HasGlobalSize,Impl>::value)
        return impl_.globalSize();
      else
        return impl_.size();
    }

    /// Resize the \ref vector to the size of the \ref basis
    template <class SizeInfo>
    void resize(SizeInfo const& sizeInfo)
    {
      init(sizeInfo, false);
    }

    /// Resize the \ref vector to the size of the \ref basis and set to zero
    template <class SizeInfo>
    void resizeZero(SizeInfo const& sizeInfo)
    {
      init(sizeInfo, true);
    }

    /// Prepare the vector for insertion of values, finish the insertion with
    /// \ref finish().
    template <class SizeInfo>
    void init(SizeInfo const& sizeInfo, bool clear)
    {
      impl_.init(sizeInfo, clear);
      state_ = clear ? VectorState::synchronized : VectorState::unknown;
    }

    /// Finish the insertion of values, see \ref init()
    void finish()
    {
      impl_.finish();
      state_ = VectorState::unknown;
    }

    /// \brief Insert a single value into the matrix (add or overwrite to existing value)
    /**
     * Inserts or adds a value into a certain location \p dof (given as dof multi-index)
     * of a vector. The insertion mode is determined by the \p assign functor. Use
     * \ref Assigner::plus_assign for adding values (default) or \ref Assigner::assign
     * for overwriting (setting) values. Different insertion modes can not be mixed!
     *
     * Insertion must be closed with a call to \ref finish().
     *
     * [[not collective]]
     */
    template <class Index, class Assign = Assigner::plus_assign,
      REQUIRES(Concepts::MultiIndex<Index>)>
    void insert(Index const& idx, typename Impl::value_type const& value, Assign assign = {})
    {
      test_exit_dbg(state_ == VectorStateOf_t<Assign>::value ||
                    state_ == VectorState::unknown ||
                    state_ == VectorState::synchronized,
        "Vector in invalid state {} for insertion by {}.", to_string(state_), Dune::className<Assign>());

      impl_.insert(idx, value, assign);

      // set the state to insert_values or add_values
      state_ = VectorStateOf_t<Assign>::value;
    }

    /// See \ref insert for assignment operation \ref Assigner::assign
    template <class Index,
      REQUIRES(Concepts::MultiIndex<Index>)>
    void set(Index const& idx, typename Impl::value_type const& value)
    {
      insert(idx, value, Assigner::assign{});
    }

    /// See \ref insert for assignment operation \ref Assigner::plus_assign
    template <class Index,
      REQUIRES(Concepts::MultiIndex<Index>)>
    void add(Index const& idx, typename Impl::value_type const& value)
    {
      insert(idx, value, Assigner::plus_assign{});
    }

    /// Return the value of the vector at the local index \ref idx
    template <class Index,
      REQUIRES(Concepts::MultiIndex<Index>)>
    typename Impl::value_type get(Index const& idx) const
    {
      const_cast<Self*>(this)->synchronize();
      return impl_.at(idx);
    }


    /// \brief Extract values from the vector referring to the given local indices
    /// and store it into a buffer
    /**
     * Collect value of indices and store them into a buffer. The buffer must be
     * a vector-like container with `buffer.resize()` and `buffer.begin()`. The
     * indices must be stored in an iterable container.
     *
     * If the vector is not in synchronized state, collects all necessary values
     * possibly from neighbouring processors.
     *
     * [[expects: localView is bound to an element]]
     * [[expects: node is in localView.tree()]]
     * [[possibly neighbor-wise collective operation]]
     */
    template <class LocalView, class Node, class Buffer,
      REQUIRES(Concepts::LocalView<LocalView>),
      REQUIRES(Concepts::BasisNode<Node>)>
    void gather(LocalView const& localView, Node const& node, Buffer& buffer) const
    {
      test_exit(state_ == VectorState::unknown ||
                state_ == VectorState::synchronized,
        "Vector in invalid state {} for gather operations.", to_string(state_));

      const_cast<Self*>(this)->synchronize();

      buffer.resize(node.size());
      impl_.gather(nodeIndices(localView, node), buffer.begin());
    }

    // [[expects: localView is bound to an element]]
    template <class LocalView, class Buffer,
      REQUIRES(Concepts::LocalView<LocalView>)>
    void gather(LocalView const& localView, Buffer& buffer) const
    {
      gather(localView, localView.tree(), buffer);
    }

    /// Insert a block of values into the vector (add or overwrite to existing values)
    /**
     * Inserts or adds values into certain locations of a vector. Insertion indices
     * are extracted from the given \p localView. The insertion mode is determined
     * by the \p assign functor. Use \ref Assigner::plus_assign for adding values
     * (default) or \ref Assigner::assign for overwriting (setting) values. Different
     * insertion modes can not be mixed! The \p localVector is assumed to be a continuous
     * memory container with a `data()` method to get a pointer to the beginning.
     *
     * The \p mask models a boolean range with at least a `begin()` method. Must
     * be forward iterable for at least `localVector.size()` elements. Does not
     * need an `end()` method. See, e.g. \ref FakeContainer.
     *
     * Insertion must be closed with a call to \ref finish(). It is not allowed
     * to switch insertion mode before calling `finish()`.
     *
     * [[expects: localView is bound to an element]]
     * [[expects: node is in localView.tree()]]
     * [[not collective]]
     */
    template <class LocalView, class Node, class NodeVector, class MaskRange, class Assign,
      REQUIRES(Concepts::LocalView<LocalView>),
      REQUIRES(Concepts::BasisNode<Node>)>
    void scatter(LocalView const& localView, Node const& node, NodeVector const& localVector,
                 MaskRange const& mask, Assign assign)
    {
      test_exit(state_ == VectorStateOf_t<Assign>::value ||
                state_ == VectorState::unknown ||
                state_ == VectorState::synchronized,
        "Vector in invalid state {} for insertion by {}.", to_string(state_), Dune::className<Assign>());

      assert(localVector.size() == node.size());

      // create a range of DOF indices on the node
      impl_.scatter(nodeIndices(localView, node), localVector, mask, assign);

      // set the state to insert_values or add_values
      state_ = VectorStateOf_t<Assign>::value;
    }

    /// Call \ref scatter with `MaskRange` set to \ref FakeContainer.
    // [[expects: localView is bound to an element]]
    // [[expects: node is in localView.tree()]]
    template <class LocalView, class Node, class NodeVector, class Assign,
      REQUIRES(Concepts::LocalView<LocalView>),
      REQUIRES(Concepts::BasisNode<Node>)>
    void scatter(LocalView const& localView, Node const& node, NodeVector const& localVector, Assign assign)
    {
      scatter(localView, node, localVector, FakeContainer<bool,true>{}, assign);
    }

    /// Call \ref scatter with `Node` given by the tree of the \p localView.
    // [[expects: localView is bound to an element]]
    template <class LocalView, class LocalVector, class Assign,
      REQUIRES(Concepts::LocalView<LocalView>)>
    void scatter(LocalView const& localView, LocalVector const& localVector, Assign assign)
    {
      scatter(localView, localView.tree(), localVector, assign);
    }

    /// Apply \p func to each value at given indices \p localInd
    /**
     * First, synchronizes the values of the vector, then applies the functor to
     * each local value associated to the local indices \p localInd.
     *
     * [[mutable]]
     **/
    template <class LocalInd, class Func>
    void forEach(LocalInd const& localInd, Func&& func)
    {
      synchronize();
      impl_.forEach(localInd, FWD(func));
    }

    /// Apply \p func to each value at given indices \p localInd
    /**
     * First, synchronizes the values of the vector, then applies the functor to
     * each local value associated to the local indices \p localInd.
     *
     * [[const]]
     **/
    template <class LocalInd, class Func>
    void forEach(LocalInd const& localInd, Func&& func) const
    {
      const_cast<Self*>(this)->synchronize();
      impl_.forEach(localInd, FWD(func));
    }

  private:
    // synchronizes ghost values. Does not touch owned values
    void synchronize()
    {
      if (state_ != VectorState::synchronized)
        impl_.synchronize();

      state_ = VectorState::synchronized;
    }

    // print the vector state to string for debugging purposes
    static std::string to_string(VectorState state)
    {
      switch (state) {
        case VectorState::synchronized: return "synchronized";
        case VectorState::insert_values: return "insert_values";
        case VectorState::add_values: return "add_values";
        default: return "unknown";
      }
    }

  private:
    /// Data backend
    Impl impl_;

    /// The current state of the vector, one of {synchronized, insert_values,
    /// add_values, unknown}
    VectorState state_ = VectorState::unknown;
  };


  namespace Recursive
  {
    template <class T, template <class> class Impl>
    struct ForEach<VectorFacade<T,Impl>>
    {
      template <class Vec, class UnaryFunction>
      static void impl (Vec&& vec, UnaryFunction f)
      {
        // NOTE: maybe needs synchronization
        Recursive::forEach(vec.impl(), f);
      }
    };

    template <class T, template <class> class Impl>
    struct Transform<VectorFacade<T,Impl>>
    {
      template <class Operation, class... Ts>
      static void impl (VectorFacade<T,Impl>& vecOut, Operation op, VectorFacade<Ts,Impl> const&... vecIn)
      {
        // NOTE: maybe needs synchronization
        Recursive::transform(vecOut.impl(), op, vecIn.impl()...);
      }
    };

    template <class S, template <class> class Impl>
    struct InnerProduct<VectorFacade<S,Impl>>
    {
      template <class In1, class In2, class T, class BinOp1, class BinOp2>
      static T impl (In1 const& in1, In2 const& in2, T init, BinOp1 op1, BinOp2 op2)
      {
        // NOTE: maybe needs synchronization
        return Recursive::innerProduct(in1.impl(), in2.impl(), std::move(init), op1, op2);
      }
    };

  } // end namespace Recursive


  namespace Impl
  {
    template <class T, class Facade>
    struct VectorTypeImpl;

    template <class T, class S, template <class> class Impl>
    struct VectorTypeImpl<T,VectorFacade<S,Impl>>
    {
      using type = VectorFacade<T,Impl>;
    };

  } // end namespace Impl

  /// Type transformation changing the value type of the vector
  template <class T, class Facade>
  using VectorType_t = typename Impl::VectorTypeImpl<T,Facade>::type;

} // end namespace AMDiS
