#include <amdis/AMDiS.hpp>
#include <amdis/AdaptiveGrid.hpp>
#include <amdis/AdaptStationary.hpp>
#include <amdis/LocalOperators.hpp>
#include <amdis/ProblemStat.hpp>
#include <amdis/common/QuadMath.hpp>
#include <amdis/functions/GlobalBasis.hpp>

#include <dune/common/version.hh>
#include <dune/grid/yaspgrid.hh>

using namespace AMDiS;

template <class T>
void test()
{
  // use T as coordinate type
  using HostGrid = Dune::YaspGrid<2, Dune::EquidistantCoordinates<T,2>>;
  HostGrid hostGrid({T(1), T(1)}, {8,8});
  AdaptiveGrid<HostGrid> grid(hostGrid);

  // use T as range type for basis
  using namespace Dune::Functions::BasisFactory;
  GlobalBasis basis{grid.leafGridView(), power<1>(lagrange<1,T>(), flatLexicographic())};
  using Basis = decltype(basis);

  // use T as coefficient type
  using Param = DefaultProblemTraits<Basis, T>;

  msg("Test<{}>", __PRETTY_FUNCTION__);
  ProblemStat<Param> prob("ellipt", grid, basis);
  prob.initialize(INIT_ALL);
  prob.boundaryManager()->setBoxBoundary({1,1,2,2});

  prob.addMatrixOperator(sot(T(1)), 0, 0);
  prob.addVectorOperator(zot(T(1)), 0);
  prob.addDirichletBC(BoundaryType{1}, 0,0, T(0));


  AdaptInfo adaptInfo("adapt");

  msg("  test copy constructor of problem");
  auto prob2(prob);
  prob2.assemble(adaptInfo);

  msg("  test move constructor of problem");
  auto prob3(std::move(prob2));
  prob3.assemble(adaptInfo);
}

int main(int argc, char** argv)
{
  Environment env(argc, argv);

#if !AMDIS_HAS_PETSC || PETSC_USE_REAL_SINGLE
  test<float>();
#endif

#if !AMDIS_HAS_PETSC || PETSC_USE_REAL_DOUBLE
  test<double>();
#endif

#if !AMDIS_HAS_PETSC
  test<long double>();
#endif

#if HAVE_QUADMATH && (!AMDIS_HAS_PETSC || PETSC_USE_REAL___FLOAT128) && (!AMDIS_HAS_EIGEN)
  test<Dune::Float128>();
#endif

  return 0;
}
