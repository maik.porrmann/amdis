#pragma once

#include <type_traits>

#include <amdis/localoperators/FirstOrderTestvecGradTrial.hpp>

namespace AMDiS
{
  /**
   * \addtogroup operators
   * @{
   **/

  namespace tag
  {
    struct gradtest_trialvec {};
  }


  /// first-order operator \f$ \langle\nabla\psi, c\,\Phi\rangle \f$
  template <class LC>
  struct GridFunctionOperatorRegistry<tag::gradtest_trialvec, LC>
  {
    static constexpr int degree = 1;
    static tag::testvec_gradtrial transposedTag(tag::gradtest_trialvec) { return {}; }
    using type = FirstOrderTestvecGradTrial;
  };

  /** @} **/

} // end namespace AMDiS
