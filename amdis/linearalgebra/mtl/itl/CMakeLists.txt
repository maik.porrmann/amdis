install(FILES
  details.hpp
  fgmres_householder.hpp
  fgmres.hpp
  gcr.hpp
  gmres2.hpp
  gmres_householder.hpp
  hypre.hpp
  masslumping.hpp
  minres.hpp
  preonly.hpp
DESTINATION ${CMAKE_INSTALL_INCLUDEDIR}/amdis/linearalgebra/mtl/itl)
