#pragma once

namespace itl
{
  namespace details
  {
    /// Compute the Givens rotation matrix parameters for a and b.
    //     template<typename T>
    //     void rotmat(const T& a, const T& b , T& c, T& s)
    //     {
    //       using std::abs; using std::sqrt; using mtl::conj;
    //
    //       const T zero = math::zero(T());
    //       if (a == zero) {
    // 	        c = 0.0;
    // 	        s = 1.0;
    //       } else {
    // 	        double temp = abs(a) / sqrt( conj(a)*a + conj(b)*b );
    // 	        c = temp;
    // 	        s = temp * (b / a);
    //       }
    //     }

    template <class T>
    inline void rotmat(const T& a, const T& b , T& c, T& s)
    {
      using std::abs;
      using std::sqrt;
      if ( b == 0.0 )
      {
        c = 1.0;
        s = 0.0;
      }
      else if ( abs(b) > abs(a) )
      {
        T temp = a / b;
        s = 1.0 / sqrt( 1.0 + temp*temp );
        c = temp * s;
      }
      else
      {
        T temp = b / a;
        c = 1.0 / sqrt( 1.0 + temp*temp );
        s = temp * c;
      }
    }

  } // end namespace details
} // end namespace itl
