#pragma once

#if AMDIS_HAS_MTL
#include <amdis/linearalgebra/mtl/Traits.hpp>

#elif AMDIS_HAS_EIGEN
#include <amdis/linearalgebra/eigen/Traits.hpp>

#elif AMDIS_HAS_PETSC
#include <amdis/linearalgebra/petsc/Traits.hpp>

#else // ISTL
#include <amdis/linearalgebra/istl/Traits.hpp>
#endif

namespace AMDiS
{
#ifdef DOXYGEN
  /**
   *  Base traits class for a linear solver for the system AX=B using an FE space described by a
   *  dune-functions Basis. This defines the general interface typedefs, all implementations are
   *  required to provide the typedefs listed here.
   */
  class BackendTraits
  {
  public:
    template <class RowBasis, class ColBasis = RowBasis>
    struct Matrix
    {
      using SparsityPattern = implementation_defined; //< The SparsityPattern for the matrix type

      template <class ValueType>
      using Impl = implementation_defined;   //< The backend matrix type
    };

    template <class Basis>
    struct Vector
    {
      template <class ValueType>
      using Impl = implementation_defined;   //< The backend vector type
    };

    template <class Basis>
    using IndexDist = implementation_defined;  //< The index distribution

    using PartitionSet = Dune::Partitions::All;  //< The dune partition set where to assemble operators
  };
#endif
} // end namespace AMDiS
