#include <dune/common/filledarray.hh>
#include <dune/grid/yaspgrid.hh>
#include <dune/functions/functionspacebases/compositebasis.hh>
#include <dune/functions/functionspacebases/powerbasis.hh>
#include <dune/functions/functionspacebases/lagrangebasis.hh>

#include <amdis/common/Order.hpp>
#include <amdis/functions/Order.hpp>
#include <amdis/typetree/FiniteElementType.hpp>
#include "Tests.hpp"

int main()
{
  using namespace AMDiS;
  using namespace Dune;
  using namespace Dune::Functions;
  using namespace Dune::Functions::BasisBuilder;
  using namespace Dune::Indices;

  // create grid
  FieldVector<double, 2> L; L = 1.0;
  auto s = Dune::filledArray<2>(1);
  YaspGrid<2> grid(L, s);
  auto gridView = grid.leafGridView();

  // create basis
  static const int k = 1;
  auto taylorHoodBasis = makeBasis(
    gridView,
    composite(
      power<2>(lagrange<k+1>()),
      lagrange<k>()
    ));

  auto localView = taylorHoodBasis.localView();

  for (auto const& e : elements(gridView)) {
    localView.bind(e);

    auto node = localView.tree();
    AMDIS_TEST_EQ( order(node), k+1 ); // maximum over all polynomial degrees
    static_assert( std::is_same_v<tag::unknown, FiniteElementType_t<decltype(node)>>, "" );

    auto v_node = TypeTree::child(node, _0);
    AMDIS_TEST_EQ( order(v_node), k+1 );

    auto p_node = TypeTree::child(node, _1);
    AMDIS_TEST_EQ( order(p_node), k );
  }

  return report_errors();
}
