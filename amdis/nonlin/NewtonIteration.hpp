#pragma once

#include <memory>
#include <string>

#include <amdis/AdaptInfo.hpp>
#include <amdis/Flag.hpp>
#include <amdis/Initfile.hpp>
#include <amdis/ProblemIterationInterface.hpp>
#include <amdis/ProblemStatBase.hpp>

namespace AMDiS
{
  template <class ProblemType>
  class NewtonIteration
      : public ProblemIterationInterface
  {
    using Self    = NewtonIteration;
    using Problem = ProblemType;

    using SolutionVector = typename Problem::SolutionVector;

  public:
    /// Constructs a nonlinear iteration scheme
    NewtonIteration (std::string name, Problem& prob)
      : name_(std::move(name))
      , prob_(&prob)
    {
      stepSolution_.reset(new SolutionVector(*prob_->solutionVector()));

      Parameters::get(name_ + "->build cycle", buildCycle_);
      Parameters::get(name_ + "->norm", norm_);
    }

    /// Implementation of \ref ProblemIterationInterface::beginIteration
    void beginIteration(AdaptInfo& /*adaptInfo*/) override { /* do nothing */ }

    /// Implementation of \ref ProblemIterationInterface::oneIteration
    Flag oneIteration(AdaptInfo& adaptInfo, Flag toDo) override;

    /// Implementation of \ref ProblemIterationInterface::endIteration
    void endIteration(AdaptInfo& adaptInfo) override
    {
      msg("{:>5} | {:>12} | {:>8} | {:>8} ", "iter.", "error", "red.", "tol");
      if (errOld_ <= 0)
        msg("{:5d} | {:12.5e} | {:->8} | {:8.2} ",
          adaptInfo.spaceIteration()+1, err_, "-",adaptInfo.spaceTolerance(""));
      else
        msg("{:5d} | {:12.5e} | {:8.2e} | {:8.2} ",
           adaptInfo.spaceIteration()+1, err_, err_/errOld_,adaptInfo.spaceTolerance(""));

      errOld_ = err_;
    }

    /// Returns \ref problemStat.
    ProblemStatBase& problem([[maybe_unused]] int n = 0) override
    {
      assert(n == 0); return *prob_;
    }

    /// Implementation of \ref ProblemIterationInterface::numProblems
    int numProblems() const override { return 1; }

    /// Returns the problem with the given name.
    ProblemStatBase& problem([[maybe_unused]] std::string const& name) override
    {
      assert(prob_->name() == name);
      return *prob_;
    }

    /// Returns the name of the problem.
    std::string const& name() const override { return name_; }


    /// Returns const-ref of \ref stepSolution_.
    std::shared_ptr<SolutionVector const> stepSolutionVector() const
    {
      return stepSolution_;
    }

    /// Return a const view to a stepSolution component
    /**
     * \tparam Range  The range type return by evaluating the view in coordinates. If not specified,
     *                it is automatically selected using \ref RangeType_t template.
     **/
    template <class Range = void, class... Indices>
    auto stepSolution(Indices... ii) const
    {
      return valueOf<Range>(*stepSolution_, ii...);
    }

  protected:
    /// Name of the newton problem, used to access initfile parameters
    std::string name_;

    /// Problem containing the Jacobian operators and objective function
    Problem* prob_;

    /// Solution of the update step
    std::shared_ptr<SolutionVector> stepSolution_;

    /// Build matrix every i'th iteration
    /**
     * 0 ........ build matrix only once
     * i >= 1 ... rebuild matrix in i'th solver iteration
     * [default: 1]
     **/
    int buildCycle_ = 1;

    /// Type of norm to use for estimating the error
    /**
     * 1... L2-norm
     * 2... H1-norm
     * [default: 1]
     **/
    int norm_ = 1;

  private:
    double err_ = 0.0;
    double errOld_ = -1.0;
  };

} // end namespace AMDiS

#include <amdis/nonlin/NewtonIteration.inc.hpp>
