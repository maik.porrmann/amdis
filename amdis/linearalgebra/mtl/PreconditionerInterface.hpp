#pragma once

#include <amdis/Output.hpp>

namespace AMDiS
{
  /// Interface for Preconditioner y = M*x
  template <class M, class X, class Y>
  class PreconditionerInterface
  {
  public:
    /// Virtual destructor.
    virtual ~PreconditionerInterface() = default;

    /// Is called a the beginning of a solution procedure
    virtual void init(M const& A) = 0;

    /// Is called at the end of a solution procedure
    virtual void finish() = 0;

    /// Apply the preconditioner to a vector \p x and store the result in \p y
    virtual void solve(X const& x, Y& y) const = 0;

    /// Apply the transposed preconditioner to a vector \p x and store the result in \p y
    virtual void adjoint_solve(X const& x, Y& y) const
    {
      error_exit("Must be implemented by derived class.");
    }
  };

} // end namespace AMDiS
