#pragma once

#include <amdis/GridFunctionOperator.hpp>

/*
 * In the following comments we use the notation
 * psi ... scalar testfunction
 * Psi ... vector testfunction
 * phi ... scalar trialfunction
 * Phi ... vector trialfunction
 * A ..... matrix coefficient
 * b ..... vector coefficient
 * c ..... scalar coefficient
 */


/**
  * \defgroup operators Operator module
  * \brief Defines operators to be assembled in the matrix/vector
  *
  * An `Operator` is a class providing methods necessary for assembling:
  * - `bind(Element, Geometry)` and `unbind()` for binding an unbinding the
  *   element to (from) an GridView entity of codim 0. Additionally the Geometry
  *   object of the element is provided.
  * - `getQuadratureRule(Nodes...)` factory for the
  *   quadrature rules used in assembling the operator on the element. `Nodes...`
  *   is either `{RowNode, ColNode}` for Matrix-Operators or `{Node}` for a
  *   Vector-Operator.
  * - `calculateElementVector(ContextGeometry, QuadratureRule, ElementVector, Node)`
  *   where the `ContextGeometry` provides a reference to the ElementGeometry and
  *   geometry of the LocalContext (that can be different), *or*
  * - `calculateElementMatrix(ContextGeometry, QuadratureRule, ElementMatrix, RowNode, ColNode, Flags...)`
  *   Same as for `calculateElementVector` but additionally two optimization flags
  *   are provided as `bool_t<...>` type:
  *   + `sameFE`: the FiniteElementSpace of `RowNode` and `ColNode` are the same.
  *   + `sameNode`: the nodes are the same in the GlobalBasis-tree.
  **/

// zero-order operators
#include <amdis/localoperators/ZeroOrderTest.hpp>             // <psi * c>
#include <amdis/localoperators/ZeroOrderTestTrial.hpp>        // <psi, c * phi>
#include <amdis/localoperators/ZeroOrderTestTrialvec.hpp>     // <psi, b * Phi>
#include <amdis/localoperators/ZeroOrderTestvec.hpp>          // <Psi * b>
#include <amdis/localoperators/ZeroOrderTestvecTrial.hpp>     // <Psi, b * phi>
#include <amdis/localoperators/ZeroOrderTestvecTrialvec.hpp>  // <Psi, A * Phi>, <Psi, c * Phi>

// first-order operators
#include <amdis/localoperators/FirstOrderPartialTest.hpp>      // <d_i(psi), c>
#include <amdis/localoperators/FirstOrderGradTest.hpp>         // <grad(psi), b>
#include <amdis/localoperators/FirstOrderDivTestvec.hpp>       // <div(Psi), c>

#include <amdis/localoperators/FirstOrderDivTestvecTrial.hpp>   // <div(Psi), c * phi>
#include <amdis/localoperators/FirstOrderGradTestTrial.hpp>     // <grad(psi), b * phi>
#include <amdis/localoperators/FirstOrderGradTestTrialvec.hpp>  // <grad(psi), c * Phi>
#include <amdis/localoperators/FirstOrderPartialTestTrial.hpp>  // <d_i(psi), c * psi>
#include <amdis/localoperators/FirstOrderTestDivTrialvec.hpp>   // <psi, c * div(Phi)>
#include <amdis/localoperators/FirstOrderTestGradTrial.hpp>     // <psi, b * grad(phi)>
#include <amdis/localoperators/FirstOrderTestPartialTrial.hpp>  // <psi, c * d_i(phi)>
#include <amdis/localoperators/FirstOrderTestvecGradTrial.hpp>  // <Psi, c * grad(phi)>

// second-order operators
#include <amdis/localoperators/SecondOrderDivTestvecDivTrialvec.hpp>    // <div(Psi), c * div(Phi)>
#include <amdis/localoperators/SecondOrderGradTestGradTrial.hpp>        // <grad(psi), A * grad(phi)>
#include <amdis/localoperators/SecondOrderPartialTestPartialTrial.hpp>  // <d_i(psi), c * d_j(phi)>
