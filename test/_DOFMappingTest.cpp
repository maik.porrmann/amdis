#include <amdis/AMDiS.hpp>

#include <cassert>
#include <iostream>
#include <sstream>

#include <dune/common/parallel/mpihelper.hh>
#include <dune/common/parallel/indexset.hh>
#include <dune/common/parallel/plocalindex.hh>
#include <dune/common/parallel/remoteindices.hh>

#include <amdis/linearalgebra/DOFMapping.hpp>

#if AMDIS_HAS_PETSC
#include <petscvec.h>
#endif

#if HAVE_PMTL
#include <boost/numeric/mtl/mtl.hpp>
#endif

namespace Attribute {
  enum Type { owner = 1, overlap = 2, copy = 3 };
}

struct GlobalIndex : std::pair<int,int>
{
  GlobalIndex(int i = 0, int j = 0) : std::pair<int,int>(i,j) {}
};

struct LexicographicOrdering
{
  LexicographicOrdering(int Nx, int Ny, int shiftx, int shifty)
    : Nx_(Nx)
    , Ny_(Ny)
    , shiftx_(shiftx)
    , shifty_(shifty)
  {}

  int operator()(int i, int j) const
  {
    return Nx_*(i - shifty_) + (j - shiftx_);
  }

  int Nx_,Ny_,shiftx_,shifty_;
};


std::ostream& operator<<(std::ostream& out, GlobalIndex const& p)
{
  out << "(" << p.first << ", " << p.second << ")";
  return out;
}

template <class PIS>
struct IndexDistribution
{
  using ParallelIndexSet = PIS;
  using RemoteIndices = Dune::RemoteIndices<PIS>;

  IndexDistribution()
    : pis_()
    , rIndices_(pis_, pis_, Dune::MPIHelper::getCommunicator())
  {}

  ParallelIndexSet& indexSet() { return pis_; }
  ParallelIndexSet const& indexSet() const { return pis_; }

  RemoteIndices& remoteIndices() { return rIndices_; }
  RemoteIndices const& remoteIndices() const { return rIndices_; }

  auto communicator() const { return Dune::MPIHelper::getCommunicator(); }

private:
  ParallelIndexSet pis_;
  RemoteIndices rIndices_;
};

int main(int argc, char** argv)
{
  AMDiS::Environment env(argc, argv);
#if HAVE_MPI
  MPI_Comm comm = MPI_COMM_WORLD;
#else
  auto comm = Dune::No_Comm{};
#endif

  using GI = GlobalIndex;
  using LI = Dune::ParallelLocalIndex<Attribute::Type>;
  using PIS = Dune::ParallelIndexSet<GI,LI>;

  int r = env.mpiRank();
  [[maybe_unused]] int s = env.mpiSize();
  assert( s == 2 );

  int N = 3;
  int M = 2;

  auto index = LexicographicOrdering{N+1, M+1, 0, r};

  IndexDistribution<PIS> c;

  PIS& pis = c.indexSet();
  pis.beginResize();
  int i0 = r;
  int i1 = i0 + M + 1;
  for (int i = i0; i < i1; ++i) {
    for (int j = 0; j < N+1; ++j) {
      bool overlap_region = (i >= 1 && i <= 2);
      pis.add(GlobalIndex{i,j}, LI(index(i,j), !overlap_region || r == 0 ? Attribute::owner : Attribute::overlap, true));
    }
  }
  pis.endResize();

  auto& remoteIndices = c.remoteIndices();
  remoteIndices.template rebuild<true>();

  using Comm = Dune::Communication<typename Dune::MPIHelper::MPICommunicator>;
  AMDiS::DofMapping_t<PIS,std::size_t,Comm> mapping;
  mapping.update(c, comm);
  for (int i = i0; i < i1; ++i) {
    for (int j = 0; j < N+1; ++j) {
      std::cout << "[" << r << "]    (" << i << "," << j << ") = " << index(i,j) << " => " << mapping.global(index(i,j)) << "\n";
    }
  }

#if AMDIS_HAS_PETSC
  Vec v;
  VecCreateMPI(comm, mapping.localSize(), mapping.globalSize(), &v);
  VecSet(v,0.0);

  for (int i = i0; i < i1; ++i) {
    for (int j = 0; j < N+1; ++j) {
      PetscInt row = mapping.global(index(i,j));
      VecSetValue(v, row, PetscScalar(index(i,j)), ADD_VALUES);
    }
  }
  VecAssemblyBegin(v);
  VecAssemblyEnd(v);

  VecView(v,PETSC_VIEWER_STDOUT_WORLD);
  VecDestroy(&v);
#endif

#if HAVE_PMTL
  mtl::par::block_distribution dist(mapping.globalStarts(), c.communicator());

  using vector_type = mtl::vector::distributed<mtl::dense_vector<double> >;
  vector_type vec(mapping.globalSize(), dist);

  { mtl::vector::inserter<vector_type, mtl::update_plus<double>> ins(vec);

    for (int i = i0; i < i1; ++i) {
      for (int j = 0; j < N+1; ++j) {
        PetscInt row = mapping.global(index(i,j));
        ins[row] << index(i,j);
      }
    }
  }
  mtl::par::rout << "My local part of v is " << local(vec) << '\n';
#endif
}
