#pragma once

#include <amdis/linearalgebra/mtl/PreconditionerInterface.hpp>

namespace AMDiS
{
  /**
   * \ingroup Solver
   *
   * \brief Wrapper for using ITL preconditioners in AMDiS.
   */
  template <class M, class X, class Y, template <class> class PreconImpl>
  class Preconditioner
      : public PreconditionerInterface<M,X,Y>
  {
    using Self = Preconditioner;
    using Super = PreconditionerInterface<M,X,Y>;
    using Precon = PreconImpl<M>;

  public:
    /// A creator to be used instead of the constructor.
    struct Creator : CreatorInterfaceName<Super>
    {
      std::unique_ptr<Super> createWithString(std::string) override
      {
        return std::make_unique<Self>();
      }
    };

  public:
    /// Implementation of \ref PreconditionerInterface::init()
    void init(M const& A) override
    {
      precon_ = std::make_shared<Precon>(A);
    }

    /// Implementation of \ref PreconditionerInterface::finish()
    void finish() override
    {
      precon_.reset();
    }

    /// Implementation of \ref PreconditionerInterface::solve()
    void solve(X const& x, Y& y) const override
    {
      test_exit_dbg(bool(precon_), "No preconditioner initialized!");
      precon_->solve(x, y);
    }

    /// Implementation of \ref PreconditionerInterface::adjointSolve()
    void adjoint_solve(X const& x, Y& y) const override
    {
      test_exit_dbg(bool(precon_), "No preconditioner initialized!");
      precon_->adjoint_solve(x, y);
    }

  private:
    std::shared_ptr<Precon> precon_;
  };

} // namespace AMDiS
