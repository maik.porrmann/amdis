#pragma once

#include <type_traits>

#include <dune/functions/functionspacebases/subentitydofs.hh>
#include <dune/functions/functionspacebases/subspacebasis.hh>

#include <amdis/LinearAlgebra.hpp>
#include <amdis/functions/EntitySet.hpp>
#include <amdis/interpolators/SimpleInterpolator.hpp>
#include <amdis/typetree/TreePath.hpp>

namespace AMDiS {

template <class B, class Row, class Col, class V>
void DirichletBC<B, Row, Col, V>::
init()
{
  auto subBasis = Dune::Functions::subspaceBasis(*basis_, col_);
  dirichletNodes_.resize(subBasis.dimension());
  dirichletNodes_.assign(subBasis.dimension(), false);

  auto localView = subBasis.localView();
  auto seDOFs = Dune::Functions::subEntityDOFs(subBasis);
  auto const& gridView = basis_->gridView();
  for (auto const& element : entitySet(*basis_)) {
    if (element.hasBoundaryIntersections()) {
      localView.bind(element);
      for(auto const& intersection: intersections(gridView, element))
        if (intersection.boundary())
          for(auto localIndex: seDOFs.bind(localView,intersection))
            dirichletNodes_[localView.index(localIndex)]
              = dirichletNodes_[localView.index(localIndex)] || boundarySubset_(intersection);
    }
  }

}


template <class B, class Row, class Col, class V>
  template <class Mat, class Sol, class Rhs>
void DirichletBC<B, Row, Col, V>::
apply(Mat& matrix, Sol& solution, Rhs& rhs, bool setDiagonal)
{
  assert(to_string(row_) == to_string(col_));
  auto interpolator = InterpolatorFactory<tag::assign>::create(*basis_, col_);
  if (setDiagonal) {
    Sol boundarySolution{solution};
    interpolator(boundarySolution, valueGridFct_, dirichletNodes_);
    dirichletBC(matrix, boundarySolution, rhs, dirichletNodes_);
    solution = std::move(boundarySolution);
  } else {
    dirichletBC(matrix, solution, rhs, dirichletNodes_, false);
  }
}

} // end namespace AMDiS
