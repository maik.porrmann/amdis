#pragma once

#include <amdis/Operations.hpp>
#include <amdis/gridfunctions/ComposerGridFunction.hpp>
#include <amdis/operations/Arithmetic.hpp>
#include <amdis/operations/FieldMatVec.hpp>

namespace AMDiS
{
  /**
    * \addtogroup GridFunctions
    * @{
    **/

  // scalar operations
  // @{

  /// \brief Applies \ref Operation::Negate to GridFunctions. \relates ComposerGridFunction
  template <class Lhs,
    REQUIRES(Concepts::AnyGridFunction<Lhs>)>
  auto operator-(Lhs&& lhs)
  {
    return invokeAtQP(Operation::Negate{}, FWD(lhs));
  }

  /// \brief Applies \ref Operation::Plus to GridFunctions. \relates ComposerGridFunction
  template <class Lhs, class Rhs,
    REQUIRES(Concepts::AnyGridFunction<Lhs,Rhs>)>
  auto operator+(Lhs&& lhs, Rhs&& rhs)
  {
    return invokeAtQP(Operation::Plus{}, FWD(lhs), FWD(rhs));
  }

  /// \brief Applies \ref Operation::Minus to GridFunctions. \relates ComposerGridFunction
  template <class Lhs, class Rhs,
    REQUIRES(Concepts::AnyGridFunction<Lhs,Rhs>)>
  auto operator-(Lhs&& lhs, Rhs&& rhs)
  {
    return invokeAtQP(Operation::Minus{}, FWD(lhs), FWD(rhs));
  }

  /// \brief Applies \ref Operation::Multiplies to GridFunctions. \relates ComposerGridFunction
  template <class Lhs, class Rhs,
    REQUIRES(Concepts::AnyGridFunction<Lhs,Rhs>)>
  auto operator*(Lhs&& lhs, Rhs&& rhs)
  {
    if constexpr(Dune::IsNumber<TYPEOF(lhs)>::value)
      return invokeAtQP(Operation::Scale<TYPEOF(lhs)>{lhs}, FWD(rhs));
    else if constexpr(Dune::IsNumber<TYPEOF(rhs)>::value)
      return invokeAtQP(Operation::Scale<TYPEOF(rhs)>{rhs}, FWD(lhs));
    else
      return invokeAtQP(Operation::Multiplies{}, FWD(lhs), FWD(rhs));
  }

  /// \brief Applies \ref Operation::Divides to GridFunctions. \relates ComposerGridFunction
  template <class Lhs, class Rhs,
    REQUIRES(Concepts::AnyGridFunction<Lhs,Rhs>)>
  auto operator/(Lhs&& lhs, Rhs&& rhs)
  {
    if constexpr(Dune::IsNumber<TYPEOF(rhs)>::value)
      return invokeAtQP(Operation::Scale<TYPEOF(1.0/rhs)>{1.0/rhs}, FWD(lhs));
    else
      return invokeAtQP(Operation::Divides{}, FWD(lhs), FWD(rhs));
  }


  /// \brief Applies \ref Operation::Max to GridFunctions. \relates ComposerGridFunction
  template <class Lhs, class Rhs,
    REQUIRES(Concepts::AnyGridFunction<Lhs,Rhs>)>
  auto max(Lhs&& lhs, Rhs&& rhs)
  {
    return invokeAtQP(Operation::Max{}, FWD(lhs), FWD(rhs));
  }

  /// \brief Applies \ref Operation::Min to GridFunctions. \relates ComposerGridFunction
  template <class Lhs, class Rhs,
    REQUIRES(Concepts::AnyGridFunction<Lhs,Rhs>)>
  auto min(Lhs&& lhs, Rhs&& rhs)
  {
    return invokeAtQP(Operation::Min{}, FWD(lhs), FWD(rhs));
  }

  /// \brief Applies \ref Operation::AbsMax to GridFunctions. \relates ComposerGridFunction
  template <class Lhs, class Rhs,
    REQUIRES(Concepts::AnyGridFunction<Lhs,Rhs>)>
  auto abs_max(Lhs&& lhs, Rhs&& rhs)
  {
    return invokeAtQP(Operation::AbsMax{}, FWD(lhs), FWD(rhs));
  }

  /// \brief Applies \ref Operation::AbsMin to GridFunctions. \relates ComposerGridFunction
  template <class Lhs, class Rhs,
    REQUIRES(Concepts::AnyGridFunction<Lhs,Rhs>)>
  auto abs_min(Lhs&& lhs, Rhs&& rhs)
  {
    return invokeAtQP(Operation::AbsMin{}, FWD(lhs), FWD(rhs));
  }

  /// \brief Applies \ref Operation::Clamp to GridFunction. \relates ComposerGridFunction
  template <class V, class T,
    REQUIRES(Concepts::AnyGridFunction<V>)>
  auto clamp(V&& v, T const& lo, T const& hi)
  {
    return invokeAtQP(Operation::Clamp<T>{lo,hi}, FWD(v));
  }

  template <class T,
    REQUIRES(Concepts::AnyGridFunction<T>)>
  auto abs(T&& value)
  {
    return invokeAtQP([](auto const& v) { using std::abs; return abs(v); }, FWD(value));
  }

  /// \brief Applies \ref Operation::Tanh to GridFunctions. \relates ComposerGridFunction
  template <class T,
    REQUIRES(Concepts::AnyGridFunction<T>)>
  auto tanh(T&& value)
  {
    return invokeAtQP(Operation::Tanh{}, FWD(value));
  }

  /// \brief Applies \ref Operation::Signum to GridFunctions. \relates ComposerGridFunction
  template <class T,
    REQUIRES(Concepts::AnyGridFunction<T>)>
  auto signum(T&& value)
  {
    return invokeAtQP(Operation::Signum{}, FWD(value));
  }

  /// \brief Applies \ref Operation::Sqr to GridFunction. \relates ComposerGridFunction
  template <class T,
    REQUIRES(Concepts::AnyGridFunction<T>)>
  auto sqr(T&& value)
  {
    return invokeAtQP(Operation::Sqr{}, FWD(value));
  }

  /// \brief Applies \ref Operation::Pow<p> to GridFunction. \relates ComposerGridFunction
  template <int p, class T,
    REQUIRES(Concepts::AnyGridFunction<T>)>
  auto pow(T&& value)
  {
    return invokeAtQP(Operation::Pow<p>{}, FWD(value));
  }

  /// \brief Applies \ref Operation::Power to GridFunction. \relates ComposerGridFunction
  template <class T,
    REQUIRES(Concepts::AnyGridFunction<T>)>
  auto pow(T&& value, double p)
  {
    return invokeAtQP(Operation::Power{p}, FWD(value));
  }

  /// \brief Applies \ref Operation::Get<I> to GridFunction. \relates ComposerGridFunction
  template <std::size_t I, class T,
    REQUIRES(Concepts::AnyGridFunction<T>)>
  auto get(T&& value)
  {
    return invokeAtQP(Operation::Get<I>{}, FWD(value));
  }

  /// \brief Applies \ref Operation::Get_ to GridFunction. \relates ComposerGridFunction
  template <class T,
    REQUIRES(Concepts::AnyGridFunction<T>)>
  auto get(T&& value, std::size_t i)
  {
    return invokeAtQP(Operation::Get_{i}, FWD(value));
  }

  /// \brief Return a reference-wrapper as GridFunction. \relates ComposerGridFunction
  template <class T>
  auto constant(T const& value)
  {
    return invokeAtQP(Operation::Constant{value});
  }

  /// \brief Return a reference-wrapper as GridFunction. \relates ComposerGridFunction
  template <class T>
  auto reference(T& value)
  {
    return invokeAtQP(Operation::Reference{value});
  }

  // @}

  // unary vector operations
  // @{

  /// \brief Applies a \ref sum() functor to a vector-valued GridFunction. \relates ComposerGridFunction
  template <class Vec,
    REQUIRES(Concepts::AnyGridFunction<Vec>)>
  auto sum(Vec&& vec)
  {
    return invokeAtQP([](auto const& v) { return sum(v); }, FWD(vec));
  }

  /// \brief Applies \ref Operation::UnaryDot to a vector-valued GridFunction. \relates ComposerGridFunction
  template <class Vec,
    REQUIRES(Concepts::AnyGridFunction<Vec>)>
  auto unary_dot(Vec&& vec)
  {
    return invokeAtQP(Operation::UnaryDot{}, FWD(vec));
  }

  /// \brief Applies a \ref one_norm() functor to a vector-valued GridFunction. \relates ComposerGridFunction
  template <class Vec,
    REQUIRES(Concepts::AnyGridFunction<Vec>)>
  auto one_norm(Vec&& vec)
  {
    return invokeAtQP([](auto const& v) { return one_norm(v); }, FWD(vec));
  }

  /// \brief Applies \ref Operation::TwoNorm to a vector-valued GridFunction. \relates ComposerGridFunction
  template <class Vec,
    REQUIRES(Concepts::AnyGridFunction<Vec>)>
  auto two_norm(Vec&& vec)
  {
    return invokeAtQP(Operation::TwoNorm{}, FWD(vec));
  }

  /// \brief Applies a \ref p_norm() functor to a vector-valued GridFunction. \relates ComposerGridFunction
  template <int p, class Vec,
    REQUIRES(Concepts::AnyGridFunction<Vec>)>
  auto p_norm(Vec&& vec)
  {
    return invokeAtQP([](auto const& v) { return p_norm<p>(v); }, FWD(vec));
  }

  /// \brief Applies a \ref infty_norm() functor to a vector-valued GridFunction. \relates ComposerGridFunction
  template <class Vec,
    REQUIRES(Concepts::AnyGridFunction<Vec>)>
  auto infty_norm(Vec&& vec)
  {
    return invokeAtQP([](auto const& v) { return infty_norm(v); }, FWD(vec));
  }

  /// \brief Applies \ref Operation::Trans to a matrix-valued GridFunction.\relates ComposerGridFunction
  template <class Mat,
    REQUIRES(Concepts::AnyGridFunction<Mat>)>
  auto trans(Mat&& mat)
  {
    return invokeAtQP(Operation::Trans{}, FWD(mat));
  }

  // @}


  // binary vector operations
  // @{

  /// \brief Applies \ref Operation::Dot to two vector-valued GridFunctions. \relates ComposerGridFunction
  template <class Lhs, class Rhs,
    REQUIRES(Concepts::AnyGridFunction<Lhs,Rhs>)>
  auto dot(Lhs&& lhs, Rhs&& rhs)
  {
    return invokeAtQP(Operation::Dot{},
      FWD(lhs), FWD(rhs));
  }

  /// \brief Applies a distance-functor to two vector-valued GridFunctions.\relates ComposerGridFunction
  template <class Lhs, class Rhs,
    REQUIRES(Concepts::AnyGridFunction<Lhs,Rhs>)>
  auto distance(Lhs&& lhs, Rhs&& rhs)
  {
    using namespace Operation;
    return invokeAtQP(compose(TwoNorm{}, Minus{}),
      FWD(lhs), FWD(rhs));
  }

  /// \brief Applies an \ref outer() functor to two vector-valued GridFunctions. \relates ComposerGridFunction
  template <class Lhs, class Rhs,
    REQUIRES(Concepts::AnyGridFunction<Lhs,Rhs>)>
  auto outer(Lhs&& lhs, Rhs&& rhs)
  {
    return invokeAtQP([](auto const& v, auto const& w) { return outer(v,w); },
      FWD(lhs), FWD(rhs));
  }

  // @}

  /** @} **/

} // end namespace AMDiS
