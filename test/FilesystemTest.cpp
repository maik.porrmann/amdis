#include <amdis/common/Filesystem.hpp>

#include "Tests.hpp"


using namespace AMDiS;

// ---------------------------------------------------------------------------------------
void test0()
{
  using namespace AMDiS::filesystem;
  AMDIS_TEST_EQ( path("a/b/c").parent_path(), path("a/b") );
  AMDIS_TEST_EQ( path("a/b/c.txt").parent_path(), path("a/b") );
  AMDIS_TEST_EQ( path("a/b/c.txt").filename(), path("c.txt") );
  AMDIS_TEST_EQ( path("a/b/c.txt").stem(), path("c") );

  AMDIS_TEST_EQ( path("a/b/c.txt").extension().string(), ".txt" );
  AMDIS_TEST_EQ( path("a/b/c.").extension().string(), "." );
  AMDIS_TEST_EQ( path(".txt").extension().string(), ".txt" );

  AMDIS_TEST( !path("a/b/c").is_absolute() );
  AMDIS_TEST( !path("a\\b\\c").is_absolute() );
#ifdef _WIN32
  AMDIS_TEST( path("a:\\b\\c").is_absolute() );
  AMDIS_TEST( !path("a:\\b\\c").is_relative() );
#else
  AMDIS_TEST( path("/a/b/c").is_absolute() );
  AMDIS_TEST( !path("/a/b/c").is_relative() );
  AMDIS_TEST( path("/").is_absolute() );
#endif

  AMDIS_TEST( path("a/b/c").is_relative() );

  AMDIS_TEST_EQ( path("a/b/c.txt").remove_filename(), path("a/b") );
  AMDIS_TEST_EQ( path("a/b/c").string(), "a/b/c" );

  AMDIS_TEST_EQ( path("a/b/../c").string(), "a/c" );
  AMDIS_TEST_EQ( path("a/b/./c").string(), "a/b/c" );
  AMDIS_TEST_EQ( path("../a/b/c").string(), "../a/b/c" );
  AMDIS_TEST_EQ( path("./a/b/c").string(), "a/b/c" );
  AMDIS_TEST_EQ( path("a/b") /= path("c"), path("a/b/c") );
}

void test1()
{
  using namespace AMDiS::filesystem;
#ifdef _WIN32

#else
  AMDIS_TEST( path("/tmp").is_directory());
  AMDIS_TEST( !path("/tmp").is_file());
  AMDIS_TEST( exists( path("/tmp") ) );

  AMDIS_TEST( path("/etc/fstab").is_file() );
  AMDIS_TEST( !path("/etc/fstab").is_directory() );
  AMDIS_TEST( exists("/etc/fstab") );
#endif

  // AMDIS_TEST(create_directories( path("testdir") ));
}

int main()
{
  test0();
  test1();

  return report_errors();
}
