#pragma once

#if HAVE_MPI
#include <cassert>
#include <functional>
#include <optional>
#include <type_traits>

#include <mpi.h>

#include <amdis/common/Concepts.hpp>

namespace AMDiS { namespace Mpi
{
  class Request
  {
  public:
    Request() = default;

    Request(MPI_Request request)
      : request_(request)
    {}

    // In advanced mode, take a callable that returns an optional<MPI_Status>
    // This functor is called in the test() function and wait() isa implemented in terms of test()
    template <class F,
      REQUIRES( Concepts::Callable<F> ) >
    explicit Request(F&& f)
      : test_(std::forward<F>(f))
      , advanced_(true)
    {}


    // Returns an MPI_Status object if the operation identified by the request is complete.
    // If the request is an active persistent request, it is marked as inactive. Any other type of
    // request is deallocated and the request handle is set to MPI_REQUEST_NULL .
    std::optional<MPI_Status> test()
    {
      if (advanced_)
        return test_();

      auto s = status();
      if (s && request_ != MPI_REQUEST_NULL)
        MPI_Request_free(&request_);
      return s;
    }

    // Access the information associated with a request, without freeing the request.
    std::optional<MPI_Status> status() const
    {
      assert( !advanced_ );

      if (status_initialized_)
        return status_;

      MPI_Status status;
      int flag = 0;
      MPI_Request_get_status(request_, &flag, &status);
      if (flag)
        return status;
      else
        return {};
    }

    // Returns when the operation identified by request is complete.
    MPI_Status wait()
    {
      if (advanced_) {
        std::optional<MPI_Status> status;
        while( !(status = test()) ) ;
        return status.value();
      } else {
        MPI_Wait(&request_, &status_);
        status_initialized_ = true;
        return status_;
      }
    }

    // Returns the underlying MPI_Request handle
    MPI_Request get() const
    {
      assert( !advanced_ );
      return request_;
    }

    MPI_Request& get()
    {
      assert( !advanced_ );
      return request_;
    }

    // Deallocate a request object without waiting for the associated communication to complete.
    void free()
    {
      assert( !advanced_ );
      MPI_Request_free(&request_);
    }

    void cancel()
    {
      assert( !advanced_ );
      MPI_Cancel(&request_);
    }

    void cancel_and_free()
    {
      cancel();
      free();
    }

  protected:

    MPI_Request request_ = MPI_REQUEST_NULL;

    MPI_Status status_; // the MPI_Status after the request object is destroyed
    bool status_initialized_ = false;

    std::function<std::optional<MPI_Status>(void)> test_;
    bool advanced_ = false;
  };

}} // end namespace AMDiS::Mpi

#endif // HAVE_MPI

