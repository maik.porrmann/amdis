#pragma once

#if HAVE_MPI
#include <array>
#include <vector>

#include <mpi.h>

#include <amdis/common/parallel/Request.hpp>
#include <amdis/common/parallel/MpiTraits.hpp>

namespace AMDiS { namespace Mpi
{
  template <class T, class Operation>
  void all_reduce(MPI_Comm comm, T const& in, T& out, Operation)
  {
    MPI_Allreduce(&in, &out, 1, type_to_mpi<T>(), op_to_mpi<Operation>(), comm);
  }

  template <class T, class Operation>
  void all_reduce(MPI_Comm comm, T& inout, Operation)
  {
    MPI_Allreduce(MPI_IN_PLACE, &inout, 1, type_to_mpi<T>(), op_to_mpi<Operation>(), comm);
  }


  template <class T>
  void all_gather(MPI_Comm comm, T const& in, std::vector<T>& out)
  {
    int size = 1;
    MPI_Comm_size(comm, &size);
    out.resize(size);
    MPI_Allgather(to_void_ptr(&in), 1, type_to_mpi<T>(), to_void_ptr(out.data()), 1, type_to_mpi<T>(), comm);
  }

  template <class T, std::size_t N>
  void all_gather(MPI_Comm comm, std::array<T,N> const& in, std::vector<T>& out)
  {
    int size = 1;
    MPI_Comm_size(comm, &size);
    out.resize(size * N);
    MPI_Allgather(to_void_ptr(in.data()), N, type_to_mpi<T>(), to_void_ptr(out.data()), N, type_to_mpi<T>(), comm);
  }


  template <class T>
  std::vector<T> all_gather(MPI_Comm comm, T const& in)
  {
    int size = 1;
    MPI_Comm_size(comm, &size);
    std::vector<T> out(size);
    MPI_Allgather(to_void_ptr(&in), 1, type_to_mpi<T>(), to_void_ptr(out.data()), 1, type_to_mpi<T>(), comm);
    return out;
  }

  template <class T, std::size_t N>
  std::vector<T> all_gather(MPI_Comm comm, std::array<T,N> const& in)
  {
    int size = 1;
    MPI_Comm_size(comm, &size);
    std::vector<T> out(size * N);
    MPI_Allgather(to_void_ptr(in.data()), N, type_to_mpi<T>(), to_void_ptr(out.data()), N, type_to_mpi<T>(), comm);
    return out;
  }


  template <class T>
  Request iall_gather(MPI_Comm comm, T const& in, std::vector<T>& out)
  {
    int size = 1;
    MPI_Comm_size(comm, &size);
    out.resize(size);
    MPI_Request request = MPI_REQUEST_NULL;
    MPI_Iallgather(to_void_ptr(&in), 1, type_to_mpi<T>(), to_void_ptr(out.data()), 1, type_to_mpi<T>(), comm, &request);

    return {request};
  }

  template <class T, std::size_t N>
  Request iall_gather(MPI_Comm comm, std::array<T,N> const& in, std::vector<T>& out)
  {
    int size = 1;
    MPI_Comm_size(comm, &size);
    out.resize(size * N);
    MPI_Request request = MPI_REQUEST_NULL;
    MPI_Iallgather(to_void_ptr(in.data()), N, type_to_mpi<T>(), to_void_ptr(out.data()), N, type_to_mpi<T>(), comm, &request);

    return {request};
  }

}} // end namespace AMDiS::Mpi

#endif // HAVE_MPI
