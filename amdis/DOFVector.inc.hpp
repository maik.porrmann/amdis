#pragma once

#include <cassert>
#include <cstdint>
#include <fstream>
#include <string>
#include <vector>

#include <amdis/operations/Assigner.hpp>

namespace AMDiS {

template <class GB, class T, class Traits>
void DOFVector<GB,T,Traits>::
backup(std::string const& filename)
{
  std::ofstream out(filename, std::ios::binary);

  std::int64_t numElements = this->basis().gridView().size(0);
  out.write((char*)&numElements, sizeof(std::int64_t));

  auto localView = this->basis().localView();
  std::vector<value_type> data;
  for (auto const& element : elements(this->basis().gridView()))
  {
    localView.bind(element);
    this->gather(localView, data);

    std::uint64_t len = data.size();
    out.write((char*)&len, sizeof(std::uint64_t));
    out.write((char*)data.data(), len*sizeof(value_type));

    localView.unbind();
  }
}


template <class GB, class T, class Traits>
void DOFVector<GB,T,Traits>::
restore(std::string const& filename)
{
  std::ifstream in(filename, std::ios::binary);

  std::int64_t numElements = 0;
  in.read((char*)&numElements, sizeof(std::int64_t));
  assert(numElements == this->basis().gridView().size(0));

  // assume the order of element traversal is not changed
  auto localView = this->basis().localView();
  std::vector<value_type> data;
  this->init(sizeInfo(this->basis()), true);
  for (auto const& element : elements(this->basis().gridView()))
  {
    std::uint64_t len = 0;
    in.read((char*)&len, sizeof(std::uint64_t));
    data.resize(len);

    in.read((char*)data.data(), len*sizeof(value_type));

    localView.bind(element);
    this->scatter(localView, data, Assigner::assign{});
    localView.unbind();
  }
  this->finish();
}

} // end namespace AMDiS
