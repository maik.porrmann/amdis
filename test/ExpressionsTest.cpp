// -*- tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 2 -*-
// vi: set et ts=4 sw=2 sts=2:

#include <iostream>

#include <amdis/AMDiS.hpp>
#include <amdis/Integrate.hpp>
#include <amdis/LocalOperators.hpp>
#include <amdis/ProblemStat.hpp>
#include <amdis/common/FieldMatVec.hpp>

#include <dune/grid/yaspgrid.hh>

#include "Tests.hpp"

using namespace AMDiS;

using ElliptParam   = LagrangeBasis<Dune::YaspGrid<2>,1>;
using ElliptProblem = ProblemStat<ElliptParam>;

// compare two floating-point numbers
bool compare(double x, double y, int ulp = 10)
{
  using std::fabs;
  return fabs(x - y) <= std::numeric_limits<double>::epsilon() * fabs(x + y) * ulp
      || fabs(x - y) < std::numeric_limits<double>::min();
}

// compare two floating-point numbers
template <class T, int n>
bool compare(Dune::FieldVector<T,n> const& x, Dune::FieldVector<T,n> const& y)
{
  return Ranges::applyIndices<n>([&](auto... i) {
    return (compare(x[i],y[i]) &&...);
  });
}

// Compare two expression by evaluating in quadrature points in all elements of a GridView
template <class GridView, class Expr1, class Expr2>
bool compare(GridView const& gridView, Expr1 const& expr1, Expr2 const& expr2)
{
  auto gf1 = makeGridFunction(expr1, gridView);
  auto gf2 = makeGridFunction(expr2, gridView);

  auto lf1 = localFunction(gf1);
  auto lf2 = localFunction(gf2);

  for (auto const& e : elements(gridView))
  {
    lf1.bind(e);
    lf2.bind(e);

    for (auto const& qp : Dune::QuadratureRules<double,GridView::dimension>::rule(e.type(), 4))
    {
      if (!compare(lf1(qp.position()), lf2(qp.position()))) {
        return false;
      }
    }
  }

  return true;
}

int main(int argc, char** argv)
{
  Environment env(argc, argv);

  ElliptProblem prob("ellipt");
  prob.initialize(INIT_ALL);

  auto u = prob.solution();

  // eval a functor at global coordinates (at quadrature points)
  auto expr1 = evalAtQP([](Dune::FieldVector<double, 2> const& x) { return x[0] + x[1]; });
  auto expr2 = [](Dune::FieldVector<double, 2> const& x) { return x[0] + x[1]; };
  auto expr3 = [](auto const& x) { return x[0] + x[1]; };

  // constant values at quadrature points
  auto expr4 = 1.0;
  auto expr5 = Dune::FieldVector<double, 2>{1.0, 2.0};
  auto expr6 = std::ref(expr4);

  // Coordinate vector and component
  auto expr7 = X();
  auto expr8 = X(0);

  // Evaluation of the DOFVector (component) at quadrature points
  auto expr9 = prob.solution();

  auto expr10 = reference(expr4);
  [[maybe_unused]] auto expr11 = expr6 * expr10;

  AMDIS_TEST(compare(prob.gridView(), expr4, expr10));


  // ---------------------------------

  // derivative of expressions

  auto diff4 = gradientOf(expr4);
  // auto diff5 = gradientOf(expr5);
  auto diff6 = gradientOf(expr6);
  // auto diff7 = gradientOf(expr7);
  auto diff8 = gradientOf(expr8);
  auto diff9 = gradientOf(expr9);

  auto diff10 = gradientOf(expr9 + expr9);
  auto diff11 = gradientOf(2 * expr9);
  [[maybe_unused]] auto diff12 = gradientOf(expr8 * expr9);

  AMDIS_TEST(compare(prob.gridView(), diff10, diff11));
  AMDIS_TEST(compare(prob.gridView(), diff11, 2*diff9));

  // ---------------------------------

  u.interpolate(expr1);
  u.interpolate(expr2);
  u.interpolate(expr3);
  u.interpolate(expr4);
  u.interpolate(expr6);
  u.interpolate(expr8);
  u.interpolate(expr9);

  u << [](auto const& x) { return std::sin(x[0] + x[1]); };

  // ---------------------------------

  // operations with expressions

  auto op1 = expr1 + expr2;
  auto op2 = expr1 * expr4;
  auto op3 = two_norm(expr5);
  auto op4 = min(expr6, expr8);
  auto op5 = one_norm(expr7);
  auto op6 = invokeAtQP([](double v) { return 2*v; }, op5);

  auto op7 = 36 * pow<2>(u) * pow<2>(1 - u);
  auto op8 = 36 * pow<2>(u) * pow<2>(1.0 - u);

  AMDIS_TEST(compare(prob.gridView(), op7, op8));

  u.interpolate(two_norm(diff4));
  u.interpolate(two_norm(diff6));
  u.interpolate(two_norm(diff8));
  u.interpolate(two_norm(diff9));

  // ---------------------------------

  // integration of expressions

  auto gv = u.basis().gridView();

  [[maybe_unused]] auto int1 = integrate(op1, gv, 5);
  [[maybe_unused]] auto int2 = integrate(op2, gv, 5);
  [[maybe_unused]] auto int3 = integrate(op3, gv);
  [[maybe_unused]] auto int4 = integrate(op4, gv, 5);
  [[maybe_unused]] auto int5 = integrate(op5, gv, 5);
  [[maybe_unused]] auto int6 = integrate(op6, gv, 5);

  return 0;
}
