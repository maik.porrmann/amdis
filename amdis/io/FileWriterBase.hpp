#pragma once

#include <limits>
#include <string>

namespace AMDiS
{
  // forward declaration
  class AdaptInfo;

  /// Interface class for filewriters
  class FileWriterInterface
  {
  public:
    /// Virtual destructor
    virtual ~FileWriterInterface() = default;

    /// Pure virtual method to be implemented by derived classes
    virtual void write(AdaptInfo& adaptInfo, bool force) = 0;
  };

  /// \brief Base class for filewriters
  /**
   * Read parameters common for all filewriters, where `BASE` is given by the
   * constructor parameter `base`:
   * - `[BASE]->filename`:  Base name of the filename, not including the file extension
   * - `[BASE]->output directory`: Directory where to put the files
   * - `[BASE]->name`:  Name of the data vector in the output file
   * - `[BASE]->write every i-th timestep`:  Timestep number interval.
   * - `[BASE]->write after timestep`:  Time interval.
   *
   * Some other parameters found in some filewriter implementations:
   * - `[BASE]->mode`:  File mode, either ASCII=0, BINARY=1, or COMPRESSED=2
   * - `[BASE]->precision`:  Output precision, either FLOAT=0, DOUBLE=1
   * - `[BASE]->animation`:  Write animation files, or append the timestep to the filename.
   **/
  class FileWriterBase
      : public FileWriterInterface
  {
  public:
    /// Constructor. Reads common parameters.
    FileWriterBase(std::string const& base);

    /// Return whether to write the current timestep or not
    bool doWrite(AdaptInfo& adaptInfo) const;

  public:
    std::string const& filename() const
    {
      return filename_;
    }

    std::string const& dir() const
    {
      return dir_;
    }

    std::string const& name() const
    {
      return name_;
    }

  protected:
    /// Base part of output filename
    std::string filename_ = "solution";

    /// Output directory
    std::string dir_ = ".";

    /// Name of the data
    std::string name_ = "solution";

    /// Write every i'th timestep
    int tsModulo_ = 0;

    /// Write after every time interval
    double timeModulo_ = 0.0;

  private:
    // time counter for the interval output
    mutable double lastWriteTime_ = std::numeric_limits<double>::lowest();
  };

} // end namespace AMDiS
