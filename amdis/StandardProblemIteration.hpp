#pragma once

#include <string>

#include "ProblemIterationInterface.hpp"

namespace AMDiS
{
  // forward declarations
  class AdaptInfo;
  class Flag;
  class ProblemStatBase;

  /// A master problem for a single non coupled problem.
  class StandardProblemIteration
      : public virtual ProblemIterationInterface
  {
  public:
    /// constructor
    explicit StandardProblemIteration(ProblemStatBase& prob)
      : problem_(prob)
    {}

    /// Implementation of \ref ProblemIterationIterface::beginIteration()
    void beginIteration(AdaptInfo& adaptInfo) override;

    /// Implementation of \ref ProblemIterationInterface::oneIteration()
    Flag oneIteration(AdaptInfo& adaptInfo, Flag toDo) override;

    /// Implementation of \ref ProblemIterationInterface::endIteration()
    void endIteration(AdaptInfo& adaptInfo) override;

    /// Returns the name of the problem.
    std::string const& name() const override;

    int numProblems() const override
    {
      return 1;
    }

    /// Return the managed ProblemStat \ref problem, by number
    ProblemStatBase& problem(int number = 0) override;

    /// Return the managed ProblemStat \ref problem, by name
    ProblemStatBase& problem(std::string const& name) override;

  protected:
    /// Nested assemblage and mesh adaption.
    Flag buildAndAdapt(AdaptInfo& adaptInfo, Flag toDo);

  protected:
    /// The problem to solve.
    ProblemStatBase& problem_;
  };


  /// \brief StandardProblemIteration when derived from ProblemStat
  /**
   * Use this adaptor when multiple inheritance is used:
   * ```
   * class Problem
   *     : public ProblemStatBase
   *     : StandardProblemIterationAdaptor<Problem>
   * {};
   * ```
   *
   * **Requirements:**
   * - Model must be derived from ProblemStatBase and from StandardProblemIterationAdaptor
   **/
  template <class Model>
  class StandardProblemIterationAdaptor
      : public StandardProblemIteration
  {
    template <class Self>
    static ProblemStatBase& asProblemStatBase(Self& self)
    {
      Model& model = static_cast<Model&>(self);
      return dynamic_cast<ProblemStatBase&>(model);
    }

  public:
    StandardProblemIterationAdaptor()
      : StandardProblemIteration(asProblemStatBase(*this))
    {}

    StandardProblemIterationAdaptor(StandardProblemIterationAdaptor const&)
      : StandardProblemIteration(asProblemStatBase(*this))
    {}

    StandardProblemIterationAdaptor(StandardProblemIterationAdaptor&&)
      : StandardProblemIteration(asProblemStatBase(*this))
    {}
  };


} // end namespace AMDiS
