#include <string>
#include <vector>

#include <amdis/common/Algorithm.hpp>
#include <amdis/common/String.hpp>
#include "Tests.hpp"

using namespace AMDiS;

// ---------------------------------------------------------------------------------------
void test0()
{
  AMDIS_TEST_EQ( to_upper("Hallo Welt"), std::string("HALLO WELT") );
  AMDIS_TEST_EQ( to_lower("Hallo Welt"), std::string("hallo welt") );

  AMDIS_TEST_EQ( trim_copy("  Hallo Welt  "), std::string("Hallo Welt") );
}

// ---------------------------------------------------------------------------------------
// test the split() function
void test1()
{
  std::vector<std::string> subs;
  std::string text = "Dies ist ein langer Text";
  split(text.begin(), text.end(), ' ', [&subs](auto it0, auto it1) { subs.push_back(std::string(it0, it1)); });
// separates text at " " and stores parts in subs
  AMDIS_TEST_EQ( subs.size(), 5 );
  AMDIS_TEST_EQ( subs[0], std::string("Dies") );
  AMDIS_TEST_EQ( subs[1], std::string("ist") );

  int n = 0;
  std::string sep = " x";
  split(text.begin(), text.end(), sep.begin(), sep.end(), [&n](auto,auto) { ++n; });
// counts occurences of " " or "x"
  AMDIS_TEST_EQ( n, 6 );

  n = 0;
  std::string text0 = "";
  split(text0.begin(), text0.end(), ' ', [&n](auto,auto) { ++n; });
  AMDIS_TEST_EQ( n, 0 );

  std::string text1 = "Hallo";
  split(text1.begin(), text1.end(), ' ', [](auto it0, auto it1)
  {
    AMDIS_TEST_EQ( std::string(it0, it1), std::string("Hallo") );
  });
}

// ---------------------------------------------------------------------------------------
// test the replaceAll() function
void test2()
{
  std::string text = "Hallo Welt!";
  replaceAll(text, "Welt", "du");
  AMDIS_TEST_EQ(text,"Hallo du!" );

  replaceAll(text, "", "Guten Tag");
  AMDIS_TEST_EQ(text,"Hallo du!" );
}


int main()
{
  test0();
  test1();
  test2();

  return report_errors();
}
