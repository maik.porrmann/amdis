#pragma once

#include <string>
#include <memory>

#include <dune/grid/io/file/vtk/subsamplingvtkwriter.hh>
#include <dune/grid/io/file/vtk/vtkwriter.hh>

#include <amdis/Initfile.hpp>
#include <amdis/common/Filesystem.hpp>
#include <amdis/common/StaticSize.hpp>
#include <amdis/common/ValueCategory.hpp>
#include <amdis/gridfunctions/DiscreteFunction.hpp>
#include <amdis/io/FileWriterBase.hpp>
#include <amdis/io/VTKSequenceWriter.hpp>

namespace AMDiS
{
  namespace Impl
  {
    template <class Tag> struct VTKFieldTypeImpl;
    template <>
    struct VTKFieldTypeImpl<tag::scalar> {
      static const Dune::VTK::FieldInfo::Type value = Dune::VTK::FieldInfo::Type::scalar;
    };
    template <>
    struct VTKFieldTypeImpl<tag::vector> {
      static const Dune::VTK::FieldInfo::Type value = Dune::VTK::FieldInfo::Type::vector;
    };
    template <>
    struct VTKFieldTypeImpl<tag::matrix> {
      static const Dune::VTK::FieldInfo::Type value = Dune::VTK::FieldInfo::Type::tensor;
    };

  } // end namespace Impl


  /// Adapter for the dune-grid VTKWriter
  /**
   * \tparam GV  GridView describing the grid to write
   * \tparam GF  GridFunction defined on that GridView
   **/
  template <class GV, class GF>
  class VTKWriter
      : public FileWriterBase
  {
    using GridView = GV;
    using Writer = Dune::VTKWriter<GridView>;
    using SeqWriter = VTKSequenceWriter<GridView>;

    using GridFunction = GF;
    using Range = typename GridFunction::Range;

    template <class R>
    static constexpr Dune::VTK::FieldInfo::Type VTKFieldType() {
      return Impl::VTKFieldTypeImpl<ValueCategory_t<R>>::value;
    }

    template <class R>
    static constexpr std::size_t VTKFieldSize() {
      return static_size_v<R>;
    }

  public:
    /// Constructor.
    VTKWriter(std::string const& name, GridView const& gridView, GridFunction const& gridFunction)
      : FileWriterBase(name)
      , gridFunction_(gridFunction)
    {
      int m = 0;
      Parameters::get(name + "->animation", animation_);
      Parameters::get(name + "->mode", m);

      mode_ = (m == 0)
        ? Dune::VTK::ascii
        : Dune::VTK::appendedraw;

      auto subSampling = Parameters::get<int>(name + "->subsampling");
      if (subSampling) {
        using SubsamplingWriter = Dune::SubsamplingVTKWriter<GridView>;
        vtkWriter_ = std::make_shared<SubsamplingWriter>(gridView, Dune::refinementIntervals(subSampling.value()));
      } else {
        vtkWriter_ = std::make_shared<Writer>(gridView);
      }

      if (animation_)
        vtkSeqWriter_ = std::make_shared<SeqWriter>(vtkWriter_);

      vtkWriter_->addVertexData(gridFunction_,
        Dune::VTK::FieldInfo(name_, VTKFieldType<Range>(), VTKFieldSize<Range>()));
    }

    /// Implements \ref FileWriterInterface::write
    void write(AdaptInfo& adaptInfo, bool force) override
    {
      filesystem::create_directories(this->dir() + "/_piecefiles");
      if (this->doWrite(adaptInfo) || force) {
        if (animation_)
          vtkSeqWriter_->pwrite(adaptInfo.time(), this->filename(), this->dir(), "_piecefiles", mode_);
        else
          vtkWriter_->pwrite(this->filename(), this->dir(), "_piecefiles", mode_);
      }
    }

  private:
    GridFunction gridFunction_;
    std::shared_ptr<Writer> vtkWriter_;
    std::shared_ptr<SeqWriter> vtkSeqWriter_;

    // write .pvd if animation=true, otherwise write only .vtu
    bool animation_ = false;

    // represents VTK::OutputType: ascii, appendedraw
    Dune::VTK::OutputType mode_ = Dune::VTK::ascii;
  };

} // end namespace AMDiS
