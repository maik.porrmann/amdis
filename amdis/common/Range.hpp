#pragma once

#include <type_traits>

#include <amdis/common/Index.hpp>
#include <amdis/common/StaticSize.hpp>

namespace AMDiS
{
  namespace Impl
  {
    /// A range of indices [I,J)
    template <class Int, Int I, Int J>
    struct range_impl
    {
      using type = range_impl;

      /// Return the first element in the range
      static constexpr auto begin() { return std::integral_constant<Int, I>{}; }

      /// Returns the element after the last element in the range
      static constexpr auto end() { return std::integral_constant<Int, J>{}; }

      /// Returns the ith index in the range as integral constant
      template <std::size_t i>
      constexpr auto operator[](index_t<i>) const
      {
        return std::integral_constant<Int, I+Int(i)>{};
      }

      /// Return whether the range is empty
      static constexpr bool empty() { return I >= J; }

      /// Returns the size of the range
      static constexpr auto size() { return std::integral_constant<Int, J-I>{}; }
    };

    /// Extracts the Ith element element from the range
    template <std::size_t I, class Int, Int begin, Int end>
    constexpr auto get(range_impl<Int, begin, end> const& r) { return r[index_<I>]; }

  } // end namespace Impl

  template <std::size_t I, std::size_t J>
  using range_t = Impl::range_impl<std::size_t, I, J>;

  template <std::size_t I, std::size_t J>
  constexpr range_t<I,J> range_ = {};

} // end namespace AMDiS

namespace std
{
  template <class Int, Int I0, Int I1>
  class tuple_size<AMDiS::Impl::range_impl<Int,I0,I1>>
      : public std::integral_constant<std::size_t, std::size_t(I1-I0)> {};

  template <std::size_t I, class Int, Int I0, Int I1>
  class tuple_element<I,AMDiS::Impl::range_impl<Int,I0,I1>>
  {
  public:
    using type = Int;
  };

} // end namespace std
