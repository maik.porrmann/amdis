/* begin amdis
   put the definitions for config.h specific to
   your project here. Everything above will be
   overwritten
*/

/* begin private */
/* Name of package */
#define PACKAGE "@DUNE_MOD_NAME@"

/* Define to the address where bug reports for this package should be sent. */
#define PACKAGE_BUGREPORT "@DUNE_MAINTAINER@"

/* Define to the full name of this package. */
#define PACKAGE_NAME "@DUNE_MOD_NAME@"

/* Define to the full name and version of this package. */
#define PACKAGE_STRING "@DUNE_MOD_NAME@ @DUNE_MOD_VERSION@"

/* Define to the one symbol short name of this package. */
#define PACKAGE_TARNAME "@DUNE_MOD_NAME@"

/* Define to the home page for this package. */
#define PACKAGE_URL "@DUNE_MOD_URL@"

/* Define to the version of this package. */
#define PACKAGE_VERSION "@DUNE_MOD_VERSION@"

/* end private */

/* Define to the version of amdis */
#define AMDIS_VERSION "@AMDIS_VERSION@"

/* Define to the major version of amdis */
#define AMDIS_VERSION_MAJOR @AMDIS_VERSION_MAJOR@

/* Define to the minor version of amdis */
#define AMDIS_VERSION_MINOR @AMDIS_VERSION_MINOR@

/* Define to the revision of amdis */
#define AMDIS_VERSION_REVISION @AMDIS_VERSION_REVISION@

/* Define to true if the MTL library is available */
#cmakedefine AMDIS_HAS_MTL 1

/* Define to true if the HYPRE library is available */
#cmakedefine AMDIS_HAS_HYPRE ENABLE_HYPRE

/* Define to true if the Eigen3 library is available */
#cmakedefine AMDIS_HAS_EIGEN 1

/* Define to true if the PETSc library is available */
#cmakedefine AMDIS_HAS_PETSC 1

#define AMDIS_BACKEND_ISTL 1
#define AMDIS_BACKEND_MTL 2
#define AMDIS_BACKEND_EIGEN 3
#define AMDIS_BACKEND_PETSC 4

/* Define one of the IDs defined above representing the AMDiS backend*/
#define AMDIS_BACKEND @AMDIS_BACKEND@

/* some detected compiler features may be used in AMDiS */
#cmakedefine AMDIS_HAS_EXPANSION_STATEMENTS 1

/* end amdis
   Everything below here will be overwritten
*/
