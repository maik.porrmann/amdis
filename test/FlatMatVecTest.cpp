#include <cmath>

#include <amdis/common/FlatMatrix.hpp>
#include <amdis/common/FlatVector.hpp>

#include "Tests.hpp"

using namespace AMDiS;

// -----------------------------------------------------------------------------
void test0()
{
  using V = FlatVector<double>;
  // create an empty vector
  V a;

  // size of the vector
  AMDIS_TEST_EQ(a.size(), 0);

  // Resize the vector
  a.resize(3, 0.0);
  AMDIS_TEST_EQ(a.size(), 3);
  AMDIS_TEST_EQ(a[0], 0.0);
  AMDIS_TEST_EQ(a[1], 0.0);
  AMDIS_TEST_EQ(a[2], 0.0);

  // access component
  a[0] = 0.0;
  a[1] = 0.5;
  a[2] = 1.0;

  // assign value to all entries
  a = 2.0;
  AMDIS_TEST_EQ(a[0], 2.0);
  AMDIS_TEST_EQ(a[1], 2.0);
  AMDIS_TEST_EQ(a[2], 2.0);
}

// -----------------------------------------------------------------------------
void test1()
{
  using M = FlatMatrix<double>;
  // create an empty matrix
  M m;
  AMDIS_TEST_EQ(m.size(), 0);
  AMDIS_TEST_EQ(m.rows(), 0);
  AMDIS_TEST_EQ(m.cols(), 0);

  // resize the matrix
  m.resize(2,3, 0.0);
  AMDIS_TEST_EQ(m.size(), 2*3);
  AMDIS_TEST_EQ(m.rows(), 2);
  AMDIS_TEST_EQ(m.cols(), 3);

  typename M::difference_type s = std::distance(m.begin(),m.end());
  AMDIS_TEST_EQ(s, 2*3);

  // test element access
  AMDIS_TEST_EQ(m[0][0], 0.0);
  AMDIS_TEST_EQ(m[1][2], 0.0);

  // test assignment of value
  m = 1.0;
  AMDIS_TEST_EQ(m[0][0], 1.0);
  AMDIS_TEST_EQ(m[1][2], 1.0);

  for (std::size_t i = 0; i < m.rows(); ++i)
    for (std::size_t j = 0; j < m.cols(); ++j)
      m[i][j] = 2.0;

  // access matrix entries
  for (std::size_t i = 0; i < m.rows(); ++i)
    for (std::size_t j = 0; j < m.cols(); ++j)
      AMDIS_TEST_EQ(m[i][j], 2.0);

  // test iterators
  for (auto& mij : m)
    mij = 3.0;

  for (auto const& mij : m)
    AMDIS_TEST_EQ(mij, 3.0);

  // test data array
  m = 4.0;
  double const* data = m.data();
  for (std::size_t i = 0; i < m.size(); ++i)
    AMDIS_TEST_EQ(data[i], 4.0);
}

int main()
{
  test0();
  test1();

  return report_errors();
}
