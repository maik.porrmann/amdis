#pragma once

#include <dune/istl/solver.hh>

namespace AMDiS
{
  template <class M, class X, class Y = X>
  class LinearSolverInterface
  {
  public:
    virtual ~LinearSolverInterface() = default;

    /// \brief Prepare the solve (and preconditioner), e.g. make a factorization
    /// of the matrix, or extract its diagonal in a jacobian precon.
    virtual void init(M const& A) = 0;

    /// \brief Cleanup the solver, e.g. free the previously created factorization.
    virtual void finish() = 0;

    /// \brief Apply the inverse operator to the rhs vector b
    virtual void apply(X& x, Y const& b, Dune::InverseOperatorResult& res) = 0;
  };

} // end namespace AMDiS
