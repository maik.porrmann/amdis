#pragma once

#include <string>
#include <memory>
#include <utility>

#include <dune/grid/common/backuprestore.hh>
#include <dune/grid/common/capabilities.hh>

#include <amdis/BackupRestore.hpp>
#include <amdis/Initfile.hpp>
#include <amdis/Output.hpp>
#include <amdis/common/Filesystem.hpp>
#include <amdis/io/FileWriterBase.hpp>

namespace AMDiS
{
  template <class SystemVector>
  class BackupWriter
      : public FileWriterBase
  {
    using GlobalBasis  = typename SystemVector::GlobalBasis;
    using GridView     = typename GlobalBasis::GridView;
    using Grid         = typename GridView::Grid;

  public:
    /// Constructor.
    BackupWriter(std::string const& name, std::shared_ptr<SystemVector> systemVector)
      : FileWriterBase(name)
      , systemVector_(std::move(systemVector))
    {
      Parameters::get(name + "->animation", animation_);
    }

    /// Implements \ref FileWriterBase::write
    void write(AdaptInfo& adaptInfo, bool force) override
    {
      std::string filename = filesystem::path({this->dir(), this->filename()}).string();

      if (animation_)
        filename += "_" + std::to_string(adaptInfo.timestepNumber());

      if (this->doWrite(adaptInfo) || force) {
        GridView const& gridView = systemVector_->basis().gridView();
        backupGrid(gridView, filename + ".grid",
          bool_t<Dune::Capabilities::hasBackupRestoreFacilities<Grid>::v>{});
        systemVector_->backup(filename + ".solution");

        msg("Backup written to files '{}' and '{}'.", filename + ".grid", filename + ".solution");
      }
    }


  private:

    template <class G = Grid,
      class = decltype(Dune::BackupRestoreFacility<G>::backup(std::declval<G>(), std::string("")))>
    void backupGrid(GridView const& gv, std::string filename, std::true_type)
    {
      Dune::BackupRestoreFacility<Grid>::backup(gv.grid(), filename);
    }

    template <bool B, class G = Grid, class GV = GridView,
      class = decltype(BackupRestoreByGridFactory<G>::backup(std::declval<GV>(), std::string("")))>
    void backupGrid(GridView const& gv, std::string filename, bool_t<B>)
    {
      warning("Falling back to backup of gridview.");
      BackupRestoreByGridFactory<Grid>::backup(gv, filename);
    }

  private:
    std::shared_ptr<SystemVector> systemVector_;

    /// append timestepnumber if animation=true, otherwise always override
    bool animation_ = false;
  };

} // end namespace AMDiS
