#pragma once

#include <list>
#include <memory>
#include <string>
#include <vector>

#include <Eigen/SparseCore>

#include <dune/common/timer.hh>

#include <amdis/Output.hpp>
#include <amdis/linearalgebra/eigen/MatrixSize.hpp>

namespace AMDiS
{
  class DefaultIndexDistribution;

  /// \brief The basic container that stores a base matrix and a corresponding
  /// row/column feSpace.
  template <class T, int Orientation = Eigen::RowMajor>
  class EigenSparseMatrix
  {
  public:
    /// The matrix type of the underlying base matrix
    using BaseMatrix = Eigen::SparseMatrix<T, Orientation>;

    /// The type of the elements of the DOFMatrix
    using value_type = typename BaseMatrix::Scalar;

    /// The index/size - type
    using size_type = typename BaseMatrix::Index;

  public:
    /// Constructor. Constructs new BaseMatrix.
    EigenSparseMatrix(DefaultIndexDistribution const&, DefaultIndexDistribution const&)
    {}

    /// Return a reference to the data-matrix \ref matrix
    BaseMatrix& matrix()
    {
      return matrix_;
    }

    /// Return a reference to the data-matrix \ref matrix
    BaseMatrix const& matrix() const
    {
      return matrix_;
    }


    /// \brief Returns an update-proxy of the inserter, to insert/update a value at
    /// position (\p r, \p c) in the matrix. Need an insertionMode inserter, that can
    /// be created by \ref init and must be closed by \ref finish after insertion.
    void insert(size_type r, size_type c, value_type const& value)
    {
      test_exit_dbg(r < matrix_.rows() && c < matrix_.cols(),
          "Indices out of range [0,{})x[0,{})", matrix_.rows(), matrix_.cols());
      tripletList_.emplace_back(r, c, value);
    }

    template <class Ind, class LocalMat>
    void scatter(Ind const& idx, LocalMat const& mat)
    {
      scatter(idx, idx, mat);
    }

    template <class RowInd, class ColInd, class LocalMat>
    void scatter(RowInd const& rows, ColInd const& cols, LocalMat const& mat)
    {
      //tripletList_.reserve(tripletList_.size() + rows.size()*cols.size());
      for (size_type i = 0; i < size_type(rows.size()); ++i)
        for (size_type j = 0; j < size_type(cols.size()); ++j)
          tripletList_.emplace_back(rows[i], cols[j], mat[i][j]);
    }


    /// Resize the matrix according to the pattern provided and set all entries to zero.
    void init(MatrixSize const& sizes)
    {
      matrix_.resize(sizes.rows(), sizes.cols());
      matrix_.setZero();
    }

    /// Set all matrix entries to zero while keeping the size unchanged.
    void init()
    {
      matrix_.setZero();
    }

    /// Set the matrix entries from the triplet list and compress the matrix afterwards.
    void finish()
    {
      matrix_.setFromTriplets(tripletList_.begin(), tripletList_.end());
      matrix_.makeCompressed();

      tripletList_.clear(); // NOTE: this does not free the memory
    }

    /// Return the number of nonzeros in the matrix
    std::size_t nnz() const
    {
      return matrix_.nonZeros();
    }

  private:
    /// The data-matrix
    BaseMatrix matrix_;

    /// A list of row,col indices and values
    std::vector<Eigen::Triplet<value_type, Eigen::Index>> tripletList_;
  };

} // end namespace AMDiS
