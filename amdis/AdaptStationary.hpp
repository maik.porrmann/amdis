#pragma once

// std c++ headers
#include <string>

// AMDiS includes
#include "AdaptBase.hpp"

namespace AMDiS
{
  // forward declarations
  class AdaptInfo;
  class ProblemIterationInterface;

  /** \defgroup Adaption Adaption module
  * @{ <img src="adaption.png"> @}
  *
  * \brief
  * Contains all classes needed for space and time adaption.
  */


  /** \ingroup Adaption
   * \brief
   * AdaptStationary contains information about the adaptive procedure and the
   * adapt procedure itself
   */
  class AdaptStationary
      : public AdaptBase
  {
  public:
    /// Creates a AdaptStationary object with given name.
    AdaptStationary(std::string const& name,
                    ProblemIterationInterface& problemIteration,
                    AdaptInfo& adaptInfo);

    /// Implementation of AdaptBase::adapt()
    int adapt() override;
  };

} // end namespace AMDiS
