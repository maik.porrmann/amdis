#pragma once

#include <memory>
#include <string>

#include <amdis/CreatorInterface.hpp>
#include <amdis/Initfile.hpp>
#include <amdis/linearalgebra/LinearSolverInterface.hpp>
#include <amdis/linearalgebra/eigen/SolverConfig.hpp>

namespace AMDiS
{
  /**
  * \ingroup Solver
  * \class AMDiS::DirectRunner
  * \brief \implements Dune::InverseOperator for the (external) direct solvers
  */
  template <class M, class X, class Y, template <class> class Solver>
  class DirectRunner
      : public LinearSolverInterface<M,X,Y>
  {
    using Self = DirectRunner;
    using EigenSolver = Solver<M>;

  public:
    struct Creator final : CreatorInterfaceName<LinearSolverInterface<M,X,Y>>
    {
      std::unique_ptr<LinearSolverInterface<M,X,Y>>
      createWithString(std::string prefix) final
      {
        return std::make_unique<Self>(prefix);
      }
    };

  public:
    /// Constructor.
    DirectRunner(std::string const& prefix)
      : solver_{}
    {
      SolverConfig<EigenSolver>::init(prefix, solver_);
      Parameters::get(prefix + "->reuse pattern", reusePattern_);
    }

    /// initialize the matrix and maybe compute factorization
    void init(M const& A) override
    {
      if (!reusePattern_ || !initialized_) {
        solver_.analyzePattern(A);
        initialized_ = true;
      }
      solver_.factorize(A);

      test_exit(solver_.info() == Eigen::Success, "Error in solver.compute(matrix)");
    }

    /// Implements \ref LinearSolverInterface::finish()
    void finish() override
    {
      initialized_ = false;
    }

    /// Implements \ref LinearSolverInterface::apply()
    void apply(X& x, Y const& b, Dune::InverseOperatorResult& stat) override
    {
      Dune::Timer t;
      x = solver_.solve(b);

      stat.converged = (solver_.info() == Eigen::Success);
      stat.elapsed = t.elapsed();
    }

  private:
    EigenSolver solver_;
    bool reusePattern_ = false;
    bool initialized_ = false;
  };

} // end namespace AMDiS
