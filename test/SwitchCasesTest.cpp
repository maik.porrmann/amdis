#include <amdis/AMDiS.hpp>
#include <amdis/common/SwitchCases.hpp>

using namespace AMDiS;

template <std::size_t i>
struct Foo
{
  void foo() {}
};

struct Bar
{
  void bar(std::size_t i) {}
};

template <std::size_t max_i>
void call_foo(std::size_t i)
{
  switchCases(Dune::range(index_t<max_i>{}), i, [](auto _i) { Foo<_i.value>{}.foo(); });
}

void call_bar(std::size_t i, std::size_t max_i)
{
  switchCases(Dune::range(max_i), i, [](auto _i) { Bar{}.bar(_i); });
}

int main(int argc, char** argv)
{
  call_foo<1>(0);
  call_foo<2>(1);
  call_foo<3>(2);
  call_foo<10>(7);

  call_bar(0, 1);
  call_bar(1, 2);
  call_bar(2, 3);
  call_bar(7, 10);
  return 0;
}
