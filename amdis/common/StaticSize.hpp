#pragma once

#include <array>
#include <tuple>
#include <type_traits>

#include <dune/common/typeutilities.hh>
#include <amdis/common/TypeTraits.hpp>

#if AMDIS_HAS_MTL
#include <boost/numeric/mtl/operation/static_size.hpp>
#endif

namespace AMDiS
{
  namespace Impl
  {
    template <class Container>
    struct SizeImpl
    {
#if AMDIS_HAS_MTL
      // MTL4: Try if a mtl::static_size is specialized for class
      template <class T>
      static constexpr auto eval(T const&, Dune::PriorityTag<6>)
        -> decltype(std::integral_constant<std::size_t,(mtl::static_num_rows<T>::value * mtl::static_num_cols<T>::value)>{})
      {
        return {};
      }
#endif

      // Eigen: Try if a static SizeAtCompileTime constant is specified for class
      template <class T>
      static constexpr auto eval(T const&, Dune::PriorityTag<5>)
        -> decltype(std::integral_constant<std::size_t,T::SizeAtCompileTime>{})
      {
        return {};
      }

      // Try if tuple_size is implemented for class
      template <class T>
      static constexpr auto eval(T const&, Dune::PriorityTag<4>)
        -> decltype(std::integral_constant<std::size_t,std::tuple_size<T>::value>{})
      {
        return {};
      }

      // Try if a static dimension constant is specified for class
      template <class T>
      static constexpr auto eval(T const&, Dune::PriorityTag<3>)
        -> decltype(std::integral_constant<std::size_t,T::dimension>{})
      {
        return {};
      }

      // Try if a static rows and cols constants are specified for class
      template <class T>
      static constexpr auto eval(T const&, Dune::PriorityTag<2>)
        -> decltype(std::integral_constant<std::size_t,(T::rows * T::cols)>{})
      {
        return {};
      }

      // Try if there's a static constexpr size()
      template <class T>
      static constexpr auto eval(T const&, Dune::PriorityTag<1>)
        -> decltype(std::integral_constant<std::size_t,T::size()>{})
      {
        return {};
      }

      // Arithmetic types have size 1 otherwise size is 0
      template <class T>
      static constexpr auto eval(T const&, Dune::PriorityTag<0>)
        -> decltype(std::integral_constant<std::size_t, (std::is_arithmetic_v<T> ? 1 : 0)>{})
      {
        return {};
      }

      static constexpr auto eval(Container const& container)
      {
        return eval(container, Dune::PriorityTag<42>{});
      }
    };

  } // end namespace Impl


  /**
  * \brief Return a static constant size of the container
  *
  * \param container Container whose size is queried
  * \return Size of t
  *
  * If the size of t is known at compile time the size is
  * returned as std::integral_constant<std::size_t, size>.
  * Otherwise 0 is returned by default, or 1 for arithmetic types.
  */
  template <class Container>
  constexpr auto static_size(Container const& container)
  {
    return Impl::SizeImpl<Container>::eval(container);
  }

  template <class Container>
  constexpr std::size_t static_size_v
    = decltype(static_size(std::declval<remove_cvref_t<Container>>()))::value;

  template <class T, std::size_t S0, std::size_t S1 = static_size_v<T>>
  struct CheckSize
  {
    static_assert(S0 == S1, "Non-matching size");
  };


  namespace Impl
  {
    template <class Matrix>
    struct NumRowsImpl
    {
#if AMDIS_HAS_MTL
      // MTL4: Try if a mtl::static_num_rows is specialized for class
      template <class T>
      static constexpr auto eval(T const&, Dune::PriorityTag<4>)
        -> decltype(std::integral_constant<std::size_t,mtl::static_num_rows<T>::value>{})
      {
        return {};
      }
#endif

      // Eigen: Try if a static RowsAtCompileTime constant is specified for class
      template <class T>
      static constexpr auto eval(T const&, Dune::PriorityTag<3>)
        -> decltype(std::integral_constant<std::size_t,T::RowsAtCompileTime>{})
      {
        return {};
      }

      // Try if a static rows constant is specified for class
      template <class T>
      static constexpr auto eval(T const&, Dune::PriorityTag<2>)
        -> decltype(std::integral_constant<std::size_t,T::rows>{})
      {
        return {};
      }

      // Try if there's a static constexpr N()
      template <class T>
      static constexpr auto eval(T const&, Dune::PriorityTag<1>)
        -> decltype(std::integral_constant<std::size_t,T::N()>{})
      {
        return {};
      }

      // Fallback-size is 1 for arithmetic types or 0
      template <class T>
      static constexpr auto eval(T const&, Dune::PriorityTag<0>)
        -> decltype(std::integral_constant<std::size_t, (std::is_arithmetic_v<T> ? 1 : 0)>{})
      {
        return {};
      }

      static constexpr auto eval(Matrix const& matrix)
      {
        return eval(matrix, Dune::PriorityTag<42>{});
      }
    };

  } // end namespace Impl


  /**
  * \brief Return a static constant rows of the matrix
  *
  * \param container Matrix whose number of rows is queried
  * \return Number of rows of matrix
  *
  * If the size of t is known at compile time the number of rows is
  * returned as std::integral_constant<std::size_t, size>.
  * Otherwise 0 is returned by default, or 1 for arithmetic types.
  */
  template <class Matrix>
  constexpr auto static_num_rows(Matrix const& matrix)
  {
    return Impl::NumRowsImpl<Matrix>::eval(matrix);
  }

  template <class Matrix>
  constexpr std::size_t static_num_rows_v
    = decltype(static_num_rows(std::declval<remove_cvref_t<Matrix>>()))::value;

  template <class T, std::size_t S0, std::size_t S1 = static_num_rows<T>>
  struct CheckNumRows
  {
    static_assert(S0 == S1, "Non-matching num rows");
  };

  namespace Impl
  {
    template <class Matrix>
    struct NumColsImpl
    {
#if AMDIS_HAS_MTL
      // MTL4: Try if a mtl::static_num_cols is specialized for class
      template <class T>
      static constexpr auto eval(T const&, Dune::PriorityTag<4>)
        -> decltype(std::integral_constant<std::size_t,mtl::static_num_cols<T>::value>{})
      {
        return {};
      }
#endif

      // Eigen: Try if a static ColsAtCompileTime constant is specified for class
      template <class T>
      static constexpr auto eval(T const&, Dune::PriorityTag<3>)
        -> decltype(std::integral_constant<std::size_t,T::ColsAtCompileTime>{})
      {
        return {};
      }

      // Try if a static cols constant is specified for class
      template <class T>
      static constexpr auto eval(T const&, Dune::PriorityTag<2>)
        -> decltype(std::integral_constant<std::size_t,T::cols>{})
      {
        return {};
      }

      // Try if there's a static constexpr M()
      template <class T>
      static constexpr auto eval(T const&, Dune::PriorityTag<1>)
        -> decltype(std::integral_constant<std::size_t,T::M()>{})
      {
        return {};
      }

      // Fallback-size is 0
      template <class T>
      static constexpr auto eval(T const&, Dune::PriorityTag<0>)
        -> decltype(std::integral_constant<std::size_t, (std::is_arithmetic_v<T> ? 1 : 0)>{})
      {
        return {};
      }

      static constexpr auto eval(Matrix const& matrix)
      {
        return eval(matrix, Dune::PriorityTag<42>{});
      }
    };

  } // end namespace Impl


  /**
  * \brief Return a static constant columns of the matrix
  *
  * \param container Matrix whose number of columns is queried
  * \return Number of columns of matrix
  *
  * If the size of matrix is known at compile time the number if cols is
  * returned as std::integral_constant<std::size_t, size>.
  * Otherwise 0 is returned by default, or 1 for arithmetic types.
  */
  template <class Matrix>
  constexpr auto static_num_cols(Matrix const& matrix)
  {
    return Impl::NumColsImpl<Matrix>::eval(matrix);
  }

  template <class Matrix>
  constexpr std::size_t static_num_cols_v
    = decltype(static_num_cols(std::declval<remove_cvref_t<Matrix>>()))::value;

  template <class T, std::size_t S0, std::size_t S1 = static_num_cols<T>>
  struct CheckNumCols
  {
    static_assert(S0 == S1, "Non-matching num rows");
  };

} // end namespace AMDiS
