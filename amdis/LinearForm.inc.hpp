#pragma once

#include <utility>

#include <amdis/GridFunctionOperator.hpp>
#include <amdis/LocalOperator.hpp>
#include <amdis/functions/EntitySet.hpp>
#include <amdis/operations/Assigner.hpp>
#include <amdis/typetree/Traversal.hpp>
#include <amdis/typetree/TreePath.hpp>

namespace AMDiS {

template <class GB, class T, class Traits>
  template <class ContextTag, class Expr, class TreePath>
void LinearForm<GB,T,Traits>::
addOperator(ContextTag contextTag, Expr const& expr, TreePath path)
{
  static_assert( Concepts::PreTreePath<TreePath>,
      "path must be a valid treepath, or an integer/index-constant");

  auto i = makeTreePath(path);

  using LocalContext = typename ContextTag::type;
  auto op = makeOperator<LocalContext>(expr, this->basis().gridView());
  operators_[i].push(contextTag, std::move(op));
}


template <class GB, class T, class Traits>
  template <class LocalView, class LocalOperators,
    REQUIRES_(Concepts::LocalView<LocalView>)>
void LinearForm<GB,T,Traits>::
assemble(LocalView const& localView, LocalOperators& localOperators)
{
  assert(localView.bound());

  elementVector_.resize(localView.size());
  elementVector_ = 0;

  auto const& gv = this->basis().gridView();
  auto const& element = localView.element();
  GlobalContext<TYPEOF(gv)> context{gv, element, element.geometry()};

  Traversal::forEachNode(localView.treeCache(), [&](auto const& node, auto tp) {
    auto& rhsOp = localOperators[tp];
    rhsOp.bind(element);
    rhsOp.assemble(context, node, elementVector_);
    rhsOp.unbind();
  });

  this->scatter(localView, elementVector_, Assigner::plus_assign{});
}


template <class GB, class T, class Traits>
void LinearForm<GB,T,Traits>::
assemble()
{
  auto localView = this->basis().localView();
  auto localOperators = AMDiS::localOperators(operators_);

  this->init(sizeInfo(this->basis()), true);
  for (auto const& element : entitySet(this->basis())) {
    localView.bind(element);
    this->assemble(localView, localOperators);
    localView.unbind();
  }
  this->finish();
}

} // end namespace AMDiS
