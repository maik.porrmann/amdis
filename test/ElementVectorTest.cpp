#include <amdis/AMDiS.hpp>
#include <dune/grid/yaspgrid.hh>

#include <amdis/AdaptiveGrid.hpp>
#include <amdis/ElementVector.hpp>
#include <amdis/gridfunctions/ElementGridFunction.hpp>

#include "Tests.hpp"

using namespace AMDiS;

int main(int argc, char** argv)
{
  Environment env(argc, argv);

  // create grid
  using HostGrid = Dune::YaspGrid<2>;
  HostGrid hostGrid({1.0,1.0}, {2,2});
  AdaptiveGrid<HostGrid> grid(hostGrid);

  using IndexType = typename HostGrid::LeafGridView::IndexSet::IndexType;
  ElementVector elementVector{grid, IndexType(0)};
  {
    auto const& gridView = grid.leafGridView();
    auto const& indexSet = gridView.indexSet();
    AMDIS_TEST(elementVector.data().size() == std::size_t(gridView.size(0)));

    for (auto const& e : elements(gridView))
    {
      elementVector.data()[indexSet.index(e)] = indexSet.index(e);
    }
  }

  // check refinement

  grid.globalRefine(1);
  {
    auto const& gridView = grid.leafGridView();
    auto const& indexSet = gridView.indexSet();
    AMDIS_TEST(elementVector.data().size() == std::size_t(gridView.size(0)));

    for (auto const& e : elements(gridView))
    {
      auto father = e.father();
      AMDIS_TEST(elementVector.data()[indexSet.index(e)] == indexSet.index(father));
    }
  }

  // check grid function

  {
    auto gf = valueOf(elementVector);
    auto const& gridView = grid.leafGridView();
    auto const& indexSet = gridView.indexSet();

    auto lf = localFunction(gf);
    for (auto const& e : elements(gridView))
    {
      lf.bind(e);

      AMDIS_TEST(elementVector.data()[indexSet.index(e)] == lf(e.geometry().center()));
    }
  }

  report_errors();
}
