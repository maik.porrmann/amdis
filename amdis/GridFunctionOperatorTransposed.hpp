#pragma once

#include <amdis/common/Transposed.hpp>

namespace AMDiS
{
  template <class Transposed>
  class GridFunctionLocalOperatorTransposed;

  /// \brief The transposed operator, implemented in terms of its transposed by
  /// calling \ref assemble with inverted arguments on the transposed element-matrix.
  template <class Transposed>
  class GridFunctionOperatorTransposed
  {
  public:
    GridFunctionOperatorTransposed(Transposed const& transposedOp)
      : transposedOp_(transposedOp)
    {}

    template <class GridView>
    void update(GridView const& gv)
    {
      transposedOp_.update(gv);
    }

    friend auto localOperator(GridFunctionOperatorTransposed const& op)
    {
      return GridFunctionLocalOperatorTransposed{localOperator(op.transposedOp_)};
    }

  private:
    Transposed transposedOp_;
  };


  template <class Transposed>
  class GridFunctionLocalOperatorTransposed
  {
  public:
    GridFunctionLocalOperatorTransposed(Transposed const& transposedLop)
      : transposedLop_(transposedLop)
    {}

    /// Redirects the bind call top the transposed operator
    template <class Element>
    void bind(Element const& element)
    {
      transposedLop_.bind(element);
    }

    /// Redirects the unbind call top the transposed operator
    void unbind()
    {
      transposedLop_.unbind();
    }

    /// Apply the assembling to the transposed elementMatrix with interchanged row-/colNode
    /**
     * \tparam CG  ContextGeometry
     * \tparam RN  RowNode
     * \tparam CN  ColNode
     * \tparam Mat ElementMatrix
     **/
    template <class CG, class RN, class CN, class Mat>
    void assemble(CG const& contextGeometry, RN const& rowNode, CN const& colNode,
                  Mat& elementMatrix) const
    {
      auto elementMatrixT = transposed(elementMatrix);
      transposedLop_.assemble(contextGeometry, colNode, rowNode, elementMatrixT);
    }

  private:
    Transposed transposedLop_;
  };

} // end namespace AMDiS
