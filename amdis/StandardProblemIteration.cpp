#include <config.h>

#include "StandardProblemIteration.hpp"

#include <amdis/AdaptInfo.hpp>
#include <amdis/Output.hpp>
#include <amdis/ProblemStatBase.hpp>

namespace AMDiS
{
  ProblemStatBase& StandardProblemIteration::problem(int number)
  {
    AMDIS_FUNCNAME_DBG("StandardProblemIteration::getProblem");
    test_exit_dbg(number == 0, "Problem number out of range!");
    return problem_;
  }

  ProblemStatBase& StandardProblemIteration::problem(std::string const& name)
  {
    AMDIS_FUNCNAME_DBG("StandardProblemIteration::getProblem");
    test_exit_dbg(name == problem_.name(), "Problem name does not match!\n");
    return problem_;
  }

  void StandardProblemIteration::beginIteration(AdaptInfo& adaptInfo)
  {
    AMDIS_FUNCNAME("StandardProblemIteration::beginIteration()");

    msg("");
    msg("begin of iteration number: {}", (adaptInfo.spaceIteration() + 1));
    msg("=============================");
  }


  Flag StandardProblemIteration::oneIteration(AdaptInfo& adaptInfo, Flag toDo)
  {
    Flag flag = buildAndAdapt(adaptInfo, toDo);

    if (toDo.isSet(SOLVE))
      problem_.solve(adaptInfo, true, false);

    if (toDo.isSet(SOLVE_RHS))
      problem_.solve(adaptInfo, true, false);

    if (toDo.isSet(ESTIMATE))
      problem_.estimate(adaptInfo);

    return flag;
  }


  void StandardProblemIteration::endIteration(AdaptInfo& adaptInfo)
  {
    AMDIS_FUNCNAME("StandardProblemIteration::endIteration()");

    msg("");
    msg("end of iteration number: {}", (adaptInfo.spaceIteration() + 1));
    msg("=============================");
  }


  Flag StandardProblemIteration::buildAndAdapt(AdaptInfo& adaptInfo, Flag toDo)
  {
    Flag flag = 0, markFlag = 0;

    if (toDo.isSet(MARK))
      markFlag = problem_.markElements(adaptInfo);

    if (toDo.isSet(ADAPT) && markFlag.isSet(MESH_ADAPTED))
      flag |= problem_.adaptGrid(adaptInfo);

    if (toDo.isSet(BUILD))
      problem_.buildAfterAdapt(adaptInfo, markFlag, true, true);

    if (toDo.isSet(BUILD_RHS))
      problem_.buildAfterAdapt(adaptInfo, markFlag, false, true);

    return flag;
  }

  /// Returns the name of the problem.
  std::string const& StandardProblemIteration::name() const
  {
    return problem_.name();
  }

} // end namespace AMDiS
