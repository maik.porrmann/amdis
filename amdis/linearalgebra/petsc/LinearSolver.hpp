#pragma once

#include <petscpc.h>
#include <petscksp.h>

#include <amdis/Initfile.hpp>
#include <amdis/linearalgebra/LinearSolverInterface.hpp>
#include <amdis/linearalgebra/petsc/Interface.hpp>
#include <amdis/linearalgebra/petsc/SolverConfig.hpp>


namespace AMDiS
{
  /// \brief Wrapper around PETSc KSP and PC objects to solve a linear system
  /**
   * Configure the KSP and PC types using initfile parameters, based on an initfile prefix `[prefix]`:
   * All parameters are names as the PETSc command-line parameters with underscore replaced by space
   * and removing the *type* postfix, e.g. instead of `ksp_type` write `ksp`, instead of `mat_solver_type`
   * write `mat solver`.
   *
   * **Examples:**
   * ```
   * [prefix]->max it: 100    % maximum solver iterations
   * [prefix]->rtol: 1.e-10   % relative residual tolerance
   * [prefix]->pc: bjacobi    % preconditioner type
   * [prefix]->pc->sub ksp: preonly   % sub KSP type used on the blocks
   * [prefix]->pc->sub ksp->pc: ilu   % preconditioner of the sub KSP type
   * ```
   *
   * For the configuration of sub PC types and sub KSP types, one can use the current parameter
   * as new prefix, e.g. `[sub-prefix] := [prefix]->pc`, or `[sub-sub-prefix] := [prefix]->pc->sub ksp`.
   * Those sub PC and sub KSP types can be configured with all the possible parameters.
   *
   * For the configuration using command-line arguments possible a ksp-prefix or pc-prefix must be
   * assigned to distinguish different KSP and PC objects. Therefore, for each initfile prefix, a
   * PETSc prefix can be set: `[prefix]->prefix: name`, where `name` can be used on the commandline, e.g.
   * `-name_ksp_type fgmres`.
   **/
  template <class Matrix, class VectorX, class VectorY = VectorX>
  class LinearSolver
      : public LinearSolverInterface<Matrix,VectorX,VectorY>
  {
    using Vector = VectorX;

  public:
    /// Constructor.
    /**
     * Stores the initfile prefix to configure the ksp and pc solver.
     *
     * Reads an info flag `[prefix]->info`:
     * 0 ... No solver convergence information
     * 1 ... Minimal output, print residual every 10th iteration
     * 2 ... Full convergence output. Print residual, true residual and rel. residual every iteration.
     **/
    LinearSolver(std::string const& /*name*/, std::string const& prefix)
      : prefix_(prefix)
    {
      Parameters::get(prefix + "->info", info_);
    }

    ~LinearSolver()
    {
      finish();
    }

    /// Implements \ref LinearSolverInterface::init()
    void init(Matrix const& A) override
    {
      finish();
      KSPCreate(A.comm(), &ksp_);

      PETSc::KSPSetOperators(ksp_, A.matrix(), A.matrix());
      initKSP(ksp_, prefix_);
      initialized_ = true;
    }

    /// Implements \ref LinearSolverInterface::finish()
    void finish() override
    {
      if (initialized_)
        KSPDestroy(&ksp_);
      initialized_ = false;
    }

    /// Implements \ref LinearSolverInterface::apply()
    void apply(VectorX& x, VectorY const& b, Dune::InverseOperatorResult& stat) override
    {
      KSPSolve(ksp_, b.vector(), x.vector());

      if (info_ > 0)
        PETSc::KSPConvergedReasonView(ksp_, PETSC_VIEWER_STDOUT_WORLD);

      KSPConvergedReason reason;
      KSPGetConvergedReason(ksp_, &reason);

      stat.converged = (reason > 0);
    }

    KSP ksp() { return ksp_; }

  protected:
    // initialize the KSP solver from the initfile
    virtual void initKSP(KSP ksp, std::string prefix) const
    {
      PETSc::configKSP(ksp, prefix);

      // show details of ksp object
      if (info_ >= 10)
        KSPView(ksp, PETSC_VIEWER_STDOUT_WORLD);
    }

  protected:
    std::string prefix_;
    int info_ = 0;

    KSP ksp_;
    bool initialized_ = false;
  };

} // end namespace AMDiS
