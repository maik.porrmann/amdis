#include <dune/common/filledarray.hh>
#include <dune/grid/yaspgrid.hh>
#include <dune/functions/functionspacebases/compositebasis.hh>
#include <dune/functions/functionspacebases/powerbasis.hh>
#include <dune/functions/functionspacebases/lagrangebasis.hh>

#include <amdis/typetree/RangeType.hpp>
#include "Tests.hpp"

int main()
{
  using namespace AMDiS;
  using namespace Dune;
  using namespace Dune::Functions;
  using namespace Dune::Functions::BasisBuilder;
  using namespace Dune::Indices;

  // create grid
  FieldVector<double, 2> L; L = 1.0;
  auto s = Dune::filledArray<2>(1);
  YaspGrid<2> grid(L, s);
  auto gridView = grid.leafGridView();

  // create basis
  static const int k = 1;
  auto taylorHoodBasis = makeBasis(
    gridView,
    composite(
      power<2>(lagrange<k+1>()),
      lagrange<k>()
    ));

  auto localView = taylorHoodBasis.localView();

  for (auto const& e : elements(gridView)) {
    localView.bind(e);

    auto node = localView.tree();
    using NodeRange = Dune::TupleVector<FieldVector<double,2>, FieldVector<double,1>>;
    static_assert( std::is_same_v<NodeRange, RangeType_t<decltype(node)>>, "" );

    auto v_node = TypeTree::child(node, _0);
    using VNodeRange = FieldVector<double,2>;
    static_assert( std::is_same_v<VNodeRange, RangeType_t<decltype(v_node)>>, "" );

    auto p_node = TypeTree::child(node, _1);
    using PNodeRange = FieldVector<double,1>;
    static_assert( std::is_same_v<PNodeRange, RangeType_t<decltype(p_node)>>, "" );
  }

  return report_errors();
}
