#pragma once

#include <dune/common/fvector.hh>
#include <dune/common/fmatrix.hh>
#include <dune/common/diagonalmatrix.hh>
#include <dune/common/typeutilities.hh>

#include <amdis/common/DerivativeTraits.hpp>
#include <amdis/gridfunctions/AnalyticGridFunction.hpp>

namespace AMDiS
{
  namespace Operation
  {
    /** \addtogroup operations
     *  @{
     **/

    /// A functor that evaluates to the global coordinates
    struct CoordsFunction
    {
      using Self = CoordsFunction;

      struct Creator
      {
        template <class GridView>
        static auto create(Self const& f, GridView const& gridView)
        {
          return AnalyticGridFunction<Self, GridView>{f, gridView};
        }
      };

      template <class T, int N>
      Dune::FieldVector<T, N> const& operator()(Dune::FieldVector<T, N> const& x) const
      {
        return x;
      }

      friend int order(CoordsFunction /*f*/, int /*d*/)
      {
        return 1;
      }

      struct Derivative
      {
        template <class T, int N>
        Dune::DiagonalMatrix<T, N> const& operator()(Dune::FieldVector<T, N> const& /*x*/) const
        {
          return Dune::DiagonalMatrix<T,N>{T(1)};
        }
      };
      friend Derivative derivativeOf(CoordsFunction const& /*f*/, tag::gradient)
      {
        return Derivative{};
      }
    };


    /// A functor that evaluates to a component of the global coordinates
    struct CoordsCompFunction
    {
      using Self = CoordsCompFunction;

      struct Creator
      {
        template <class GridView>
        static auto create(Self const& f, GridView const& gridView)
        {
          return AnalyticGridFunction<Self, GridView>{f, gridView};
        }
      };

    public:
      /// Constructor. Stores the component `comp` of the coordinates.
      explicit CoordsCompFunction(int comp)
        : comp_(comp)
      {}

      template <class T, int N>
      T const& operator()(Dune::FieldVector<T, N> const& x) const
      {
        return x[comp_];
      }

      friend int order(CoordsCompFunction /*f*/, int /*d*/)
      {
        return 1;
      }

      struct Derivative
      {
        explicit Derivative(int comp)
          : comp_(comp)
        {}

        template <class T, int N>
        Dune::FieldVector<T, N> operator()(Dune::FieldVector<T, N> const& /*x*/) const
        {
          Dune::FieldVector<T, N> result(0);
          result[comp_] = T(1);

          return result;
        }

      private:
        int comp_;
      };

      friend Derivative derivativeOf(Self const& f, tag::gradient)
      {
        return Derivative{f.comp_};
      }

    private:
      int comp_;
    };

    /** @} **/

  } // end namespace Operation

  /// Generator for \ref CoordsFunction
  inline auto X()
  {
    return Operation::CoordsFunction{};
  }

  /// Generator for \ref CoordsCompFunction
  inline auto X(int comp)
  {
    return Operation::CoordsCompFunction{comp};
  }

  namespace Traits
  {
    template <>
    struct IsPreGridFunction<Operation::CoordsFunction>
      : std::true_type {};

    template <>
    struct IsPreGridFunction<Operation::CoordsCompFunction>
      : std::true_type {};
  }

} // end namespace AMDiS
