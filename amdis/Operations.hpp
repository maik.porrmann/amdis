#pragma once

/**
  * \defgroup operations Functor module
  * \brief Collection of functor that can be used in Expression and
  * in \ref ComposerGridFunction.
  *
  * Functors wrapping arithmetic and other mathematical operations
  * with definition of polynomial order (if applicable) and partial
  * derivatives w.r.t. the functor arguments.
  **/

#include <amdis/operations/Arithmetic.hpp>
#include <amdis/operations/Basic.hpp>
#include <amdis/operations/CMath.hpp>
#include <amdis/operations/Composer.hpp>
#include <amdis/operations/Composer.impl.hpp>
#include <amdis/operations/FieldMatVec.hpp>
#include <amdis/operations/MaxMin.hpp>
