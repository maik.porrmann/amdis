#pragma once

#include <optional>
#include <tuple>
#include <type_traits>

#include <amdis/common/Index.hpp>

namespace AMDiS { namespace Mpi
{
  template <class... Reqs>
  class RequestChain
  {
    static constexpr std::size_t N = sizeof...(Reqs);
    using Data = typename std::tuple_element_t<N-1, std::tuple<Reqs...>>::DataType;

    RequestChain(Reqs... reqs)
      : reqs_{reqs...}
    {}

    std::optional<Data const*> test()
    {
      auto op = std::get<0>(reqs_).test();
      return test(op, index_t<1>{});
    }

    template <class Optional, std::size_t i = 0>
    std::optional<Data const*> test(Optional op, index_t<i> = {})
    {
      if (!op)
        return {};

      auto op_next = std::get<i>(reqs_).test(op);
      return test(op_next, index_t<i+1>{});
    }

    template <class Optional>
    std::optional<Data const*> test(Optional&& result, index_t<N>)
    {
      return std::forward<Optional>(result);
    }

  protected:

    std::tuple<Reqs...> reqs_;
  };

}} // end namespace AMDiS::Mpi
