#include <amdis/AMDiS.hpp>
#include <amdis/Output.hpp>
#include <amdis/common/parallel/Communicator.hpp>
#include <amdis/common/parallel/RequestOperations.hpp>
#include <amdis/linearalgebra/AttributeSet.hpp>
#include <amdis/linearalgebra/ParallelIndexSet.hpp>

#include <dune/grid/onedgrid.hh>
#include <dune/grid/yaspgrid.hh>
#if HAVE_DUNE_SPGRID
#include <dune/grid/spgrid.hh>
#endif
#if HAVE_DUNE_ALUGRID
#include <dune/alugrid/grid.hh>
#endif
#if HAVE_DUNE_UGGRID
#include <dune/grid/uggrid.hh>
#endif

#include <dune/functions/functionspacebases/defaultglobalbasis.hh>
#include <dune/functions/functionspacebases/lagrangebasis.hh>
#include <dune/functions/functionspacebases/lagrangedgbasis.hh>
#include <dune/functions/functionspacebases/powerbasis.hh>
#include <dune/functions/functionspacebases/compositebasis.hh>

#include "Tests.hpp"
using namespace AMDiS;

template <class Basis>
void test_basis(Basis const& basis, std::string gridName, std::string basisName)
{
  msg("test grid={}, basis={}...", gridName, basisName);

  Mpi::Communicator world(basis.gridView().comm());

  using Traits = BackendTraits;
  using IndexDist = typename Traits::IndexDist<Basis>;
  using ParallelIndexSet = typename IndexDist::IndexSet;
  using RemoteIndices = typename IndexDist::RemoteIndices;

  // test the parallel indexset
  ParallelIndexSet pis;
  buildParallelIndexSet(basis, pis);

  using IdType = typename ParallelIndexSet::GlobalIndex;
  using Attribute = typename AttributeSet<ParallelIndexSet>::type;
  std::vector<std::vector<std::pair<IdType,Attribute>>> data(world.size());

  std::size_t owner = 0, notOwner = 0;
  data[world.rank()].reserve(pis.size());
  for (auto const& localPair : pis) {
    data[world.rank()].push_back({localPair.global(), localPair.local().attribute()});
    if (localPair.local().attribute() == Attribute::owner)
      owner++;
    else
      notOwner++;
  }

  Mpi::Tag tag{738541};
  if (world.rank() != 0)
    world.send(data[world.rank()], 0, tag);
  else {
    std::vector<Mpi::Request> recvRequests;
    for (int p = 1; p < world.size(); ++p)
      recvRequests.emplace_back( world.irecv(data[p], p, tag) );

    Mpi::wait_all(recvRequests.begin(), recvRequests.end());

    std::map<IdType,std::multiset<Attribute>> globalDofs;
    for (auto const& d : data)
      for (auto const& p : d)
        globalDofs[p.first].insert(p.second);

    for (auto const& dofAttributes : globalDofs) {
      // for each global DOF there is exactly 1 owner
      AMDIS_TEST(dofAttributes.second.count(Attribute::owner) == 1);
    }
  }

  // test the remote indices
  RemoteIndices remoteIndices(pis, pis, basis.gridView().comm());
  remoteIndices.template rebuild<true>();

  std::size_t remoteOwner = 0;
  for (auto const& rim : remoteIndices) {
    for (auto const& ri : *rim.second.second) {
      auto const& lip = ri.localIndexPair();
      Attribute remoteAttr = ri.attribute();
      Attribute myAttr = lip.local().attribute();
      if (myAttr != Attribute::owner && remoteAttr == Attribute::owner)
        remoteOwner++;
    }
  }

  // all notOwner DOFs must have a remote owner
  AMDIS_TEST_EQ(remoteOwner, notOwner);
}

std::string numCells(int dim)
{
  std::string result = "";
  for (int i = 0; i < dim; ++i)
    result += "8 ";
  return result;
}

template <class Grid>
void test_grid(std::string gridName)
{
  Parameters::set("mesh->num cells", numCells(Grid::dimension));
  Parameters::set("mesh->overlap", "0");

  auto grid = MeshCreator<Grid>("mesh").create();
  grid->loadBalance();

  auto gv = grid->leafGridView();
  auto const& indexSet = gv.indexSet();

  bool allSimplex = true;
  for (auto const& type : indexSet.types(0))
    allSimplex = allSimplex && type.isSimplex();

  // continuouse basis
  Ranges::forIndices<1,4>([&](auto const k) {
    if (allSimplex || k < 3) {
      using namespace Dune::Functions::BasisFactory;
      auto basis = makeBasis(gv, lagrange<k>());
      test_basis(basis, gridName, "lagrange<" + std::to_string(k.value) + ">");
    }
  });

  // discontinuous basis
  Ranges::forIndices<1,5>([&](auto const k) {
    using namespace Dune::Functions::BasisFactory;
    auto basis = makeBasis(gv, lagrangeDG<k>());
    test_basis(basis, gridName, "lagrangeDG<" + std::to_string(k.value) + ">");
  });

  // Taylor-Hood basis
  {
    using namespace Dune::Functions::BasisFactory;
    auto basis = makeBasis(gv, composite(power<Grid::dimensionworld>(lagrange<2>(), flatInterleaved()), lagrange<1>(), flatLexicographic()));
    test_basis(basis, gridName, "TaylorHood");
  }
}


int main(int argc, char** argv)
{
  Environment env(argc, argv);

#if GRID_ID == 0
  test_grid<Dune::YaspGrid<2>>("YaspGrid<2>");
  test_grid<Dune::YaspGrid<3>>("YaspGrid<3>");
#elif GRID_ID == 1 && HAVE_DUNE_UGGRID
  Parameters::set("mesh->structured", "cube");
  test_grid<Dune::UGGrid<2>>("UGGrid<2,cube>");
  test_grid<Dune::UGGrid<3>>("UGGrid<3,cube>");
#elif GRID_ID == 2 && HAVE_DUNE_ALUGRID
  Parameters::set("mesh->structured", "cube");
  test_grid<Dune::ALUGrid<2,2,Dune::cube,Dune::nonconforming>>("ALUGrid<2,cube,nonconforming>");
  test_grid<Dune::ALUGrid<3,3,Dune::cube,Dune::nonconforming>>("ALUGrid<3,cube,nonconforming>");
  //test_grid<Dune::ALUGrid<2,3,Dune::cube,Dune::nonconforming>>();
#elif GRID_ID == 3 && HAVE_DUNE_SPGRID
  test_grid<Dune::SPGrid<double,2>>("SPGrid<2>");
  test_grid<Dune::SPGrid<double,3>>("SPGrid<3>");
#elif GRID_ID == 4 && HAVE_DUNE_UGGRID
  Parameters::set("mesh->structured", "simplex");
  test_grid<Dune::UGGrid<2>>("UGGrid<2,simplex>");
  test_grid<Dune::UGGrid<3>>("UGGrid<3,simplex>");
#elif GRID_ID == 5 && HAVE_DUNE_ALUGRID
  Parameters::set("mesh->structured", "simplex");
  test_grid<Dune::ALUGrid<2,2,Dune::simplex,Dune::nonconforming>>("ALUGrid<2,simplex,nonconforming>");
  test_grid<Dune::ALUGrid<3,3,Dune::simplex,Dune::nonconforming>>("ALUGrid<3,simplex,nonconforming>");
  //test_grid<Dune::ALUGrid<2,3,Dune::simplex,Dune::nonconforming>>();
#elif GRID_ID == 6 && HAVE_DUNE_ALUGRID
  Parameters::set("mesh->structured", "simplex");
  test_grid<Dune::ALUGrid<2,2,Dune::simplex,Dune::conforming>>("ALUGrid<2,simplex,conforming>");
  test_grid<Dune::ALUGrid<3,3,Dune::simplex,Dune::conforming>>("ALUGrid<2,simplex,conforming>");
  //test_grid<Dune::ALUGrid<2,3,Dune::simplex,Dune::conforming>>();
#endif

  return env.mpiRank() == 0 ? report_errors() : 0;
}
