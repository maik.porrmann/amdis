#include <config.h>

#include <dune/grid/yaspgrid.hh>

#include <dune/functions/functionspacebases/compositebasis.hh>
#include <dune/functions/functionspacebases/lagrangebasis.hh>
#include <dune/functions/functionspacebases/powerbasis.hh>

#include <amdis/Environment.hpp>
#include <amdis/typetree/Traversal.hpp>
#include <amdis/typetree/TreeContainer.hpp>

#include "Tests.hpp"

using namespace AMDiS;

int main (int argc, char** argv)
{
  Environment env(argc, argv);

  Dune::YaspGrid<2> grid({1.0,1.0}, {1,1});
  auto gridView = grid.leafGridView();

  using namespace Dune::Functions::BasisFactory;
  auto basis = makeBasis(gridView,
    composite(
      power<2>(lagrange<2>()),
      lagrange<1>()
    ));

  auto localView = basis.localView();
  auto const& tree = localView.tree();

  auto c1 = TypeTree::treeContainer<double>(tree);
  auto c2 = TypeTree::treeContainer<decltype(c1)>(tree);
  auto c3 = TypeTree::treeContainer(tree, [&](auto const&) { return TypeTree::treeContainer<double>(tree); });

  // fill 1d treeContainer with data
  Traversal::forEachLeafNode(tree, [&](auto const& node, auto tp) {
    c1[tp] = double(node.treeIndex());
  });

  // copy construction
  auto c4 = c1;
  AMDIS_TEST(c4 == c1);

  // fill 2d treeContainer with data
  Traversal::forEachLeafNode(tree, [&](auto const& row_node, auto row_tp) {
    Traversal::forEachLeafNode(tree, [&](auto const& col_node, auto col_tp) {
      c3[row_tp][col_tp] = double(row_node.treeIndex() + col_node.treeIndex());
    });
  });

  // copy construction
  auto c5 = c3;
  AMDIS_TEST(c5 == c3);

  // copy-assignment of container
  c2 = c3;
  AMDIS_TEST(c2 == c3);

  return report_errors();
}
