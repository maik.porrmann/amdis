#include <config.h>

#include "ProblemStat.hpp"

#include <dune/grid/yaspgrid.hh>

namespace AMDiS
{
  // explicit template instatiation
  template class ProblemStat<LagrangeBasis<Dune::YaspGrid<2>,1>>;
  template class ProblemStat<LagrangeBasis<Dune::YaspGrid<2>,1,1>>;

} // end namespace AMDiS
