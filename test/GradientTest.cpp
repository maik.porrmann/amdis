// -*- tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 2 -*-
// vi: set et ts=4 sw=2 sts=2:

#include <iostream>

#include <amdis/AMDiS.hpp>

#include <amdis/AdaptiveGrid.hpp>
#include <amdis/LinearAlgebra.hpp>
#include <amdis/GridFunctions.hpp>
#include <amdis/Integrate.hpp>
#include <amdis/gridfunctions/DiscreteFunction.hpp>

#if HAVE_DUNE_UGGRID
  #include <dune/grid/uggrid.hh>
#else
  #include <dune/grid/yaspgrid.hh>
#endif

#include <dune/grid/utility/structuredgridfactory.hh>

#include "Tests.hpp"

using namespace AMDiS;

template <std::size_t p, class HostGrid>
void test(HostGrid& hostGrid)
{
  std::cout << "[p = " << p << "]" << std::endl;
  using Basis1 = LagrangeBasis<HostGrid,p,p>;
  AdaptiveGrid<HostGrid> grid(hostGrid);
  auto const& gridView = grid.leafGridView();

  auto basis = Basis1::create(gridView);

  auto uVector = makeDOFVector(basis);

  auto u = valueOf(uVector);
  auto u_0 = valueOf(uVector, 0);
  auto u_1 = valueOf(uVector, 1);

  // eval a functor at global coordinates (at quadrature points)
  u_0 << evalAtQP([](auto const& x) { return x[0] + x[1]; });
  u_1 << evalAtQP([](auto const& x) { return -2*x[0] + 3*x[1]; });

  // test gradient
  AMDIS_TEST_APPROX((std::sqrt(integrate(unary_dot(gradientOf(u_0)), gridView))), std::sqrt(2.0));
  AMDIS_TEST_APPROX((std::sqrt(integrate(unary_dot(gradientOf(u_1)), gridView))), std::sqrt(13.0));

  // test divergence
  AMDIS_TEST_APPROX((integrate(divergenceOf(u), gridView)), 4.0);

  // test partial derivative
  double d0u_0 = integrate(partialDerivativeOf(u_0,0), gridView);
  double d1u_1 = integrate(partialDerivativeOf(u_1,1), gridView);
  AMDIS_TEST_APPROX(d0u_0, 1.0);
  AMDIS_TEST_APPROX(d1u_1, 3.0);

  auto gf_d0u_0 = makeGridFunction(partialDerivativeOf(u_0,0), gridView);
  auto lf_d0u_0 = localFunction(gf_d0u_0);
  for (auto const& e : elements(gridView)) {
    lf_d0u_0.bind(e);
    double v = lf_d0u_0(e.geometry().center());
    AMDIS_TEST_APPROX(v, 1.0);
    lf_d0u_0.unbind();
  }

  u_0 << evalAtQP([](auto const& x) { return std::sin(x[0]) + std::cos(x[1]); });
  u_1 << evalAtQP([](auto const& x) { return -std::sin(x[0]) * std::cos(x[1]); });

  AMDIS_TEST_APPROX((integrate(divergenceOf(u), gridView)),
                    (integrate(partialDerivativeOf(u_0,0) + partialDerivativeOf(u_1,1), gridView)));

  auto vVector(uVector);
  auto v = valueOf(vVector);
  auto v_0 = valueOf(vVector, 0);
  auto v_1 = valueOf(vVector, 1);

  // test gradient
  v << evalAtQP([](auto const& x) {
    return Dune::FieldVector<double,2>{std::cos(x[0]), -std::sin(x[1])};
  });
  AMDIS_TEST((std::sqrt(integrate(unary_dot(v - gradientOf(u_0)), gridView))) < 0.05);

  // test divergence
  v_0 << evalAtQP([](auto const& x) { return std::cos(x[0]) + std::sin(x[0])*std::sin(x[1]); });
  AMDIS_TEST((integrate(v_0 - divergenceOf(u), gridView)) < 0.05);

  // test partial derivative
  v_0 << evalAtQP([](auto const& x) { return std::cos(x[0]); });
  v_1 << evalAtQP([](auto const& x) { return std::sin(x[0])*std::sin(x[1]); });
  AMDIS_TEST((integrate(v_0 - partialDerivativeOf(u_0,0), gridView)) < 0.05);
  AMDIS_TEST((integrate(v_1 - partialDerivativeOf(u_1,1), gridView)) < 0.05);

  { // test the gradient interpolation

    using namespace Dune::Functions::BasisFactory;
    DOFVector U0{gridView, lagrange<p>()};
    DOFVector gradU0{gridView, power<HostGrid::dimensionworld>(lagrange<p>())};
    valueOf(U0) << valueOf(uVector,0);
    valueOf(gradU0) << gradientOf(U0);
    [[maybe_unused]] auto integral1 = integrate(valueOf(gradU0,0), gridView);
  }

  { // test the divergence interpolation

    using namespace Dune::Functions::BasisFactory;
    DOFVector divU{gridView, lagrange<p>()};
    valueOf(divU) << divergenceOf(uVector);
    [[maybe_unused]] auto integral1 = integrate(valueOf(divU), gridView);
  }

  { // test the partial derivative interpolation

    using namespace Dune::Functions::BasisFactory;
    DOFVector U0{gridView, lagrange<p>()};
    DOFVector partialU0{gridView, lagrange<p>()};
    valueOf(U0) << valueOf(uVector,0);
    valueOf(partialU0) << partialDerivativeOf(U0,0);
    [[maybe_unused]] auto integral1 = integrate(valueOf(partialU0), gridView);
  }

}

int main(int argc, char** argv)
{
  Environment env(argc, argv);

#if HAVE_DUNE_UGGRID
  std::cout << std::endl << "UGGrid<Simplex>..." << std::endl;
  {
    using HostGrid = Dune::UGGrid<2>;
    auto numElements = Dune::filledArray<2,unsigned int>(16);
    auto hostGridPtr = Dune::StructuredGridFactory<HostGrid>::createSimplexGrid({0.0,0.0}, {1.0,1.0}, numElements);

    test<1u>(*hostGridPtr);
    test<2u>(*hostGridPtr);
    test<3u>(*hostGridPtr);
    test<4u>(*hostGridPtr);
  }

  std::cout << std::endl << "UGGrid<Cube>..." << std::endl;
  {
    using HostGrid = Dune::UGGrid<2>;
    auto numElements = Dune::filledArray<2,unsigned int>(16);
    auto hostGridPtr = Dune::StructuredGridFactory<HostGrid>::createCubeGrid({0.0,0.0}, {1.0,1.0}, numElements);

    test<1u>(*hostGridPtr);
    test<2u>(*hostGridPtr);
    test<3u>(*hostGridPtr);
    test<4u>(*hostGridPtr);
  }
#else
  std::cout << "YaspGrid..." << std::endl;
  {
    using HostGrid = Dune::YaspGrid<2>;
    HostGrid hostGrid({1.0, 1.0}, {16,16});

    test<1u>(hostGrid);
    test<2u>(hostGrid);
    test<3u>(hostGrid);
    test<4u>(hostGrid);
  }
#endif

  return report_errors();
}
