#pragma once

#include <memory>
#include <optional>
#include <string>

#include <dune/istl/owneroverlapcopy.hh>
#include <dune/istl/solvercategory.hh>
#include <dune/istl/paamg/pinfo.hh>

#include <amdis/Environment.hpp>
#include <amdis/functions/GlobalIdSet.hpp>
#include <amdis/linearalgebra/IndexDistribution.hpp>
#include <amdis/linearalgebra/ParallelIndexSet.hpp>

namespace AMDiS
{
  /// Dummy implementation for ISTL-specific index distribution when no MPI is found
  class SequentialISTLIndexDistribution
  {
    using Impl = Dune::Amg::SequentialInformation;

  public:
    using Sequential = SequentialISTLIndexDistribution;

    SequentialISTLIndexDistribution() = default;

    template <class Basis>
    SequentialISTLIndexDistribution(Basis const&)
    {}

    typename Dune::SolverCategory::Category category() const
    {
      return Dune::SolverCategory::sequential;
    }

    Impl const& impl() const
    {
      return impl_;
    }

    Sequential const& sequential() const
    {
      return *this;
    }

    template <class Basis>
    void update(Basis const&) { /* do nothing */ }

  private:
    Impl impl_;
  };


  template <class G, class L, class Comm>
  struct ISTLIndexDistributionType
  {
    using type = SequentialISTLIndexDistribution;
  };

#if HAVE_MPI
  /// Implementation class for ISTL-specific communication to be used in parallel solvers
  template <class GlobalId, class LocalIndex>
  class ISTLIndexDistribution
  {
    using Impl = Dune::OwnerOverlapCopyCommunication<GlobalId, LocalIndex>;

  public:
    using Sequential = SequentialISTLIndexDistribution;
    using IndexSet = typename Impl::ParallelIndexSet;
    using RemoteIndices = typename Impl::RemoteIndices;

  public:
    template <class Basis>
    ISTLIndexDistribution(Basis const& basis);

    IndexSet const& indexSet() const
    {
      assert(bool(impl_));
      return impl_->indexSet();
    }

    RemoteIndices const& remoteIndices() const
    {
      assert(bool(impl_));
      return impl_->remoteIndices();
    }

    typename Dune::SolverCategory::Category category() const
    {
      return cat_;
    }

    Impl const& impl() const
    {
      assert(bool(impl_));
      return *impl_;
    }

    Sequential sequential() const
    {
      return Sequential{};
    }

    template <class Basis>
    void update(Basis const& basis)
    {
      impl_ = std::make_unique<Impl>(MPI_Comm(basis.gridView().comm()), cat_);

      auto& pis = impl_->indexSet();
      buildParallelIndexSet(basis, pis);

      auto& ris = impl_->remoteIndices();
      ris.setIndexSets(pis, pis, impl_->communicator());
      ris.template rebuild<true>();
    }

  private:
    typename Dune::SolverCategory::Category cat_;
    std::unique_ptr<Impl> impl_ = nullptr;
  };

  template <class G, class L>
  struct ISTLIndexDistributionType<G,L,Dune::Communication<MPI_Comm>>
  {
    using type = ISTLIndexDistribution<G,L>;
  };

#endif

  template <class B>
  using ISTLIndexDistribution_t
    = typename ISTLIndexDistributionType<GlobalIdType_t<B>, typename B::size_type, typename B::GridView::CollectiveCommunication>::type;

} // end namespace AMDiS

#include <amdis/linearalgebra/istl/IndexDistribution.inc.hpp>
