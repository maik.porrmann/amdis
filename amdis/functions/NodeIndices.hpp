#pragma once

#include <dune/common/rangeutilities.hh>

#include <amdis/common/Concepts.hpp>
#include <amdis/typetree/MultiIndex.hpp>
#include <amdis/utility/MappedRangeView.hpp>

namespace AMDiS
{
  /// Returns a range over (flat) DOF indices on a node, given by the localView
  template <class LocalView, class Node>
  auto nodeIndices(LocalView const& localView, Node const& node)
  {
    static_assert(Concepts::LocalView<LocalView>, "");
    static_assert(Concepts::BasisTree<Node, typename LocalView::GridView>, "");

    return mappedRangeView(Dune::range(node.size()), [&](std::size_t j) -> typename LocalView::MultiIndex {
      return localView.index(node.localIndex(j));
    });
  }

  /// Returns a range over (flat) DOF indices on the basis tree, given by the localView
  template <class LocalView>
  auto nodeIndices(LocalView const& localView)
  {
    static_assert(Concepts::LocalView<LocalView>, "");

    return mappedRangeView(Dune::range(localView.size()), [&](std::size_t i) -> typename LocalView::MultiIndex {
      return localView.index(i);
    });
  }


  /// Returns the number of DOF indices on a node, given by the localView
  template <class LocalView, class Node>
  std::size_t nodeIndexCount(LocalView const& /*localView*/, Node const& node)
  {
    return node.size();
  }

  /// Returns the number of DOF indices on the basis tree, given by the localView
  template <class LocalView>
  std::size_t nodeIndexCount(LocalView const& localView)
  {
    return localView.size();
  }

} // end namespace AMDiS
