#pragma once

#include <cstdint>

namespace AMDiS
{
  /** \ingroup Common
   * \brief
   * The Flag class encapsulates flags which represents simple information.
   * Used e.g. while mesh traversal to specify, which elements should be
   * visited.
   */
  class Flag
  {
  public:
    /// Constructs a unset Flag
    constexpr Flag() = default;

    /// Constructs a Flag initialized by f
    constexpr Flag(const std::uint64_t f)
      : flags_(f)
    {}

    /// Copy constructor
    constexpr Flag(Flag const&) = default;

    /// Move constructor
    constexpr Flag(Flag&&) = default;

    /// Compares two Flags
    constexpr bool operator==(Flag const& f) const
    {
      return (flags_ == f.flags_);
    }

    /// Compares two Flags
    constexpr bool operator!=(Flag const& f) const
    {
      return !(f == *this);
    }

    /// Assignment operator
    constexpr Flag& operator=(Flag const& f)
    {
      if (this != &f)
        flags_ = f.flags_;
      return *this;
    }

    /// Typecast
    constexpr operator bool() const
    {
      return isAnySet();
    }

    /// Set \ref flags_
    constexpr void setFlags(const std::uint64_t f)
    {
      flags_ = f;
    }

    /// Set \ref flags_
    constexpr void setFlags(Flag const& f)
    {
      flags_ = f.flags_;
    }

    /// Sets \ref flags_ to \ref flags_ | f
    constexpr void setFlag(const std::uint64_t f)
    {
      flags_ |= f;
    }

    /// Sets \ref flags_ to \ref flags_ | f.flags_
    constexpr void setFlag(Flag const& f)
    {
      flags_ |= f.flags_;
    }

    /// Sets \ref flags_ to \ref flags_ & ~f
    constexpr void unsetFlag(const std::uint64_t f)
    {
      flags_ &= ~f;
    }

    /// Sets \ref flags_ to \ref flags_ & ~f.flags_
    constexpr void unsetFlag(Flag const& f)
    {
      flags_ &= ~f.flags_;
    }

    constexpr std::uint64_t flags() const
    {
      return flags_;
    }

    /// Returns \ref flags_ | f.flags_
    friend Flag operator+(Flag r, Flag const& f)
    {
      r.setFlag(f);
      return r;
    }

    /// Returns \ref flags_ & ~f.flags_
    friend Flag operator-(Flag r, Flag const& f)
    {
      r.unsetFlag(f);
      return r;
    }

    /// Returns \ref flags_ | f.flags_
    friend Flag operator|(Flag r, Flag const& f)
    {
      r.setFlag(f);
      return r;
    }

    /// Returns \ref flags_ & f.flags_
    friend Flag operator&(Flag r, Flag const& f)
    {
      r.flags_ &= f.flags_;
      return r;
    }

    /// Sets \ref flags_ to \ref flags_ &= f.flags_
    constexpr Flag operator&=(Flag const& f)
    {
      flags_ &= f.flags_;
      return *this;
    }

    /// Returns \ref flags_ ^ f.flags_
    friend Flag operator^(Flag r, Flag const& f)
    {
      r.flags_ ^= f.flags_;
      return r;
    }

    /// Sets \ref flags_ to \ref flags_ & f.flags_
    constexpr Flag& operator|=(Flag const& f)
    {
      if (this != &f)
        flags_ |= f.flags_;
      return *this;
    }

    /// Returns ~\ref flags_
    constexpr Flag operator~() const
    {
      Flag r;
      r.flags_ = ~flags_;
      return r;
    }

    /// Checks whether all set bits of f.flags_ are set in \ref flags_ too.
    constexpr bool isSet(Flag const& f) const
    {
      return ((flags_ & f.flags_) == f.flags_);
    }

    /// Returns !\ref isSet(f)
    constexpr bool isUnset(Flag const& f) const
    {
      return !isSet(f);
    }

    /// Returns true if \ref flags_ != 0
    constexpr bool isAnySet() const
    {
      return (flags_ != 0);
    }

  protected:
    /// Internal flag representation
    std::uint64_t flags_ = 0;
  };

} // end namespace AMDiS
