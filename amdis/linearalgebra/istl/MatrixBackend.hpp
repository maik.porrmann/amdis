#pragma once

#include <list>
#include <string>
#include <memory>

#include <dune/istl/bcrsmatrix.hh>
#include <dune/istl/matrixindexset.hh>

#include <amdis/Output.hpp>
#include <amdis/linearalgebra/SparsityPattern.hpp>
#include <amdis/linearalgebra/SymmetryStructure.hpp>

namespace AMDiS
{
  template <class T, class = void>
  struct BlockMatrixType
  {
    using type = Dune::FieldMatrix<T,1,1>;
  };

  template <class T>
  struct BlockMatrixType<T, typename T::field_type>
  {
    using type = T;
  };

  template <class T, class CommType>
  class ISTLBCRSMatrix
  {
  public:
    /// The matrix type of the underlying base matrix
    using BaseMatrix = Dune::BCRSMatrix<typename BlockMatrixType<T>::type>;

    /// Communication type
    using Comm = CommType;

    /// The type of the elements of the DOFMatrix
    using value_type = typename BaseMatrix::block_type;

    /// The index/size - type
    using size_type = typename BaseMatrix::size_type;

  public:
    /// Constructor. Constructs new BaseVector.
    ISTLBCRSMatrix(Comm const& comm, Comm const&)
      : comm_(comm)
    {}

    /// Return the data-vector \ref vector
    BaseMatrix const& matrix() const
    {
      return matrix_;
    }

    /// Return the data-vector \ref vector
    BaseMatrix& matrix()
    {
      return matrix_;
    }

    Comm const& comm() const { return comm_; }

    /// Create occupation pattern and apply it to the matrix
    void init(SparsityPattern const& pattern)
    {
      pattern.applyTo(matrix_);
      initialized_ = true;
    }

    /// Set all entries to zero while keeping the occupation pattern intact
    void init()
    {
      matrix_ = value_type(0);
      initialized_ = true;
    }

    void finish()
    {
      initialized_ = false;
    }


    /// Insert a single value into the matrix (add to existing value)
    void insert(size_type r, size_type c, value_type const& value)
    {
      test_exit_dbg( initialized_, "Occupation pattern not initialized!");
      test_exit_dbg( r < matrix_.N() && c < matrix_.M() ,
          "Indices out of range [0,{})x[0,{})", matrix_.N(), matrix_.M() );
      matrix_[r][c] += value;
    }

    template <class Ind, class LocalMat>
    void scatter(Ind const& idx, LocalMat const& mat)
    {
      scatter(idx, idx, mat);
    }

    template <class RowInd, class ColInd, class LocalMat>
    void scatter(RowInd const& rows, ColInd const& cols, LocalMat const& mat)
    {
      test_exit_dbg( initialized_, "Occupation pattern not initialized!");
      for (size_type i = 0; i < size_type(rows.size()); ++i)
        for (size_type j = 0; j < size_type(cols.size()); ++j)
          matrix_[rows[i]][cols[j]] += mat[i][j];
    }

    /// Return the number of entries allocated in the sparsity pattern of the matrix
    std::size_t nnz() const
    {
      return matrix_.nonzeroes();
    }

  private:
    BaseMatrix matrix_;
    Comm const& comm_;

    bool initialized_ = false;
  };

} // end namespace AMDiS
