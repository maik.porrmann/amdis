#pragma once

#include <map>
#include <memory>
#include <optional>
#include <utility>
#include <vector>

#include <dune/common/concept.hh>
#include <dune/common/shared_ptr.hh>
#include <amdis/Observer.hpp>

namespace AMDiS
{
  /// \brief An adaptive container that stores a value per grid element
  /**
   * \tparam G  Type of the grid
   * \tparam T  Type of the coefficients
   **/
  template <class G, class T = double>
  class ElementVector
      : private Observer<event::preAdapt>
      , private Observer<event::adapt>
      , private Observer<event::postAdapt>
  {
    using Self = ElementVector;

  public:
    /// Type of the grid
    using Grid = G;

    /// The index/size - type
    using size_type  = std::size_t;

    /// The type of the elements of the ElementVector
    using value_type = T;

    /// The data container for the ElementVector
    using Data = std::vector<T>;

  public:
    /// (1) Constructor. Stores the shared_ptr of the grid.
    ElementVector(std::shared_ptr<G const> const& grid, T value = 0)
      : Observer<event::preAdapt>(*grid)
      , Observer<event::adapt>(*grid)
      , Observer<event::postAdapt>(*grid)
      , grid_(grid)
      , data_(grid->size(0), value)
    {}

    /// (2) Constructor. Forwards to (1) by wrapping into a shared_ptr.
    ElementVector(G const& grid, T value = 0)
      : ElementVector(Dune::wrap_or_move(FWD(grid)), value)
    {}

    /// Return the GridView the data is defined on
    auto gridView() const
    {
      return grid_->leafGridView();
    }

    /// Get a const-ref to the internal data vector
    Data const& data() const
    {
      return data_;
    }

    /// Get a ref to the internal data vector
    Data& data()
    {
      return data_;
    }

    /// Resize the internal data without interpolating
    void resize()
    {
      data_.resize(grid_->size(0));
    }

    /// Resize the internal data without interpolating and set all values to 0
    void resizeZero()
    {
      data_.resize(grid_->size(0));
      std::fill(data_.begin(), data_.end(), T(0));
    }

  protected:

    /// Implementation of Observer update(event::preAdapt) method.
    void updateImpl(event::preAdapt e) override
    {
      persistentData_.emplace();

      // Store the data in a persistent container
      auto const& indexSet = grid_->leafIndexSet();
      auto const& idSet = grid_->localIdSet();
      for (auto const& e : elements(grid_->leafGridView())) {
        (*persistentData_)[idSet.id(e)] = data_[indexSet.index(e)];

        // store also the same value on all father elements
        if (e.mightVanish() && e.hasFather()) {
          T value = data_[indexSet.index(e)];
          auto father = e.father();
          for (; ; father = father.father()) {
            (*persistentData_)[idSet.id(father)] = value;
            if (!father.hasFather())
              break;
          }
        }
      }
    }

    /// Implementation of Observer update(event::adapt) method.
    void updateImpl(event::adapt e) override
    {
      resize();

      // interpolate the data
      auto const& indexSet = grid_->leafIndexSet();
      auto const& idSet = grid_->localIdSet();
      for (auto const& e : elements(grid_->leafGridView()))
      {
        T value = 0;
        auto father = e;
        if (e.isNew() && e.hasFather())
          father = e.father();

        for (; ; father = father.father()) {
          auto it = persistentData_->find(idSet.id(father));
          if (it != persistentData_->end()) {
            value = it->second;
            break;
          }
          if (!father.hasFather())
            break;
        }

        data_[indexSet.index(e)] = value;
      }
    }

    /// Implementation of Observer update(event::postAdapt) method.
    void updateImpl(event::postAdapt) override
    {
      persistentData_.reset();
    }

  private:
    std::shared_ptr<Grid const> grid_;
    Data data_;

    std::optional<std::map<typename Grid::LocalIdSet::IdType, T>> persistentData_;
  };

} // end namespace AMDiS
