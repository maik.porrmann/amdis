install(FILES
    AddAmdisExecutable.cmake
    AmdisMacros.cmake
    AmdisCXXFeatures.cmake
    FindHYPRE.cmake
    FindMTL.cmake
    FindPETSc.cmake
    PkgConfigLinkLibraries.cmake
  DESTINATION ${DUNE_INSTALL_MODULEDIR})
