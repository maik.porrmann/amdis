# Getting Started

[TOC]

## Download and Install
AMDiS is a Finite-Element discretization library implemented as a
[Dune](https://www.dune-project.org) module. This means, it depends on several
other Dune modules, mainly the core modules and some staging modules. And it
means that it can be extended by other Dune modules.

Additionally, it depends on external libraries, like linear solvers, graph
partitioners, multi-precision arithmetic, or parallel communication libraries.
Those dependencies are often optional and may be installed when needed, but
sometimes require the Dune modules to be rebuild. So, we start with a common
set of external libraries here.

### Installing External Libraries
A recommended collection of external packages include a direct solver, the message
passing interface and sequential/parallel graph partitioner. Additionally, we
recommend to install a package that defines an external grid type to be used with
Dune.

- [Alberta](http://www.alberta-fem.de/download.html)
- [METIS](http://glaros.dtc.umn.edu/gkhome/metis/metis/overview)
- [OpenMPI](https://www.open-mpi.org)
- [ParMETIS](http://glaros.dtc.umn.edu/gkhome/metis/parmetis/overview)
- [SuiteSparse](http://faculty.cse.tamu.edu/davis/suitesparse.html)

On recent Linux distributions, all of these libraries are available as prebuild
package. As an example, on a recent Debian based system, you can simply use `apt-get`:

```bash
sudo apt-get install \
  libalberta-dev \
  libmetis-dev \
  libopenmpi-dev \
  libparmetis-dev \
  libsuitesparse-dev
```

Other distributions may have similar packages. For an installation from source,
visit the corresponding website of the library.

### Installing Dune
The Dune modules can be installed as a prebuild package in your (Linux) distribution,
by downloading source packages, or by cloning a Git repository. See the following
resources for information of getting and installing Dune

- <https://www.dune-project.org/doc/beginners-resources>
- <https://www.dune-project.org/doc/installation>

One way is described here: Cloning a Git repository. The easiest way to organize
your Dune dependencies is to create a new directory, call it `DUNE_DIR`, and to
download all modules into this directory

```bash
mkdir DUNE_DIR
cd DUNE_DIR
git clone https://gitlab.dune-project.org/core/dune-common
git clone https://gitlab.dune-project.org/core/dune-geometry
git clone https://gitlab.dune-project.org/core/dune-grid
git clone https://gitlab.dune-project.org/core/dune-istl
git clone https://gitlab.dune-project.org/core/dune-localfunctions
git clone https://gitlab.dune-project.org/staging/dune-functions
git clone https://gitlab.dune-project.org/staging/dune-typetree
git clone https://gitlab.dune-project.org/staging/dune-uggrid
```

Building the Dune modules can be done using the shipped `dunecontrol` script. It
resolves the inter-module dependencies and calls *cmake* and *make* in the Dune
sources directories.

To control the building parameters (e.g. cmake parameters or make parameters),
you may pass a config file with the `--opts=` parameter, or by setting environment
variables. Here, the second option is explained. Option files are documented in
the links above.

```bash
cd DUNE_DIR
export CMAKE_FLAGS="-DCMAKE_BUILD_TYPE=RelWithDebInfo"
export MAKE_FLAGS="-j4"
dune-common/bin/dunecontrol all
```

### Installing AMDiS
Assume that you have downloaded the AMDiS source into the directory `AMDIS_DIR`, i.e.,

```bash
git clone https://gitlab.com/amdis/amdis.git AMDIS_DIR
```

Then we can use the `dunecontrol` script from the Dune installation to configure
and build AMDiS as well. Therefore, we need to tell the `dunecontrol` script
where to find the Dune modules. This is dune by setting the `DUNE_CONTROL_PATH`
variable.

```bash
cd AMDIS_DIR
export DUNE_CONTROL_PATH=DUNE_DIR
DUNE_DIR/dune-common/bin/dunecontrol --current all
```

Again, the build parameters can be controlled by the `CMAKE_FLAGS` and `MAKE_FLAGS`
variables as above.

!!! note
    If `AMDIS_DIR` is a sub-directory of `DUNE_DIR`, one could call `dunecontrol` from the
    `DUNE_DIR` directly and in combination with the configuration of the other dune modules
    as above. This allows to use just one call to configure and build all dune and AMDiS
    modules and no need to set the `DUNE_CONTROL_PATH` variable.


## Starting a new Project
Creating a new project that uses AMDiS is fairly simple, by using the shipped
`amdisproject` script. It expects to find the Dune modules and AMDiS is the
`DUNE_CONTROL_PATH` directories and gets a name of the new project as first
argument. All other (optional) information is asked by the script.

```bash
cd PROJECT_DIR
export DUNE_CONTROL_PATH=AMDIS_DIR:DUNE_DIR
AMDIS_DIR/bin/amdisproject my_first_project
```

This will create a new folder `PROJECT_DIR/my_first_project` and fills it with
some commonly used files and directory structure, especially a `CMakeLists.txt`
file, a `dune.module` file and some example source code. This allows to build
your project with the `dunecontrol` script from Dune.

```bash
cd PROJECT_DIR/my_first_project
export DUNE_CONTROL_PATH=AMDIS_DIR:DUNE_DIR
DUNE_DIR/dune-common/bin/dunecontrol --current all
```

Now, you can add your code, modify the cmake files and rebuild and then just run

```bash
build-cmake/src/my_first_project init/my_first_project.dat
```

## My first AMDiS Project

### The Poisson Equation
As an example of usage, we want to discretize an elliptic PDE, the Poisson equation,
$` -\Delta u = f `$ in $` \Omega `$ with $` u = g `$ on a subset of the boundary
$` \Gamma\subset\partial\Omega `$. For simplicity, we assume $` f(x) \equiv -1 `$
and $` g(x) \equiv 0 `$, the domain $` \Omega `$ a square domain $` [0,1]^2 `$
and $` \Gamma `$ the lower and left edge of the boundary.

```c++
#include <amdis/AMDiS.hpp>
#include <amdis/AdaptInfo.hpp>
#include <amdis/ProblemStat.hpp>

// A dune grid type
#include <dune/grid/uggrid.hh>

// Aliasing the grid type for shorter typing
using Grid = Dune::UGGrid<2>;

// The namespace all AMDiS classes and functions are defined in
using namespace AMDiS;

// A dune-functions globalBasis tight to a grid type,
// here representing local polynomial shape functions of degree 1 on a
// 2 dimensional UGGrid
using Traits = LagrangeBasis<Grid, 1>;

int main(int argc, char* argv[])
{
  // Every AMDiS program should start with an environment that
  // initializes the linear-algebra backend and read parameters from file
  Environment env(argc, argv);

  // Create a problem class containing all data for assembling
  ProblemStat<Traits> prob("poisson");
  // Initialize grid, globalBasis, solution vector and system matrix
  prob.initialize(INIT_ALL);

  // An operator representing the weak laplacian with coefficient = 1.0
  auto opL = makeOperator(tag::gradtest_gradtrial{}, 1.0);
  prob.addMatrixOperator(opL);

  // A rhs-operator representing an analytic function f(x) = -1
  auto opF = makeOperator(tag::test{}, [](auto const& x) { return -1.0; }, 0);
  prob.addVectorOperator(opF);

  // Define the boundary Gamma
  auto predicate = [](auto const& x){ return x[0] < 1.e-8 || x[1] < 1.e-8; };
  // Set a value g(x) = 0 at this part of the boundary
  prob.addDirichletBC(predicate, 0.0);

  // Create a container that stores information about the solution process
  AdaptInfo adaptInfo("adapt");

  // assemble and solve the linear system
  prob.assemble(adaptInfo);
  prob.solve(adaptInfo);

  // Write the solution to a file specified in the initfile;
  prob.writeFiles(adaptInfo, false);
}
```

### Summary
An AMDiS program consists of several main ingredients:

1. A Grid and GlobalBasis defining the discretization of the domain and function space.
2. A Problem class that holds all information necessary for assembling a linear
   system.
3. Operators describing the (bi)linear-form of your PDE.
4. A parameter file controlling several parts of the solution process.
