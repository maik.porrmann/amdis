# MatrixFacade and VectorFacade {: #group-matvecbase }
## Summary
The class [`VectorFacade`](#class-vectorfacade) is a base class and interface class for all vectors associated to the
indices of a global basis and, correspondingly, the class [`MatrixFacade`](#class-matrixfacade) is a base class and interface
class for all matrices associated to the indices of a row and column global basis.

### Classes

 Class                                | Descriptions
--------------------------------------|-----------------------------------------
[`MatrixFacade`](#class-matrixfacade) | Base class for matrices
[`VectorFacade`](#class-vectorfacade) | Base class for vectors


## class `MatrixFacade`
Defined in header [`<amdis/linearalgebra/MatrixFacade.hpp>`](https://gitlab.com/amdis/amdis/blob/master/amdis/linearalgebra/MatrixFacade.hpp)

```c++
template <class T, template <class> class MatrixImpl>
class MatrixFacade
```

`MatrixFacade` is an interface class for matrices storing the implementation and rediricting to its interface functions.

The template parameter `T` defines the coefficient type of the matrix, `MatrixImpl` refers to the actual implementation of the matrix data-structure.

### Member Types

 Member Type  | Definition
--------------|---------------------------------------------------------------
`Impl`        | The Linear-Algebra implementation used to store the assembled coefficients


## class `VectorFacade`
Defined in header [`<amdis/linearalgebra/VectorFacade.hpp>`](https://gitlab.com/amdis/amdis/blob/master/amdis/linearalgebra/VectorFacade.hpp)

```c++
template <class T, template <class> class VectorImpl>
class VectorFacade
```

`VectorFacade` is an interface class for vectors storing the implementation and rediricting to its interface functions.

The template parameter `T` defines the coefficient type of the vector, `VectorImpl` refers to the actual implementation of the vector data-structure.

### Member Types

 Member Type | Definition
-------------|----------------------------------------------------------------
`Impl`       | The Linear-Algebra implementation used to store the assembled coefficients

### Member functions

 Function                       | Descriptions
--------------------------------|---------------------------------------------
[*(constructor)*](#function-vectorfacadevectorfacade)  | Construct the VectorFacade
[`impl`](#function-vectorfacadeimpl)                   | Return the vector implementation

**Capacity**

 Function                       | Descriptions
--------------------------------|---------------------------------------------
[`localSize,globalSize`](#function-vectorfacadesize)   | The number of entries in the local/global part of the vector
[`resize,resizeZero`](#function-vectorfacaderesize)    | Resize the vector to the size of the basis
[`init`](#function-vectorfacadeinit)                   | Prepare the vector for insertion of values
[`finish`](#function-vectorfacadefinish)               | Finish the insertion of values

**Element access**

 Function                       | Descriptions
--------------------------------|---------------------------------------------
[`at`](#function-vectorfacadeat)                       | Return the value of the vector at the given local index
[`gather`](#function-vectorfacadegather)               | Extract values from the vector referring to the given local indices

**Modifiers**

 Function                       | Descriptions
--------------------------------|---------------------------------------------
[`insert,set,add`](#function-vectorfacadeinsert)       | Insert a single value into the matrix (add or overwrite to existing value)
[`scatter`](#function-vectorfacadescatter)             | Insert a block of values into the vector
[`forEach`](#function-vectorfacadescatter)             | Apply a functor to each value at given indices


## function `VectorFacade::VectorFacade`
```c++
VectorFacade(GlobalBasis const& basis)
```

Constructor that passes the global basis to the implementation.


## function `VectorFacade::impl`
```c++
Impl const& impl() const    // (1)
Impl&       impl()          // (2)
```
Return the underlying linear algebra implementation


## function `VectorFacade::localSize,globalSize` {: #function-vectorfacadesize }
```c++
std::size_t localSize() const     // (1)
std::size_t globalSize() const    // (2)
```
Return the number of entries in the (1) local part of the vector, or (2) in the global vector.


## function `VectorFacade::resize,resizeZero` {: #function-vectorfacaderesize }
```c++
std::size_t resize() const        // (1)
std::size_t resizeZero() const    // (2)
```
Resize the vector to the size of the basis (1) and in case of (2) also set all entries to zero.


## function `VectorFacade::init`
```c++
void init(bool clear)
```
Prepare the vector for insertion of values, finish the insertion with a call to [finish()](#function-vectorfacadefinish).

#### Arguments
`bool clear`
:   If true, sets the vector to zero before insertion


## function `VectorFacade::finish`
```c++
void finish()
```
Finish the insertion of values. Must be called at the end of the insertion. See also [init()](#function-vectorfacadeinit).


## function `VectorFacade::at` {: #function-vectorfacadeat }
```c++
template <class Index>
typename Backend::value_type at(Index const& dof) const
```
Return the value of the vector at the local index. Note, an internal synchronization of the ghost values of the vector
might be necessary. This is a collective communication operation. This communication is performed only if the vector is in
unsynchronized state.

!!! note
    Prefer a block operation to retrieve data, like [gather()](#function-vectorfacadegather).

#### Arguments
`Index dof`
:   Any (Multi-)index type. Currently only flat multiindices are supported. This will be relaxed in future versions of the library.


## function `VectorFacade::gather`
```c++
// (1)
template <class Node, class Buffer>
void gather(LocalView const& localView, Node const& node, Buffer& buffer) const
// (2)
template <class Buffer>
void gather(LocalView const& localView, Buffer& buffer) const
```
Collect value at indices and store them into a buffer. The buffer must be
a vector-like container with `buffer.resize()` and `buffer.begin()`. The
indices are defined by (1) the `nodeIndices(localView, node)` or (2) the
`nodeIndices(localView)`.

If the vector is not in synchronized state, collects all necessary values
possibly from neighboring processors. Gather can not be called during insertion
state. This must be closed using [finish()](#function-vectorfacadefinish) before.

#### Arguments
`LocalView localView`
:   The localView object of a global basis

`Node node`
:   A node in the basis tree

`Buffer buffer`
:   A resizable output range with `resize()` and `begin()` member functions.

#### Requirements
- `Node` must be a model of the concept `Dune::Functions::Concept::BasisNode`

#### Preconditions
- **expects:** `localView` is bound to an element
- **expects:** `node` is in `localView.tree()`


## function `VectorFacade::insert,set,add` {: #function-vectorfacadeinsert }
```c++
// (1)
template <class Index, class Assign = Assigner::plus_assign>
void insert(Index const& dof, typename Backend::value_type const& value,
            Assign assign = {})

// (2)
template <class Index>
void set(Index const& dof, typename Backend::value_type const& value)

// (3)
template <class Index>
void add(Index const& dof, typename Backend::value_type const& value)
```
(1) Inserts or adds a value into a certain location `dof` (given as dof multi-index)
of a vector. The insertion mode is determined by the `assign` functor. Use
`Assigner::plus_assign` for adding values (default) or `Assigner::assign`
for overwriting (setting) values. Different insertion modes must not be mixed!

Insertion must be closed with a call to [finish()](#function-vectorfacadefinish).

(2) and (3) are abbreviations for the general `insert()` method (1) by fixing the insertion mode.

!!! note
    Prefer a block operation to insert data, like [scatter()](#function-vectorfacadescatter).

#### Arguments
`Index dof`
:   Any (Multi-)index type. Currently only flat multiindices are supported. This will be relaxed in future versions of the library.

`value_type value`
:   The single value to insert or to add to the container

`Assign assign`
:   An assignment functor with the signature `void operator()(T const& from, S&& to) const`


## function `VectorFacade::scatter`
```c++
// (1)
template <class Node, class NodeVector, class MaskRange, class Assign>
void scatter(LocalView const& localView, Node const& node, NodeVector const& nodeVec,
             MaskRange const& mask, Assign assign)
// (2)
template <class Node, class NodeVector, class Assign>
void scatter(LocalView const& localView, Node const& node, NodeVector const& nodeVec,
             Assign assign)
// (3)
template <class NodeVector, class Assign>
void scatter(LocalView const& localView, NodeVector const& nodeVec, Assign assign)
```
Inserts or adds values into certain locations of a vector. Insertion indices
are extracted from the given `localView`. The insertion mode is determined
by the `assign` functor. Use `Assigner::plus_assign` for adding values
(default) or `Assigner::assign` for overwriting (setting) values. Different
insertion modes can not be mixed! The `nodeVec` is assumed to be a continuous
memory container with a `data()` method to get a pointer to the beginning and must
have the same size as the `node`.

The mask models a boolean range with at least a `begin()` method. Must
be forward iterable for at least `localVector.size()` elements. Does not
need an `end()` method. See, e.g. `FakeContainer`.

Insertion must be closed with a call to \ref finish(). It is not allowed
to switch insertion mode before calling [finish()](#function-vectorfacadefinish).

(2) Same as (1) but all indices are inserted.

(3) Same as (2) but `node == localView.tree()`

#### Arguments
`LocalView localView`
:   The localView object of a global basis

`Node node`
:   A node in the basis tree

`NodeVector nodeVec`
:   Continuous range with a `data()` method to get a pointer to the beginning

`MaskRange mask`
:   An boolean forward iterable range with at least a `begin()` member functions.

#### Requirements
- `Node` must be a model of the concept `Dune::Functions::Concept::BasisNode`

#### Preconditions
- **expects:** Vector is either in the same insertion mode as before (no mixing of different insertion modes), or in non-insertion mode.
- **expects:** `localView` is bound to an element
- **expects:** `node` is in `localView.tree()`
- **expects:** `nodeVec` is of size `node.size()`
- **expects:** `mask` must be forward iterable for at least `nodeVec.size()` elements

### Postconditions
- **ensures:** Vector is in insertion mode defined by the `assign` functor.
