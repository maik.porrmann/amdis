# How to backup and restore problem data?

In very long simulations, i.e. many timesteps, it is recommended to backup an
intermediate state from time to time, so that it is possible to resume a
simulation from the last stored backup file. Therefore, you mostly need
a representation of the grid in memory incorporating the grid hierarchy and
the data for the solution vector. Other parameters are fixed or problem specific,
so need to be stored manually.

In the `ProblemStat` there is a pair of functions `backup()` and `restore()`
that can be used for this purpose. The backup function saves the grid and the
solution vector to files and the restore function initializes a new problem from
stored files.

```c++
ProblemStat prob("prob");
prob.initialize(INIT_ALL);
AdaptInfo adaptInfo("adapt");

// some calculation ...

prob.backup(adaptInfo);
```

This create at least two files, one for the grid and one for the solution, where
the filenames are either fixed to `backup_TIMESTEP.grid` and `backup_TIMESTEP.solution`
or specified in the initfile as

```
prob->backup->grid: backup_xyz.grid
prob->backup->solution: backup_xyz.solution
```

To restart a simulation it is recommended to initialize the problem directly with
the grid and solution file, i.e.

```c++
ProblemStat prob("prob");
prob.restore(INIT_ALL);

// some more calculation ...
```

where the restore filenames **must** be given in the initfile as

```
prob->restore->grid: backup_xyz.grid
prob->restore->solution: backup_xyz.solution
```
