#pragma once

#include <type_traits>

#include <petscmat.h>
#include <petscvec.h>

#include <dune/grid/common/partitionset.hh>
#include <amdis/linearalgebra/petsc/IndexDistribution.hpp>
#include <amdis/linearalgebra/petsc/MatrixNnzStructure.hpp>
#include <amdis/linearalgebra/petsc/MatrixBackend.hpp>
#include <amdis/linearalgebra/petsc/VectorBackend.hpp>

namespace AMDiS
{
  /** Traits class for a linear solver for the system AX=B using an FE space described by a dune-functions Basis
   *  Contains typedefs specific to the PETSc backend.
   */
  struct PetscTraits
  {
    template <class Basis>
    using IndexDist = PetscIndexDistribution_t<Basis>;

    template <class RowBasis, class ColBasis>
    struct Matrix
    {
      using SparsityPattern = MatrixNnzStructure;

      static_assert(std::is_same_v<IndexDist<RowBasis>, IndexDist<ColBasis>>);

      template <class Value>
      using Impl = PetscMatrix<IndexDist<RowBasis>>;
    };

    template <class Basis>
    struct Vector
    {
      template <class Value>
      using Impl = PetscVector<IndexDist<Basis>>;
    };

    using PartitionSet = Dune::Partitions::Interior;
  };

  using BackendTraits = PetscTraits;

} // end namespace AMDiS
