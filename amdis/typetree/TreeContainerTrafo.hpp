#pragma once

#include <amdis/algorithm/ForEach.hpp>
#include <amdis/algorithm/Map.hpp>
#include <amdis/typetree/TreeContainer.hpp>

namespace AMDiS {
namespace Recursive {

// specializations of recursive utilities for TreeContainer entries

template <class Value>
struct Map<TypeTree::LeafNodeStorage<Value>>
{
  template <class F, class VC>
  static auto impl(F&& f, VC const& vc)
  {
    return TypeTree::LeafNodeStorage{
      Recursive::map(f,vc.value())};
  }
};

template <class Value, class Container>
struct Map<TypeTree::InnerNodeStorage<Value,Container>>
{
  template <class F, class VC>
  static auto impl(F&& f, VC const& vc)
  {
    return TypeTree::InnerNodeStorage{
      Recursive::map(f,vc.value()),
      Recursive::map(f,vc.container())};
  }
};

template <class Container>
struct Map<TypeTree::TreeContainerStorage<Container>>
{
  template <class F, class TC>
  static auto impl(F&& f, TC const& c)
  {
    return TypeTree::TreeContainerStorage{Recursive::map(f,c.data())};
  }
};

// -------------------------------------------------------------------------


template <class Value>
struct ForEach<TypeTree::LeafNodeStorage<Value>>
{
  template <class VC, class F>
  static void impl(VC&& vc, F&& f)
  {
    Recursive::forEach(vc.value(),f);
  }
};

template <class Value, class Container>
struct ForEach<TypeTree::InnerNodeStorage<Value,Container>>
{
  template <class VC, class F>
  static void impl(VC&& vc, F&& f)
  {
    Recursive::forEach(vc.value(),f);
    Recursive::forEach(vc.container(),f);
  }
};

template <class Container>
struct ForEach<TypeTree::TreeContainerStorage<Container>>
{
  template <class F, class TC>
  static void impl(TC&& c, F&& f)
  {
    Recursive::forEach(c.data(),f);
  }
};

}} // end namespace AMDiS::Recursive
