#pragma once

#include <type_traits>

#include <amdis/localoperators/FirstOrderTestPartialTrial.hpp>

namespace AMDiS
{
  /**
   * \addtogroup operators
   * @{
   **/

  namespace tag
  {
    struct partialtest_trial
    {
      std::size_t comp;
    };

    struct partial_test
    {
      std::size_t comp;
    };
  }


  /// first-order operator \f$ \langle\partial_i\psi, c\,\phi\rangle \f$
  template <class LC>
  struct GridFunctionOperatorRegistry<tag::partialtest_trial, LC>
  {
    static constexpr int degree = 1;
    static tag::test_partialtrial transposedTag(tag::partialtest_trial t)
    {
      return {t.comp};
    }
    using type = FirstOrderTestPartialTrial;
  };


  /// Create a first-order term with derivative on trial-function
  template <class Expr>
  auto fot(Expr&& expr, tag::partial_test t, int quadOrder = -1)
  {
    return makeOperator(tag::partialtest_trial{t.comp}, FWD(expr), quadOrder);
  }

  /** @} **/

} // end namespace AMDiS
