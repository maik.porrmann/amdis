#pragma once

// std c++ headers
#include <cassert>
#include <string>

#include <dune/common/parallel/mpihelper.hh>

namespace AMDiS
{
  /// Establishes an environment for sequential and parallel AMDiS programs
  /**
   * This object initializes the MPI environment, parses initfiles and may
   * initialize other external libraries. It is constructed with the program
   * commandline arguments. In its destruction, the MPI environment is finalized.
   * In the vast majority of AMDiS programs, an instance of Environment should
   * be declared at the very beginning of the main function.
   **/
  class Environment
  {
    // internal static container holding a pointer to the Dune::MPIHelper.
    struct Mpi
    {
      static Mpi& instance()
      {
        static Mpi mpi;
        return mpi;
      }

      void registerMpiHelper(Dune::MPIHelper& mpiHelper)
      {
        mpiHelper_ = &mpiHelper;
      }

      int rank()
      {
        assert(mpiHelper_ != nullptr);
        return mpiHelper_->rank();
      }

      int size()
      {
        assert(mpiHelper_ != nullptr);
        return mpiHelper_->size();
      }

      Dune::MPIHelper& mpiHelper()
      {
        return *mpiHelper_;
      }

    private:
      Dune::MPIHelper* mpiHelper_ = nullptr;
    };

  public:
    /// Create an environment without mpi initialization, with a fixed initfile given as string
    Environment(std::string const& initFileName = "");

    /// Create an environment with initialization of MPI and initifiles from commandline arguments
    /// or the provided initfile filename.
    Environment(int& argc, char**& argv, std::string const& initFileName = "");

    /// Finishes MPI and PETSc
    ~Environment();

    /// Return the MPI_Rank of the current processor.
    static int mpiRank()
    {
      return Mpi::instance().rank();
    }

    /// Return the MPI_Size of the group created by Dune::MPIHelper.
    static int mpiSize()
    {
      return Mpi::instance().size();
    }

    /// Return a reference to the stored \ref MPIHelper
    static Dune::MPIHelper& mpiHelper()
    {
      return Mpi::instance().mpiHelper();
    }

    /// Return the MPI_Comm object (or a fake communicator)
    static typename Dune::MPIHelper::MPICommunicator comm()
    {
      return Dune::MPIHelper::getCommunicator();
    }

    /// Return the info level for messages in `info()`
    static int infoLevel();

    /// Return whether all ranks print `msg()`
    static bool msgAllRanks();

  private:
#if AMDIS_HAS_PETSC
    bool petscInitialized_ = false;
#endif
  };

} // end namespace AMDiS
