#pragma once

#include <cassert>
#include <type_traits>

#include <amdis/GridFunctionOperatorTransposed.hpp>
#include <amdis/Output.hpp>
#include <amdis/common/Order.hpp>
#include <amdis/common/TypeTraits.hpp>
#include <amdis/utility/QuadratureFactory.hpp>

namespace AMDiS
{
  /**
   * \addtogroup operators
   * @{
   **/

  template <class LF, class Imp>
  class GridFunctionLocalOperator;


  /// \brief The main implementation of an operator depending on a grid-function
  /**
   * An Operator that takes a grid-function as coefficient.
   * Generates a \ref GridFunctionLocalOperator on \ref localOperator()
   *
   * The class implements the interface of an \ref Operator.
   *
   * \tparam GF   The class type of the grid-function
   * \tparam Imp  Class providing the local assembling method, forwarded to
   *              GridFunctionLocalOperator class
   *
   * **Requirements:**
   * - `GF` models the \ref Concepts::GridFunction
   **/
  template <class GF, class Imp>
  class GridFunctionOperator
  {
  public:
    using GridFunction = GF;
    using Implementation = Imp;

    /// \brief Constructor. Stores a copy of `gridFct` and `impl`.
    /**
     * A GridFunctionOperator takes a grid-function and
     * an implementation class for the assemble method.
     **/
    template <class GridFct, class Impl>
    GridFunctionOperator(GridFct&& gridFct, Impl&& impl,
                         int derivDeg, int gridFctOrder)
      : gridFct_(FWD(gridFct))
      , impl_(FWD(impl))
      , derivDeg_(derivDeg)
      , gridFctOrder_(gridFctOrder)
    {}

    template <class GridView>
    void update(GridView const&) { /* do nothing */ }

    friend auto localOperator(GridFunctionOperator const& op)
    {
      return GridFunctionLocalOperator{localFunction(op.gridFct_), op.impl_,
        op.derivDeg_, op.gridFctOrder_};
    }

  private:
    /// The grid-function associated to this operator
    GridFunction gridFct_;

    /// An implementation class for the assembling
    Implementation impl_;

    /// Maximal degree of derivative this operator represents
    int derivDeg_;

    /// Polynomial degree of the grid-function (or -1)
    int gridFctOrder_;
  };

  template <class GridFct, class Impl>
  GridFunctionOperator(GridFct const& gridFct, Impl const& impl, int, int)
    -> GridFunctionOperator<GridFct, Impl>;


  /// \brief The main implementation of a local-operator depending on a local-function
  /**
   * A LocalOperator that takes a local-function as coefficient.
   * Provides quadrature rules and passes the local-function, bound to an element,
   * to the assemble method of an implementation class.
   *
   * The class implements the interface of a \ref LocalOperator.
   *
   * \tparam LF   The class type of the local-function
   * \tparam Imp  Class providing the local assembling method
   *
   * **Requirements:**
   * - `LF` models the \ref Concepts::LocalFunction
   **/
  template <class LF, class Imp>
  class GridFunctionLocalOperator
  {
  private:
    /// The type of the localFunction
    using LocalFunction = LF;

    /// Type of the implementation class
    using Implementation = Imp;

  public:
    /// \brief Constructor. Stores a copy of `localFct` and `impl`.
    /**
     * A GridFunctionLocalOperator takes a local-function, an implementation class,
     * the differentiation order of the operator and the local-function polynomial
     * degree, to calculate the quadrature degree of the operator
     **/
    template <class LocalFct, class Impl>
    GridFunctionLocalOperator(LocalFct&& localFct, Impl&& impl,
                              int derivDeg, int localFctOrder)
      : localFct_(FWD(localFct))
      , impl_(FWD(impl))
      , derivDeg_(derivDeg)
      , localFctOrder_(localFctOrder)
    {}

    /// \brief Binds operator to `element`.
    /**
     * By default, it binds the \ref localFct_ to the `element`.
     **/
    template <class Element>
    void bind(Element const& element)
    {
      localFct_.bind(element);
    }

    /// Unbinds operator from element.
    void unbind()
    {
      localFct_.unbind();
    }

    /// Assemble a local element matrix on the element that is bound.
    /**
     * This function calls the assemble method from the impl_ class and
     * additionally passes a quadrature rule and  the localFct_ to that method.
     **/
    template <class CG, class RN, class CN, class Mat>
    void assemble(CG const& contextGeo, RN const& rowNode, CN const& colNode,
                  Mat& elementMatrix) const
    {
      auto const& quad = getQuadratureRule(contextGeo.localContext().geometry(),
        derivDeg_, localFctOrder(), rowNode, colNode);
      impl().assemble(contextGeo, rowNode, colNode, quad, localFct_, elementMatrix);
    }

    /// Assemble a local element vector on the element that is bound.
    /**
     * This function calls the assemble method from the impl_ class and
     * additionally passes a quadrature rule and the localFct_ to that method.
     **/
    template <class CG, class Node, class Vec>
    void assemble(CG const& contextGeo, Node const& node,
                  Vec& elementVector) const
    {
      auto const& quad = getQuadratureRule(contextGeo.localContext().geometry(),
        derivDeg_, localFctOrder(), node);
      impl().assemble(contextGeo, node, quad, localFct_, elementVector);
    }

    Implementation      & impl()       { return impl_; }
    Implementation const& impl() const { return impl_; }

  protected:
    // Return the order of the coefficients function, either localFctOrder_ or
    // the order of the local-function
    int localFctOrder() const
    {
      if (localFctOrder_ >= 0)
        return localFctOrder_;
      else {
        int coeffOrder = -1;
        // Try to get polynomial order of local-functions
        if constexpr (Concepts::Polynomial<LF>)
          coeffOrder = order(localFct_);

        test_exit(coeffOrder >= 0,
          "Polynomial degree of coefficients cannot be determined. "
          "Please provide a quadrature order manually.");

        return coeffOrder;
      }
    }

  private:
    /// The local-function to be used within the operator
    LocalFunction localFct_;

    /// Implementation details of the assembling
    Implementation impl_;

    /// Maximal degree of derivative this operator represents
    int derivDeg_;

    /// Polynomial degree of the local-function (or -1)
    int localFctOrder_;
  };

  // deduction guide
  template <class LocalFct, class Impl>
  GridFunctionLocalOperator(LocalFct const& localFct, Impl const& impl, int, int)
    -> GridFunctionLocalOperator<LocalFct, Impl>;


  /// Registry to specify a tag for each implementation type
  template <class Tag, class LocalContext>
  struct GridFunctionOperatorRegistry {};

  template <class Tag, class Expr>
  struct OperatorTerm
  {
    Tag tag;
    Expr expr;
    int gridFctDeg;

    OperatorTerm(Tag tag, Expr const& expr, int gridFctDeg = -1)
      : tag(tag)
      , expr(expr)
      , gridFctDeg(gridFctDeg)
    {}
  };

  /// Store tag and expression into a \ref OperatorTerm to create
  /// a \ref GridFunctionOperator
  template <class Tag, class Expr>
  auto makeOperator(Tag const& tag, Expr&& expr, int gridFctDeg = -1)
  {
    return OperatorTerm{tag, FWD(expr), gridFctDeg};
  }

  template <class Tag, class Expr>
  auto operatorTerm(Tag const& tag, Expr&& expr, int gridFctDeg = -1)
  {
    return OperatorTerm{tag, FWD(expr), gridFctDeg};
  }

  /** @} **/

#ifndef DOXYGEN

  template <class R, class Tag>
  using IsTransposed = decltype(R::transposedTag(std::declval<Tag>()));

  /// Generate an \ref GridFunctionOperator from a OperatorTerm (tag, expr).
  /// @{
  template <class Context, class Tag, class Expr, class GridView>
  auto makeOperator(OperatorTerm<Tag,Expr> const& op, GridView const& gridView)
  {
    auto gf = makeGridFunction(std::move(op.expr), gridView);

    using Registry = GridFunctionOperatorRegistry<Tag,Context>;
    if constexpr (Dune::Std::is_detected_v<IsTransposed,Registry,Tag>) {
      auto impl = typename Registry::type{Registry::transposedTag(op.tag)};
      return GridFunctionOperatorTransposed{
        GridFunctionOperator{std::move(gf), std::move(impl), Registry::degree, op.gridFctDeg}
      };
    } else {
      auto impl = typename Registry::type{op.tag};
      return GridFunctionOperator{std::move(gf), std::move(impl), Registry::degree, op.gridFctDeg};
    }
  }
  /// @}

#endif // DOXYGEN

} // end namespace AMDiS
