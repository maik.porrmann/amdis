#pragma once

#include <amdis/common/FieldMatVec.hpp>
#include <amdis/operations/Arithmetic.hpp>
#include <amdis/operations/Basic.hpp>
#include <amdis/operations/CMath.hpp>
#include <amdis/operations/Composer.hpp>

namespace AMDiS
{
  namespace Operation
  {
    /** \addtogroup operations
     *  @{
     **/

    /// (Binary-)Functor representing the euclidean dot-product
    struct Dot
    {
      template <class T0, class T1, int N>
      constexpr auto operator()(Dune::FieldVector<T0,N> const& lhs, Dune::FieldVector<T1,N> const& rhs) const
      {
        return lhs.dot(rhs);
      }

      friend constexpr int order(Dot const&, int d1, int d2)
      {
        return d1 + d2;
      }

      friend constexpr auto partial(Dot const&, index_t<0>)
      {
        return Arg<1>{};
      }

      friend constexpr auto partial(Dot const&, index_t<1>)
      {
        return Arg<0>{};
      }
    };

    // -------------------------------------------------------------------------

    /// (Unary-)Functor representing the euclidean dot-product
    struct UnaryDot
    {
      template <class V>
      constexpr auto operator()(V const& vec) const
      {
        using Dune::unary_dot;
        return unary_dot(vec);
      }

      friend constexpr int order(UnaryDot const&, int d)
      {
        return 2*d;
      }

      friend auto partial(UnaryDot const&, index_t<0>)
      {
        return compose(Multiplies{}, StaticConstant<int,2>{}, Id{});
      }
    };

    // -------------------------------------------------------------------------

    /// (Unary-)Functor representing the euclidean 2-norm
    struct TwoNorm
        : public Composer<Sqrt, UnaryDot>
    {
      constexpr TwoNorm()
        : Composer<Sqrt, UnaryDot>(Sqrt{}, UnaryDot{})
      {}

      template <class V>
      constexpr auto operator()(V const& vec) const
      {
        using Dune::two_norm;
        return two_norm(vec);
      }
    };

    // -------------------------------------------------------------------------

    struct Trans
    {
      template <class M>
      constexpr auto operator()(M const& mat) const
      {
        using Dune::trans;
        return trans(mat);
      }

      friend constexpr int order(Trans const&, int d)
      {
        return d;
      }
    };


    /** @} **/

  } // end namespace Operation
} // end namespace AMDiS
