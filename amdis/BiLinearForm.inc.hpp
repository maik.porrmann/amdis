#pragma once

#include <utility>

#include <amdis/ContextGeometry.hpp>
#include <amdis/GridFunctionOperator.hpp>
#include <amdis/LocalOperator.hpp>
#include <amdis/typetree/Traversal.hpp>

namespace AMDiS {

template <class RB, class CB, class T, class Traits>
  template <class ContextTag, class Expr, class RowTreePath, class ColTreePath>
void BiLinearForm<RB,CB,T,Traits>::
addOperator(ContextTag contextTag, Expr const& expr,
            RowTreePath row, ColTreePath col)
{
  static_assert( Concepts::PreTreePath<RowTreePath>,
      "row must be a valid treepath, or an integer/index-constant");
  static_assert( Concepts::PreTreePath<ColTreePath>,
      "col must be a valid treepath, or an integer/index-constant");

  auto i = makeTreePath(row);
  auto j = makeTreePath(col);

  using LocalContext = typename ContextTag::type;
  auto op = makeOperator<LocalContext>(expr, this->rowBasis().gridView());
  operators_[i][j].push(contextTag, std::move(op));
  updatePattern_ = true;
}


template <class RB, class CB, class T, class Traits>
  template <class RowLocalView, class ColLocalView, class LocalOperators,
    REQUIRES_(Concepts::LocalView<RowLocalView>),
    REQUIRES_(Concepts::LocalView<ColLocalView>)>
void BiLinearForm<RB,CB,T,Traits>::
assemble(RowLocalView const& rowLocalView, ColLocalView const& colLocalView,
         LocalOperators& localOperators)
{
  assert(rowLocalView.bound());
  assert(colLocalView.bound());

  elementMatrix_.resize(rowLocalView.size(), colLocalView.size());
  elementMatrix_ = 0;

  auto const& gv = this->rowBasis().gridView();
  auto const& element = rowLocalView.element();
  GlobalContext<TYPEOF(gv)> context{gv, element, element.geometry()};

  Traversal::forEachNode(rowLocalView.treeCache(), [&](auto const& rowCache, auto rowTp) {
    Traversal::forEachNode(colLocalView.treeCache(), [&](auto const& colCache, auto colTp) {
      auto& matOp = localOperators[rowTp][colTp];
      matOp.bind(element);
      matOp.assemble(context, rowCache, colCache, elementMatrix_);
      matOp.unbind();
    });
  });

  this->scatter(rowLocalView, colLocalView, elementMatrix_);
}


template <class RB, class CB, class T, class Traits>
void BiLinearForm<RB,CB,T,Traits>::
assemble()
{
  auto rowLocalView = this->rowBasis().localView();
  auto colLocalView = this->colBasis().localView();

  this->init();
  auto localOperators = AMDiS::localOperators(operators_);
  for (auto const& element : entitySet(this->rowBasis())) {
    rowLocalView.bind(element);
    if constexpr(std::is_same_v<RB,CB>) {
      if (this->rowBasis_ == this->colBasis_)
        this->assemble(rowLocalView, rowLocalView, localOperators);
      else {
        colLocalView.bind(element);
        this->assemble(rowLocalView, colLocalView, localOperators);
        colLocalView.unbind();
      }
    }
    else {
      colLocalView.bind(element);
      this->assemble(rowLocalView, colLocalView, localOperators);
      colLocalView.unbind();
    }
    rowLocalView.unbind();
  }
  this->finish();
}


} // end namespace AMDiS
