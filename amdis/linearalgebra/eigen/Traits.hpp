#pragma once

#include <dune/grid/common/partitionset.hh>
#include <amdis/linearalgebra/IndexDistribution.hpp>
#include <amdis/linearalgebra/eigen/MatrixSize.hpp>
#include <amdis/linearalgebra/eigen/MatrixBackend.hpp>
#include <amdis/linearalgebra/eigen/VectorBackend.hpp>

namespace AMDiS
{
  /** Traits class for a linear solver for the system AX=B using an FE space described by a dune-functions Basis
   *  Contains typedefs specific to the EIGEN backend.
   */
  struct EigenTraits
  {
    template <class>
    using IndexDist = DefaultIndexDistribution;

    template <class,class>
    struct Matrix
    {
      using SparsityPattern = MatrixSize;
      template <class Value>
      using Impl = EigenSparseMatrix<Value, Eigen::RowMajor>;
    };

    template <class>
    struct Vector
    {
      template <class Value>
      using Impl = EigenVector<Value>;
    };

    using PartitionSet = Dune::Partitions::All;
  };

  using BackendTraits = EigenTraits;

} // end namespace AMDiS
