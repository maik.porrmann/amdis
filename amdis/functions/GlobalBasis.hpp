#pragma once

#include <algorithm>
#include <list>
#include <memory>
#include <type_traits>
#include <utility>

#include <dune/common/concept.hh>
#include <dune/common/reservedvector.hh>
#include <dune/common/shared_ptr.hh>
#include <dune/common/typeutilities.hh>
#include <dune/common/version.hh>
#include <dune/functions/common/type_traits.hh>
#include <dune/functions/functionspacebases/concepts.hh>
#include <dune/functions/functionspacebases/defaultglobalbasis.hh>
#include <dune/functions/functionspacebases/flatmultiindex.hh>
#include <dune/grid/common/adaptcallback.hh>
#include <dune/typetree/treepath.hh>

#include <amdis/AdaptiveGrid.hpp>
#include <amdis/Observer.hpp>
#include <amdis/Output.hpp>
#include <amdis/common/Concepts.hpp>
#include <amdis/common/TypeTraits.hpp>
#include <amdis/functions/FlatPreBasis.hpp>
#include <amdis/functions/LocalView.hpp>
#include <amdis/linearalgebra/IndexDistribution.hpp>
#include <amdis/linearalgebra/Traits.hpp>
#include <amdis/typetree/MultiIndex.hpp>

namespace AMDiS
{
  /**
   * \brief Global basis defined on a pre-basis
   *
   * This class is an expansion to Dune::Functions::DefaultGlobalBasis<PB>.
   * It adds a communicator to use the basis in parallel as well as automatic
   * update functionality.
   *
   * \tparam PB  Pre-basis providing the implementation details
   */
  template <class PB>
  class GlobalBasis
      : public Dune::Functions::DefaultGlobalBasis<PB>
      , public Notifier<event::adapt>
      , private Observer<event::adapt>
  {
    using Self = GlobalBasis<PB>;
    using Super = Dune::Functions::DefaultGlobalBasis<PB>;

  public:
    /// Pre-basis providing the implementation details
    using PreBasis = PB;

    /// The grid view that the FE space is defined on
    using GridView = typename PreBasis::GridView;
    using Grid = typename GridView::Grid;

    /// Type of the local view on the restriction of the basis to a single element
    using LocalView = AMDiS::LocalView<Self>;

    /// Type of the communicator
    using IndexDist = BackendTraits::IndexDist<Self>;

    struct DummyImpl {};
    using ADH = Dune::AdaptDataHandle<Grid, DummyImpl>;

  public:
    /// Construct this global basis with given name and grid, and constructing a preBasis.
    /**
     * \param name     Name associated with this basis for initfile parameter attachment.
     * \param grid     The Grid providing the GridView for this basis
     * \param args...  Argument list for PreBasis
     *
     * This will forward all arguments to the constructor of PreBasis
     */
    template <class... Args,
      Dune::Functions::enableIfConstructible<PreBasis, Args...> = 0>
    GlobalBasis(std::string const& name, Grid const& grid, Args&&... args)
      : Super(FWD(args)...)
      , Observer<event::adapt>(grid)
      , indexDist_(static_cast<Super const&>(*this))
    {}

    /// Construct this global basis with a preBasisFactory
    template <class PBF>
    GlobalBasis(std::string const& name, GridView const& gridView,
                PBF const& preBasisFactory)
      : GlobalBasis(name, gridView.grid(), flatPreBasis(preBasisFactory(gridView)))
    {}

    /// Construct this global basis with empty name
    template <class Arg, class... Args,
      REQUIRES(!std::is_same_v<std::string, remove_cvref_t<Arg>>)>
    GlobalBasis(Arg&& arg, Args&&... args)
      : GlobalBasis(std::string(""), FWD(arg), FWD(args)...)
    {}

    /// Copy constructor
    GlobalBasis(GlobalBasis const&) = delete;

    /// Move constructor
    GlobalBasis(GlobalBasis&&) = default;

  public:
    /// \brief Update the stored grid view
    /**
     * This will update the indexing information of the global basis as well as
     * the communicator. It is called automatically if the grid has changed.
     */
    void update(GridView const& gv)
    {
      Super::preBasis().update(gv);
      Super::preBasis().initializeIndices();
      indexDist_.update(*this);
    }

    /// Return local view for basis
    LocalView localView() const
    {
      return LocalView(*this);
    }

    /// Return local view as shared_ptr to prevent from copy construction
    std::shared_ptr<LocalView> localViewPtr() const
    {
      return std::make_shared<LocalView>(*this);
    }

    /// Return the set of entities this basis can be bound to
    auto entitySet() const
    {
      return elements(this->gridView(), BackendTraits::PartitionSet{});
    }

    /// Return *this because we are not embedded in a larger basis
    GlobalBasis const& rootBasis() const
    {
      return *this;
    }

    /// \brief Return the index distribution.
    /**
     * This provides the means to communicate indices associated to the basis with
     * other processes.
     **/
    IndexDist const& indexDistribution() const  { return indexDist_; }
    IndexDist&       indexDistribution()        { return indexDist_; }

    ADH globalRefineCallback() const
    {
      // TODO(FM): Implement
      error_exit("Not implemented: GlobalBasis::globalRefineCallback()");
      return ADH{};
    }

  protected:
    /// Updates the underlying basis when event::adapt is triggered by the observed grid
    void updateImpl(event::adapt e) override
    {
      if (e.value) {
        update(Super::gridView());
        Notifier<event::adapt>::notify(e);
      }
    }

    using Observer<event::adapt>::update;

  protected:
    IndexDist indexDist_;
  };


  // Deduction guides
  template <class GV, class PBF>
  GlobalBasis(std::string const& name, GV const& gridView, PBF const& preBasisFactory)
    -> GlobalBasis<std::decay_t<decltype(flatPreBasis(preBasisFactory(gridView)))>>;

  template <class GV, class PBF>
  GlobalBasis(GV const& gridView, PBF const& preBasisFactory)
    -> GlobalBasis<std::decay_t<decltype(flatPreBasis(preBasisFactory(gridView)))>>;

} // end namespace AMDiS
