#include <config.h>

#include "AdaptStationary.hpp"

// AMDiS includes
#include "AdaptInfo.hpp"
#include "Flag.hpp"
#include "Initfile.hpp"
#include "ProblemIterationInterface.hpp"

namespace AMDiS {

AdaptStationary::AdaptStationary(std::string const& name,
                                 ProblemIterationInterface& problemIteration,
                                 AdaptInfo& adaptInfo)
  : AdaptBase(name, &problemIteration, adaptInfo)
{}


int AdaptStationary::adapt()
{
  // initial iteration
  if (adaptInfo_.spaceIteration() == -1)
  {
    problemIteration_->beginIteration(adaptInfo_);
    problemIteration_->oneIteration(adaptInfo_, NO_ADAPTION);
    problemIteration_->endIteration(adaptInfo_);
    adaptInfo_.incSpaceIteration();
  }

  // adaption loop
  while (!adaptInfo_.spaceToleranceReached() &&
          (adaptInfo_.spaceIteration() < adaptInfo_.maxSpaceIteration() ||
          adaptInfo_.maxSpaceIteration() < 0) )
  {

    problemIteration_->beginIteration(adaptInfo_);
    Flag adapted = problemIteration_->oneIteration(adaptInfo_, FULL_ITERATION);
    problemIteration_->endIteration(adaptInfo_);

    if (adapted == Flag{0})
      break;

    adaptInfo_.incSpaceIteration();
  }

  return 0;
}

} // end namespace AMDiS
