#pragma once

#include <string>

// MTL4 includes
#include <boost/numeric/itl/itl.hpp>

// more solvers defined in AMDiS
#include <amdis/linearalgebra/mtl/itl/minres.hpp>
#include <amdis/linearalgebra/mtl/itl/gcr.hpp>
#include <amdis/linearalgebra/mtl/itl/fgmres.hpp>
#include <amdis/linearalgebra/mtl/itl/fgmres_householder.hpp>
#include <amdis/linearalgebra/mtl/itl/gmres2.hpp>
#include <amdis/linearalgebra/mtl/itl/gmres_householder.hpp>
#include <amdis/linearalgebra/mtl/itl/preonly.hpp>

#include <amdis/CreatorMap.hpp>
#include <amdis/Initfile.hpp>
#include <amdis/linearalgebra/mtl/KrylovRunner.hpp>
#include <amdis/linearalgebra/mtl/Traits.hpp>
#include <amdis/linearalgebra/mtl/UmfpackRunner.hpp>

namespace AMDiS
{
  /**
   * \ingroup Solver
   * \brief ITL_Solver <\ref cg_solver_type> implementation of conjugate gradient
   * method \implements ITL_Solver
   *
   * Solves a linear system \f$ Ax=b \f$ by the conjugate gradient method (CG)
   * and can be used for symmetric positive definite system matrices.
   * Right preconditioner is ignored.
   */
  class cg_solver_type
  {
  public:
    explicit cg_solver_type(std::string const& /*prefix*/) {}
    template <class LinOp, class X, class B, class P, class I>
    int operator()(LinOp const& A, X& x, B const& b, P const& precon, I& iter)
    {
      return itl::cg(A, x, b, precon, iter);
    }
  };


  /**
   * \ingroup Solver
   * \brief ITL_Solver <\ref cgs_solver_type> implementation of squared conjugate
   * gradient method \implements ITL_Solver
   *
   * Solves a linear system \f$ Ax=b \f$ by the squared conjugate gradient method
   * (CGS). Right preconditioner is ignored.
   */
  class cgs_solver_type
  {
  public:
    explicit cgs_solver_type(std::string const& /*prefix*/) {}
    template <class LinOp, class X, class B, class P, class I>
    int operator()(LinOp const& A, X& x, B const& b, P const& precon, I& iter)
    {
      return itl::cgs(A, x, b, precon, iter);
    }
  };


  /**
   * \ingroup Solver
   * \brief ITL_Solver <\ref bicg_solver_type> implementation of bi-conjugate
   * gradient method \implements ITL_Solver
   *
   * Solves a linear system \f$ Ax=b \f$ by a BiCG method and can be used for
   * system matrices.
   */
  class bicg_solver_type
  {
  public:
    explicit bicg_solver_type(std::string const& /*prefix*/) {}
    template <class LinOp, class X, class B, class P, class I>
    int operator()(LinOp const& A, X& x, B const& b, P const& precon, I& iter)
    {
      return itl::bicg(A, x, b, precon, iter);
    }
  };


  /**
   * \ingroup Solver
   * \brief ITL_Solver <\ref bicgstab_type> implementation of stabilized
   * bi-conjugate gradient method \implements ITL_Solver
   *
   * Solves a linear system \f$ Ax=b \f$ by a stabilized BiCG method and can be
   * used for system matrices.
   */
  class bicgstab_type
  {
  public:
    explicit bicgstab_type(std::string const& /*prefix*/) {}
    template <class LinOp, class X, class B, class P, class I>
    int operator()(LinOp const& A, X& x, B const& b, P const& precon, I& iter)
    {
      return itl::bicgstab(A, x, b, precon, iter);
    }
  };


  /**
   * \ingroup Solver
   * \brief ITL_Solver <\ref bicgstab2_type> implementation of BiCGStab(l) method
   * with l=2 \implements ITL_Solver
   *
   * Solves a linear system \f$ Ax=b \f$ by a stabilized BiCG(2) method and can
   * be used for system matrices.
   */
  class bicgstab2_type
  {
  public:
    explicit bicgstab2_type(std::string const& /*prefix*/) {}
    template <class LinOp, class X, class B, class P, class I>
    int operator()(LinOp const& A, X& x, B const& b, P const& precon, I& iter)
    {
      return itl::bicgstab_2(A, x, b, precon, iter);
    }
  };


  /**
   * \ingroup Solver
   * \brief ITL_Solver <\ref qmr_solver_type> implementation of Quasi-Minimal
   * Residual method \implements ITL_Solver
   *
   * Solves a linear system \f$ Ax=b \f$ by the Quasi-Minimal Residual method (QMR).
   */
  class qmr_solver_type
  {
  public:
    explicit qmr_solver_type(std::string const& /*prefix*/) {}
    template <class LinOp, class X, class B, class P, class I>
    int operator()(LinOp const& A, X& x, B const& b, P const& precon, I& iter)
    {
      itl::pc::identity<LinOp> id(A);
      return itl::qmr(A, x, b, precon, id, iter);
    }
  };


  /**
   * \ingroup Solver
   * \brief ITL_Solver <\ref tfqmr_solver_type> implementation of Transposed-Free
   * Quasi-Minimal Residual method \implements ITL_Solver
   *
   * Solves a linear system by the Transposed-Free Quasi-Minimal Residual method
   * (TFQMR).
   */
  class tfqmr_solver_type
  {
  public:
    explicit tfqmr_solver_type(std::string const& /*prefix*/) {}
    template <class LinOp, class X, class B, class P, class I>
    int operator()(LinOp const& A, X& x, B const& b, P const& precon, I& iter)
    {
      itl::pc::identity<LinOp> id(A);
      return itl::tfqmr(A, x, b, precon, id, iter);
    }
  };


  /**
   * \ingroup Solver
   * \brief ITL_Solver <\ref bicgstab_ell_type> implementation of stabilized
   * BiCG(ell) method \implements ITL_Solver
   *
   * Solves a linear system by a stabilized BiCG(ell) method and can be used for
   * system matrices. The parameter ell [3] can be specified.
   */
  class bicgstab_ell_type
  {
    int ell;
  public:
    explicit bicgstab_ell_type(std::string const& prefix) : ell(3)
    {
      Parameters::get(prefix + "->ell", ell);
    }
    template <class LinOp, class X, class B, class P, class I>
    int operator()(LinOp const& A, X& x, B const& b, P const& precon, I& iter)
    {
      itl::pc::identity<LinOp> id(A);
      return itl::bicgstab_ell(A, x, b, precon, id, iter, ell);
    }
  };


  /**
   * \ingroup Solver
   * \brief ITL_Solver <\ref gmres_type> implementation of generalized minimal
   * residual method \implements ITL_Solver
   *
   * Solves a linear system by the GMRES method.
   * The parameter restart [30] is the maximal number of orthogonalized vectors.
   * The method is not preconditioned
   */
  class gmres_type
  {

  public:
    explicit gmres_type(std::string const& prefix) : restart(30), ortho(GRAM_SCHMIDT)
    {
      Parameters::get(prefix + "->restart", restart);
      Parameters::get(prefix + "->orthogonalization", ortho);
    }
    template <class LinOp, class X, class B, class P, class I>
    int operator()(LinOp const& A, X& x, B const& b, P const& precon, I& iter)
    {
      itl::pc::identity<LinOp> id(A);
      switch (ortho)
      {
      default:
      case GRAM_SCHMIDT:
        return itl::gmres2(A, x, b, id, precon, iter, restart);
        break;
      case HOUSEHOLDER:
        return itl::gmres_householder(A, x, b, precon, iter, restart);
        break;
      }
    }

  private:
    static constexpr int GRAM_SCHMIDT = 1;
    static constexpr int HOUSEHOLDER = 2;

    int restart;
    int ortho;
  };


  /**
   * \ingroup Solver
   * \brief ITL_Solver <\ref idr_s_type> implementation of Induced Dimension
   * Reduction method \implements ITL_Solver
   *
   * Solves a linear system by an Induced Dimension Reduction method and can be
   * used for system matrices.  The parameter s [30] can be specified.
   *
   * Peter Sonneveld and Martin B. van Gijzen, IDR(s): a family of simple and fast
   * algorithms for solving large nonsymmetric linear systems.
   * SIAM J. Sci. Comput. Vol. 31, No. 2, pp. 1035-1062 (2008). (copyright SIAM)
   *
   * NOTE: preconditioners are ignored.
   */
  class idr_s_type
  {
    int s;
  public:
    explicit idr_s_type(std::string const& prefix) : s(30)
    {
      Parameters::get(prefix + "->s", s);
    }
    template <class LinOp, class X, class B, class P, class I>
    int operator()(LinOp const& A, X& x, B const& b, P const&, I& iter)
    {
      itl::pc::identity<LinOp> id(A);
      return itl::idr_s(A, x, b, id, id, iter, s);
    }
  };


  /**
   * \ingroup Solver
   * \brief ITL_Solver <\ref minres_solver_type> implementation of minimal
   * residual method \implements ITL_Solver
   *
   * Solves a linear system by the Minres method. Can be used for symmetric
   * indefinite systems.
   */
  class minres_solver_type
  {
  public:
    explicit minres_solver_type(std::string const& /*prefix*/) {}
    template <class LinOp, class X, class B, class P, class I>
    int operator()(LinOp const& A, X& x, B const& b, P const& precon, I& iter)
    {
      return itl::minres(A, x, b, precon, iter);
    }
  };


  /**
   * \ingroup Solver
   * \brief ITL_Solver <\ref gcr_type> implementation of generalized conjugate
   * residual method \implements ITL_Solver
   *
   * Solves a linear system by the GCR method - generalized conjugate residual
   * method. The parameter restart [30] is the maximal number of orthogonalized
   * vectors.
   */
  class gcr_type
  {
    int restart;

  public:
    explicit gcr_type(std::string const& prefix) : restart(30)
    {
      Parameters::get(prefix + "->restart", restart);
    }
    template <class LinOp, class X, class B, class P, class I>
    int operator()(LinOp const& A, X& x, B const& b, P const& precon, I& iter)
    {
      itl::pc::identity<LinOp> id(A);
      return itl::gcr(A, x, b, id, precon, iter, restart);
    }
  };


  /**
   * \ingroup Solver
   * \brief ITL_Solver <\ref fgmres_type> implementation of flexible GMRes method
   * \implements ITL_Solver
   *
   * Solves a linear system by the FGMRES method.
   * The parameter restart [30] is the maximal number of orthogonalized vectors.
   * See reference "A Flexible Inner-Outer Preconditiones GMRES Algorithm",
   * Youcef Saad, (1993)
   */
  class fgmres_type
  {
  public:
    explicit fgmres_type(std::string const& prefix)
      : restart(30), orthogonalization(GRAM_SCHMIDT)
    {
      Parameters::get(prefix + "->restart", restart);
      Parameters::get(prefix + "->orthogonalization", orthogonalization);
    }
    template <class LinOp, class X, class B, class P, class I>
    int operator()(LinOp const& A, X& x, B const& b, P const& precon, I& iter)
    {
      itl::pc::identity<LinOp> id(A);
      switch (orthogonalization)
      {
      default:
      case GRAM_SCHMIDT:
        return itl::fgmres(A, x, b, id, precon, iter, restart);
        break;
      case HOUSEHOLDER:
        return itl::fgmres_householder(A, x, b, precon, iter, restart);
        break;
      }
    }

  private:
    static constexpr int GRAM_SCHMIDT = 1;
    static constexpr int HOUSEHOLDER = 2;

    int restart;
    int orthogonalization;
  };


  /**
   * \ingroup Solver
   * \brief ITL_Solver <\ref preonly_type> implementation of preconditioner as
   * \implements ITL_Solver
   *
   * Solves a linear system by applying a preconditioner only.
   */
  class preonly_type
  {
  public:
    explicit preonly_type(std::string const& /*prefix*/) {}
    template <class LinOp, class X, class B, class P, class I>
    int operator()(LinOp const& A, X& x, B const& b, P const& precon, I& iter)
    {
      return itl::preonly(A, x, b, precon, iter);
    }
  };


  // ===========================================================================

  /// Adds default creators for linear solvers based on `mtl::compressed2D`.
  /**
   * Adds creators for full-matrix aware solvers.
   * - *cg*: conjugate gradient method, \see cg_solver_type
   * - *cgs*: stabilized conjugate gradient mtheod, \see cgs_solver_type
   * - *bicg*: BiCG mtheod, \see bicg_solver_type
   * - *bcgs*: stabilized bi-conjugate gradient method, \see bicgstab_type
   * - *bcgs2*: stabilized bi-conjugate gradient method, \see bicgstab2_type
   * - *qmr*: Quasi-Minimal Residual method, \see qmr_solver_type
   * - *tfqmr*: Transposed-Free Quasi-Minimal Residual method, \see tfqmr_solver_type
   * - *bcgsl*: stabilized BiCG(ell) method, \see bicgstab_ell_type
   * - *gmres*: Generalized minimal residula method, \see gmres_type
   * - *fgmres*: Flexible GMRES, \see fgmres_type
   * - *minres*: Minimal residul method, \see minres_solver_type
   * - *idrs*: Induced Dimension Reduction method, \see idr_s_type
   * - *gcr*: Generalized conjugate residual method, \see gcr_type
   * - *preonly*: Just a pply a preconditioner, \see preonly_type
   * - *umfpack*: external UMFPACK solver, \see UmfpackRunner
   **/
  template <class M, class X, class Y>
  class DefaultCreators< LinearSolverInterface<M,X,Y> >
  {
    using SolverBase = LinearSolverInterface<M,X,Y>;

    template <class ITLSolver>
    using SolverCreator = typename KrylovRunner<M,X,Y,ITLSolver>::Creator;

#ifdef HAVE_UMFPACK
    using UmfpackSolverCreator = typename UmfpackRunner<M,X,Y>::Creator;
#endif

    using Map = CreatorMap<SolverBase>;

  public:
    static void init()
    {
      auto cg = new SolverCreator<cg_solver_type>;
      Map::addCreator("cg", cg);

      auto cgs = new SolverCreator<cgs_solver_type>;
      Map::addCreator("cgs", cgs);

      auto bicg = new SolverCreator<bicg_solver_type>;
      Map::addCreator("bicg", bicg);

      auto bcgs = new SolverCreator<bicgstab_type>;
      Map::addCreator("bicgstab", bcgs);
      Map::addCreator("bcgs", bcgs);

      auto bcgs2 = new SolverCreator<bicgstab2_type>;
      Map::addCreator("bicgstab2", bcgs2);
      Map::addCreator("bcgs2", bcgs2);

      auto qmr = new SolverCreator<qmr_solver_type>;
      Map::addCreator("qmr", qmr);

      auto tfqmr = new SolverCreator<tfqmr_solver_type>;
      Map::addCreator("tfqmr", tfqmr);

      auto bcgsl = new SolverCreator<bicgstab_ell_type>;
      Map::addCreator("bicgstab_ell", bcgsl);
      Map::addCreator("bcgsl", bcgsl);

      auto gmres = new SolverCreator<gmres_type>;
      Map::addCreator("gmres", gmres);

      auto fgmres = new SolverCreator<fgmres_type>;
      Map::addCreator("fgmres", fgmres);

      auto minres = new SolverCreator<minres_solver_type>;
      Map::addCreator("minres", minres);

      auto idrs = new SolverCreator<idr_s_type>;
      Map::addCreator("idrs", idrs);

      auto gcr = new SolverCreator<gcr_type>;
      Map::addCreator("gcr", gcr);

      auto preonly = new SolverCreator<preonly_type>;
      Map::addCreator("preonly", preonly);

      // default iterative solver
      Map::addCreator("default", gmres);

      init_direct(std::is_same<typename Dune::FieldTraits<typename M::value_type>::real_type, double>{});
    }

    static void init_direct(std::false_type) {}
    static void init_direct(std::true_type)
    {
#ifdef HAVE_UMFPACK
      auto umfpack = new UmfpackSolverCreator;
      Map::addCreator("umfpack", umfpack);

      // default direct solvers
      Map::addCreator("direct", umfpack);
#endif
    }
  };

} // end namespace AMDiS
