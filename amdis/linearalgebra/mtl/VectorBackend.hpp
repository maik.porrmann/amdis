#pragma once

#include <boost/numeric/mtl/vector/dense_vector.hpp>

#include <dune/common/ftraits.hh>

#include <amdis/Output.hpp>
#include <amdis/common/FakeContainer.hpp>
#include <amdis/linearalgebra/VectorBase.hpp>
#include <amdis/typetree/MultiIndex.hpp>

namespace AMDiS
{
  class DefaultIndexDistribution;

  /// \brief The basic container that stores a base vector data
  template <class T>
  class MTLVector
      : public VectorBase<MTLVector<T>>
  {
  public:
    /// The type of the base vector
    using BaseVector = mtl::dense_vector<T>;

    /// The type of the elements of the DOFVector
    using value_type = typename BaseVector::value_type;

    /// The index/size - type
    using size_type  = typename BaseVector::size_type;

  private:
    /// The type of the elements of the DOFVector
    using block_type = value_type;

    /// The underlying field type
    using field_type = typename Dune::FieldTraits<value_type>::field_type;

  public:
    /// Constructor. Constructs new BaseVector.
    explicit MTLVector(DefaultIndexDistribution const&) {}

    /// Return the data-vector \ref vector_
    BaseVector const& vector() const
    {
      return vector_;
    }

    /// Return the data-vector \ref vector_
    BaseVector& vector()
    {
      return vector_;
    }

    /// Return the current size of the \ref vector_
    std::size_t size() const
    {
      return mtl::vec::size(vector_);
    }


    /// Resize the \ref vector_ to the size \p s
    template <class SizeInfo>
    void init(SizeInfo const& sizeInfo, bool clear)
    {
      vector_.change_dim(sizeInfo({}));
      if (clear)
        set_to_zero(vector_);
    }

    /// Access the entry \p i of the \ref vector with read-access.
    template <class MultiIndex>
    value_type& at(MultiIndex const& idx)
    {
      const size_type i = flatMultiIndex(idx);
      test_exit_dbg(i < size(),"Index {} out of range [0,{})", i, size());
      return vector_[i];
    }

    /// Access the entry \p i of the \ref vector with read-access.
    template <class MultiIndex>
    value_type const& at(MultiIndex const& idx) const
    {
      const size_type i = flatMultiIndex(idx);
      test_exit_dbg(i < size(), "Index {} out of range [0,{})", i, size());
      return vector_[i];
    }

  private:
    /// The data-vector (can hold a new BaseVector or a pointer to external data
    BaseVector vector_;
  };


  namespace Recursive
  {
    template <class T>
    struct ForEach<MTLVector<T>> : ForEach<VectorBase<MTLVector<T>>> {};

    template <class T>
    struct Transform<MTLVector<T>> : Transform<VectorBase<MTLVector<T>>> {};

    template <class T>
    struct InnerProduct<MTLVector<T>> : InnerProduct<VectorBase<MTLVector<T>>> {};

  } // end namespace Recursive
} // end namespace AMDiS
