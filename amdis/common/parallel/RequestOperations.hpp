#pragma once

#include <list>
#include <vector>

#include <mpi.h>

namespace AMDiS { namespace Mpi
{
  // Blocks until all communication operations associated with active handles in the range complete.
  template <class ReqIter>
  void wait_all(ReqIter first, ReqIter last)
  {
    std::list<ReqIter> remaining;
    for (ReqIter it = first; it != last; ++it) remaining.push_back(it);

    while (!remaining.empty()) {
      auto remove_it = remaining.end();
      for (auto it = remaining.begin(); it != remaining.end(); ++it) {
        if ((*it)->test()) {
          remove_it = it;
          break;
        }
      }

      if (remove_it != remaining.end())
        remaining.erase(remove_it);
    }
  }

  template <class ReqIter, class Apply>
  void wait_all_apply(ReqIter first, ReqIter last, Apply apply)
  {
    std::list<ReqIter> remaining;
    for (ReqIter it = first; it != last; ++it) remaining.push_back(it);

    while (!remaining.empty()) {
      auto remove_it = remaining.end();
      for (auto it = remaining.begin(); it != remaining.end(); ++it) {
        if ((*it)->test()) {
          apply(*it); // call a functor with the request iterator
          remove_it = it;
          break;
        }
      }

      if (remove_it != remaining.end())
        remaining.erase(remove_it);
    }
  }

  template <class ReqIter>
  void wait_all_weak(ReqIter first, ReqIter last)
  {
    std::list<ReqIter> remaining;
    for (ReqIter it = first; it != last; ++it) remaining.push_back(it);

    while (!remaining.empty()) {
      auto remove_it = remaining.end();
      for (auto it = remaining.begin(); it != remaining.end(); ++it) {
        if ((*it)->status()) {
          remove_it = it;
          break;
        }
      }

      if (remove_it != remaining.end())
        remaining.erase(remove_it);
    }
  }

  template <class ReqIter>
  void wait_all(ReqIter first, ReqIter last, std::vector<MPI_Status>& statuses)
  {
    statuses.resize(std::distance(first, last));

    std::list<ReqIter> remaining;
    for (ReqIter it = first; it != last; ++it) remaining.push_back(it);

    while (!remaining.empty()) {
      auto remove_it = remaining.end();
      for (auto it = remaining.begin(); it != remaining.end(); ++it) {
        auto status = (*it)->test();
        if (status) {
          remove_it = it;
          statuses[std::distance(first,*it)] = *status;
          break;
        }
      }

      if (remove_it != remaining.end())
        remaining.erase(remove_it);
    }
  }


  // Tests for completion of either one or none of the operations associated with active handles.
  // In the former case, it returns an iterator to the finished handle.
  template <class ReqIter>
  ReqIter test_any(ReqIter first, ReqIter last)
  {
    for (auto it = first; it != last; ++it) {
      if (it->test())
        return it;
    }
    return last;
  }


  // Blocks until one of the operations associated with the active requests in the range has completed.
  template <class ReqIter>
  void wait_any(ReqIter first, ReqIter last)
  {
    while (test_any(first, last) == last) ;
  }

}} // end namespace AMDiS::Mpi
