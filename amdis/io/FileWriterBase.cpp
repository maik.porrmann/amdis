#include <config.h>

#include "FileWriterBase.hpp"

#include <amdis/AdaptInfo.hpp>
#include <amdis/Initfile.hpp>
#include <amdis/common/Filesystem.hpp>

namespace AMDiS
{

FileWriterBase::FileWriterBase(std::string const& base)
{
  Parameters::get(base + "->filename", filename_);
  Parameters::get(base + "->output directory", dir_);
  Parameters::get(base + "->name", name_);
  Parameters::get(base + "->write every i-th timestep", tsModulo_);
  Parameters::get(base + "->write after timestep", timeModulo_);

  test_exit(dir_ == "." || filesystem::exists(dir_), "Output directory '{}' does not exist!", dir_);
}


bool FileWriterBase::doWrite(AdaptInfo& adaptInfo) const
{
  if (timeModulo_ > 0.0 &&
      lastWriteTime_ > adaptInfo.startTime() &&
      lastWriteTime_ + timeModulo_ > adaptInfo.time() + 0.5*adaptInfo.minTimestep())
    return false;

  else if (tsModulo_ > 0 && adaptInfo.timestepNumber() % tsModulo_ != 0)
    return false;

  lastWriteTime_ = adaptInfo.time();
  return true;
}

} // end namespace AMDiS
