# DOFVector and DiscreteFunction {: #group-dofvector }
## Summary
The class [`DOFVector`](#class-dofvector) acts as a container for storing the coefficients of the solution discrete function.
It is attached to a global basis to give its coefficients a meaning. A [`DiscreteFunction`](#class-discretefunction) goes
one step further and transforms a DOFVector or subspaces of a DOFVector (with respecto to a sub basis)
into a [`GridFunction`](../GridFunctions) that allows to use it like a function defined on a grid.

Let $`\{\phi_i\}`$ be the set of basis functions of a finite-element space $`V`$. A function $`u\in V`$ can be represented
as
```math
u(\mathbf{x}) = \sum_i u_i \phi_i(\mathbf{x})
```
with coefficients $`(u_i)`$.

The pair $`\{(u_i),\{\phi_i\}\}`$ is called `DOFVector` and the function $`u=u(\mathbf{x})`$ is called `DiscreteFunction`.

### Classes

 Class                          | Descriptions
--------------------------------|---------------------------------------------
[`DOFVector`](#class-dofvector) | Standard container for storing solution coefficients associated to a functionspace basis
[`DiscreteFunction`](#class-discretefunction) | Wrapper that transforms a DOFVector into a GridFunction


## class `DOFVector`
Defined in header [`<amdis/DOFVector.hpp>`](https://gitlab.com/amdis/amdis/blob/master/amdis/DOFVector.hpp)

```c++
template <class GB,
          class T = double,
          class Traits = BackendTraits>
class DOFVector
  : public VectorFacade<T, Traits::template Vector<GB>::template Impl>
```

`DOFVector` is a vector data-structure for the coefficients of a discrete function associated to a GlobalBasis `GB`.

The template parameter `GB` defines the global basis type and `T` the value type of the coefficients
in the vector. It defaults to `double`. The third parameter `Traits` is a collection of parameters
characterizing the Linear-Algebra backend. It defaults to the default backend Traits.

### Member Types

 Member Type                    | Definition
--------------------------------|---------------------------------------------
`GlobalBasis`                   | `GB`
`size_type`                     | `typename GlobalBasis::size_type`
`value_type`                    | `typename Backend::value_type`

The `value_type` is often the same as `T`, but might be just something similar, like `PetscScalar`.

### Member functions

 Function                       | Descriptions
--------------------------------|---------------------------------------------
[*(constructor)*](#function-dofvectordofvector)               | Construct the DOFVector
[`backup`](#function-dofvectorbackup)                         | Write DOFVector to file
[`restore`](#function-dofvectorrestore)                       | Read backup data from file
[`dataTransfer`](#function-dofvectordataTransfer)             | Return the associated DataTransfer object

??? seealso "Functions inherited from [`VectorFacade`](../MatVecFacade/#class-vectorfacade)"
    Function                       | Descriptions
    --------------------------------|---------------------------------------------
    [`impl`](../MatVecFacade#function-vectorbasebackend)                | Return the backend vector wrapper implementing the actual algebra
    [`localSize,globalSize`](../MatVecFacade#function-vectorbasesize)   | The number of entries in the local part of the vector
    [`resize,resizeZero`](../MatVecFacade#function-vectorbaseglobalSize)| Resize the vector to the size of the basis
    [`init`](../MatVecFacade#function-vectorbaseglobalSize)             | Prepare the vector for insertion of values
    [`finish`](../MatVecFacade#function-vectorbaseglobalSize)           | Finish the insertion of values
    [`at`](../MatVecFacade#function-vectorbaseat)                       | Return the value of the vector at the given local index
    [`insert,set,add`](../MatVecFacade#function-vectorbaseinsert)       | Insert a single value into the matrix
    [`gather`](../MatVecFacade#function-vectorbasegather)               | Extract values from the vector referring to the given local indices
    [`scatter`](../MatVecFacade#function-vectorbasescatter)             | Insert a block of values into the vector
    [`forEach`](../MatVecFacade#function-vectorbasescatter)             | Apply a functor to each value at given indices


## function `DOFVector::DOFVector`
```c++
template <class Basis>
DOFVector(Basis&& basis, DataTransferOperation op = INTERPOLATE)

template <class GV, class PBF>
DOFVector(GV const& gridView, PBF const& preBasisFactory, DataTransferOperation op = INTERPOLATE)
```

Constructs the DOFVector from a given global basis or a grid view and a basis factory, and optionally a DataTransferOperation that defines how the DOFVector is handled during grid adaption.

#### Arguments
`Basis basis`
:   Either a `shared_ptr` to the `GlobalBasis` type or anything that can be converted to that, e.g. a reference or a `unique_ptr`. References to lvalues are wrapped into non-destroying `shared_ptr`s whereas rvalue-references are moved into new basis objects.

`GV gridView`
:   A grid view provided by the grid

`PBF preBasisFactory`
:   A factory to create a pre-basis, see [the ref page on GlobalBasis](../GlobalBasis#making-a-prebasis)

`DataTransferOperation op`
:   The operation performed during data-transfer, either `DataTransferOperation::INTERPOLATE` or `DataTransferOperation::NO_OPERATION`

#### Example
```c++
using Grid = Dune::YaspGrid<2>;
Grid grid({1.0, 1.0}, {2, 2});

using namespace Dune::Functions::BasisFactory;
GlobalBasis basis1(grid.leafGridView(), lagrange<2>());

DOFVector<decltype(basis1)> vec0(basis1);
DOFVector vec1(basis1);                     // C++17 only

auto basis2 = makeUniquePtr(GlobalBasis{grid.leafGridView(), lagrange<2>()});
DOFVector<Underlying_t<decltype(basis2)>> vec2(std::move(basis2));
```

#### See Also
- Generator function to construct a DOFVector: [`makeDOFVector()`](#function-makedofvector)


## function `DOFVector::backup`
```c++
void backup(std::string const& filename);
```

Writes the coefficients of the DOFVector to file. Thereby the data is first extracted element-wise and then
written using binary ofstreams.

#### Arguments
`std::string filename`
:   The filename of the backup file that is created.


## function `DOFVector::restore`
```c++
void restore(std::string const& filename);
```

Reads a backup of the DOFVector from file previously created using the [backup()](#function-dofvectorbackup) function.

#### Arguments
`std::string filename`
:   The filename of the backup file to read from.


## function `makeDOFVector`
Defined in header [`<amdis/DOFVector.hpp>`](https://gitlab.com/amdis/amdis/blob/master/amdis/DOFVector.hpp)

```c++
template <class T = double,
          class Basis>
DOFVector<...> makeDOFVector(Basis&& basis, DataTransferOperation op = INTERPOLATE)
```

Creates a DOFVector from a basis.

This generator function accepts the basis as reference, temporary, or
`shared_ptr`. Internally the reference is wrapped into a non-destroying
`shared_ptr` and the temporary is moved into a new `shared_ptr`, see [DOFVector::DOFVector](#function-dofvectordofvector).

The `DataTransferOperation` controls what is done during grid changes with the
DOFVector. The default is interpolation of the data to the new grid.

#### Arguments
`Basis basis`
:   Reference, temporary, or `shared_ptr` to dune global basis or `GlobalBasis`

`DataTransferOperation op`
:   The operation performed during data-transfer, either `DataTransferOperation::INTERPOLATE` or `DataTransferOperation::NO_OPERATION`

#### Template types
`T`
:   The type of the coefficients to store in the DOFVector

#### Return value
Returns a `DOFVector<GB, T>` with `GB = GlobalBasis<typename Underlying_t<Basis>::PreBasis>`. The
underlying type is either the `remove_cvref_t` for references, or the pointed-to type for smart pointers.

#### Example
```c++
using namespace Dune::Functions::BasisFactory;

// pass a reference to the basis
GlobalBasis basis1(gridView, lagrange<2>());
auto vec1 = makeDOFVector<double>(basis1);

// pass a smart pointer
auto basis2 = std::make_shared<decltype(basis1)>(basis1);
auto vec2 = makeDOFVector(basis2, DataTransferOperation::NO_OPERATION);
```


## class `DiscreteFunction`
Defined in header [`<amdis/gridfunctions/DiscreteFunction.hpp>`](https://gitlab.com/amdis/amdis/blob/master/amdis/gridfunctions/DiscreteFunction.hpp)

```c++
template <class Coefficients,
          class GlobalBasis,
          class TreePath>
class DiscreteFunction
```

A `DiscreteFunction` is the interpretation of a `DOFVector` as grid function.

### Template parameters
`Coefficients`
:   Type of the coefficient vector

`GlobalBasis`
:   The type of the global basis associated with the DOFVector

`TreePath`
:   The path in the basis tree representing the subspace represented by this DiscreteFunction

### Member Types

 Member Type         | Definition
---------------------|---------------------------------------------
`EntitySet`          | Set of entities the DiscreteFunction is defined on, `GridViewEntitySet<GlobalBasis::GridView, 0>`
`Domain`             | Global coordinates of the EntitySet
`Range`              | Range type of this DiscreteFunction, given by sub-tree and coefficient type

### Member functions of the const DiscreteFunction

 Function                       | Descriptions
--------------------------------|---------------------------------------------
[*(constructor)*](#function-discretefunctiondiscretefunction) | Construct the const DiscreteFunction
[`entitySet`](#function-discretefunctionentityset)            | Return the stored GridViewEntitySet
[`basis`](#function-discretefunctionbasis)                    | Return global basis bound to the DOFVector
[`treePath`](#function-discretefunctiontreepath)              | Return the treePath associated with this discrete function
[`coefficients`](#function-discretefunctioncoefficients)      | Return const coefficient vector
[`child`](#function-discretefunctionchild)                    | Return the sub-range view of this DiscreteFunction

### Member functions of the mutable DiscreteFunction

 Function                       | Descriptions
--------------------------------|---------------------------------------------
[*(constructor)*](#function-discretefunctiondiscretefunction)  | Construct the mutable DiscreteFunction
[`interpolate_noalias`](#function-discretefunctioninterpolate) | Interpolation of GridFunction to DOFVector assuming no aliasing
[`interpolate`](#function-discretefunctioninterpolate)         | Interpolation of GridFunction to DOFVector
[`operator<<`](#function-discretefunctioninterpolate)          | Interpolation of GridFunction to DOFVector
[`operator+=`](#function-discretefunctioninterpolate)          | Accumulated interpolation of GridFunction to DOFVector
[`operator-=`](#function-discretefunctioninterpolate)          | Accumulated interpolation of GridFunction to DOFVector


## function `DiscreteFunction::DiscreteFunction`
```c++
// (1)
DiscreteFunction(DOFVector<GlobalBasis,T> const& dofVector, GlobalBasis const& basis,
                 TreePath const& treePath = {})
// (2)
DiscreteFunction(DOFVector<GlobalBasis,T>& dofVector, GlobalBasis const& basis,
                 TreePath const& treePath = {})
```

Construction of a `DiscreteFunction` from a const (1) or mutable (2) `DOFVector`. The corresponding constructor is
only available for the const or mutable `is_const` template parameter specialization.

#### Arguments
`DOFVector<...> dofvector`
:   The container storing the global coefficients. Is stored as pointer in the DiscreteFunction and thus must have a longer lifetime
    than the DiscreteFunction.

`GlobalBasis const& basis`
:   The global basis associated with the dofvector.

`TreePath treePath`
:   A `Dune::TypeTree::HybridTreePath<...>` representing the coordinates of a node in the basis tree this DiscreteFunction is
    defined on. The type of the treePath also defines the `Range` type of the DiscreteFunction.


## function `DiscreteFunction::entitySet`
```c++
EntitySet const& entitySet() const
```

Returns a `Dune::Functions::GridViewEntitySet` of the `GridView` associated to the `GlobalBasis`.


## function `DiscreteFunction::basis`
```c++
std::shared_ptr<GlobalBasis const> basis() const
```

Returns the global basis bound to the DOFVector.


## function `DiscreteFunction::treePath`
```c++
TreePath const& treePath() const
```

Returns the treePath associated with this DiscreteFunction.


## function `DiscreteFunction::coefficients`
```c++
DOFVector<GB,VT> const& coefficients() const  // (1)
DOFVector<GB,VT>&       coefficients()        // (2)
```

Returns the const (1) or mutable (2) coefficient vector.


## function `DiscreteFunction::child`
```c++
// (1)
template <class... Indices>
auto child(Indices... ii) const

// (2)
template <class... Indices>
auto child(Indices... ii)
```

Returns the const (1) or mutable (2) sub-range view of the stored DOFVector.

#### Arguments
`Indices... ii`
:   Components of the tree-path identifying a node in the basis tree.

#### Example
```c++
GlobalBasis basis{gridView, power<3>(lagrange<1>())};
auto vec = makeDOFVector(basis);
auto df = valueOf(vec);

auto df1 = df.child();
auto df2 = df.child(0);
auto df3 = df.child(_0);
auto df4 = valueOf(df,0);
auto df5 = valueOf(vec,0); // df4 == df5
```


## function `DiscreteFunction::interpolate*` {: #function-discretefunctioninterpolate }
```c++
// (1)
template <class Expr, class Tag = tag::average>
void interpolate_noalias(Expr&& expr, Tag strategy)

// (2)
template <class Expr, class Tag = tag::average>
void interpolate(Expr&& expr, Tag strategy)

// (3)
template <class Expr>
DiscreteFunction& operator<<(Expr&& expr)

// (4)
template <class Expr>
DiscreteFunction& operator+=(Expr&& expr)

// (5)
template <class Expr>
DiscreteFunction& operator-=(Expr&& expr)
```

(1) Interpolation of a `GridFunction` (or Expression) to the subtree of the DOFVector, assuming that there is no
reference to this DOFVector in the expression (no aliasing).

(2) Interpolation of a `GridFunction` (or Expression) to the subtree of the DOFVector, allowing aliasing.

(3) Operator notation of (2) using averaging strategy.

(4) Interpolate `(*this) + expr` to the subtree of the DOFVector.

(5) Interpolate `(*this) - expr` to the subtree of the DOFVector.

In case *aliasing* is allowed, a temporary DOFVector is created first as copy of *this* and the interpolation
performed on the temporary using *noalias* interpolation (1). Then, this tempoary is moved back to *this*.

Note, the range type of the expression must be compatible with the `Range` type of the DiscreteFunction.

#### Arguments
`Expr expr`
:   An Expression representing a `GridFunction`

`Tag strategy`
:   An interpolation strategy, either `tag::average` or `tag::assign`.
    The `average `strategy accumulates interpolation values on each dof and takes the average by
    divising through the number of assignees. The `assign` strategy diretly assigns an interpolation
    value to a dof that might be overwritten in a subsequent interpolation step.

#### Example
```c++
GlobalBasis basis{gridView, power<3>(lagrange<1>())};
auto vec = makeDOFVector(basis);

// Range type is FieldVector<double,3>
valueOf(vec) << [](FieldVector<double,3> const& x) -> FieldVector<double,3> { return x; };

// Range type is double
valueOf(vec,0) += 42.0;
```
