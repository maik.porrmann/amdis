#pragma once

#include <type_traits>

#include <amdis/localoperators/FirstOrderTestDivTrialvec.hpp>

namespace AMDiS
{
  /**
   * \addtogroup operators
   * @{
   **/

  namespace tag
  {
    struct divtestvec_trial {};
  }

  /// first-order operator \f$ \langle\nabla\cdot\Psi, c\,\phi\rangle \f$
  template <class LC>
  struct GridFunctionOperatorRegistry<tag::divtestvec_trial, LC>
  {
    static constexpr int degree = 1;

    static tag::test_divtrialvec transposedTag(tag::divtestvec_trial) { return {}; }

    using type = FirstOrderTestDivTrialvec;
  };

  /** @} **/

} // end namespace AMDiS
