#include <amdis/AMDiS.hpp>
#include <amdis/utility/UniqueBorderPartition.hpp>

#include <dune/grid/yaspgrid.hh>

#include "Tests.hpp"

using namespace AMDiS;

template <class GridView>
void test(GridView const& gv)
{
  using Grid = typename GridView::Grid;
  using DataHandle = UniqueBorderPartition<Grid>;

  DataHandle borderEntities(gv.comm().rank(), gv.grid());
  gv.communicate(borderEntities,
    Dune::InterfaceType::InteriorBorder_All_Interface,
    Dune::CommunicationDirection::ForwardCommunication);
}

int main(int argc, char** argv)
{
  Environment env(argc, argv);

  Dune::YaspGrid<2> grid1({1.0, 1.0}, {8,8}, 0, 0); // no overlap
  Dune::YaspGrid<2> grid2({1.0, 1.0}, {8,8}, 0, 1); // overlap = 1
  Dune::YaspGrid<3> grid3({1.0, 1.0, 1.0}, {8,8,8}, 0, 1); // overlap = 1

  test(grid1.leafGridView());
  test(grid2.leafGridView());
  test(grid3.leafGridView());

  return report_errors();
}
