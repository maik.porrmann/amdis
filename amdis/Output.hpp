#pragma once

#include <iostream>
#include <string>

/**
 * \def AMDIS_NO_THROW
 * \brief The preprocessor constant sets whether to use c-asserts (if defined) or
 * to throw an exception in case of an error (if not defined).
 **/
#ifdef AMDIS_NO_THROW
  #include <cassert>
#else
  #include <stdexcept>
#endif

/// Use the formatting librart fmtlib::fmt
#include <fmt/core.h>
#include <fmt/ostream.h>

#include <amdis/Environment.hpp>
#include <amdis/common/TypeTraits.hpp>

/**
 * \def AMDIS_ENABLE_MSG_DBG
 * \brief The preprocessor constant enables the functions \ref AMDiS::MSG_DBG
 * and \ref AMDiS::TEST_EXIT_DBG.
 *
 * If the value is set to 1 the functions \ref AMDiS::MSG_DBG and \ref AMDiS::TEST_EXIT_DBG
 * are implemented, otherwise empty. Default is value 0 if \ref NDEBUG is not
 * defined, otherwise value 1.
 **/
#ifndef AMDIS_ENABLE_MSG_DBG
  #ifndef NDEBUG
    #define AMDIS_ENABLE_MSG_DBG 1
  #else
    #define AMDIS_ENABLE_MSG_DBG 0
  #endif
#endif

#define AMDIS_FUNCNAME(nn) [[maybe_unused]] const char *funcName = nn;

#ifdef NDEBUG
  #define AMDIS_FUNCNAME_DBG(nn)
  #define AMDIS_DBG_VAR(var)
#else
  #define AMDIS_FUNCNAME_DBG(nn) [[maybe_unused]] const char *funcName = nn;
  #define AMDIS_DBG_VAR(var) var
#endif


namespace AMDiS
{
  namespace Impl
  {
    template <class OStream, class... Args>
    OStream& msg_all(OStream& out, std::string const& str, Args&&... args)
    {
      if (Environment::mpiSize() > 1)
        fmt::print(out, "[" + std::to_string(Environment::mpiRank()) + "]  " + str, FWD(args)...);
      else
        fmt::print(out, str, FWD(args)...);
      return out;
    }

    template <class OStream, class... Args>
    OStream& msg(OStream& out, std::string const& str, Args&&... args)
    {
      if (Environment::msgAllRanks())
        return msg_all(out, str, FWD(args)...);

      if (Environment::mpiSize() > 1 && Environment::mpiRank() == 0)
        fmt::print(out, "[0]  " + str, FWD(args)...);
      else if (Environment::mpiSize() == 1)
        fmt::print(out, str, FWD(args)...);
      return out;
    }

    template <class T>
    void printType (T const&)
    {
      std::cout << __PRETTY_FUNCTION__ << std::endl;
    }

  } // end namespace Impl


  /// \brief print a message
  /**
   * Example:
   * ```
   * msg("Hello {}: {}", "World", 123); // prints "Hello World: 123\n"
   * ```
   **/
  template <class... Args>
  void msg(std::string const& str, Args&&... args)
  {
    Impl::msg(std::cout, str + "\n", FWD(args)...);
  }

  /// prints a message, if Environment::infoLevel() >= noInfoLevel
  template <class... Args>
  void info(int noInfoLevel, std::string const& str, Args&&... args)
  {
    if (Environment::infoLevel() >= noInfoLevel)
      Impl::msg(std::cout, str + "\n", FWD(args)...);
  }


  /// \brief print a message (without appended newline)
  /**
   * Example:
   * ```
   * msg_("Hello {}: {}", "World", 123); // prints "Hello World: 123"
   * ```
   **/
  template <class... Args>
  void msg_(std::string const& str, Args&&... args)
  {
    Impl::msg(std::cout, str, FWD(args)...);
  }

  /// prints a message, if Environment::infoLevel() >= noInfoLevel
  template <class... Args>
  void info_(int noInfoLevel, std::string const& str, Args&&... args)
  {
    if (Environment::infoLevel() >= noInfoLevel)
      Impl::msg(std::cout, str, FWD(args)...);
  }



  /// \brief print a message and exit
  /**
   * If the preprocessor constant \ref AMDIS_NO_THROW is defined,
   * the c-assert macro is called, otherwise an exception of
   * type \ref std::runtime_Error is thrown.
   **/
  template <class... Args>
  void error_exit(std::string const& str, Args&&... args)
  {
#ifdef AMDIS_NO_THROW
    Impl::msg_all(std::cerr, "ERROR: " + str + "\n", FWD(args)...);
    std::abort();
#else
    throw std::runtime_error( std::string("ERROR: ") + fmt::format(str, FWD(args)...));
#endif
  }


  /// \brief test for condition and in case of failure print message and exit
  /**
   * This function is equivalent to
   * ```
   * if (condition == false) error_exit(text);
   * ```
   * where `text` correspond to the arguments passed after the
   * \p condition argument.
   **/
  template <class... Args>
  void test_exit(bool condition, std::string const& str, Args&&... args)
  {
    if (!condition) { error_exit(str, FWD(args)...); }
  }


  template <class... Args>
  void warning(std::string const& str, Args&&... args)
  {
    Impl::msg(std::cout, "WARNING: " + str + "\n", FWD(args)...);
  }


  /// \brief test for condition and in case of failure print message
  /**
   * Same as \ref TEST_EXIT but does not throw an exception, or call assert.
   * It just tests for the condition and prints a message with prepended
   * string "WARNING".
   **/
  template <class... Args>
  void test_warning(bool condition, std::string const& str, Args&&... args)
  {
    if (!condition) { warning(str, FWD(args)...); }
  }


#if AMDIS_ENABLE_MSG_DBG
  /// \brief print message, in debug mode only
  /**
   * Same as \ref MSG, but is available only if preprocessor constant
   * \ref AMDIS_ENABLE_MSG_DBG is set to 1, otherwise the function is empty.
   **/
  template <class... Args>
  void msg_dbg(Args&&... args) { msg(FWD(args)...); }


  /// \brief call assert_msg, in debug mode only
  /**
   * Same as \ref TEST_EXIT, but is available only if preprocessor constant
   * \ref AMDIS_ENABLE_MSG_DBG is set to 1, otherwise the function is empty.
   **/
  template <class... Args>
  void test_exit_dbg(bool condition, Args&&... args)
  {
    test_exit(condition, FWD(args)...);
  }
#else
  template <class... Args>
  void msg_dbg(Args&&...) {}

  template <class... Args>
  void test_exit_dbg(bool, Args&&...) {}
#endif

} // end namespace AMDiS
