#include <iostream>
#include <memory>
#include <string>

#include <amdis/Observer.hpp>

#include "Tests.hpp"

namespace AMDiS {

namespace event {
  struct fooEvent {};
  struct barEvent {};
  struct bazEvent {};
}

} // end namespace AMDiS

std::string testValue = "";

bool checkAndResetTestValue(std::string testName, std::string ref, std::string refOpt = "X")
{
  bool result = testValue == ref || testValue == refOpt;
  testValue.clear();
  return result;
}

using namespace AMDiS;

class A
    : public Notifier<event::fooEvent, event::barEvent, event::bazEvent>
{
public:
  void foo()
  {
    std::cout << "A::foo()\n";
    testValue += "A.foo ";
    event::fooEvent e;
    this->notify(e);
  }

  void bar()
  {
    std::cout << "A::bar()\n";
    testValue += "A.bar ";
    event::barEvent e;
    this->notify(e);
  }

  void baz()
  {
    std::cout << "A::baz()\n";
    testValue += "A.baz ";
    event::bazEvent e;
    this->notify(e);
  }
};

class B
    : private Observer<event::fooEvent>
    , private Observer<event::bazEvent>
    , public Notifier<event::bazEvent>
{
public:
  B(A const& a)
    : Observer<event::fooEvent>(a)
    , Observer<event::bazEvent>(a)
    , a_(a)
  {}

  void updateImpl(event::fooEvent) override
  {
    std::cout << "B::update(foo)\n";
    testValue += "B.foo ";
  }

  void updateImpl(event::bazEvent e) override
  {
    std::cout << "B::update(baz)\n";
    testValue += "B.baz ";
    this->notify(e);
  }

  // Provide access to a_. This allows a class with access to B to observe A.
  A const& a() const { return a_; }
  A const& a_;
};

class C
    : private Observer<event::barEvent>
    , private Observer<event::bazEvent>
{
public:
  C(B const& b)
    : Observer<event::barEvent>(b.a())
    , Observer<event::bazEvent>(b)
  {}

  void updateImpl(event::barEvent) override
  {
    std::cout << "C::update(bar)\n";
    testValue += "C.bar ";
  }

  void updateImpl(event::bazEvent) override
  {
    std::cout << "C::update(baz)\n";
    testValue += "C.baz ";
  }
};

class D
    : public Notifier<event::barEvent>
{
public:
  void bar()
  {
    std::cout << "D::bar()\n";
    testValue += "D.bar ";
    event::barEvent e;
    this->notify(e);
  }
};

class E
    : public Observer<event::fooEvent>
    , public Observer<event::barEvent>
{
public:
  E(A const& a, D const& d)
      : Observer<event::fooEvent>(a)
      , Observer<event::barEvent>(d)
  {}

  void updateImpl(event::fooEvent) override
  {
    std::cout << "E::update(foo)\n";
    testValue += "E.foo ";
  }

  void updateImpl(event::barEvent) override
  {
    std::cout << "E::update(bar)\n";
    testValue += "E.bar ";
  }
};

class F
    : public ObserverSequence<event::barEvent, 2>
{
public:
  F(D const& d1, D const& d2)
    : ObserverSequence<event::barEvent, 2>(d1, d2)
  {}

  void updateImpl(event::barEvent, index_t<0> i) override
  {
    std::cout << "F::update(bar, 0)\n";
    testValue += "F.bar0 ";
  }

  void updateImpl(event::barEvent, index_t<1> i) override
  {
    std::cout << "F::update(bar, 1)\n";
    testValue += "F.bar1 ";
  }
};

/// Test the observer hierarchy.
/**
 *  There are 5 hierarchies tested.
 *  A::foo(): A -> B, E     (multiple observers [B,E] of one event)
 *  A::bar(): A -> (B) -> C (indirect observer [C of A] via access function)
 *  A::baz(): A -> B -> C   (simultanious observer and notifier of an event [B])
 *  D::bar(): D -> E        (observer of multiple classes [E])
 *  F::bar(): D1/D2 -> F    (observer of multiple classes with the same event [F])
 */
int main()
{
  using namespace AMDiS;

  A a;
  std::string ref, refOpt;
  {
    B b(a);
    C c(b);
    D d;
    E e(a, d);
    D d1;
    D d2;
    D d3;
    F f1(d1, d2);
    F f2(d3, d3);

    a.foo();
    ref = "A.foo B.foo E.foo ";
    refOpt = "A.foo E.foo B.foo ";  // Order of b.foo() and e.foo() unspecified
    AMDIS_TEST(checkAndResetTestValue("Test: both b and e are notified", ref, refOpt));

    a.bar();
    ref = "A.bar C.bar ";
    AMDIS_TEST(checkAndResetTestValue("Test: c is notified", ref));

    a.baz();
    ref = "A.baz B.baz C.baz ";
    AMDIS_TEST(checkAndResetTestValue("Test: b and c are notified in order", ref));

    d.bar();
    ref = "D.bar E.bar ";
    AMDIS_TEST(checkAndResetTestValue("Test: e is notified and the correct overload is chosen", ref));

    d1.bar();
    ref = "D.bar F.bar0 ";
    AMDIS_TEST(checkAndResetTestValue("Test: both d1 and d2 notify f1: d1", ref));
    d2.bar();
    ref = "D.bar F.bar1 ";
    AMDIS_TEST(checkAndResetTestValue("Test: both d1 and d2 notify f1: d2", ref));

    d3.bar();
    ref = "D.bar F.bar0 F.bar1 ";
    refOpt = "D.bar F.bar1 F.bar0 ";
    AMDIS_TEST(checkAndResetTestValue("Test: d3 notifies f2 twice", ref, refOpt));
  }

  // Observers went out of scope
  try {
    a.foo();
    ref = "A.foo ";
    AMDIS_TEST(checkAndResetTestValue("Test: observers detach properly: foo", ref));
    a.bar();
    ref = "A.bar ";
    AMDIS_TEST(checkAndResetTestValue("Test: observers detach properly: bar", ref));
    a.baz();
    ref = "A.baz ";
    AMDIS_TEST(checkAndResetTestValue("Test: observers detach properly: baz", ref));
  } catch(...) {
    std::cout << "Observer did not detach properly and threw an exception\n";
    Impl::num_errors()++;
  }

  return report_errors();
}
