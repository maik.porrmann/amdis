#pragma once

#if HAVE_MPI

namespace AMDiS { namespace Mpi {

// send mpi datatype
template <class Data, REQUIRES_(not Concepts::RawPointer<Data>)>
void Communicator::send(Data const& data, int to, Tag tag) const
{
  MPI_Send(to_void_ptr(&data), 1, type_to_mpi<Data>(), to, tag.value, comm_);
}


// send array of mpi datatypes
template <class Data>
void Communicator::send(Data const* data, std::size_t size, int to, Tag tag) const
{
  MPI_Send(to_void_ptr(data), int(size), type_to_mpi<Data>(), to, tag.value, comm_);
}


template <class T>
void Communicator::send(std::vector<T> const& vec, int to, Tag tag) const
{
  MPI_Send(to_void_ptr(vec.data()), int(vec.size()), type_to_mpi<T>(), to, tag.value, comm_);
}


// -------------------------------------------------------------------------------------


// send mpi datatype (non-blocking)
template <class Data, REQUIRES_(not Concepts::RawPointer<Data>)>
Request Communicator::isend(Data const& data, int to, Tag tag) const
{
  MPI_Request request = MPI_REQUEST_NULL;
  MPI_Isend(to_void_ptr(&data), 1, type_to_mpi<Data>(), to, tag.value, comm_, &request);
  return {request};
}


// send array of mpi datatypes (non-blocking)
template <class Data>
Request Communicator::isend(Data const* data, std::size_t size, int to, Tag tag) const
{
  MPI_Request request = MPI_REQUEST_NULL;
  MPI_Isend(to_void_ptr(data), size, type_to_mpi<Data>(), to, tag.value, comm_, &request);
  return {request};
}


template <class T>
Request Communicator::isend(std::vector<T> const& vec, int to, Tag tag) const
{
  MPI_Request request = MPI_REQUEST_NULL;
  MPI_Isend(to_void_ptr(vec.data()), int(vec.size()), type_to_mpi<T>(), to, tag.value, comm_, &request);
  return {request};
}

// -------------------------------------------------------------------------------------

// receive mpi datatype
template <class Data, REQUIRES_(not Concepts::RawPointer<Data>)>
MPI_Status Communicator::recv(Data& data, int from, Tag tag) const
{
  MPI_Status status;
  MPI_Recv(&data, 1, type_to_mpi<Data>(), from, tag.value, comm_, &status);
  return status;
}


// receive array of mpi datatypes
template <class Data>
MPI_Status Communicator::recv(Data* data, std::size_t size, int from, Tag tag) const
{
  MPI_Status status;
  MPI_Recv(data, size, type_to_mpi<Data>(), from, tag.value, comm_, &status);
  return status;
}


// receive array of mpi datatypes
template <class T>
MPI_Status Communicator::recv(std::vector<T>& vec, int from, Tag tag) const
{
  MPI_Status status;
  MPI_Probe(from, tag.value, comm_, &status);

  int size = 0;
  MPI_Get_count(&status, type_to_mpi<T>(), &size);
  int min_size = std::max(size,1);

  vec.resize(min_size);
  MPI_Recv(vec.data(), min_size, type_to_mpi<T>(), from, tag.value, comm_, MPI_STATUS_IGNORE);
  if (size != min_size)
    vec.resize(size);
  return status;
}


// -------------------------------------------------------------------------------------

// receive mpi datatype
template <class Data, REQUIRES_(not Concepts::RawPointer<Data>)>
Request Communicator::irecv(Data& data, int from, Tag tag) const
{
  MPI_Request request = MPI_REQUEST_NULL;
  MPI_Irecv(&data, 1, type_to_mpi<Data>(), from, tag.value, comm_, &request);
  return {request};
}


// receive array of mpi datatypes
template <class Data>
Request Communicator::irecv(Data* data, std::size_t size, int from, Tag tag) const
{
  MPI_Request request = MPI_REQUEST_NULL;
  MPI_Irecv(data, size, type_to_mpi<Data>(), from, tag.value, comm_, &request);
  return {request};
}


template <class T>
Request Communicator::irecv(std::vector<T>& vec, int from, Tag tag) const
{
  return Request{ RecvDynamicSize(from,tag.value,comm_,
    [comm=comm_,&vec](MPI_Status status) -> MPI_Request
    {
      int size = 0;
      MPI_Get_count(&status, type_to_mpi<T>(), &size);
      int min_size = std::max(size,1);

      vec.resize(min_size);
      MPI_Request req = MPI_REQUEST_NULL;
      MPI_Irecv(vec.data(), min_size, type_to_mpi<T>(), status.MPI_SOURCE, status.MPI_TAG, comm, &req);
      return req;
    },
    [&vec](MPI_Status status)
    {
      int size = 0;
      MPI_Get_count(&status, type_to_mpi<T>(), &size);
      vec.resize(size);
    }) };
}

}} // end namespace AMDiS::Mpi

#endif // HAVE_MPI
