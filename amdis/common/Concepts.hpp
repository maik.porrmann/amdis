#pragma once

#include <type_traits>

#include <dune/functions/common/functionconcepts.hh>
#include <dune/functions/functionspacebases/concepts.hh>

#include <amdis/common/ConceptsBase.hpp>
#include <amdis/common/Logical.hpp>
#include <amdis/common/TypeTraits.hpp>

namespace AMDiS
{
  /**
    * \defgroup Concepts Concepts
    * \brief Concept definitions
    * @{
    **/

  namespace Traits
  {
    template <class A, class B>
    struct IsSimilar
      : std::is_same<remove_cvref_t<A>, remove_cvref_t<B>> {};

    template <class A, class B>
    struct IsSimilar<Types<A>, Types<B>>
      : IsSimilar<A,B> {};

    template <>
    struct IsSimilar<Types<>, Types<>> : std::true_type {};

    template <class A0, class... As, class B0, class... Bs>
    struct IsSimilar<Types<A0,As...>, Types<B0,Bs...>>
      : and_t<IsSimilar<A0, B0>::value, IsSimilar<Types<As...>, Types<Bs...>>::value> {};


    template <class... Ts>
    struct IsSame;

    template <class T0, class... Ts>
    struct IsSame<T0, Ts...>
      : std::is_same<Types<T0,Ts...>, Types<Ts...,T0>> {};

    template <>
    struct IsSame<> : std::true_type {};


    template <class T>
    struct IsReferenceWrapper
      : std::false_type {};

    template <class T>
    struct IsReferenceWrapper<std::reference_wrapper<T>>
      : std::true_type {};

  } // end namespace Traits


  namespace Concepts
  {
#ifndef DOXYGEN
    namespace Definition
    {
      // f(args...)
      struct Callable
      {
        template <class F, class... Args>
        auto require(F&& f, Args&&... args) -> decltype(
          f(FWD(args)...)
        );
      };

      // idx[0]
      struct MultiIndex
      {
        template <class MI>
        auto require(MI&& idx) -> decltype(
          idx[0],
          idx.size(),
          idx.max_size()
        );
      };

      struct PreBasisFactory
      {
        template <class PBF, class GV>
        auto require(PBF const& pbf, GV const& gridView) -> decltype(
          pbf(gridView)
        );
      };

    } // end namespace Definition
#endif // DOXYGEN


    /// Types are the same
    template <class... Ts>
    constexpr bool Same = Traits::IsSame<Ts...>::value;

    template <class... Ts>
    using Same_t = Traits::IsSame<Ts...>;


    /// Types are the same, up to decay of qualifiers
    template <class A, class B>
    constexpr bool Similar = Traits::IsSimilar<A, B>::value;

    template <class A, class B>
    using Similar_t = Traits::IsSimilar<A, B>;


    /// \brief A Collable is a function `F` that can be called with arguments of type `Args...`.
    /**
      * To be used as follows: `Concepts::Collable<F, Args...>`. Returns true, if
      * an instance of `F` can be called by `operator()` with arguments of type
      * `Args...`.
      **/
    template <class F, class... Args>
    constexpr bool Callable = models<Definition::Callable(F, Args...)>;

    template <class F, class... Args>
    using Callable_t = models_t<Definition::Callable(F, Args...)>;


    /// \brief A Functor is a function `F` with signature `Signature`.
    /**
      * To be used as follows: `Concepts::Functor<F, R(Args...)>`. Returns true, if
      * an instance of `F` can be called by `operator()` with arguments of type
      * `Args...` and returns a value of type `R`, i.e. `Signature := R(Args...)`.
      **/
    template <class F, class Signature> // F, Signature=Return(Arg)
    constexpr bool Functor = Dune::Functions::Concept::isFunction<F, Signature>();

    template <class F, class Signature> // F, Signature=Return(Arg)
    using Functor_t = bool_t<Functor<F,Signature>>;


    /// A predicate is a function that returns a boolean.
    template <class F, class... Args>
    constexpr bool Predicate = Functor<F, bool(Args...)>;

    template <class F, class... Args>
    using Predicate_t = Functor_t<F, bool(Args...)>;


    /// A multi-index type
    template <class MI>
    constexpr bool MultiIndex = models<Definition::MultiIndex(MI)>;

    template <class MI>
    using MultiIndex_t = models_t<Definition::MultiIndex(MI)>;


    /// A Dune::Functions::HasIndexAccess type
    template <class Range, class Index>
    constexpr bool HasIndexAccess = models<Dune::Functions::Concept::HasIndexAccess(Range, Index)>;

    template <class Range, class Index>
    using HasIndexAccess_t = models_t<Dune::Functions::Concept::HasIndexAccess(Range, Index)>;


    /// A Dune::Functions::BasisNode type
    template <class N>
    constexpr bool BasisNode = models<Dune::Functions::Concept::BasisNode(N)>;

    template <class N>
    using BasisNode_t = models_t<Dune::Functions::Concept::BasisNode(N)>;


    /// A Dune::Functions::BasisTree type
    template <class Tree, class GV>
    constexpr bool BasisTree = models<Dune::Functions::Concept::BasisTree<GV>(Tree)>;

    template <class Tree, class GV>
    using BasisTree_t = models_t<Dune::Functions::Concept::BasisTree<GV>(Tree)>;


    /// A Dune::Functions::LocalView type
    template <class LV, class GB = typename LV::GlobalBasis>
    constexpr bool LocalView = models<Dune::Functions::Concept::LocalView<GB>(LV)>;

    template <class LV, class GB = typename LV::GlobalBasis>
    using LocalView_t = models_t<Dune::Functions::Concept::LocalView<GB>(LV)>;


    /// A Dune::Functions::GlobalBasis type
    template <class GB, class GV = typename GB::GridView>
    constexpr bool GlobalBasis = models<Dune::Functions::Concept::GlobalBasis<GV>(GB)>;

    template <class GB, class GV = typename GB::GridView>
    using GlobalBasis_t = models_t<Dune::Functions::Concept::GlobalBasis<GV>(GB)>;

    template <class PBF, class GV>
    constexpr bool PreBasisFactory = models<Definition::PreBasisFactory(PBF,GV)>;

    template <class PBF, class GV>
    using PreBasisFactory_t = models_t<Definition::PreBasisFactory(PBF,GV)>;

    /** @} **/

  } // end namespace Concepts
} // end namespace AMDiS
