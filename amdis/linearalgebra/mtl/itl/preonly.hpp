#pragma once

#include <cassert>
#include <boost/numeric/mtl/concept/collection.hpp>

namespace itl
{
  /// Solver that simply applies a preconditioner to the rhs vector
  template <typename Matrix, typename Vector, typename Preconditioner, typename Iteration>
  int preonly(const Matrix& A, Vector& x, const Vector& b, const Preconditioner& P, Iteration& iter)
  {
    assert(size(b) != 0);

    typedef typename mtl::Collection<Vector>::value_type Scalar;

    // simple richardson iteration
    Vector r(b - A*x);
    Scalar res = two_norm(r);
    for (; !iter.finished(res); ++iter)
    {
      x += Vector(solve(P, r));
      r = b - A*x;
      res = two_norm(r);
    }
    return iter;
  }

} // end namespace itl
