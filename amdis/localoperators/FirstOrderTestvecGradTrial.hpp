#pragma once

#include <type_traits>

#include <amdis/GridFunctionOperator.hpp>
#include <amdis/common/StaticSize.hpp>

namespace AMDiS
{
  /**
   * \addtogroup operators
   * @{
   **/

  namespace tag
  {
    struct testvec_gradtrial {};
  }


  /// first-order operator \f$ \langle\Psi, c\,\nabla\phi\rangle \f$
  class FirstOrderTestvecGradTrial
  {
  public:
    FirstOrderTestvecGradTrial(tag::testvec_gradtrial) {}

    template <class CG, class RN, class CN, class Quad, class LocalFct, class Mat>
    void assemble(CG const& contextGeo, RN const& rowNode, CN const& colNode,
                  Quad const& quad, LocalFct const& localFct, Mat& elementMatrix) const
    {
      static_assert(static_size_v<typename LocalFct::Range> == 1,
        "Expression must be of scalar type.");
      static_assert(RN::isPower && CN::isLeaf,
        "col-node must be Leaf-Node and row-node must be a Power-Node.");

      assert( rowNode.degree() == CG::dow );

      std::size_t rowSize = rowNode.child(0).size();
      std::size_t colSize = colNode.size();

      using RangeFieldType = typename CN::LocalBasis::Traits::RangeFieldType;
      using WorldVector = FieldVector<RangeFieldType,CG::dow>;
      std::vector<WorldVector> colGradients;

      for (auto const& qp : quad) {
        // Position of the current quadrature point in the reference element
        decltype(auto) local = contextGeo.local(qp.position());

        // The transposed inverse Jacobian of the map from the reference element to the element
        const auto jacobian = contextGeo.geometry().jacobianInverseTransposed(local);

        // The multiplicative factor in the integral transformation formula
        const auto factor = localFct(local) * contextGeo.integrationElement(qp.position()) * qp.weight();

        // the values of the shape functions on the reference element at the quadrature point
        auto const& shapeValues = rowNode.child(0).localBasisValuesAt(local);

        // The gradients of the shape functions on the reference element
        auto const& shapeGradients = colNode.localBasisJacobiansAt(local);

        // Compute the shape function gradients on the real element
        colGradients.resize(shapeGradients.size());

        for (std::size_t i = 0; i < colGradients.size(); ++i)
          jacobian.mv(shapeGradients[i][0], colGradients[i]);

        for (std::size_t i = 0; i < rowSize; ++i) {
          const auto value = factor * shapeValues[i];
          for (std::size_t j = 0; j < colSize; ++j) {
            const auto local_j = colNode.localIndex(j);
            for (std::size_t k = 0; k < rowNode.degree(); ++k) {
              const auto local_ki = rowNode.child(k).localIndex(i);
              elementMatrix[local_ki][local_j] += value * colGradients[j][k];
            }
          }
        }
      }

    }
  };

  template <class LC>
  struct GridFunctionOperatorRegistry<tag::testvec_gradtrial, LC>
  {
    static constexpr int degree = 1;
    using type = FirstOrderTestvecGradTrial;
  };

  /** @} **/

} // end namespace AMDiS
