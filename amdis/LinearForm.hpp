#pragma once

#include <dune/common/typeutilities.hh>

#include <amdis/LinearAlgebra.hpp>
#include <amdis/Observer.hpp>
#include <amdis/OperatorList.hpp>
#include <amdis/common/Concepts.hpp>
#include <amdis/common/ConceptsBase.hpp>
#include <amdis/common/FlatVector.hpp>
#include <amdis/common/TypeTraits.hpp>
#include <amdis/typetree/TreePath.hpp>

namespace AMDiS
{
  /// \brief The basic container that stores a base vector and a corresponding basis
  /**
   * A vector storing all the assembled Operators indexed with DOF indices. The vector
   * data is associated to a global basis.
   *
   * \tparam GB  Basis of the vector
   * \tparam T   Coefficient type to store in the vector
   * \tparam Traits  Collection of parameter for the linear-algebra backend
   **/
  template <class GB, class T = double, class Traits = BackendTraits>
  class LinearForm
      : public VectorFacade<T, Traits::template Vector<GB>::template Impl>
      , private Observer<event::adapt>
  {
    using Super = VectorFacade<T, Traits::template Vector<GB>::template Impl>;
    using Self = LinearForm;

  public:
    /// The type of the functionspace basis associated to this linearform
    using GlobalBasis = GB;

    /// The type of the elements of the DOFVector
    using CoefficientType = T;

    /// The type of the vector filled on an element with local contributions
    using ElementVector = FlatVector<CoefficientType>;

  public:
    /// Constructor. Stores the shared_ptr of the basis and creates a new DataTransfer.
    explicit LinearForm(std::shared_ptr<GB> const& basis)
      : Super(*basis)
      , Observer<event::adapt>(*basis)
      , basis_(basis)
    {
      auto const localSize = basis->localView().maxSize();
      elementVector_.resize(localSize);
    }

    /// Wraps the passed global basis into a (non-destroying) shared_ptr
    template <class GB_,
      REQUIRES(Concepts::Similar<GB_,GB>)>
    explicit LinearForm(GB_&& basis)
      : LinearForm(Dune::wrap_or_move(FWD(basis)))
    {}

    GlobalBasis const& basis() const { return *basis_; }

    /// \brief Associate a local operator with this LinearForm
    /**
     * Stores an operator in a list that gets assembled during a call to \ref assemble().
     * The operator may be assigned to a specific context, i.e. either an element
     * operator, an intersection operator, or a boundary operator.
     * The \p path tree paths specify the sub-basis for test functions the operator
     * is applied to.
     *
     * \tparam ContextTag  One of \ref tag::element_operator, \ref tag::intersection_operator
     *                     or \ref tag::boundary_operator indicating where to assemble
     *                     this operator.
     * \tparam Operator    An (pre-)operator that can be bound to a gridView, or a valid
     *                     GridOperator.
     * \tparam path        A tree-path for the Basis
     *
     * [[expects: path is valid tree-path in Basis]]
     * @{
     **/
    template <class ContextTag, class Operator, class TreePath>
    void addOperator(ContextTag contextTag, Operator const& op, TreePath path);

    // Add an operator to be assembled on the elements of the grid
    template <class Operator, class TreePath = RootTreePath>
    void addOperator(Operator const& op, TreePath path = {})
    {
      using E = typename GlobalBasis::LocalView::Element;
      addOperator(tag::element_operator<E>{}, op, path);
    }

    // Add an operator to be assembled on the intersections of the grid
    template <class Operator, class TreePath = RootTreePath>
    void addIntersectionOperator(Operator const& op, TreePath path = {})
    {
      using I = typename GlobalBasis::LocalView::GridView::Intersection;
      addOperator(tag::intersection_operator<I>{}, op, path);
    }
    /// @}


    /// Assemble the vector operators on the bound element.
    template <class LocalView, class LocalOperators,
      REQUIRES(Concepts::LocalView<LocalView>)>
    void assemble(LocalView const& localView, LocalOperators& localOperators);

    /// Assemble all vector operators added by \ref addOperator().
    void assemble();

    auto& operators() { return operators_; }

    /// Update all operators on the updated gridView
    void updateImpl(event::adapt e) override
    {
      if (e.value) {
        Recursive::forEach(operators_, [&](auto& op) { op.update(basis_->gridView()); });
      }
    }

  private:
    /// Dense vector to store coefficients during \ref assemble()
    ElementVector elementVector_;

    /// List of operators associated to nodes, filled in \ref addOperator().
    VectorOperators<GlobalBasis,ElementVector> operators_;

    std::shared_ptr<GlobalBasis const> basis_;
  };


  // deduction guide
  template <class GB>
  LinearForm(GB&& basis)
    -> LinearForm<Underlying_t<GB>>;


  template <class T = double, class GB>
  auto makeLinearForm(GB&& basis)
  {
    using LF = LinearForm<Underlying_t<GB>, T>;
    return LF{FWD(basis)};
  }

} // end namespace AMDiS

#include <amdis/LinearForm.inc.hpp>
