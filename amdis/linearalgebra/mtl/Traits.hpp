#pragma once

#include <dune/grid/common/partitionset.hh>
#include <amdis/linearalgebra/IndexDistribution.hpp>
#include <amdis/linearalgebra/mtl/SlotSize.hpp>
#include <amdis/linearalgebra/mtl/MatrixBackend.hpp>
#include <amdis/linearalgebra/mtl/VectorBackend.hpp>

namespace AMDiS
{
  /** Traits class for a linear solver for the system AX=B using an FE space described by a dune-functions Basis
   *  Contains typedefs specific to the MTL backend.
   */
  struct MTLTraits
  {
    template <class>
    using IndexDist = DefaultIndexDistribution;

    template <class,class>
    struct Matrix
    {
      using SparsityPattern = SlotSize;
      template <class Value>
      using Impl = MTLSparseMatrix<Value>;
    };

    template <class>
    struct Vector
    {
      template <class Value>
      using Impl = MTLVector<Value>;
    };

    using PartitionSet = Dune::Partitions::All;
  };

  using BackendTraits = MTLTraits;

} // end namespace AMDiS
