# FindMTL.cmake
#
# Finds the MTL4 library
#
# This will define the following variables
#
#    MTL_FOUND
#    MTL_INCLUDE_DIR
#    MTL_COMPILE_DEFINITIONS
#
# and the following imported targets
#
#     MTL::MTL
#
# Author: Simon Praetorius <simon.praetorius@tu-dresden.de>


mark_as_advanced(MTL_COMPILE_DEFINITIONS MTL_INCLUDE_DIR)

find_path(MTL_INCLUDE_DIR boost/numeric/mtl/mtl.hpp
  HINTS
    /opt/software
    /opt/sources
    /opt/development
    ENV EBROOTMTL
  PATH_SUFFIXES
    mtl mtl4
)

set(MTL_COMPILE_DEFINITIONS "MTL_ASSERT_FOR_THROW")
foreach(feature "MOVE" "AUTO" "RANGEDFOR" "INITLIST" "STATICASSERT" "DEFAULTIMPL")
  list(APPEND MTL_COMPILE_DEFINITIONS MTL_WITH_${feature})
endforeach(feature)

include(FindPackageHandleStandardArgs)
find_package_handle_standard_args(MTL
  REQUIRED_VARS MTL_INCLUDE_DIR
)

# text for feature summary
set_package_properties("MTL" PROPERTIES
  DESCRIPTION "Matrix Template Library")

if(MTL_FOUND AND NOT TARGET MTL::MTL)
  add_library(MTL::MTL INTERFACE IMPORTED)

  find_package(SuiteSparse COMPONENTS UMFPACK)
  if (SuiteSparse_UMFPACK_FOUND)
    list(APPEND MTL_COMPILE_DEFINITIONS "MTL_HAS_UMFPACK")
    target_link_libraries(MTL::MTL INTERFACE SuiteSparse::UMFPACK)
  endif ()

  find_package(HYPRE)
  if (HYPRE_FOUND)
    list(APPEND MTL_COMPILE_DEFINITIONS "MTL_HAS_HYPRE")
    target_link_libraries(MTL::MTL INTERFACE HYPRE::HYPRE)
  endif ()

	find_package(OpenMP)
  if (OpenMP_CXX_FOUND)
    list(APPEND MTL_COMPILE_DEFINITIONS "MTL_WITH_OPENMP")
    target_link_libraries(MTL::MTL INTERFACE OpenMP::OpenMP_CXX)
  endif ()

  set_target_properties(MTL::MTL PROPERTIES
    INTERFACE_INCLUDE_DIRECTORIES "${MTL_INCLUDE_DIR}"
    INTERFACE_COMPILE_DEFINITIONS "${MTL_COMPILE_DEFINITIONS}")
endif()
