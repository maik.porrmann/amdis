#pragma once

#include <type_traits>

#include <amdis/GridFunctionOperator.hpp>
#include <amdis/common/StaticSize.hpp>

namespace AMDiS
{
  /**
   * \addtogroup operators
   * @{
   **/

  namespace tag
  {
    struct partialtest_partialtrial
    {
      std::size_t comp_test; // i
      std::size_t comp_trial; // j
    };
  }


  /// second-order operator \f$ \langle\partial_i\psi, c\,\partial_j\phi\rangle \f$
  class SecondOrderPartialTestPartialTrial
  {
    int compTest_;
    int compTrial_;

  public:
    SecondOrderPartialTestPartialTrial(tag::partialtest_partialtrial tag)
      : compTest_(tag.comp_test)
      , compTrial_(tag.comp_trial)
    {}

    template <class CG, class RN, class CN, class Quad, class LocalFct, class Mat>
    void assemble(CG const& contextGeo, RN const& rowNode, CN const& colNode,
                  Quad const& quad, LocalFct const& localFct, Mat& elementMatrix) const
    {
      static_assert(static_size_v<typename LocalFct::Range> == 1,
        "Expression must be of scalar type." );
      static_assert(RN::isLeaf && CN::isLeaf,
        "Operator can be applied to Leaf-Nodes only.");

      LocalToGlobalBasisAdapter rowLocalBasis(rowNode, contextGeo.geometry());
      LocalToGlobalBasisAdapter colLocalBasis(colNode, contextGeo.geometry());

      for (auto const& qp : quad) {
        // Position of the current quadrature point in the reference element
        decltype(auto) local = contextGeo.local(qp.position());

        // The multiplicative factor in the integral transformation formula
        auto factor = contextGeo.integrationElement(qp.position()) * qp.weight();
        factor *= localFct(local);

        auto const& rowPartial = rowLocalBasis.partialsAt(local, compTest_);
        auto const& colPartial = colLocalBasis.partialsAt(local, compTrial_);

        for (std::size_t j = 0; j < colLocalBasis.size(); ++j) {
          const auto local_j = colNode.localIndex(j);
          const auto value = factor * colPartial[j];
          for (std::size_t i = 0; i < rowLocalBasis.size(); ++i) {
            const auto local_i = rowNode.localIndex(i);
            elementMatrix[local_i][local_j] += value * rowPartial[i];
          }
        }
      }

    }
  };

  template <class LC>
  struct GridFunctionOperatorRegistry<tag::partialtest_partialtrial, LC>
  {
    static constexpr int degree = 2;
    using type = SecondOrderPartialTestPartialTrial;
  };


  /// Create a second-order term of partial derivatives
  template <class Expr>
  auto sot_ij(Expr&& expr, std::size_t comp_test, std::size_t comp_trial, int quadOrder = -1)
  {
    return makeOperator(tag::partialtest_partialtrial{comp_test, comp_trial}, FWD(expr), quadOrder);
  }

  /** @} **/

} // end namespace AMDiS
