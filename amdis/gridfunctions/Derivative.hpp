#pragma once

#include <type_traits>

#include <amdis/common/Concepts.hpp>
#include <amdis/common/DerivativeTraits.hpp>
#include <amdis/common/Index.hpp>

namespace AMDiS
{
  /// The derivative of a localfunction as localfunction itself
  template <class LocalFunction, class Type,
    REQUIRES(std::is_convertible_v<tag::derivative_type, Type>)>
  auto derivativeOf(LocalFunction const& lf, Type const& type)
    -> decltype(lf.makeDerivative(type))
  {
    return lf.makeDerivative(type);
  }

  /// Implementation of local-function derivative interface of dune-functions.
  /// Implements the jacobian derivative type.
  template <class LocalFunction>
  auto derivative(LocalFunction const& lf)
    -> decltype(lf.makeDerivative(tag::gradient{}))
  {
    return lf.makeDerivative(tag::gradient{});
  }


  namespace Concepts
  {
    /** \addtogroup Concepts
     *  @{
     **/

    namespace Definition
    {
      struct HasDerivative
      {
        template <class F, class T>
        auto require(F&& f, T&& t) -> decltype( derivativeOf(f,t) );
      };

      struct HasLocalFunctionDerivative
      {
        template <class F, class T>
        auto require(F&& f, T&& t) -> decltype( derivativeOf(localFunction(f),t) );
      };

      struct HasPartial
      {
        template <class F, class I>
        auto require(F&& f, I&& i) -> decltype( partial(f, i) );
      };

    } // end namespace Definition


    /// \brief GridFunction GF has free function `derivativeOf(F,type)`
    template <class GF, class Type>
    constexpr bool HasDerivative = models<Definition::HasDerivative(GF,Type)>;

    template <class GF, class Type>
    using HasDerivative_t = models_t<Definition::HasDerivative(GF,Type)>;


    /// \brief GridFunction GF has free function `derivativeOf(localFunction(F))`
    template <class GF, class Type>
    constexpr bool HasLocalFunctionDerivative = models<Definition::HasLocalFunctionDerivative(GF,Type)>;

    template <class GF, class Type>
    using HasLocalFunctionDerivative_t = models_t<Definition::HasLocalFunctionDerivative(GF,Type)>;


    /// \brief Functor F has free function `partial(F,_0)`
    template <class F>
    constexpr bool HasPartial = models<Definition::HasPartial(F,index_t<0>)>;

    template <class F>
    using HasPartial_t = models_t<Definition::HasPartial(F,index_t<0>)>;

    /** @} **/

  } // end namespace Concepts

} // end namespace AMDiS
