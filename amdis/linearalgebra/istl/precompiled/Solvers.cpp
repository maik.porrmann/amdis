#include <config.h>

#include "Solvers.hpp"

namespace Dune
{
  template class CGSolver<Precompiled::Vector>;
  template class GeneralizedPCGSolver<Precompiled::Vector>;
  template class BiCGSTABSolver<Precompiled::Vector>;
  template class MINRESSolver<Precompiled::Vector>;
  template class RestartedGMResSolver<Precompiled::Vector>;
  template class RestartedFCGSolver<Precompiled::Vector>;
  template class CompleteFCGSolver<Precompiled::Vector>;
  template class RestartedFlexibleGMResSolver<Precompiled::Vector>;

#if HAVE_SUITESPARSE_UMFPACK
  template class UMFPack<Precompiled::Matrix>;
#endif

#if HAVE_SUITESPARSE_LDL
  template class LDL<Precompiled::Matrix>;
#endif

#if HAVE_SUITESPARSE_SPQR
  template class SPQR<Precompiled::Matrix>;
#endif

} // end namespace Dune
