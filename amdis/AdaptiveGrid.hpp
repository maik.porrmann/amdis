#pragma once

#include <algorithm>
#include <list>
#include <memory>
#include <mutex>
#include <shared_mutex>
#include <type_traits>
#include <utility>

#include <dune/common/hybridutilities.hh>
#include <dune/common/timer.hh>
#include <dune/common/version.hh>
#include <dune/common/parallel/mpihelper.hh>

#include <dune/geometry/type.hh>
#include <dune/grid/common/backuprestore.hh>
#include <dune/grid/common/capabilities.hh>
#include <dune/grid/common/grid.hh>
#include <dune/grid/utility/structuredgridfactory.hh>

#include <amdis/common/Concepts.hpp>
#include <amdis/common/DefaultGridView.hpp>
#include <amdis/common/SharedPtr.hpp>
#include <amdis/Observer.hpp>
#include <amdis/Output.hpp>

namespace AMDiS
{
  // forward declaration
  template <class HostGrid>
  class AdaptiveGridFamily;


  /// \brief Wrapper class for Dune-grids that allows automatic signalling of events
  /// during grid adaptation.
  /**
   *  Calls to grid.preAdapt(), grid.adapt() and grid.postAdapt() need to be replaced by calls to
   *  the corresponding methods of this class to use the automatic update functionality.
   *
   * \tparam HG  Host grid to be wrapped. Must implement the dune grid interface.
   **/
  template <class HG>
  class AdaptiveGrid
      : public Dune::GridDefaultImplementation<
          HG::dimension, HG::dimensionworld, typename HG::ctype, AdaptiveGridFamily<HG> >
      , public Notifier<event::preAdapt, event::adapt, event::postAdapt>
  {
    using Self = AdaptiveGrid<HG>;

  public:

    using HostGrid   = HG;
    using GridFamily = AdaptiveGridFamily<HG>;
    using Traits     = typename GridFamily::Traits;
    using Element    = typename Traits::template Codim<0>::Entity;


  public:

    template <class HostGrid_,
      Dune::disableCopyMove<Self, HostGrid_> = 0>
    explicit AdaptiveGrid(HostGrid_&& hostGrid)
      : hostGrid_(wrap_or_share(FWD(hostGrid)))
    {}

    /// Return the underlying grid
    std::shared_ptr<HostGrid> const& hostGrid() const { return hostGrid_; }


  public:

    /// Grid iterators
    /// @{

    /// Iterator to first entity of given codim on level
    template <int codim, Dune::PartitionIteratorType pt = Dune::All_Partition>
    auto lbegin(int level) const { return hostGrid_->levelGridView(level).template begin<codim,pt>(); }

    /// one past the end on this level
    template <int codim, Dune::PartitionIteratorType pt = Dune::All_Partition>
    auto lend(int level) const { return hostGrid_->levelGridView(level).template end<codim,pt>(); }


    /// Iterator to first leaf entity of given codim
    template <int codim, Dune::PartitionIteratorType pt = Dune::All_Partition>
    auto leafbegin() const { return hostGrid_->leafGridView().template begin<codim,pt>(); }

    /// One past the end of the sequence of leaf entities
    template <int codim, Dune::PartitionIteratorType pt = Dune::All_Partition>
    auto leafend() const { return hostGrid_->leafGridView().template end<codim,pt>(); }


    /// Obtain begin intersection iterator with respect to the level GridView
    auto ilevelbegin(Element const& e) const { return hostGrid_->levelGridView(e.level()).ibegin(e); }

    /// Obtain end intersection iterator with respect to the level GridView
    auto ilevelend(Element const& e) const { return hostGrid_->levelGridView(e.level()).iend(e); }

    /// Obtain begin intersection iterator with respect to the leaf GridView
    auto ileafbegin(Element const& e) const { return hostGrid_->leafGridView().ibegin(e); }

    /// Obtain end intersection iterator with respect to the leaf GridView
    auto ileafend(Element const& e) const { return hostGrid_->leafGridView().iend(e); }

    /// @}


    /// Size methods
    /// @{

    /// Return maximum level defined in this grid.
    int maxLevel() const { return hostGrid_->maxLevel(); }

    /// Number of grid entities per level and codim
    int size(int level, int codim) const { return hostGrid_->size(level, codim); }

    /// Return number of leaf entities of a given codim in this process
    int size(int codim) const { return hostGrid_->size(codim); }

    /// Return number of entities per level and geometry type in this process
    int size(int level, Dune::GeometryType type) const { return hostGrid_->size(level, type); }

    /// Return number of leaf entities per geometry type in this process
    int size(Dune::GeometryType type) const { return hostGrid_->size(type); }

    /// Returns the number of boundary segments within the macro grid
    std::size_t numBoundarySegments() const { return hostGrid_->numBoundarySegments(); }

    /// @}


    /// Access to index and id sets
    /// @{

    /// Return const reference to the grids global id set
    auto const& globalIdSet() const { return hostGrid_->globalIdSet(); }

    /// Return const reference to the host grids local id set
    auto const& localIdSet() const { return hostGrid_->localIdSet(); }

    /// Return const reference to the host grids level index set for level level
    auto const& levelIndexSet(int level) const { return hostGrid_->levelIndexSet(level); }

    /// Return const reference to the host grids leaf index set
    auto const& leafIndexSet() const { return hostGrid_->leafIndexSet(); }

    /// @}


    /// Adaptivity and grid refinement
    /// @{

    /// Refines all grid elements refCount times.
    void globalRefine(int refCount)
    {
      for (int i = 0; i < refCount; ++i) {
        // mark all entities for grid refinement
        for (const auto& element : elements(hostGrid_->leafGridView()))
          hostGrid_->mark(1, element);
        preAdapt();
        adapt();
        postAdapt();
      }
    }

    /// Marks an entity to be refined/coarsened in a subsequent adapt
    bool mark(int refCount, Element const& e) { return hostGrid_->mark(refCount, e); }

    /// Return refinement mark for entity
    int getMark(Element const& e) const { return hostGrid_->getMark(e); }

    /// Prepare the grid for adaptation and notify observers of the preAdapt event
    bool preAdapt()
    {
      Dune::Timer t;
      mightCoarsen_ = hostGrid_->preAdapt();
      hostGrid_->comm().max(&mightCoarsen_, 1);
      this->notify(event::preAdapt{mightCoarsen_});
      info(2,"AdaptiveGrid::preAdapt needed {} seconds", t.elapsed());
      return mightCoarsen_;
    }

    /// Adapt the grid and notify observers of the adapt event
    bool adapt()
    {
      Dune::Timer t;
      refined_ = hostGrid_->adapt();
      hostGrid_->comm().max(&refined_, 1);
      this->notify(event::adapt{mightCoarsen_ || refined_});
      info(2,"AdaptiveGrid::adapt needed {} seconds", t.elapsed());
      return refined_;
    }

    /// Perform cleanup after grid adaptation and notify observers of the postAdapt event
    void postAdapt()
    {
      Dune::Timer t;
      hostGrid_->postAdapt();
      this->notify(event::postAdapt{});
      changeIndex_++;
      info(2,"AdaptiveGrid::postAdapt needed {} seconds", t.elapsed());
    }

    /// Update all registered dependent objects if grid is changed manually
    void update()
    {
      this->notify(event::adapt{true});
    }

    /// Returns the grid change index, see \ref changeIndex.
    unsigned long changeIndex() const
    {
      return changeIndex_;
    }

    // @}


    /// Parallel data distribution and communication
    /// @{

    /// Return const reference to a collective communication object.
    auto const& comm() const { return hostGrid_->comm(); }

    /// Communicate data of level gridView
    template <class DataHandle>
    void communicate(DataHandle& handle, Dune::InterfaceType iftype,
                     Dune::CommunicationDirection dir, int level) const
    {
      hostGrid_->levelGridView(level).communicate(handle,iftype,dir);
    }

    /// Communicate data of leaf gridView
    template <class DataHandle>
    void communicate(DataHandle& handle, Dune::InterfaceType iftype,
                     Dune::CommunicationDirection dir) const
    {
      hostGrid_->leafGridView().communicate(handle,iftype,dir);
    }

    /// \brief Calls loadBalance on the underlying grid.
    /**
     *  Re-balances the load each process has to handle for a parallel grid.
     *
     *  \return true if the grid has changed.
     **/
    bool loadBalance()
    {
      if constexpr (std::is_convertible_v<decltype(std::declval<HG>().loadBalance()), bool>)
        return hostGrid_->loadBalance();
      else {
        hostGrid_->loadBalance();
        return true;
      }
    }

    /// \brief Calls loadBalance(handle) on the underlying grid.
    /**
     *  Re-balances the load each process has to handle for a parallel grid and moves the data.
     *
     *  \param data  A data handle telling the method which data should be communicated
     *               and how. Has to adhere to the interface described by Dune::CommDataHandleIf.
     *  \return true if the grid has changed.
     **/
    template <class DataHandle>
    bool loadBalance(DataHandle& handle)
    {
      if constexpr (std::is_convertible_v<decltype(std::declval<HG>().loadBalance(handle)), bool>)
        return hostGrid_->loadBalance(handle);
      else {
        hostGrid_->loadBalance(handle);
        return true;
      }
    }


    /// Return size of the overlap region for a given codim on the level grid view.
    int overlapSize(int level, int codim) const { return hostGrid_->levelGridView(level).overlapSize(codim); }

    /// Return size of the overlap region for a given codim on the leaf grid view.
    int overlapSize(int codim) const { return hostGrid_->leafGridView().overlapSize(codim); }

    /// Return size of the ghost region for a given codim on the level grid view.
    int ghostSize(int level, int codim) const { return hostGrid_->levelGridView(level).ghostSize(codim); }

    /// Return size of the ghost region for a given codim on the leaf grid view.
    int ghostSize(int codim) const { return hostGrid_->leafGridView().ghostSize(codim); }

    /// @}


    /// Obtain Entity from EntitySeed of the HostGrid.
    template <class EntitySeed>
    auto entity(EntitySeed const& seed) const { return hostGrid_->entity(seed); }


  private:

    /// The underlying grid implementation
    std::shared_ptr<HostGrid> hostGrid_;

    /// Flag set during \ref preAdapt(), indicating whether any element might be
    /// coarsened in \ref adapt()
    bool mightCoarsen_ = false;

    /// Flag set during \ref adapt() indicating that at least one entity was refined
    bool refined_ = false;

    /// This index is incremented every time the grid is changed, e.g. by refinement
    /// or coarsening.
    unsigned long changeIndex_ = 0;
  };

  // deduction guide
  template <class HostGrid>
  AdaptiveGrid(HostGrid const&)
    -> AdaptiveGrid<HostGrid>;


  template <class HostGrid>
  class AdaptiveGridFamily
  {
  public:
    struct Traits : HostGrid::Traits
    {
      using Grid = AdaptiveGrid<HostGrid>;
      using LeafGridView = Dune::GridView< AMDiS::DefaultLeafGridViewTraits<const Grid> >;
      using LevelGridView = Dune::GridView< AMDiS::DefaultLevelGridViewTraits<const Grid> >;
    };
  };


  /// Return a change index of a grid that is not an \ref AdaptiveGrid
  template <class HostGrid>
  unsigned long changeIndex(HostGrid const& /*hostGrid*/)
  {
    return 0;
  }

  /// Return a change index of an \ref AdaptiveGrid
  template <class HostGrid>
  unsigned long changeIndex(AdaptiveGrid<HostGrid> const& grid)
  {
    return grid.changeIndex();
  }


  namespace Impl
  {
    template <class HostGrid>
    struct AdaptiveGridImpl
    {
      using type = AdaptiveGrid<HostGrid>;
    };

    template <class HostGrid>
    struct AdaptiveGridImpl<AdaptiveGrid<HostGrid>>
    {
      using type = AdaptiveGrid<HostGrid>;
    };
  }

  /// Always returning an \ref AdaptiveGrid. Returns the grid itself if it is
  /// already an AdaptiveGrid.
  template <class HostGrid>
  using AdaptiveGrid_t = typename Impl::AdaptiveGridImpl<HostGrid>::type;


} // end namespace AMDiS


namespace Dune
{
  /// Specialization of a \ref GridFactory to \ref AdaptiveGrid.
  /// Provide a generic factory class for unstructured grids.
  template <class HostGrid>
  class GridFactory<AMDiS::AdaptiveGrid<HostGrid> >
      : public GridFactoryInterface<AMDiS::AdaptiveGrid<HostGrid> >
  {
    using Self = GridFactory;
    using Super = GridFactoryInterface<AMDiS::AdaptiveGrid<HostGrid> >;
    using GridType = AMDiS::AdaptiveGrid<HostGrid>;
    using HostGridFactory = GridFactory<HostGrid>;

  public:

    using ctype = typename GridType::ctype;

    enum { dim = GridType::dimension };
    enum { dimworld = GridType::dimensionworld };

    template <class... Args,
      Dune::disableCopyMove<Self, Args...> = 0>
    GridFactory(Args&&... args)
      : hostFactory_(FWD(args)...)
    {}

    /// Insert a vertex into the coarse grid
    void insertVertex(FieldVector<ctype,dimworld> const& pos) override
    {
      hostFactory_.insertVertex(pos);
    }

    template <class F, class... Args>
    using HasInsertElement = decltype(std::declval<F>().insertElement(std::declval<Args>()...));

    /// Insert an element into the coarse grid
    void insertElement(GeometryType const& type,
                       std::vector<unsigned int> const& vertices) override
    {
      hostFactory_.insertElement(type, vertices);
    }

    using ElementParametrizationType
      = std::function<FieldVector<ctype,dimworld>(FieldVector<ctype,dim>)>;

    /// Insert a parametrized element into the coarse grid
    void insertElement(GeometryType const& type,
                       std::vector<unsigned int> const& vertices,
                       ElementParametrizationType elementParametrization) override
    {
      using A0 = GeometryType;
      using A1 = std::vector<unsigned int>;
      using A2 = ElementParametrizationType;
      if constexpr (Std::is_detected<HasInsertElement, HostGridFactory, A0,A1,A2>::value)
        hostFactory_.insertElement(type, vertices, elementParametrization);
      else
        AMDiS::error_exit("insertElement() not implemented for HostGrid type.");
    }
    using Super::insertElement;

    template <class F, class... Args>
    using HasInsertBoundarySegment = decltype(std::declval<F>().insertBoundarySegment(std::declval<Args>()...));

    //// Insert a boundary segment
    void insertBoundarySegment(std::vector<unsigned int> const& vertices) override
    {
      hostFactory_.insertBoundarySegment(vertices);
    }

    using BoundarySegmentType = std::shared_ptr<BoundarySegment<dim,dimworld> >;

    /// Insert an arbitrarily shaped boundary segment
    void insertBoundarySegment(std::vector<unsigned int> const& vertices,
                               BoundarySegmentType const& boundarySegment) override
    {
      using A0 = std::vector<unsigned int>;
      using A1 = BoundarySegmentType;
      if constexpr (Std::is_detected<HasInsertBoundarySegment, HostGridFactory, A0,A1>::value)
        hostFactory_.insertBoundarySegment(vertices, boundarySegment);
      else
        AMDiS::error_exit("insertBoundarySegment() not implemented for HostGrid type.");
    }

    /// Finalize grid creation and hand over the grid
    std::unique_ptr<GridType> createGrid() override
    {
      std::unique_ptr<HostGrid> hostGrid(hostFactory_.createGrid());
      return std::make_unique<GridType>(std::move(hostGrid));
    }

  private:
    HostGridFactory hostFactory_;
  };


  namespace Impl
  {
    template <class HostGrid, bool = Dune::Capabilities::hasBackupRestoreFacilities<HostGrid>::v>
    class BackupRestoreFacilityImpl {};

    template <class HostGrid>
    class BackupRestoreFacilityImpl<HostGrid,true>
    {
      using Grid = AMDiS::AdaptiveGrid<HostGrid>;
      using HostBackupRestoreFacility = BackupRestoreFacility<HostGrid>;

    public:

      /// Backup the grid to file or stream
      template <class Output>
      static void backup(Grid const& grid, Output const& filename_or_stream)
      {
        HostBackupRestoreFacility::backup(*grid.hostGrid(), filename_or_stream);
      }

      /// Restore the grid from file or stream
      template <class Input>
      static Grid* restore(Input const& filename_or_stream)
      {
        std::unique_ptr<HostGrid> hostGrid(HostBackupRestoreFacility::restore(filename_or_stream));
        return new Grid(std::move(hostGrid));
      }
    };

  } // end namespace Impl


  /// Specialization of \ref BackupRestoreFacility to \ref AdaptiveGrid.
  /// Facility for writing and reading grids.
  template <class HostGrid>
  class BackupRestoreFacility<AMDiS::AdaptiveGrid<HostGrid>>
      : public Impl::BackupRestoreFacilityImpl<HostGrid>
  {
  public:
    using Impl::BackupRestoreFacilityImpl<HostGrid>::BackupRestoreFacilityImpl;
  };


  namespace Capabilities
  {
    template <class HostGrid, int codim>
    struct hasEntity<AMDiS::AdaptiveGrid<HostGrid>, codim>
        : hasEntity<HostGrid,codim>{};

    template <class HostGrid, int codim>
    struct hasEntityIterator<AMDiS::AdaptiveGrid<HostGrid>, codim>
        : hasEntityIterator<HostGrid, codim> {};

    template <class HostGrid>
    struct isLevelwiseConforming<AMDiS::AdaptiveGrid<HostGrid> >
        : isLevelwiseConforming<HostGrid> {};

    template <class HostGrid>
    struct isLeafwiseConforming<AMDiS::AdaptiveGrid<HostGrid> >
        : isLeafwiseConforming<HostGrid> {};

    template <class HostGrid>
    struct hasSingleGeometryType<AMDiS::AdaptiveGrid<HostGrid> >
        : hasSingleGeometryType<HostGrid> {};

    template <class HostGrid, int codim >
    struct canCommunicate<AMDiS::AdaptiveGrid<HostGrid>, codim>
        : canCommunicate<HostGrid, codim> {};

    template <class HostGrid>
    struct hasBackupRestoreFacilities<AMDiS::AdaptiveGrid<HostGrid> >
        : hasBackupRestoreFacilities<HostGrid> {};

    template <class HostGrid>
    struct threadSafe<AMDiS::AdaptiveGrid<HostGrid> >
        : threadSafe<HostGrid> {};

    template <class HostGrid>
    struct viewThreadSafe<AMDiS::AdaptiveGrid<HostGrid> >
        : viewThreadSafe<HostGrid> {};

  } // end namespace Capabilities
} // end namespace Dune
