AMDiS
=====
The *Adaptive Multi-Dimensional Simulation Toolbox* (AMDiS) is implemented as a
discretization module on top of the Dune framework.

Documentation
=============
See the online documentation [amdis.readthedocs.io](https://amdis.readthedocs.io) that is
generated from the Markdown files in the `docs/` directory.

Mirror
======
This repositories is mirrored to https://gitlab.mn.tu-dresden.de/amdis/amdis-core.

Installation
============
A detailed explanation how to get started with AMDiS and how to install all dependencies and
the library is given in [docs/getting-started.md](docs/getting-started.md). Here a simplified
installation instruction is provided for users already familiar with Dune.

We provide a *cmake*-based configuration and use the `dunecontrol` build system of the Dune
framework. Simply run

```
dunecontrol --current all
```

The `dunecontrol` script searches for the required
(and suggested) dune modules this library depends on. These include:
- [dune-common](https://gitlab.dune-project.org/core/dune-common)
- [dune-geometry](https://gitlab.dune-project.org/core/dune-geometry)
- [dune-grid](https://gitlab.dune-project.org/core/dune-grid)
- [dune-localfunctions](https://gitlab.dune-project.org/core/dune-localfunctions)
- [dune-istl](https://gitlab.dune-project.org/core/dune-istl)
- [dune-typetree](https://gitlab.dune-project.org/staging/dune-typetree)
- [dune-functions](https://gitlab.dune-project.org/staging/dune-functions)

(See the file `dune.module` for an up-to-date list of dependencies). The dune modules
can be obtained from https://gitlab.dune-project.org and need to be found in a
subdirectory of `DUNE_CONTROL_PATH`. See also https://dune-project.org/doc/installation
for details about the installation of dune modules. You can use the script

```
bin/install_all_dune_modules.sh --prefix DOWNLOAD_DIR
```

to clone all dune repositories into `DOWNLOAD_DIR` and provide a simple way using `dunecontrol`
to install all of them at once.

Additionally the following optional libraries can be used:
- [MTL4](https://gitlab.math.tu-dresden.de/spraetor/mtl4) (use this fork to get up-to-date changes)
- [Eigen3](http://eigen.tuxfamily.org) >= 3.3
- [SuiteSparse](http://faculty.cse.tamu.edu/davis/suitesparse.html)
- [PETSc](https://www.mcs.anl.gov/petsc)
- libalberta >= 3.0 (For Alberta-Grids)

And a compiler that supports the C++17 standard, e.g. g++ >= 7.0 and clang >= 5.0, and cmake >= 3.13.

By default, the `dune-istl` linear-algebra backend is used. To choose one of `ISTL`, `MTL`,
`PETSC`, or `EIGEN`, you can specify the cmake parameter `-DBACKEND=[ISTL,MTL,PETSC,EIGEN]`.

If, for example, your MTL4 installation is not found by default, you have to specify the path,
where the file `MTLConfig.cmake` is found, here called `MTL_ROOT`. Then simply use
`dunecontrol` to configure and `cmake` to build:

```
CMAKE_FLAGS="-DBACKEND=MTL -DMTL_ROOT:PATH=[MTL_ROOT]" dunecontrol --current configure
cmake --build build-cmake
```
