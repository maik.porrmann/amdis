#pragma once

#include <type_traits>

#include <dune/common/shared_ptr.hh>

#include <amdis/common/Concepts.hpp>
#include <amdis/common/ConceptsBase.hpp>
#include <amdis/common/TypeTraits.hpp>
#include <amdis/functions/NodeIndices.hpp>
#include <amdis/typetree/MultiIndex.hpp>

namespace AMDiS
{
  /**
   * Basis implementation of DOFMatrix, i.e. a sparse matrix storing all the
   * assembled Operators indexed with DOF indices. The matrix data is associated
   * to a row and column global basis.
   *
   * \tparam T          The coefficient type of the matrix
   * \tparam MatrixImpl A linear-algebra backend for the matrix storage
   **/
  template <class T, template <class> class MatrixImpl>
  class MatrixFacade
  {
    using Self = MatrixFacade;
    using Impl = MatrixImpl<T>;

  public:
    /// Constructor. Forwards the bases to the implementation class and
    /// constructs a matrix sparsity pattern.
    template <class RowBasis, class ColBasis>
    MatrixFacade(RowBasis const& rowBasis, ColBasis const& colBasis)
      : impl_(rowBasis.indexDistribution(), colBasis.indexDistribution())
    {}

    /// Return the underlying matrix backend
    Impl const& impl() const { return impl_; }
    Impl&       impl()       { return impl_; }

    /// Initialize the matrix for insertion and allocate the non-zero pattern
    template <class SparsityPattern>
    void init(SparsityPattern const& pattern)
    {
      impl_.init(pattern);
    }

    /// Initialize the matrix for insertion while keeping the pattern unchanged
    void init()
    {
      impl_.init();
    }

    /// Finish the matrix insertion, e.g. cleanup or final insertion
    void finish()
    {
      impl_.finish();
    }

    /// Insert a single value into the matrix (add to existing value)
    template <class RowIndex, class ColIndex,
      REQUIRES(Concepts::MultiIndex<RowIndex>),
      REQUIRES(Concepts::MultiIndex<ColIndex>)>
    void insert(RowIndex const& row, ColIndex const& col, typename Impl::value_type const& value)
    {
      impl_.insert(row, col, value);
    }

    /// Insert a block of values into the sparse matrix (add to existing values)
    /// The global matrix indices are determined by the corresponding localviews.
    template <class RowLocalView, class ColLocalView, class LocalMatrix,
      REQUIRES(Concepts::LocalView<RowLocalView>),
      REQUIRES(Concepts::LocalView<ColLocalView>)>
    void scatter(RowLocalView const& r, ColLocalView const& c, LocalMatrix const& localMatrix)
    {
      assert(r.size() * c.size() == localMatrix.size());
      assert(r.size() == localMatrix.rows());
      assert(c.size() == localMatrix.cols());

      const bool optimized = std::is_same_v<RowLocalView,ColLocalView>
        && r.tree().treeIndex() == c.tree().treeIndex();

      if (optimized)
        impl_.scatter(nodeIndices(r), localMatrix);
      else
        impl_.scatter(nodeIndices(r), nodeIndices(c), localMatrix);
    }

    /// Number of nonzeros in the matrix
    std::size_t nnz() const
    {
      return impl_.nnz();
    }

  protected:
    /// The matrix backend
    Impl impl_;
  };

} // end namespace AMDiS
