#pragma once

#include <memory>
#include <string>

#include <dune/common/typeutilities.hh>

#include <amdis/BoundaryManager.hpp>
#include <amdis/LinearAlgebra.hpp>
#include <amdis/Output.hpp>
#include <amdis/gridfunctions/DiscreteFunction.hpp>
#include <amdis/io/BackupWriter.hpp>
#include <amdis/io/DuneVtkWriter.hpp>
#include <amdis/io/FileWriterBase.hpp>
#include <amdis/io/GmshWriter.hpp>
#include <amdis/io/VTKWriter.hpp>

namespace AMDiS
{
  /// \brief Creator class for filewriters depending on a given type name
  /**
   * \tparam SystemVector  Type of the DOFVector to write with the filewriter
   **/
  template <class SystemVector>
  class FileWriterCreator
  {
    using GlobalBasis  = typename SystemVector::GlobalBasis;
    using GridView     = typename GlobalBasis::GridView;
    using Grid         = typename GridView::Grid;

  public:
    /// Constructor. Stores the pointer to the systemVector and to the (optional) boundaryManager.
    FileWriterCreator(std::shared_ptr<SystemVector> systemVector,
                      std::shared_ptr<BoundaryManager<Grid>> boundaryManager = nullptr)
      : systemVector_(std::move(systemVector))
      , boundaryManager_(std::move(boundaryManager))
    {}

    /// Create a new FileWriter of type `type`
    /**
     * \param type      String representing the type of the writer. One of {vtk, dune-vtk, gmsh, backup}
     * \param prefix    The initfile prefix tp configure the filewriter
     * \param ii...     Indices of the treepath to the component of the systemVector to be handled by the fileWriter.
     **/
    template <class... Indices>
    std::unique_ptr<FileWriterInterface>
    create(std::string type, std::string prefix, Indices... ii) const
    {
      auto data = valueOf(*systemVector_, ii...);
      return create_impl(std::move(type), std::move(prefix), data, Dune::PriorityTag<42>{});
    }

  private:
    template <class Data,
      REQUIRES(not std::is_same_v<ValueCategory_t<typename Data::Range>,tag::unknown>),
      REQUIRES(std::is_arithmetic_v<typename Dune::FieldTraits<typename Data::Range>::field_type>)>
    std::unique_ptr<FileWriterInterface>
    create_impl(std::string type, std::string prefix, Data const& data, Dune::PriorityTag<2>) const
    {
      GridView const& gridView = systemVector_->basis().gridView();

      // ParaView VTK format, writer from dune-grid
      if (type == "vtk")
      {
        return std::make_unique<VTKWriter<GridView,Data>>(prefix, gridView, data);
      }
#if HAVE_DUNE_VTK
      // ParaView VTK format, writer from dune-vtk
      else if (type == "dune-vtk")
      {
        return std::make_unique<DuneVtkWriter<GridView,Data>>(prefix, gridView, data);
      }
#endif
      // GMsh file format, writing just the grid and optionally boundary ids
      else if (type == "gmsh")
      {
        if (!!boundaryManager_)
          return std::make_unique<GmshWriter<GridView>>(prefix, gridView,
            std::vector<int>{}, boundaryManager_->boundaryIds());
        else
          return std::make_unique<GmshWriter<GridView>>(prefix, gridView);
      }
      // Backup writer, writing the grid and the solution vector
      else if (type == "backup")
      {
        return std::make_unique<BackupWriter<SystemVector>>(prefix, systemVector_);
      }

      error_exit("Unknown filewriter type '{}' given for '{}'. Use one of "
                 "(vtk, gmsh, backup, [dune-vtk]), where the last one is only available "
                 "if the dune module dune-vtk is found.", type, prefix);
      return {};
    }


    // The value-category is unknown, like a composite/hierarchic vector or any unknown type.
    template <class Data,
      REQUIRES(std::is_same_v<ValueCategory_t<typename Data::Range>,tag::unknown>),
      REQUIRES(std::is_arithmetic_v<typename Dune::FieldTraits<typename Data::Range>::field_type>)>
    std::unique_ptr<FileWriterInterface>
    create_impl(std::string type, std::string prefix, Data const& /*data*/, Dune::PriorityTag<1>) const
    {
      // Backup writer, writing the grid and the solution vector
      if (type == "backup")
      {
        return std::make_unique<BackupWriter<SystemVector>>(prefix, systemVector_);
      }

      error_exit("Filewriter '{}' cannot be applied to this component in the tree. "
                 "Either use the writer 'backup' to write the whole tree, or add a treepath "
                 "to the output prefix '{}'.", type, prefix);
      return {};
    }

    // fallback implementation
    template <class Data>
    std::unique_ptr<FileWriterInterface>
    create_impl(std::string, std::string, Data const&, Dune::PriorityTag<0>) const
    {
      error_exit("Filewriter cannot be used with unsupported Range and field_type");
      return {};
    }

  private:
    std::shared_ptr<SystemVector> systemVector_;
    std::shared_ptr<BoundaryManager<Grid>> boundaryManager_ = nullptr;
  };

} // end namespace AMDiS
