#pragma once

#include <dune/common/typeutilities.hh>
#include <amdis/linearalgebra/Traits.hpp>

namespace AMDiS
{
  namespace Impl
  {
    template <class Basis>
    auto entitySetImpl(Basis const& basis, Dune::PriorityTag<2>)
      -> decltype(basis.entitySet())
    {
      return basis.entitySet();
    }

    template <class Basis>
    auto entitySetImpl(Basis const& basis, Dune::PriorityTag<1>)
    {
      return elements(basis.gridView(), BackendTraits::PartitionSet{});
    }

  } // end namespace Impl

  /// \brief Return the set of entities a basis can be bound to.
  /**
   * This function either return the entitySet defined by the basis as
   * `basis.entitySet()` or the entitySet `element(gridView)` restricted
   * to the `PartitionSet` given in the linear-algebra `BackendTraits`
   * for the basis.
   **/
  template <class Basis>
  auto entitySet(Basis const& basis)
  {
    return Impl::entitySetImpl(basis, Dune::PriorityTag<5>{});
  }

} // end namespace AMDiS
