#pragma once

#include <type_traits>

#include <dune/common/typeutilities.hh>

#include <amdis/common/Concepts.hpp>
#include <amdis/common/Logical.hpp>
#include <amdis/common/Order.hpp>
#include <amdis/gridfunctions/Derivative.hpp>

namespace AMDiS
{
  // The localFunction of a GridFunction
  template <class GridFunction>
  auto localFunction(GridFunction const& gf)
    -> decltype(gf.makeLocalFunction())
  {
    return gf.makeLocalFunction();
  }


  namespace Traits
  {
    template <class T>
    struct IsPreGridFunction
      : std::false_type {};

  } // end namespace Traits


  namespace Concepts
  {
    /** \addtogroup Concepts
     *  @{
     **/

    namespace Definition
    {
      struct HasLocalFunction
      {
        template <class F>
        auto require(F&& f) -> decltype(
          localFunction(f)
        );
      };

      struct HasGridFunctionTypes
      {
        template <class GF>
        auto require(GF const& /*gf*/) -> std::void_t<
          typename GF::Range,
          typename GF::Domain,
          typename GF::EntitySet
        >;
      };

    } // end namespace Definition


    /// \brief GridFunction GF has free function `localFunction(GF)`
    template <class GF>
    constexpr bool HasLocalFunction = models<Definition::HasLocalFunction(GF)>;

    template <class GF>
    using HasLocalFunction_t = models_t<Definition::HasLocalFunction(GF)>;


    /// \brief GridFunction GF is a Type that has LocalFunction and provides some
    /// typedefs for `Domain`, `Range`, and `EntitySet`.
    template <class GF>
    constexpr bool GridFunction =
      HasLocalFunction<GF> && models<Definition::HasGridFunctionTypes(GF)>;

    template <class GF>
    using GridFunction_t = bool_t<GridFunction<GF>>;


    /// \brief Concept is fulfilled, if at least one of the massed Expressions
    /// can be converted to a GridFunction, or is already a GridFunction.
    template <class... GFs>
    constexpr bool AnyGridFunction =
      (GridFunction<remove_cvref_t<GFs>> ||...) ||
      (Traits::IsPreGridFunction<remove_cvref_t<GFs>>::value ||...);

    template <class... GFs>
    using AnyGridFunction_t = bool_t<AnyGridFunction<GFs...>>;

    /** @} **/

  } // end namespace Concepts


#ifndef DOXYGEN
  template <class PreGridFct, class = void>
  struct GridFunctionCreator
      : PreGridFct::Creator {};

  // forward declaration
  template <class Function>
  struct AnalyticPreGridFunction;

  namespace Impl
  {
    // Specialization for type that is already a GridFunction
    template <class GridFct, class GridView>
    GridFct const& makeGridFunctionImpl(GridFct const& gridFct, GridView const& /*gridView*/, std::true_type, Dune::PriorityTag<2>)
    {
      return gridFct;
    }

    // If F is a callable with global coordinates, create an AnalyticGridFunction
    template <class F, class GridView,
      class Coordinate = typename GridView::template Codim<0>::Entity::Geometry::GlobalCoordinate,
      std::enable_if_t<std::is_invocable_v<F, Coordinate>, int> = 0>
    auto makeGridFunctionImpl(F const& f, GridView const& gridView, std::false_type, Dune::PriorityTag<1>)
    {
      AnalyticPreGridFunction<F> preGridFct{f};
      return AnalyticPreGridFunction<F>::Creator::create(preGridFct, gridView);
    }

    // Use the \ref GridFunctionCreator to create a gridFunction from a preGridFunction
    template <class PreGridFct, class GridView,
      class Creator = GridFunctionCreator<PreGridFct>>
    decltype(auto) makeGridFunctionImpl(PreGridFct const& preGridFct, GridView const& gridView, std::false_type, Dune::PriorityTag<0>)
    {
      return Creator::create(preGridFct, gridView);
    }
  }
#endif


  /// \brief Generator for Gridfunctions from Expressions (PreGridfunctions)
  /**
   * \ingroup GridFunctions
   * Create an evaluable GridFunction from an expression that itself can not be
   * evaluated. Therefore, it binds the GridFunction to a GridView.
   *
   * **Example:**
   * ```
   * ProblemStat<Traits> prob("name");
   * prob.initialize(INIT_ALL);
   *
   * auto gridFct = makeGridFunction(Expression, prob.leafGridView());
   *
   * // eval GridFunction at GlobalCoordinates
   * auto value = gridFct(Dune::FieldVector<double,2>{1.0, 2.0});
   *
   * auto localFct = localFunction(gridFct);
   * for (auto const& element : elements(prob.leafGridView())) {
   *   localFct.bind(element);
   *   // eval LocalFunction at local coordinates
   *   auto x = localFct(element.geometry().center());
   *   localFct.unbind();
   * }
   * ```
   *
   * In contrast to Expressions, GridFunctions can be evaluated and
   * - have the free-function \ref localFunction() to obtain a LocalFunction
   * - its LocalFunctions have the free-function \ref order() to obtain the
   *   polynomial order of the Expression (if available)
   * - its LocalFunctions have the free-function \ref derivativeOf() to
   *   differentiate the Expression with respect to global Coordinates.
   *   A derivative Expression can be created, using \ref gradientOf() that
   *   can be converted to a GridFunction afterwards.
   **/
  template <class PreGridFct, class GridView>
  decltype(auto) makeGridFunction(PreGridFct const& preGridFct, GridView const& gridView)
  {
    using isGridFct = Concepts::GridFunction_t<PreGridFct>;
    return Impl::makeGridFunctionImpl(preGridFct, gridView, isGridFct{}, Dune::PriorityTag<5>{});
  }

} // end namespace AMDiS
