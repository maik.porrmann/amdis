#pragma once

#include <tuple>
#include <type_traits>
#include <utility>

#include <amdis/common/Index.hpp>
#include <amdis/common/StaticSize.hpp>
#include <amdis/common/TypeTraits.hpp>

namespace AMDiS
{
  namespace Ranges
  {
    namespace Impl_
    {
      template <class Functor, class Tuple, std::size_t... I>
      constexpr decltype(auto) applyImpl(Functor&& f, Tuple&& t, std::index_sequence<I...>)
      {
        using std::get;
        return f(get<I>(FWD(t))...);
      }

      template <class Functor, std::size_t I0, std::size_t... I>
      constexpr decltype(auto) applyIndicesImpl(Functor&& f, index_t<I0>, std::index_sequence<I...>)
      {
        return f(index_t<I0+I>{}...);
      }
    }  // namespace Impl_

    template <class F, class Tuple>
    constexpr decltype(auto) apply(F&& f, Tuple&& t)
    {
      return Impl_::applyImpl(FWD(f), FWD(t),
          std::make_index_sequence<static_size_v<Tuple>>{});
    }

    template <class F, std::size_t... I>
    constexpr decltype(auto) apply(F&& f, std::index_sequence<I...> seq)
    {
      return Impl_::applyIndicesImpl(FWD(f), index_t<0>{}, seq);
    }

    template <class Functor, class... Args>
    constexpr decltype(auto) applyVariadic(Functor&& f, Args&&... args)
    {
      return Impl_::applyImpl(FWD(f), std::forward_as_tuple(args...),
          std::make_index_sequence<sizeof...(Args)>{});
    }

    template <std::size_t N, class Functor>
    constexpr decltype(auto) applyIndices(Functor&& f)
    {
      return Impl_::applyIndicesImpl(FWD(f), index_t<0>{},
          std::make_index_sequence<N>{});
    }

    template <std::size_t I0, std::size_t I1, class Functor>
    constexpr decltype(auto) applyIndices(Functor&& f)
    {
      return Impl_::applyIndicesImpl(FWD(f), index_t<I0>{},
          std::make_index_sequence<I1-I0>{});
    }

    template <class Functor, std::size_t N>
    constexpr decltype(auto) applyIndices(Functor&& f, index_t<N>)
    {
      return Impl_::applyIndicesImpl(FWD(f), index_t<0>{},
          std::make_index_sequence<N>{});
    }

    template <class Functor, std::size_t I0, std::size_t I1>
    constexpr decltype(auto) applyIndices(Functor&& f, index_t<I0>, index_t<I1>)
    {
      return Impl_::applyIndicesImpl(FWD(f), index_t<I0>{},
          std::make_index_sequence<I1-I0>{});
    }

  } // end namespace Ranges
} // end namespace AMDiS
