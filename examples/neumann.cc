#include <config.h>

#include <iostream>

#if HAVE_DUNE_SPGRID
#include <dune/grid/spgrid.hh>
#endif
#if HAVE_DUNE_ALUGRID
#include <dune/alugrid/grid.hh>
#endif
#include <dune/grid/uggrid.hh>

#include <amdis/AMDiS.hpp>
#include <amdis/Integrate.hpp>
#include <amdis/ProblemStat.hpp>
#include <amdis/LocalOperators.hpp>

#define GRID_ID 3

using namespace AMDiS;

template <class Grid>
void run(Grid& grid)
{
  grid.globalRefine(Grid::dimension == 2 ? 4 : 2);

  using Traits = LagrangeBasis<Grid, 2>;
  ProblemStat<Traits> prob("ellipt", grid);
  prob.initialize(INIT_ALL);
  prob.boundaryManager()->setBoxBoundary({1,2,2,1});

  auto opL = makeOperator(tag::gradtest_gradtrial{}, 1.0);
  prob.addMatrixOperator(opL);

  // set dirichlet boundary condition
  auto g = [](auto const& x) { return 0.0; };
  prob.addDirichletBC(BoundaryType{1}, g);

  // set neumann boundary condition
  auto opNeumann = makeOperator(tag::test{}, 1.0);
  prob.addVectorOperator(BoundaryType{2}, opNeumann);

  AdaptInfo adaptInfo("adapt");

  prob.assemble(adaptInfo);
  prob.solve(adaptInfo);
  prob.writeFiles(adaptInfo);
}


int main(int argc, char** argv)
{
  Environment env(argc, argv);

#if GRID_ID == 0
  // 2d grids
  Dune::YaspGrid<2> grid0({1.0,1.0},{2,2});
  run(grid0);
#endif

#if GRID_ID == 1 && HAVE_DUNE_SPGRID
  Dune::SPDomain<double,2> domain({0.0,0.0}, {1.0,1.0});
  Dune::SPGrid<double,2> grid1(domain, Dune::SPMultiIndex<2>({2,2}));
  run(grid1);
#endif

#if GRID_ID == 2 && HAVE_DUNE_ALUGRID
  using Grid2 = Dune::ALUGrid<2,2,Dune::simplex,Dune::conforming>;
  using Factory2 = Dune::StructuredGridFactory<Grid2>;
  auto grid2 = Factory2::createSimplexGrid({0.0,0.0}, {1.0,1.0},
                                            std::array<unsigned int,2>{2u,2u});
  run(*grid2);
#endif

#if GRID_ID == 3 && HAVE_DUNE_UGGRID
  using Grid3 = Dune::UGGrid<2>;
  using Factory3 = Dune::StructuredGridFactory<Grid3>;
  auto grid3 = Factory3::createSimplexGrid({0.0,0.0}, {1.0,1.0},
                                            std::array<unsigned int,2>{2u,2u});
  run(*grid3);
#endif

#if GRID_ID == 4
  // 3d grids
  Dune::YaspGrid<3> grid4({1.0,1.0,1.0},{2,2,2});
  run(grid4);
#endif

  return 0;
}
