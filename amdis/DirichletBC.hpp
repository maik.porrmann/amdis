#pragma once

#include <functional>
#include <utility>
#include <vector>

#include <amdis/Boundary.hpp>
#include <amdis/BoundaryCondition.hpp>
#include <amdis/BoundarySubset.hpp>
#include <amdis/common/Concepts.hpp>
#include <amdis/common/TypeTraits.hpp>
#include <amdis/typetree/RangeType.hpp>
#include <amdis/typetree/TreePath.hpp>

namespace AMDiS
{
  /// Implements a boundary condition of Dirichlet-type.
  /**
   * By calling the methods \ref init and \ref finish before and after
   * assembling the system-matrix, respectively, dirichlet boundary conditions
   * can be applied to the matrix and system vector. Therefore, a predicate
   * functions indicates the DOFs where values should be enforced and a second
   * functor provided in the constructor is responsible for determining the
   * values to be set at the DOFs.
   *
   * In the \ref finish method the matrix is called with \ref apply
   * to erase the corresponding rows and columns for the DOF indices. This
   * application of boundary conditions can be symmetric if the matrix does
   * support this symmetric modification.
   *
   * \tparam Basis    GlobalBasis of the solution FE space
   * \tparam RowPath  Path to the row-node where the boundary condition should be applied to.
   * \tparam ColPath  Path to the col-node where the boundary condition should be applied to.
   * \tparam ValueGridFct  Type of the GridFunction representing the Dirichlet values.
   **/
  template <class Basis, class RowPath, class ColPath, class ValueGridFct>
  class DirichletBC
  {
    using GridView = typename Basis::GridView;
    using Intersection = typename GridView::Intersection;

    using Domain = typename GridView::template Codim<0>::Geometry::GlobalCoordinate;
    using Range = TYPEOF(std::declval<ValueGridFct>()(std::declval<Domain>()));

  public:
    /// Make a DirichletBC from a basis with treepath arguments
    template <class B>
    DirichletBC(B&& basis, RowPath const& row, ColPath const& col,
                BoundarySubset<Intersection> boundarySubset, ValueGridFct values)
      : basis_{wrap_or_share(FWD(basis))}
      , row_{row}
      , col_{col}
      , boundarySubset_(std::move(boundarySubset))
      , valueGridFct_(std::move(values))
    {}

    /// Make a DirichletBC from a global basis
    template <class B>
    DirichletBC(B&& basis, BoundarySubset<Intersection> boundarySubset, ValueGridFct values)
      : DirichletBC(FWD(basis), makeTreePath(), makeTreePath(),
                    std::move(boundarySubset), std::move(values))
    {}


    /// Fill \ref dirichletNodes_ with 1 or 0 whether DOF corresponds to the
    /// boundary or not.
    /**
     * \see BoundaryCondition::init
     **/
    void init();

    /// \brief Apply dirichlet BC to matrix and vector
    /**
     * Add a unit-row to the matrix and optionally delete the corresponding matrix-column.
     * Uses a backend-specific implementation.
     **/
    template <class Mat, class Sol, class Rhs>
    void apply(Mat& matrix, Sol& solution, Rhs& rhs, bool setDiagonal = true);

  private:
    std::shared_ptr<Basis const> basis_;
    RowPath row_;
    ColPath col_;
    BoundarySubset<Intersection> boundarySubset_;
    ValueGridFct valueGridFct_;

    std::vector<bool> dirichletNodes_;
  };

  // deduction guides

  // Make a DirichletBC from a basis with treepath arguments
  template <class B, class Row, class Col, class Values, class Basis = Underlying_t<B>,
    REQUIRES(Concepts::GlobalBasis<Basis>)>
  DirichletBC(B const&, Row const&, Col const&, BoundarySubset<typename Basis::GridView::Intersection>, Values const&)
    -> DirichletBC<Basis, Row, Col, Underlying_t<Values>>;

  // Make a DirichletBC from a global basis
  template <class B, class Values, class Basis = Underlying_t<B>,
    REQUIRES(Concepts::GlobalBasis<Basis>)>
  DirichletBC(B const&, BoundarySubset<typename Basis::GridView::Intersection>, Values const&)
    -> DirichletBC<Basis,TYPEOF(makeTreePath()),TYPEOF(makeTreePath()), Underlying_t<Values>>;

} // end namespace AMDiS

#include "DirichletBC.inc.hpp"
