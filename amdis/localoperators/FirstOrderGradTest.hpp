#pragma once

#include <type_traits>

#include <amdis/GridFunctionOperator.hpp>
#include <amdis/common/StaticSize.hpp>

namespace AMDiS
{
  /**
   * \addtogroup operators
   * @{
   **/

  namespace tag
  {
    struct gradtest {};
  }


  /// first-order operator \f$ \langle\nabla\psi, b\rangle \f$
  class FirstOrderGradTest
  {
  public:
    FirstOrderGradTest(tag::gradtest) {}

    template <class CG, class Node, class Quad, class LocalFct, class Vec>
    void assemble(CG const& contextGeo, Node const& node, Quad const& quad,
                  LocalFct const& localFct, Vec& elementVector) const
    {
      static_assert(static_size_v<typename LocalFct::Range> == CG::dow, "Expression must be of vector type.");
      static_assert(Node::isLeaf, "Node must be Leaf-Node.");

      std::size_t feSize = node.size();

      using RangeFieldType = typename Node::LocalBasis::Traits::RangeFieldType;
      using WorldVector = FieldVector<RangeFieldType,CG::dow>;
      std::vector<WorldVector> gradients;

      for (auto const& qp : quad) {
        // Position of the current quadrature point in the reference element
        auto&& local = contextGeo.local(qp.position());

        // The transposed inverse Jacobian of the map from the reference element to the element
        const auto jacobian = contextGeo.geometry().jacobianInverseTransposed(local);

        // The multiplicative factor in the integral transformation formula
        const auto factor = localFct(local);
        const auto dx = contextGeo.integrationElement(qp.position()) * qp.weight();

        // The gradients of the shape functions on the reference element
        auto const& shapeGradients = node.localBasisJacobiansAt(local);

        // Compute the shape function gradients on the real element
        gradients.resize(shapeGradients.size());

        for (std::size_t i = 0; i < gradients.size(); ++i)
          jacobian.mv(shapeGradients[i][0], gradients[i]);

        for (std::size_t i = 0; i < feSize; ++i) {
          const auto local_i = node.localIndex(i);
          elementVector[local_i] += dx * (factor * gradients[i]);
        }
      }
    }
  };


  template <class LC>
  struct GridFunctionOperatorRegistry<tag::gradtest, LC>
  {
    static constexpr int degree = 1;
    using type = FirstOrderGradTest;
  };

  /** @} **/

} // end namespace AMDiS
