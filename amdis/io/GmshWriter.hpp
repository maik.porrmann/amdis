#pragma once

#include <string>
#include <limits>
#include <memory>
#include <utility>

#include <dune/grid/io/file/gmshwriter.hh>

#include <amdis/Initfile.hpp>
#include <amdis/Output.hpp>
#include <amdis/common/Filesystem.hpp>
#include <amdis/io/FileWriterBase.hpp>

namespace AMDiS
{
  /// The GmshWriter just writes the grid of a given gridView to a gmsh compatible .msh file
  /**
   * \tparam GV  GridView describing the grid to write
   **/
  template <class GV>
  class GmshWriter
      : public FileWriterBase
  {
    using GridView = GV;
    using Writer = Dune::GmshWriter<GridView>;

  public:
    /// Constructor.
    GmshWriter(std::string const& name, GridView const& gridView,
               std::vector<int> const& physicalEntities = std::vector<int>(),
               std::vector<int> const& physicalBoundaries = std::vector<int>())
      : FileWriterBase(name)
      , physicalEntities_(physicalEntities)
      , physicalBoundaries_(physicalBoundaries)
    {
      int p = 0;
      Parameters::get(name + "->animation", animation_);
      Parameters::get(name + "->precision", p);

      int precision = (p == 0)
        ? std::numeric_limits<float>::max_digits10
        : std::numeric_limits<double>::max_digits10;

      writer_ = std::make_shared<Writer>(gridView, precision);
    }

    /// Implements \ref FileWriterBase::write
    void write(AdaptInfo& adaptInfo, bool force) override
    {
      std::string filename = this->filename();
      if (animation_)
        filename += "_" + std::to_string(adaptInfo.time());

      if (this->doWrite(adaptInfo) || force) {
        writer_->write(filesystem::path({this->dir(), filename + ".msh"}).string(),
          physicalEntities_, physicalBoundaries_);

        msg("Grid written to file '{}'.", filesystem::path({this->dir(), filename + ".msh"}).string());
      }
    }

  private:
    std::vector<int> const& physicalEntities_;
    std::vector<int> const& physicalBoundaries_;

    std::shared_ptr<Writer> writer_;

    /// write .pvd if animation=true, otherwise write only .vtu
    bool animation_ = false;
  };

} // end namespace AMDiS
