#pragma once

#include <type_traits>

#include <amdis/localoperators/FirstOrderTestGradTrial.hpp>

namespace AMDiS
{
  /**
   * \addtogroup operators
   * @{
   **/

  namespace tag
  {
    struct gradtest_trial {};
    struct grad_test {};
  }


  /// first-order operator \f$ \langle\nabla\cdot\Psi, c\,\phi\rangle \f$
  template <class LC>
  struct GridFunctionOperatorRegistry<tag::gradtest_trial, LC>
  {
    static constexpr int degree = 1;
    static tag::test_gradtrial transposedTag(tag::gradtest_trial) { return {}; }
    using type = FirstOrderTestGradTrial;
  };


  /// Create a first-order term with derivative on trial-function
  template <class Expr>
  auto fot(Expr&& expr, tag::grad_test, int quadOrder = -1)
  {
    return makeOperator(tag::gradtest_trial{}, FWD(expr), quadOrder);
  }

  /** @} **/

} // end namespace AMDiS
