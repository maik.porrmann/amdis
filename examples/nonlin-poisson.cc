#include <amdis/AMDiS.hpp>
#include <amdis/AdaptStationary.hpp>
#include <amdis/LocalOperators.hpp>
#include <amdis/ProblemStat.hpp>
#include <amdis/common/Literals.hpp>
#include <amdis/nonlin/NewtonIteration.hpp>

#include <dune/grid/yaspgrid.hh>

using namespace AMDiS;
using namespace Dune::Functions::BasisFactory;

int main(int argc, char** argv)
{
  Environment env(argc, argv);

  auto f = [](auto const& x) {
    double r2 = dot(x,x);
    double ux = std::exp(-10.0 * r2);
    return -(400.0 * r2 - 20.0 * x.size()) * ux;
  };

  using Grid = Dune::YaspGrid<GRIDDIM>;
  auto gridPtr = MeshCreator<Grid>{"mesh"}.create();

  ProblemStat prob("poisson", *gridPtr, lagrange<2>());
  prob.initialize(INIT_ALL);

  NewtonIteration newton{"newton", prob};

  // <grad(du),grad(theta)> + <4*u_k^3 du,theta> = -<grad(u_k),grad(theta)> + <-u_k^4 + f(x),theta>
  // du = -u_k on \partial\Omega

  // define the Jacobian
  prob.addMatrixOperator(sot(1.0));
  prob.addMatrixOperator(zot(4*pow<3>(prob.solution())));

  // define objective function
  prob.addVectorOperator(makeOperator(tag::gradtest{}, -gradientOf(prob.solution())));
  prob.addVectorOperator(zot(-pow<4>(prob.solution())));
  prob.addVectorOperator(zot(f, 6));

  // set boundary condition
  prob.addDirichletBC([](auto){return true;}, -prob.solution());

  AdaptInfo adaptInfo{"adapt"};
  AdaptStationary adaptStat{"adapt", newton, adaptInfo};
  adaptStat.adapt();

  return 0;
}
