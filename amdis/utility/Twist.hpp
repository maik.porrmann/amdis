#pragma once

#include <vector>

#include <dune/geometry/referenceelements.hh>
#include <dune/localfunctions/common/localkey.hh>

#include <amdis/Output.hpp>

namespace AMDiS
{
  /// \brief Permutate the dof indices w.r.t. a global entity orientation
  template <class IdSet, int dim>
  class Twist
  {
    using IdType = typename IdSet::IdType;

    using RefElements = typename Dune::ReferenceElements<double,dim>;
    using RefElement = typename RefElements::ReferenceElement;

  public:
    /// Constructor. Stores a reference to the passed idSet
    Twist(IdSet const& idSet)
      : idSet_(idSet)
    {}

    /// Bind the twist to a codim-0 element. Calculates the global ids of all the element vertices.
    template <class Element>
    void bind(Element const& element)
    {
      static_assert(dim == Element::mydimension, "");
      refElem_ = &RefElements::general(element.type());

      ids_.resize(refElem_->size(dim));
      for (int i = 0; i < refElem_->size(dim); ++i)
        ids_[i] = idSet_.subId(element, i, dim);
    }

    /// Get the permutated local dof index, living on a subEntity of the bound element.
    /**
     * If the global entity orientation differs from the local orientation, perform a
     * permutation of the local dof indices on that entity. This is currently implemented
     * for edge entities only.
     *
     * \param localKey The Dune::LocalKey of the local dof, identifying the entity and local index
     * \param size     The number of local dofs on that entity.
     */
    unsigned int get(Dune::LocalKey const& localKey, unsigned int size) const
    {
      int subDim = dim - localKey.codim();
      if (subDim == 1 && 1 < dim) // facet == edge
        return id(localKey,0) < id(localKey,1) ? localKey.index() : size - localKey.index() - 1;
      else if (subDim == 2 && 2 < dim) { // facet == face
        test_exit(size == 1, "Bases with more then one DoF per subentity are not yet supported.");
        return localKey.index();
      }

      return localKey.index();
    }

  private:
    IdType const& id(Dune::LocalKey const& localKey, int ii) const
    {
      return ids_[refElem_->subEntity(localKey.subEntity(), localKey.codim(), ii, dim)];
    }

  private:
    IdSet const& idSet_;
    RefElement const* refElem_ = nullptr;
    std::vector<IdType> ids_;
  };

} // end namespace AMDiS
