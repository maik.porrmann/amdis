#include <amdis/AMDiS.hpp>
#include <amdis/typetree/Traversal.hpp>

#include <dune/grid/yaspgrid.hh>
#include <dune/functions/functionspacebases/compositebasis.hh>
#include <dune/functions/functionspacebases/powerbasis.hh>
#include <dune/functions/functionspacebases/lagrangebasis.hh>

using namespace AMDiS;

int main(int argc, char** argv)
{
  Environment env(argc, argv);

  // create grid
  Dune::YaspGrid<2> grid({1.0,1.0}, {1,1});
  auto gridView = grid.leafGridView();

  // create basis
  using namespace Dune::Functions::BasisFactory;
  auto basis1 = makeBasis(gridView,
    power<10>(composite(power<10>(lagrange<2>()), power<10>(lagrange<1>()))) );

  auto basis2 = makeBasis(gridView,
    power<2>(power<2>(power<2>(power<2>(power<2>(power<2>(power<2>(lagrange<1>()))))))) );

  auto basis3 = makeBasis(gridView,
    composite(power<2>(lagrange<2>(), flatInterleaved()), lagrange<1>(), flatLexicographic()));

  auto basis4 = makeBasis(gridView,
    power<2>(power<2>(lagrange<2>(), flatInterleaved()), flatLexicographic()));

  auto localView = basis1.localView();

  Traversal::forEachLeafNode(localView.tree(), [&](auto const& rowNode, auto const& r)
  {
    Traversal::forEachLeafNode(localView.tree(), [&](auto const& colNode, auto const& c)
    {
      std::cout << r << " , " << c << std::endl;
      // std::cout << colNode.tp() << std::endl;
    });
  });
}
