#include <dune/grid/yaspgrid.hh>
#include <dune/functions/functionspacebases/compositebasis.hh>
#include <dune/functions/functionspacebases/powerbasis.hh>
#include <dune/functions/functionspacebases/lagrangebasis.hh>

#include <amdis/functions/NodeIndices.hpp>
#include "Tests.hpp"

int main()
{
  using namespace AMDiS;
  using namespace Dune::Functions::BasisFactory;

  // create grid
  Dune::YaspGrid<2> grid({1.0, 1.0}, {1, 1});
  auto gridView = grid.leafGridView();

  // create basis
  auto taylorHoodBasis = makeBasis(
    gridView,
    composite(
      power<2>(lagrange<2>(), flatInterleaved()),
      lagrange<1>(),
      flatLexicographic()
    ));

  auto localView = taylorHoodBasis.localView();

  for (auto const& e : elements(gridView)) {
    localView.bind(e);

    std::size_t numDofs = 2*(3*3) + (2*2);
    std::size_t numNodeIndices = nodeIndexCount(localView);
    AMDIS_TEST_EQ(numNodeIndices, numDofs);

    std::size_t num = 0;
    for ([[maybe_unused]] std::size_t dof : nodeIndices(localView)) {
      num++;
    }
    AMDIS_TEST_EQ(num, numDofs);

    auto node = localView.tree();
    numNodeIndices = nodeIndexCount(localView, node);
    AMDIS_TEST_EQ(numNodeIndices, numDofs);

    // count the number of velocity dofs
    std::size_t numVelDofs = 2*(3*3);
    auto v_node = Dune::TypeTree::child(node, Dune::Indices::_0);
    std::size_t numVelNodeIndices = nodeIndexCount(localView, v_node);
    AMDIS_TEST_EQ(numVelNodeIndices, numVelDofs);
    num = 0;
    for ([[maybe_unused]] std::size_t dof : nodeIndices(localView, v_node)) {
      num++;
    }
    AMDIS_TEST_EQ(num, numVelDofs);

    // count the number of pressure dofs
    std::size_t numPDofs = 2*2;
    auto p_node = Dune::TypeTree::child(node, Dune::Indices::_1);
    std::size_t numPNodeIndices = nodeIndexCount(localView, p_node);
    AMDIS_TEST_EQ(numPNodeIndices, numPDofs);
    num = 0;
    for ([[maybe_unused]] std::size_t dof : nodeIndices(localView, p_node)) {
      num++;
    }
    AMDIS_TEST_EQ(num, numPDofs);
  }

  return report_errors();
}
